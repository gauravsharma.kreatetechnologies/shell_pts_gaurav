//
//  StationwiseURSVC.swift
//  PCS
//
//  Created by lokesh chand on 26/09/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StationwiseURSVC: UIViewController {

    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var URSButton: UIButton!
    @IBOutlet weak var NTPCButton: UIButton!
    @IBOutlet weak var myPickerView: UIPickerView!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet weak var labelmarquee: MarqueeLabel!
    
    var Lognotification = ""
    var ntpcArray = ["COAL","GAS","OTHER"]
    var ntpcArraySelect = ""
    var buttonType = "UrsButton"
    var responseTable = [JSON]()
    var mainResponc:JSON?
    var regiontype = [String:Any]()
    var marqregiontype = [String:Any]()
    var keyArray = [String]()
    var marqkeyArray = [String]()
    var regionSelect = "NRLDC"
    var getpickerRevision = ""
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        labelmarquee.type = .continuous
        labelmarquee.animationCurve = .easeInOut
        ntpcArraySelect = "NTPCCOAL"
        myPickerView.isHidden = true
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        Tablelist_api()
        self.NTPCButton .setTitle(ntpcArray[0], for: UIControlState .normal)

        
        
        
    }
    
    //pragma mark - SlideNavigationController Methods -
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        
    }
    //MARK:- Click Any where on view Hide the UIElemnts.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        myPickerView.isHidden = true
    }
    
    
    
    func Tablelist_api()
    {
        // SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        let someDict =  ["owner" :ntpcArraySelect,"region":regionSelect,"type":"REGION","device_id":AppHelper.userDefaults(forKey: "deviceId")] as [String : Any]
        
     //   print(someDict)

        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            let parameters : Parameters = ["data":theJSONText!.description]
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: station_Table, parameters:parameters)
            {
                (APIData) -> Void in
                
               // print(APIData)
                self.mainResponc = APIData
                
                /// For Marquee
                let combination = NSMutableAttributedString()
                let lineArr = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 25.0)!]
                
                let line = NSMutableAttributedString(string: "|", attributes: lineArr)
                
                let getName = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 16.0)!]
                
                let getAvail = [NSAttributedStringKey.foregroundColor: UIColor.init(red: 58.0/255.0, green: 255.0/255.0, blue: 73.0/255.0, alpha: 1.0),NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 16.0)!] //as [String : Any]
                
                let getRevision = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 16.0)!]
                
                
                
                if let marqdict = APIData["region"].dictionaryObject {
                    self.marqregiontype = marqdict
                }
                let marqkeyCollection = self.marqregiontype.keys //
                self.marqkeyArray = Array(marqkeyCollection)
                
                for i in 0 ..< self.marqkeyArray.count
                {
                let marqcurrentKey = self.marqkeyArray[i]
                combination.append(NSAttributedString(string:marqcurrentKey, attributes: getName))
                combination.append(NSAttributedString(string:"   "))
                    
                    
                    
                    combination.append(NSAttributedString(string:APIData["region"][marqcurrentKey]["available"].stringValue, attributes: getAvail))
                    combination.append(NSAttributedString(string:"   "))
                    
                    
                    combination.append(NSAttributedString(string:"R " +  APIData["region"][marqcurrentKey]["revision"].stringValue, attributes: getRevision ))
                    combination.append(NSAttributedString(string:"   "))
                    
                    
                    combination.append(line)
                    combination.append(NSAttributedString(string:"  "))
                    
  
                }
                
                //SVProgressHUD.dismiss()
                CustomHUD.shredObject.removeXib(view: self.view)
                self.responseTable = APIData["data"].arrayValue
                if self.responseTable.count > 0
                {
                    self.noDataLbl.isHidden = true
                    self.noDataLbl.superview?.isHidden = true
                    self.myTableView.isHidden = false
                }else
                {
                   self.noDataLbl.isHidden = false
                     self.noDataLbl.superview?.isHidden = false
                   self.myTableView.isHidden = true
                }
                
                
                if let dict = APIData["region"].dictionaryObject {
                     self.regiontype = dict
                }
                //print(self.regiontype)
                
                let keyCollection = self.regiontype.keys //
                self.keyArray = Array(keyCollection)
                
                    for i in 0 ..< self.keyArray.count{
                        let currentKey = self.keyArray[i]
                        let selectedValue = APIData["region"][currentKey]["select"].stringValue
                        if selectedValue == "YES"{
                            self.URSButton .setTitle(currentKey, for: UIControlState .normal)
                            
                            self.getpickerRevision = APIData["region"][currentKey]["revision"].stringValue
                            
                            //API CALL --
                            break
                        }
                     }
            
                
                self.labelmarquee.attributedText = combination
                self.labelmarquee.layer.speed = 3.0
               self.dateLbl.text = APIData["date"].stringValue
               self.myTableView.reloadData()
            }
        }
        catch
        {
            
        }
    }

}

extension StationwiseURSVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseTable.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 39
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StationTableCell", for: indexPath) as! StationTableCell
    cell.genLbl.text = responseTable[indexPath.row]["station_name"].stringValue
    cell.resrchLbl.text = responseTable[indexPath.row]["reschedule_to_beneficiary"].stringValue
    cell.ursLbl.text = responseTable[indexPath.row]["urs_remaining"].stringValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let second = storyboard?.instantiateViewController(withIdentifier: "StationwiseDetailVCID") as! StationwiseDetailVC
        
        second.getStation_name = responseTable[indexPath.row]["station_name"].stringValue
        second.getregion_name = regionSelect
        second.getpickerrevision = getpickerRevision
        self.navigationController?.pushViewController(second, animated: true)
        

    }
    
    
}

extension StationwiseURSVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    @IBAction func pickerViewClick(_ sender: UIButton)
    {
//        myPickerView.isHidden = false
        if sender.tag == 1 && keyArray.count>0
        {
            myPickerView.isHidden = false
            buttonType = "UrsButton"
        }
        else if sender.tag == 2
        {
            myPickerView.isHidden = false
            buttonType = "NtpcButton"
        }
        self.myPickerView.reloadAllComponents()
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if  buttonType == "UrsButton"
        {
            return keyArray.count
        }
        else if buttonType == "NtpcButton"
        {
            return ntpcArray.count
        }
        else
        {
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if buttonType == "UrsButton"
        {
            return keyArray[row]
        }
        else if buttonType == "NtpcButton"
        {
            return ntpcArray[row]
        }
        else
        {
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if buttonType == "UrsButton"{
            URSButton.setTitle(keyArray[row], for: UIControlState.normal)
            regionSelect = keyArray[row]
            getpickerRevision = self.mainResponc!["region"][regionSelect]["revision"].stringValue
            
            
//            print(getpickerRevision)
        }
        else if buttonType == "NtpcButton"
        {
            ntpcArraySelect = ""
            NTPCButton.setTitle(ntpcArray[row], for: UIControlState.normal)
            ntpcArraySelect = ntpcArray[row]
            if ntpcArray[row] == "OTHER"{
                ntpcArraySelect = "OTHER"
            }else{
                ntpcArraySelect = "NTPC" + ntpcArraySelect
                
            }
        }
        
        Tablelist_api()
        myPickerView.isHidden = true
        
    }
    
    
}


