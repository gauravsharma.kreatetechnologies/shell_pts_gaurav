//
//  StationwiseAvlTbleCell.swift
//  PCS
//
//  Created by lokesh chand on 27/09/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit

class StationwiseAvlTbleCell: UITableViewCell {
    @IBOutlet weak var srNoLbl: UILabel!
    @IBOutlet weak var blockLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
