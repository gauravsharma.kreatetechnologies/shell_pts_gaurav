//
//  StationTableCell.swift
//  PCS
//
//  Created by lokesh chand on 26/09/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit

class StationTableCell: UITableViewCell {

    @IBOutlet weak var genLbl: UILabel!
    @IBOutlet weak var resrchLbl: UILabel!
    @IBOutlet weak var ursLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
