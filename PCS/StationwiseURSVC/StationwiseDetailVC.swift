//
//  StationwiseDetailVC.swift
//  PCS
//
//  Created by lokesh chand on 27/09/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StationwiseDetailVC: UIViewController {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var dataDescriptionLbl: UILabel!
    var getStation_name = ""
    var getregion_name = ""
    var getbeneficiary = ""
    var responseTable = [JSON]()
    var getpickerrevision = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        dataDescriptionLbl.text = getStation_name
        tableDetail_api()
//        print(self.getregion_name)
//        print(self.getStation_name)
        
       // print(self.getbeneficiary)
        
        
    }
    @IBAction func btnBack_Click(_ sender: UIButton)
    {
        
        navigationController?.popViewController(animated: true)
    }

    func tableDetail_api()
    {
        CustomHUD.shredObject.loadXib(view: self.view)
        let someDict =  ["station":getStation_name,"region":getregion_name,"device_id":AppHelper.userDefaults(forKey: "deviceId")] as [String : Any]
     //   print(someDict)
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            let parameters : Parameters = ["data":theJSONText!.description]
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: stationTable_detail, parameters:parameters)
            {
                (APIData) -> Void in
                
                //print(APIData)
                if APIData["status"].stringValue == "SUCCESS"
                {
                    self.responseTable = APIData["value"]["states"].arrayValue
                    if self.responseTable.count > 0
                    {
                        
                    }else
                    {
                        self.noDataLbl.isHidden = false
                        self.myTableView.isHidden = true
                    }
                }
                else
                {
                    self.noDataLbl.isHidden = false
                    self.myTableView.isHidden = true
                }
                self.myTableView.reloadData()
                CustomHUD.shredObject.removeXib(view: self.view)
            }
           
            
        }
        catch
        {
            
        }
        
    }

}



extension StationwiseDetailVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseTable.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StationwiseDetailTableCell", for: indexPath) as! StationwiseDetailTableCell
        cell.stateLbl.text = responseTable[indexPath.row]["name"].stringValue
        cell.URSLbl.text = responseTable[indexPath.row]["qtmvalue"].stringValue
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let second = storyboard?.instantiateViewController(withIdentifier: "StationwiseAvlDetailVCID") as! StationwiseAvlDetailVC
        
        second.getStn_name = getStation_name
        second.getregion_namee = getregion_name
        second.getpickerrevision = getpickerrevision
        second.setTitleLbl = responseTable[indexPath.row]["name"].stringValue
        second.setType = "STATIONWISE"
        
        
        
        
        
        second.getbeneficiary_name = responseTable[indexPath.row]["name"].stringValue
        self.navigationController?.pushViewController(second, animated: true)
    }
    
    
    
}
