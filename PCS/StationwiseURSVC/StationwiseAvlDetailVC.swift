//
//  StationwiseAvlDetailVC.swift
//  PCS
//
//  Created by lokesh chand on 27/09/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StationwiseAvlDetailVC: UIViewController {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var dataDescriptionLbl: UILabel!
   
    var responseTable = [JSON]()
    var getStation_name = ""
    var getregion_namee = ""
    var getStn_name = ""
    var getbeneficiary_name = ""
    var getpickerrevision = ""
    var setTitleLbl = ""
    var setType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
       dataDescriptionLbl.text = setTitleLbl + " - " + getStn_name
        
        tableDetail_api()
        
        
//      
//             print(self.getregion_namee)
//             print(self.getStn_name)
//             print(self.getbeneficiary_name)
    }
    @IBAction func btnBack_Click(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    
    func tableDetail_api()
    {
        CustomHUD.shredObject.loadXib(view: self.view)
        let someDict =  ["station":getStn_name,"revision":getpickerrevision,"type":setType, "beneficiary":getbeneficiary_name,"region":getregion_namee,"device_id":AppHelper.userDefaults(forKey: "deviceId")] as [String : Any]
        
     //  print(someDict)
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            let parameters : Parameters = ["data":theJSONText!.description]
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: stationblockwisedata_detail, parameters:parameters)
            {
                (APIData) -> Void in
                
                
              //  print(APIData)
                if APIData["status"].stringValue == "SUCCESS"
                {
                    self.responseTable = APIData["value"]["block_data"].arrayValue
                    if self.responseTable.count > 0
                    {
                        
                    }else
                    {
                        self.noDataLbl.isHidden = false
                        self.myTableView.isHidden = true
                    }
                }
                else
                {
                    self.noDataLbl.isHidden = false
                    self.myTableView.isHidden = true
                }
                self.myTableView.reloadData()
                CustomHUD.shredObject.removeXib(view: self.view)
            }
        }
        catch
        {
            
        }
        
    }


}
extension StationwiseAvlDetailVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseTable.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StationwiseAvlTbleCell", for: indexPath) as! StationwiseAvlTbleCell
        cell.srNoLbl.text = "\(indexPath.row + 1)"
       // cell.amountLbl.text = responseTable[indexPath.row]["available"].stringValue
        
        //MARK:- Convert string to Int
        cell.amountLbl.text = String(responseTable[indexPath.row]["available"].intValue)
        
        if self.responseTable[indexPath.row]["fromtime"].stringValue == ""
        {
            cell.blockLbl.text =  "null" + self.responseTable[indexPath.row]["fromtime"].stringValue + " - " + self.responseTable[indexPath.row]["totime"].stringValue
        }
        else
        {
            cell.blockLbl.text =  self.responseTable[indexPath.row]["fromtime"].stringValue + " - " + self.responseTable[indexPath.row]["totime"].stringValue
        }
        
        if self.responseTable[indexPath.row]["disable"].stringValue == "YES"
        {
           
            cell.amountLbl.textColor =  UIColor.red
            cell.srNoLbl.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            cell.amountLbl.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            cell.blockLbl.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            
        }
        else
        {
        
            cell.amountLbl.textColor =  UIColor.green
            cell.srNoLbl.backgroundColor = UIColor.white
            cell.amountLbl.backgroundColor = UIColor.white
            cell.blockLbl.backgroundColor = UIColor.white
        }
        
        
        
        
       
        return cell
    }
}
