//
//  PDFVC.swift
//  PCS
//
//  Created by Gaurav Sharma on 9/1/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import WebKit

class PDFVC: UIViewController, WKNavigationDelegate {
    
    //MARK: - IBOUTLETS
    let imageLogo = UIImage(named: "PTS_LOGO")
    
    @IBOutlet weak var webView: WKWebView!
    //MARK: - Var& Objects
    
    @objc var dateArray = [""]
    @objc var descrArray = [""]
    @objc var balanceArray = [""]
    @objc var amountRecievedArray = [""]
    @objc var amountRecievableArray = [""]


    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        createPDF()
        
    }
    
    @IBAction func btnDissmiss(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func btnCross(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }    }
    
    // Create and Save Pdf
    func createPDF() {
        
        let text = getDetails()
        
        let html = "\(text)"
        let fmt = UIMarkupTextPrintFormatter(markupText: html)
        
        // 2. Assign print formatter to UIPrintPageRenderer
        
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(fmt, startingAtPageAt: 0)
        
        // 3. Assign paperRect and printableRect
        
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        let printable = page.insetBy(dx: 20, dy: 80)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, .zero, nil)
        
        for i in 1...render.numberOfPages {
            UIGraphicsBeginPDFPage();
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i - 1, in: bounds)
        }
        
        UIGraphicsEndPDFContext();
        
        // 5. Save PDF file
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        guard let outputURL = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("PDFReport").appendingPathExtension("pdf")
        
        else { fatalError("Destination URL not created") }
        
        pdfData.write(toFile: "\(documentsPath)/PDFReport.pdf", atomically: true)
        drawImageOnPDF(path: "\(documentsPath)/PDFReport.pdf")
        print("file saved [\(documentsPath)]")

        loadPDF(filename: "PDFReport.pdf")
        
        
        
    }
    //Open Pdf on web view
    func loadPDF(filename: String) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        var url = URL(fileURLWithPath: documentsPath, isDirectory: true).appendingPathComponent(filename)
//        let urlRequest = URLRequest(url: url)
        print(url)

            if #available(iOS 9.0, *) {
                // iOS9 and above. One year later things are OK.
                webView.loadFileURL(url, allowingReadAccessTo: url)
            } else {
            
                do {
                    url = try fileURLForBuggyWKWebView8(fileURL: url)
                    webView.load(URLRequest(url: url))
                } catch let error as NSError {
                    print("Error: " + error.debugDescription)
                }
            }
        
    }
    //get recorded deails in html string
    func getDetails() -> String {
        
        var text = """
<h1>Account Summary</h1>
   
<br>

<table style="width:  100%; border-collapse: collapse;">
    <thead style="border-top: 1px solid black; border-bottom: 1px solid black;">
        <tr>
            <th style="text-align: left; font-weight: 600;"> Date </th>
            <th style="text-align: left; font-weight: 600;"> Description </th>
            <th style="text-align: left; font-weight: 600;"> Amount Receivable </th>
            <th style="text-align: left; font-weight: 600;"> Amount Recieved </th>
            <th style="text-align: left; font-weight: 600;"> Balance </th>

        </tr>
    </thead>
    <tbody style="border-bottom: 1px solid black;">
"""
        for recordIndex in 0..<dateArray.count
 {
            
            let name = (dateArray[recordIndex])
            let fatherName = descrArray[recordIndex]
            let id = amountRecievedArray[recordIndex]
            let dob = balanceArray[recordIndex]
            let amountReceivalbe = amountRecievableArray[recordIndex]
            text = text + """
 <tr>
            <td>\(name)</td>
            <td>\(fatherName)</td>
            <td>\(id)</td>
            <td>\(amountReceivalbe)</td>
 <td>\(dob)</td>
        </tr>
 """
        }
        
        text = text + """
            </tbody>
            </table>
            """
        return text
    }
    //pic file from phone directory and draw imge there on given Rect
    func drawImageOnPDF(path: String) {
        
        let pdf = CGPDFDocument(NSURL(fileURLWithPath: path))
        let pageCount = pdf?.numberOfPages
        UIGraphicsBeginPDFContextToFile(path, CGRect.zero, nil)
        for index in 1...pageCount! {
            let page =  pdf?.page(at: index)
            let pageFrame = page?.getBoxRect(.mediaBox)
            UIGraphicsBeginPDFPageWithInfo(pageFrame!, nil)
            let ctx = UIGraphicsGetCurrentContext()
            ctx?.saveGState()
            ctx?.scaleBy(x: 1, y: -1)
            ctx?.translateBy(x: 0, y: -pageFrame!.size.height)
            ctx?.drawPDFPage(page!)
            ctx?.restoreGState()
            
            if index == 1 {
                imageLogo!.draw(in: CGRect(x: 455.2, y: 40, width: 95, height: 45))
            }
            
        }
        UIGraphicsEndPDFContext()
    }
    
    func fileURLForBuggyWKWebView8(fileURL: URL) throws -> URL {
        // Some safety checks
        if !fileURL.isFileURL {
            throw NSError(
                domain: "BuggyWKWebViewDomain",
                code: 1001,
                userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("URL must be a file URL.", comment:"")])
        }
        try! fileURL.checkResourceIsReachable()

        // Create "/temp/www" directory
        let fm = FileManager.default
        let tmpDirURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("www")
        try! fm.createDirectory(at: tmpDirURL, withIntermediateDirectories: true, attributes: nil)

        // Now copy given file to the temp directory
        let dstURL = tmpDirURL.appendingPathComponent(fileURL.lastPathComponent)
        let _ = try? fm.removeItem(at: dstURL)
        try! fm.copyItem(at: fileURL, to: dstURL)

        // Files in "/temp/www" load flawlesly :)
        return dstURL
    }
}
