//
//  ReportsTableViewCell.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/24/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class ReportsTableViewCell: UITableViewCell {

    @IBOutlet weak var viewStack: UIStackView!
    @IBOutlet weak var lblBuySell: UILabel!
    @IBOutlet weak var lblMWH: UILabel!
    @IBOutlet weak var lblObligationAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNetOA: UILabel!
    @IBOutlet weak var lblTaxes: UILabel!
    @IBOutlet weak var lblFess: UILabel!
    @IBOutlet weak var lblStoaCharges: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
            self.viewStack.layer.cornerRadius = 10.0
    }

    

}
