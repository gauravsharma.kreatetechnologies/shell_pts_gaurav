

#import <Foundation/Foundation.h>
#import "Defines.h"
//#import "UIAlertView+Block.h"

@interface Util : NSObject

extern NSString * const UUID_STRING;

+ (NSString *) generateUUID;

+(void)displayOkDialogWithTitle:(NSString *)title andMessage:(NSString *)message;
+(void)displayOkDialogWithTitle:(NSString *)title andMessage:(NSString *)message andCompletion:(CompletionBlock) completion;

+ (BOOL)validateEmailWithString:(NSString*)email;
+ (BOOL)validateUsername:(NSString*)username;

//+ (UIAlertView *)showYesNoAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message withDelegate:(UIViewController *)delegate;

+(NSString *)urlEncoded:(NSString *) input;

+(BOOL) IsValidEmail:(NSString *)checkString;

@end
