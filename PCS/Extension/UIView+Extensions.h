

#import <UIKit/UIKit.h>

@interface UIView (Extensions)

-(UIView *)findFirstResponder;
-(void)forceResignFirstResponder;

@end
