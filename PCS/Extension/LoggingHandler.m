

#import "LoggingHandler.h"

@implementation LoggingHandler

+(void)logError:(NSError *)error {
    NSLog(@"Error: %@", error);
}

@end
