
#import <Foundation/Foundation.h>

@interface LoggingHandler : NSObject

+(void)logError:(NSError *)error;

@end
