

#import "UIView+Extensions.h"

@implementation UIView (Extensions)

-(UIView *)findFirstResponder {
    if (self.isFirstResponder) {
        return self;
    }
    for (UIView *subView in self.subviews) {
        UIView *view = [subView findFirstResponder];
        if (view)
            return view;
    }
    return nil;
}
-(void)forceResignFirstResponder {
    UIView *view = [self findFirstResponder];
    if (view)
        [view resignFirstResponder];
}

@end
