/*
 
     File: UIImage+Resize.h
 Abstract: Category methods to resize an image to fit or fill a given rect.
  Version: 1.0
 
  
 
 */

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

+ (UIImage *)imageWithImage:(UIImage *)image
          scaledToFitToSize:(CGSize)newSize;

+ (UIImage *)imageWithImage:(UIImage *)image
         scaledToFillToSize:(CGSize)newSize;

@end
