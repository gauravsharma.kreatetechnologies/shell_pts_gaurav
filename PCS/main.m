//
//  main.m
//  PCS
//
//  Created by lab4code on 21/11/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
