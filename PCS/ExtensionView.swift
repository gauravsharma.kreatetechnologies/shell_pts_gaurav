//
//  ExtensionView.swift
//  PCS
//
//  Created by Pawan Yadav on 26/03/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

class button: UIButton
{
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0{
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
        {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    
    @IBInspectable var cornerRadius:CGFloat = 0
        {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0
        {
        
        didSet{
            
            self.layer.borderWidth = borderWidth
            
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
        
    }
    
    @IBInspectable var adjustsFontSizeToFitWidth:Bool = true
        {
        
        didSet{
            
            self.titleLabel?.adjustsFontSizeToFitWidth = true
            
        }
        
    }
    
    
}


//  uiLabel class.

@IBDesignable class labelExt: UILabel
{
    @IBInspectable  var cornerRadius:CGFloat = 0
        {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
            
            
        }
        
        
    }
    
    @IBInspectable var borderWidth:CGFloat = 0{
        
        didSet{
            
            self.layer.borderWidth = borderWidth
            
        }
        
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    
}


//Navigation height.

class TTNavigationBar: UINavigationBar
{
    
    override func sizeThatFits(_ size: CGSize) -> CGSize
        
    {
        
        UINavigationBar.appearance().tintColor = UIColor.white
        let customFont = UIFont(name: "Chalkduster", size: 13)!
        
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font : customFont,NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.backgroundColor:UIColor.black],
                                                            for: UIControlState.normal)
        
        return CGSize(width: UIScreen.main.bounds.width, height: 27)
    }
    
}



@IBDesignable class uiRoundView: UIView
{
    @IBInspectable  var cornerRadius:CGFloat = 0.0
    {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
            
            
        }
        
        
    }
    
    @IBInspectable var borderWidth:CGFloat = 0.0
    {
        
        didSet{
            
            self.layer.borderWidth = borderWidth
            
        }
        
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    @IBInspectable  var masksToBounds:Bool = true
    {
        
        didSet{
            
            self.layer.masksToBounds = masksToBounds
            
        }
    }
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        
        didSet{
            
            self.layer.shadowColor = shadowColor.cgColor
            
        }
    }
    
    @IBInspectable var shadowOpacity : CGFloat = 0.0
    {
        
        didSet{
            
            self.layer.shadowOpacity = Float(shadowOpacity)
            
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = CGSize(width: -0, height: 0){
        
        didSet{
            
            self.layer.shadowOffset =  shadowOffset
            
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0
    {
        didSet{
            
            self .layer .shadowRadius = CGFloat(shadowRadius)
            
        }
    }
    
    
    
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
// Use it like this
//view.roundCorners(corners: [.topLeft, .topRight], radius: 20)


extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        // iOS 14 and above
        if #available(iOS 14, *) {// Added condition for iOS 14
          datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}

class RadioButtonController: NSObject {
    var buttonsArray: [UIButton]! {
        didSet {
            for b in buttonsArray {
                b.setImage(UIImage(named: "uncheckedImg"), for: .normal)
                b.setImage(UIImage(named: "checkedImg"), for: .selected)
            }
        }
    }
    var selectedButton: UIButton?
    var defaultButton: UIButton = UIButton() {
        didSet {
            buttonArrayUpdated(buttonSelected: self.defaultButton)
        }
    }

    func buttonArrayUpdated(buttonSelected: UIButton) {
        for b in buttonsArray {
            if b == buttonSelected {
                selectedButton = b
                b.isSelected = true
            } else {
                b.isSelected = false
            }
        }
    }
}


extension UIView {

    // Using CABasicAnimation
        func shake(duration: TimeInterval = 0.02, shakeCount: Float = 2, xValue: CGFloat = 3, yValue: CGFloat = 0){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = duration
            animation.repeatCount = shakeCount
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - xValue, y: self.center.y - yValue))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + xValue, y: self.center.y - yValue))
            self.layer.add(animation, forKey: "shake")
        }
    
}


extension UIViewController{
    func alertView(message:String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: 10, width: 250, height: 100))
        toastLabel.backgroundColor = UIColor(hexString: "#fbce07")
        toastLabel.textColor = UIColor.black
        toastLabel.font = font
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.center = self.view.center
        let theCenter = toastLabel.center
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
extension CGRect {
    var center: CGPoint { .init(x: midX, y: midY) }
}
extension UIViewController{
    func showToast(controller: UIViewController, message : String, seconds: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = .black
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 15
        controller.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
        alert.dismiss(animated: true)

        }
        
    }
}


extension Collection where Iterator.Element == String {
    var convertToDouble: [Double] {
        return compactMap{ Double($0) }
    }
    var convertToFloat: [Float] {
        return compactMap{ Float($0) }
    }
}



@IBDesignable class RoundedImageView : UIImageView{}
@IBDesignable class RoundedCorner : UIView{}

