//
//  AppDelegate.h
//  PCS
//
//  Created by lab4code on 21/11/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SlideNavigationController.h"
#import <UserNotifications/UserNotifications.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic,strong)NSString *strLatitude;
@property(nonatomic,strong)NSString *strLongitude;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+(AppDelegate *)getAppdelegate;
+(void)logout;

- (void)registerForRemoteNotification;

@end




