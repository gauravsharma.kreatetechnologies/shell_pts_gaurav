//
//  BlockViewController.swift
//  PCS
//
//  Created by pavan yadav on 14/09/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts

class BlockViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,ChartViewDelegate
{
    @objc var isPresented: Bool = false
    @IBOutlet weak var buttongreen: UIButton!
    @IBOutlet weak var buttonBlue: UIButton!
    @IBOutlet weak var buttonsky: UIButton!
    @IBOutlet weak var buttonRed: UIButton!
    @IBOutlet weak var buttongreenline: UIButton!
    @IBOutlet weak var buttonBlueline: UIButton!
    @IBOutlet weak var buttonskyline: UIButton!
    @IBOutlet weak var buttonRedline: UIButton!
    
    @IBOutlet weak var myTableView: UITableView!
    
    
    var arrResponse = [JSON]()
    var response = [JSON]()
    var StateID: String = ""
    var ExcType: String = ""
    
    
    
    //Chart
    var months = [Any]()
    var unitsSold = [Any]()
    var unitQty = [Any]()
    var datacellData1 = [String]()
    var datacellData2 = [String]()
    var datacellData3 = [String]()
    var datacellData4 = [String]()
    var xaxisData = [String]()
    
    var strGreenselect: String = ""
    var strBlueselect: String = ""
    var strRedselect: String = ""
    var strSkyselect: String = ""
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.isPresented = true
        
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        self.ApiCalling()
        //marck-- Bool Value Define.
        strGreenselect = "Greenselect"
        strBlueselect = "Blueselect"
        strSkyselect = "Skyselect"
        strRedselect = "Redselect"
        
    }
    
    func ApiCalling()
    {
        
        let someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : StateID,"type" : ExcType]
        
        
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: lastYearMarketprice, parameters:parameters){(APIData) -> Void in
                
                
                if APIData["type"].stringValue != "ERROR"
                {
                
                if APIData.count>0
                {
                    self.arrResponse = APIData.arrayValue
                    
                    if self.arrResponse.count > 0
                    {
                        self.buttongreen.setTitle(self.arrResponse[0]["date"].stringValue, for: UIControlState.normal)
                        self.buttonBlue.setTitle(self.arrResponse[1]["date"].stringValue, for: UIControlState.normal)
                        self.buttonsky.setTitle(self.arrResponse[2]["date"].stringValue, for: UIControlState.normal)
                        self.buttonRed.setTitle(self.arrResponse[3]["date"].stringValue, for: UIControlState.normal)
                    }
                    
                    
                    self.myTableView.separatorStyle = UITableViewCellSeparatorStyle .none
                    
                    self.datacellData1.removeAll()
                    self.datacellData2.removeAll()
                    self.datacellData3.removeAll()
                    self.datacellData4.removeAll()
                    self.xaxisData.removeAll()
                    
                    
                    for var i in 0..<self.arrResponse[0]["data"].count
                    {
                        self.datacellData1.append(self.arrResponse[0]["data"][i].stringValue)
                        self.datacellData2.append(self.arrResponse[1]["data"][i].stringValue)
                        self.datacellData3.append(self.arrResponse[2]["data"][i].stringValue)
                        self.datacellData4.append(self.arrResponse[3]["data"][i].stringValue)
                        self.xaxisData.append("\(i)")
                        
                    }
                    self.myTableView.reloadData()
                    
                }
            }
            
            }}
        catch
        {
            
        }
        
    }
    
    
    
    // tableView Delegate ............................
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            cell.alpha = 0
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseIn, animations: {() -> Void in
                cell.alpha = 1
            }, completion: {(_ completed: Bool) -> Void in
            })
        }
        else if indexPath.section == 2
        {
            do {
                let rotationTransform: CATransform3D = CATransform3DTranslate(CATransform3DIdentity, 0, 50, 0)
                cell.layer.transform = rotationTransform
                cell.alpha = 0
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {() -> Void in
                    cell.layer.transform = CATransform3DIdentity
                    cell.alpha = 1
                }, completion: {(_ completed: Bool) -> Void in
                })
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 1
        }
        else if section == 2
        {
            if self.arrResponse.count > 0
            {
                return self.arrResponse[0]["data"].count
            }
        }
        return 0
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 230
        }
        if indexPath.section == 1
        {
            return 55
        }
        myTableView.separatorColor = UIColor.black
        return 30
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if (indexPath.section == 0)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LineChart", for: indexPath)as! LineChartCell
            
            cell.selectionStyle = .none
            
            if self.arrResponse.count > 0
            {
                
                
                cell.lineChartView.chartDescription.enabled = false
                cell.lineChartView.leftAxis.enabled = true
                cell.lineChartView.rightAxis.drawAxisLineEnabled = true
                cell.lineChartView.rightAxis.drawGridLinesEnabled = true
                cell.lineChartView.xAxis.drawAxisLineEnabled = true
                cell.lineChartView.xAxis.drawGridLinesEnabled = true
                cell.lineChartView.drawGridBackgroundEnabled = true
                cell.lineChartView.drawBordersEnabled = true
                cell.lineChartView.dragEnabled = true
                cell.lineChartView.setScaleEnabled(true)
                cell.lineChartView.pinchZoomEnabled = false
                
                
                // chart leftAxis Detail
                let leftAxis: YAxis? = cell.lineChartView.leftAxis
                leftAxis?.axisMinimum = 0
                leftAxis?.axisMaximum = 8
                
                // chart RightAxis Detail
                let rightAxis: YAxis? = cell.lineChartView.rightAxis
                rightAxis?.axisMinimum = 0
                rightAxis?.axisMaximum = 8
                
                
                let xAxis = cell.lineChartView.xAxis
                xAxis.axisMinimum = 0
                xAxis.axisMaximum = Double(self.arrResponse[0]["data"].count)
                xAxis.labelPosition  = .bottom
                xAxis.labelCount = xaxisData.count
                xAxis.drawLabelsEnabled = true
                xAxis.drawLimitLinesBehindDataEnabled = true
                xAxis.avoidFirstLastClippingEnabled = false
                xAxis.labelFont = UIFont.systemFont(ofSize: 5.0)
                xAxis.valueFormatter = IndexAxisValueFormatter(values: self.xaxisData)
                
                
                xAxis.granularityEnabled = true
                xAxis.granularity = 1.0
                
                
                
                cell.lineChartView.legend.enabled = true
                
                var dataSets = [ChartDataSet]()
                
                
                var arr1: [ChartDataEntry] = []
                var arr2: [ChartDataEntry] = []
                var arr3: [ChartDataEntry] = []
                var arr4: [ChartDataEntry] = []
                
                
                if datacellData1[0] == "NA"
                {
                    
                }
                else
                {
                    
                    for i in 0..<self.arrResponse[0]["data"].count
                    {
                        
                        let dataEntry1 = ChartDataEntry(x: Double(i+1), y: Double(datacellData1[i])!)
                        let dataEntry2 = ChartDataEntry(x: Double(i+1), y: Double(datacellData2[i])!)
                        let dataEntry3 = ChartDataEntry(x: Double(i+1), y: Double(datacellData3[i])!)
                        let dataEntry4 = ChartDataEntry(x: Double(i+1), y: Double(datacellData4[i])!)
                        
                        arr1.append(dataEntry1)
                        arr2.append(dataEntry2)
                        arr3.append(dataEntry3)
                        arr4.append(dataEntry4)
                        
                    }
                }
                
                
                
                if (strGreenselect == "Greenselect")
                {
                    //Data1 Line Data Setting
                    let data1 = LineChartDataSet(entries: arr1, label: "")
                    data1.lineWidth = 1
                    data1.circleRadius = 2.0
                    data1.circleHoleRadius = 1.0
                    data1.setColor(UIColor .magenta)
                    data1.setCircleColor(UIColor .magenta)
                    dataSets.append(data1)
                }
                
                if (strBlueselect == "Blueselect")
                {
                    //Data4 Line Data Setting
                    let data4 = LineChartDataSet(entries: arr4, label: "")
                    data4.lineWidth = 1
                    data4.circleRadius = 2.0
                    data4.circleHoleRadius = 1.0
                    data4.setColor(UIColor .blue)
                    data4.setCircleColor(UIColor .blue)
                    dataSets.append(data4)
                }
                
                
                if (strSkyselect == "Skyselect")
                {
                    //Data2 Line Data Setting
                    let data2 = LineChartDataSet(entries: arr2, label: "")
                    data2.lineWidth = 1
                    data2.circleRadius = 2.0
                    data2.circleHoleRadius = 1.0
                    let color = UIColor(red: (0 / 255.0), green: (255 / 255.0), blue: (255 / 255.0), alpha: 1)
                    data2.setColor(color)
                    data2.setCircleColor(color)
                    dataSets.append(data2)
                }
                if (strRedselect == "Redselect")
                {
                    let data3 = LineChartDataSet(entries: arr3, label: "")
                    data3.lineWidth = 1
                    data3.circleRadius = 2.0
                    data3.circleHoleRadius = 1.0
                    data3.setColor(UIColor .red)
                    data3.setCircleColor(UIColor .red)
                    dataSets.append(data3)
                }
                
                
                if !(strGreenselect == "Greenselect") && !(strBlueselect == "Blueselect") && !(strSkyselect == "Skyselect") && !(strRedselect == "Redselect")
                {
                    
                    
                    //Data1 Line Data Setting
                    let data1 = LineChartDataSet(entries: arr1, label: "")
                    data1.lineWidth = 1
                    data1.circleRadius = 2.0
                    data1.circleHoleRadius = 1.0
                    data1.setColor(UIColor .magenta)
                    data1.setCircleColor(UIColor .magenta)
                    dataSets.append(data1)
                    
                    
                    
                    //Data2 Line Data Setting
                    let data2 = LineChartDataSet(entries: arr2, label: "")
                    data2.lineWidth = 1
                    data2.circleRadius = 2.0
                    data2.circleHoleRadius = 1.0
                    var color = UIColor(red: (0 / 255.0), green: (255 / 255.0), blue: (255 / 255.0), alpha: 1)
                    data2.setColor(color)
                    data2.setCircleColor(color)
                    dataSets.append(data2)
                    
                    
                    
                    //Data3 Line Data Setting
                    let data3 = LineChartDataSet(entries: arr3, label: "")
                    data3.lineWidth = 1
                    data3.circleRadius = 2.0
                    data3.circleHoleRadius = 1.0
                    color = UIColor.red
                    data3.setColor(color)
                    data3.setCircleColor(color)
                    dataSets.append(data3)
                    
                    
                    //Data4 Line Data Setting
                    let data4 = LineChartDataSet(entries: arr4, label: "")
                    data4.lineWidth = 1
                    data4.circleRadius = 2.0
                    data4.circleHoleRadius = 1.0
                    color = UIColor.blue
                    data4.setColor(UIColor .blue)
                    data4.setCircleColor(UIColor .blue)
                    dataSets.append(data4)
                    
                    
                    strGreenselect = "Greenselect"
                    buttongreen.setTitleColor(UIColor.magenta, for: .normal)
                    buttongreenline.backgroundColor = UIColor.magenta
                    strBlueselect = "Blueselect"
                    buttonBlue.setTitleColor(UIColor.blue, for: .normal)
                    buttonBlueline.backgroundColor = UIColor.blue
                    strSkyselect = "Skyselect"
                    buttonsky.setTitleColor(UIColor(red: (0 / 255.0), green: (255 / 255.0), blue: (255 / 255.0), alpha: 1), for: .normal)
                    buttonskyline.backgroundColor = UIColor(red: (0 / 255.0), green: (255 / 255.0), blue: (255 / 255.0), alpha: 1)
                    
                }
                
                
                let data = LineChartData(dataSets: dataSets)
                // data.valueFont = UIFont(name: "HelveticaNeue-Light", size: 7.0)
                cell.lineChartView.data = data
                cell.lineChartView.reloadInputViews()
            }
            return cell
        }
            
            
        else if (indexPath.section==1)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "lblDatecell", for: indexPath)as! MarketpriceCell
            
            if self.arrResponse.count > 0
            {
                
                
                cell.selectionStyle = .none
                cell.labelDatehed1?.text = self.arrResponse[0]["date"].stringValue
                cell.labelDatehed2?.text = self.arrResponse[1]["date"].stringValue
                cell.labelDatehed3?.text = self.arrResponse[2]["date"].stringValue
                cell.labelDatehed4?.text = self.arrResponse[3]["date"].stringValue
            }
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "priceCell", for: indexPath)as! MarketpriceCell
        
        
        cell.selectionStyle = .none
        cell.labldatefir.layer.cornerRadius = 8.0
        cell.labldatefir.layer.masksToBounds = true
        cell.labldatesec.layer.cornerRadius = 8.0
        cell.labldatesec.layer.masksToBounds = true
        cell.labldatethird.layer.cornerRadius = 8.0
        cell.labldatethird.layer.masksToBounds = true
        cell.labldatefourth.layer.cornerRadius = 8.0
        cell.labldatefourth.layer.masksToBounds = true
        
        if self.arrResponse.count > 0
        {
            cell.lblSrNo.text = "\(indexPath.row + 1)"
            cell.labldatefir?.text = self.arrResponse[0]["data"][indexPath.row].stringValue
            cell.labldatesec?.text = self.arrResponse[1]["data"][indexPath.row].stringValue
            cell.labldatethird?.text = self.arrResponse[2]["data"][indexPath.row].stringValue
            cell.labldatefourth?.text = self.arrResponse[3]["data"][indexPath.row].stringValue
        }
        
        return cell
        
        
    }
    
    
    
    
    @IBAction func buttonfirst(_ sender: UIButton)
    {
        if (strGreenselect == "GreenselectNo")
        {
            strGreenselect = "Greenselect"
            buttongreen.setTitleColor(UIColor.magenta, for: .normal)
            buttongreenline.backgroundColor = UIColor.magenta
            myTableView.reloadData()
        }
        else {
            strGreenselect = "GreenselectNo"
            buttongreen.setTitleColor(UIColor.gray, for: .normal)
            buttongreenline.backgroundColor = UIColor.gray
            myTableView.reloadData()
        }
    }
    
    @IBAction func buttonsec(_ sender: UIButton)
    {
        
        if (strBlueselect == "BlueselectNo")
        {
            strBlueselect = "Blueselect"
            buttonBlue.setTitleColor(UIColor.blue, for: .normal)
            buttonBlueline.backgroundColor = UIColor.blue
            myTableView.reloadData()
        }
        else
        {
            strBlueselect = "BlueselectNo"
            buttonBlue.setTitleColor(UIColor.gray, for: .normal)
            buttonBlueline.backgroundColor = UIColor.gray
            myTableView.reloadData()
        }
    }
    
    
    @IBAction func buttonthird(_ sender: UIButton)
    {
        if (strSkyselect == "SkyselectNo") {
            strSkyselect = "Skyselect"
            buttonsky.setTitleColor(UIColor(red: (0 / 255.0), green: (255 / 255.0), blue: (255 / 255.0), alpha: 1), for: .normal)
            buttonskyline.backgroundColor = UIColor(red: (0 / 255.0), green: (255 / 255.0), blue: (255 / 255.0), alpha: 1)
            myTableView.reloadData()
        }
        else {
            strSkyselect = "SkyselectNo"
            buttonsky.setTitleColor(UIColor.gray, for: .normal)
            buttonskyline.backgroundColor = UIColor.gray
            myTableView.reloadData()
        }
    }
    
    
    
    @IBAction func butoofourth(_ sender: UIButton)
    {
        if (strRedselect == "RedselectNo")
        {
            strRedselect = "Redselect"
            buttonRed.setTitleColor(UIColor.red, for: .normal)
            buttonRedline.backgroundColor = UIColor.red
            myTableView.reloadData()
        }
        else {
            strRedselect = "RedselectNo"
            buttonRed.setTitleColor(UIColor.gray, for: .normal)
            buttonRedline.backgroundColor = UIColor.gray
            myTableView.reloadData()
        }
    }
    
    
    @IBAction func bckButton(_ sender: UIButton)
    {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        navigationController?.popViewController(animated: true)
    }
    
}







class LineChartCell: UITableViewCell
{
    
    @IBOutlet weak var lineChartView: LineChartView!
    
}



class MarketpriceCell: UITableViewCell
{
    @IBOutlet weak var labelDatehed1: UILabel!
    @IBOutlet weak var labelDatehed2: UILabel!
    @IBOutlet weak var labelDatehed3: UILabel!
    @IBOutlet weak var labelDatehed4: UILabel!
    @IBOutlet weak var lblSrNo: UILabel!
    @IBOutlet weak var labldatefir: UILabel!
    @IBOutlet weak var labldatesec: UILabel!
    @IBOutlet weak var labldatethird: UILabel!
    @IBOutlet weak var labldatefourth: UILabel!
}



