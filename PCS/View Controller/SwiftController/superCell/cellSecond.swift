//
//  cellSecond.swift
//  ptsApp swift
//
//  Created by pavan yadav on 05/01/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class cellSecond: UITableViewCell,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var tableViewnewsfeed: UITableView!
    
 var labelPower = UILabel()
    
// Dasboard second cell property Property.-----------------------
    @IBOutlet var labelNRLDC: UILabel!
    @IBOutlet var labelWRLDC: UILabel!
    @IBOutlet var labelSRLDC: UILabel!
    @IBOutlet var labelNERLDC: UILabel!
    @IBOutlet var labelERLDC: UILabel!
    
    var viewControllera = UIViewController()
    
    
    var arrNew = [JSON]()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    
//        self.tableViewnewsfeed.register(UINib(nibName: "secNewsCell", bundle: nil), forCellReuseIdentifier: "secNewsid")

        
       
        let someDict =  [""]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: viewControllera, apiURL: newsFeed, parameters:parameters)
            {(APIData) -> Void in
                
                
                self.arrNew = [APIData]
                self.tableViewnewsfeed.reloadData()
            }
        }
        catch
        {
            
        }
    }

    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    

    // table View-----
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 43;
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
      
        if self.arrNew.count > 0
        {
            return self.arrNew[0]["Power"].count
        }
        
        return 0
    }
    
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        
    {
        let Cell = tableView.dequeueReusableCell(withIdentifier: "secNewsidd", for: indexPath)as! containerCell

        Cell.selectionStyle = .none
        
        
        labelPower = UILabel(frame: CGRect(x:30, y: 23, width: self.arrNew[0]["Power"][indexPath.row]["type"] .stringValue.characters.count+20, height: 9))
        
        labelPower.layer.masksToBounds = true
        labelPower.layer.cornerRadius = 4.0
        labelPower.textColor = #colorLiteral(red: 0.9901960784, green: 1, blue: 1, alpha: 1)
        labelPower.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        labelPower.textAlignment = .center
        labelPower.font = labelPower.font.withSize(7)
        labelPower.text = self.arrNew[0]["Power"][indexPath.row]["type"] .stringValue
        Cell.addSubview(labelPower)
        
        Cell.newsHeading.text = self.arrNew[0]["Power"][indexPath.row]["source"] .stringValue
        
        Cell.labelHeading.text = self.arrNew[0]["Power"][indexPath.row]["title"] .stringValue
//        Cell.typeEnergy.text = self.arrNew[0]["Renewable energy"][indexPath.row]["type"] .stringValue
        
        
        Cell.labelDateTime.text = "\(self.arrNew[0]["Power"][indexPath.row]["time"] .stringValue)- \(self.arrNew[0]["Power"][indexPath.row]["pubdate"] .stringValue)"
            return Cell
    }
    
   
@IBAction func URSpusViewButton(_ sender: UIButton)
{
    
//
//    let mainStoryboarda: UIStoryboard? = window?.rootViewController?.storyboard
//
//    let detailViewController: CurrentAvailableURSViewController? = (mainStoryboarda?.instantiateViewController(withIdentifier: "CurrentAvailableURS") as? CurrentAvailableURSViewController)
//        if sender.tag == 1
//        {
//          detailViewController?.ursType = "NRLDC"
//        }
//        else if sender.tag == 2
//        {
//            detailViewController?.ursType = "WRLDC"
//        }
//        else if sender.tag == 3
//        {
//            detailViewController?.ursType = "SRLDC"
//        }
//        else if sender.tag == 4
//        {
//            detailViewController?.ursType = "NERLDC"
//        }
//        else if sender.tag == 5
//        {
//            detailViewController?.ursType = "ERLDC"
//        }
//
//            (window?.rootViewController as? UINavigationController)?.pushViewController(detailViewController!, animated: false)
//
  }
    


}
