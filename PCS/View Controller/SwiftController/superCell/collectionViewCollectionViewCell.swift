//
//  collectionViewCollectionViewCell.swift
//  ptsApp swift
//
//  Created by pavan yadav on 05/01/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Charts

class collectionViewCollectionViewCell: UICollectionViewCell
{
    // FirstCell Property.-----------------------
    @IBOutlet var labelLinechart: UILabel!
    @IBOutlet var lineChartView: LineChartView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var buttonlineChart: UIButton!
    
    // secondCell Property.-----------------------
    
    @IBOutlet var pieChartView: PieChartView!
    
    @IBAction func LineChartselect(_ sender: Any)
    {
        
        let mainStoryboarda: UIStoryboard? = window?.rootViewController?.storyboard
        
        let detailViewController: stateLoadViewController? = (mainStoryboarda?.instantiateViewController(withIdentifier: "StateLoadID") as? stateLoadViewController)
        
        detailViewController?.typeStr = (buttonlineChart.titleLabel?.text)!
        (window?.rootViewController as? UINavigationController)?.pushViewController(detailViewController!, animated: false)
    }

}
