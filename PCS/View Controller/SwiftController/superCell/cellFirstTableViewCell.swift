//
//  cellFirstTableViewCell.swift
//  ptsApp swift
//
//  Created by pavan yadav on 05/01/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts


class cellFirstTableViewCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource ,ChartViewDelegate
{
    @IBOutlet weak var tableViewcellfirst: UITableView!
    @IBOutlet weak var graphCollction: UICollectionView!
    @IBOutlet var lineGraphView: LineChartView!
    
    
    
    @IBOutlet weak var labelMCP: UILabel!
    @IBOutlet weak var ButtonType: UIButton!
    @IBOutlet weak var buttonDate: UIButton!
    
    var ResponseData = [JSON]()
    var responseGraph = [JSON]()
    
    var demand = [String]()
    var Scheduling = [String]()
    var lineState = [String]()
    
    var name = [String]()
    var values = [Double]()
    
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.tableViewcellfirst.separatorStyle = UITableViewCellSeparatorStyle.none
        // SVProgressHUD.show(withStatus: "Please wait...")
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    
    
// table View---------------------------------------------------------
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        
        if indexPath.section == 0
        {
            return 26;
        }
        else
        {
            return 20;
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == tableViewcellfirst
        {
            return nil
            
        }
        let frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(tableViewcellfirst.bounds.size.width), height: CGFloat(30))
        let label = UILabel(frame: frame)
        label.backgroundColor = UIColor(red: CGFloat(39 / 255.0), green: CGFloat(174 / 255.0), blue: CGFloat(171 / 255.0), alpha: CGFloat(1.0))
        label.textAlignment = .center
        label.font = label.font.withSize(10)
        label.textColor = UIColor .white
        label.text = "Real Time Scheduling "
        return label
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if section == 0
        {
            return 1
        }
        return ResponseData.count
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            let Cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            return Cell
        }
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "cellmcp", for: indexPath)as! containerCell
        
        Cell.labelDate.text = ResponseData[indexPath.row]["date"] .stringValue
        Cell.labelMAX.text = ResponseData[indexPath.row]["max"] .stringValue
        Cell.labelMIIN.text = ResponseData[indexPath.row]["min"] .stringValue
        Cell.labelAVG.text = ResponseData[indexPath.row]["avg"] .stringValue
        
        Cell.selectionStyle = .none
        return Cell
    }
    
    
    
    // Collection View methode------
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        if responseGraph.count>0
        {
            
            return responseGraph[0]["stateload"].count
        }
        else
        {
            return 0
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if indexPath.section == 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellSec", for: indexPath) as! collectionViewCollectionViewCell
            
            name = []
            values = []
            lineState.removeAll()
            
            cell.pieChartView.delegate = self
            cell.pieChartView.animate(xAxisDuration: 1, easingOption: ChartEasingOption.easeInCirc)
            cell.pieChartView.chartDescription.enabled = false
            cell.pieChartView.usePercentValuesEnabled = true
            cell.pieChartView.holeRadiusPercent = 0.50
            cell.pieChartView.usePercentValuesEnabled = true
            cell.pieChartView.drawSlicesUnderHoleEnabled = true
            cell.pieChartView.drawHoleEnabled = true
            cell.pieChartView.legend.font = UIFont .systemFont(ofSize: 8.0)
            cell.pieChartView.legend.xEntrySpace = 3.0
            cell.pieChartView.legend.yEntrySpace = 3.0
            cell.pieChartView.legend.horizontalAlignment = .center
            cell.pieChartView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            
            if responseGraph.count>0
            {
                for j in 0..<responseGraph[0]["scheduling"].count
                {
                    self.name.append(responseGraph[0]["scheduling"][j]["name"] .stringValue)
                    self.values.append(responseGraph[0]["scheduling"][j]["value"].doubleValue)
                }
                
            }
            
           
        
            
            var dataEntries: [ChartDataEntry] = []
            
            for i in 0..<name.count
            {
                let dataEntry1 = PieChartDataEntry(value: values[i], label: name[i])
                dataEntries.append(dataEntry1)
            }
            
            
            let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
            pieChartDataSet.entryLabelFont = UIFont .systemFont(ofSize: 5.0)
            pieChartDataSet.entryLabelColor = UIColor .black
            pieChartDataSet.valueFont = UIFont .systemFont(ofSize: 5.0)
            pieChartDataSet.valueColors = [UIColor .black]
            pieChartDataSet.sliceSpace = 3.0
            pieChartDataSet.selectionShift = 5.0
            
            
            //  pieChartDataSet.
            let pieChartData = PieChartData(dataSet: pieChartDataSet)
            
            
            //  pieChart Slice color ----------------------
            var colors: [UIColor] = []
            //            for _ in 0..<name.count {
            //                let red = Double(arc4random_uniform(256))
            //                let green = Double(arc4random_uniform(256))
            //                let blue = Double(arc4random_uniform(256))
            //
            // let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            
            let colora = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
            let colorb = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
            let colorc = #colorLiteral(red: 0.430973453, green: 0.7243080021, blue: 0.3582761421, alpha: 1)
            let colord = #colorLiteral(red: 0.957757761, green: 0.3957115113, blue: 0.1661794968, alpha: 1)
            let colore = #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)
            
            //colors.append(color)
            
            colors.append(colorb)
            colors.append(colord)
            colors.append(colora)
            colors.append(colorc)
            colors.append(colore)
            
            
            
            //            }
            
            
            pieChartDataSet.colors = colors
            
// pieChart Slice label value % formater----------------------
            let formatter = NumberFormatter()
            formatter.numberStyle = .percent
            formatter.maximumFractionDigits = 1
            formatter.multiplier = 1.0
            pieChartData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
            
            cell.pieChartView.data = pieChartData
            
            return cell
        }
        
        
        
        
        
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollection", for: indexPath) as! collectionViewCollectionViewCell
        
        demand = []
        Scheduling = []
        
        
        if responseGraph.count>0
        {
            
            var newsTitle = [String]()
            
            for (key, subJson) in responseGraph[0]["stateload"]
            {
                
                newsTitle.append(key)
            }
            
            
            Cell.labelLinechart.text = "\("State load") \(newsTitle[indexPath.row])"
            Cell.buttonlineChart.setTitle(newsTitle[indexPath.row], for: UIControlState .normal)
            
            lineState.removeAll()
            for i in 0..<responseGraph[0]["stateload"][newsTitle[indexPath.row]].count
            {
                self.demand.append(responseGraph[0]["stateload"][newsTitle[indexPath.row]][i]["demand"] .stringValue)
                self.Scheduling.append(responseGraph[0]["stateload"][newsTitle[indexPath.row]][i]["sch_drawal"].stringValue)
                
                self.lineState.append(responseGraph[0]["stateload"][newsTitle[indexPath.row]][i]["state"].stringValue)
                
            }
            
            
        }
        
        
        Cell.lineChartView.chartDescription.text="";
        Cell.lineChartView.leftAxis.drawLabelsEnabled = true
        Cell.lineChartView.leftAxis.enabled = true
        Cell.lineChartView.xAxis.drawLabelsEnabled = true
        Cell.lineChartView.xAxis.enabled = true
        Cell.lineChartView.rightAxis.drawLabelsEnabled = false
        Cell.lineChartView.rightAxis.enabled = false
        Cell.lineChartView.borderColor = UIColor .gray
        Cell.lineChartView.borderLineWidth = 0.5
        Cell.lineChartView.legend.enabled = true
        Cell.lineChartView.legend.textHeightMax=1.0;
        Cell.lineChartView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        setChart(  Cell.lineChartView,dataPoints:Scheduling, values: demand)
        
        return Cell
    }
  


    
    
    // LineChart  methode------
    fileprivate func setChart(_ lineChartView: LineChartView, dataPoints: [String], values: [String])
    {
        
        let leftLabel = lineChartView.leftAxis
        leftLabel.labelFont = UIFont .systemFont(ofSize: 7.0)
        
        
        
        var dataEntries: [ChartDataEntry] = []
        var dataEntrires2: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = ChartDataEntry(x: Double(i+1), y: Double(demand[i])!)
            let dataEntry2 = ChartDataEntry(x: Double(i+1), y: Double(Scheduling[i])!)
            
            dataEntries.append(dataEntry)
            dataEntrires2.append(dataEntry2)
        }
        
        
        
         let xaxis = lineChartView.xAxis

       // xaxis.axisMinimum = 0.0
        
        xaxis.labelPosition = .top
        xaxis.labelWidth = 5
        xaxis.centerAxisLabelsEnabled = true
        xaxis.labelFont = UIFont .systemFont(ofSize: 5.0)
        xaxis.granularity = 3.0
        xaxis.valueFormatter = IndexAxisValueFormatter(values: lineState)

        
        
        //arrMin Line Data Setting
        
        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "Demand")
        lineChartDataSet.setColor(UIColor.green)
        lineChartDataSet.mode = .linear
        lineChartDataSet.fillColor  = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lineChartDataSet.drawCirclesEnabled = true
        lineChartDataSet.lineWidth = 1.0
        lineChartDataSet.circleHoleRadius = 1.0
        lineChartDataSet.circleRadius = 3.0
        lineChartDataSet.circleColors = [#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)]
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = true
        lineChartDataSet.drawFilledEnabled = true
        lineChartDataSet.colors = [#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)]
        
        
        let lineChartDataSet2 = LineChartDataSet(entries: dataEntrires2, label: "Scheduling")
        lineChartDataSet2.setColor(UIColor.blue)
        lineChartDataSet2.mode = .linear
        lineChartDataSet2.drawCirclesEnabled = true
        lineChartDataSet2.lineWidth = 1.0
        lineChartDataSet2.circleHoleRadius = 1.0
        lineChartDataSet2.circleRadius = 3.0
        lineChartDataSet2.circleColors = [#colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)]
        lineChartDataSet2.fillColor  = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        lineChartDataSet2.drawHorizontalHighlightIndicatorEnabled = true
        lineChartDataSet2.drawFilledEnabled = true
        lineChartDataSet2.colors = [#colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)]
        
        
        
        
        var dataSets = [ChartDataSet]()
        
        dataSets.append(lineChartDataSet)
        dataSets.append(lineChartDataSet2)
        
        let lineChartData = LineChartData(dataSets: dataSets)
        lineChartView.data = lineChartData
        //SVProgressHUD.dismiss()
    }
    
    
    
// PieChart Slice didSelect ------------------------------------------

    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight)
    {
        let mainStoryboarda: UIStoryboard? = window?.rootViewController?.storyboard
        
        let detailViewController: reatimechartViewController? = (mainStoryboarda?.instantiateViewController(withIdentifier: "RealTimeScheduling") as? reatimechartViewController)
        
        (window?.rootViewController as? UINavigationController)?.pushViewController(detailViewController!, animated: false)
     }
    
   
    
}





