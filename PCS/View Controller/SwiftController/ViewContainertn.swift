//
//  ViewContainertn.swift
//  ptsApp swift
//
//  Created by pavan yadav on 14/02/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewContainertn: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var arrNew = [JSON]()
    var controllerTitle = [String]()
    var controllerArray : [UIViewController] = []
    var tableView = UITableView()
    var labelPower = UILabel()
    var labelNews = UILabel()
  var strTrue = ""
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let someDict =  [""]

        strTrue = "truee"
        do
        {
        
       
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: newsFeed, parameters:parameters){(APIData) -> Void in
              
                
                self.arrNew = [APIData]
                
                for (key, subJson) in APIData
                {
                    self.controllerTitle.append(key)
                }
                
                
                for i in 0..<self.controllerTitle.count
                {
                   
                    
                    let controller : UIViewController = UIViewController()
                    controller.title = self.controllerTitle[i]
                    
                    self.tableView = UITableView(frame: CGRect(x: 0, y:2, width: controller.view.bounds.width, height: controller.view.bounds.height-165))
                    self.tableView.register(UINib(nibName: "containerCell", bundle: nil), forCellReuseIdentifier: "containerid")
                    self.tableView.tag = i
                    self.tableView.showsVerticalScrollIndicator = false
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                    self.tableView.reloadData()
                    
                    controller.view.addSubview(self.tableView)
                    self.controllerArray.append(controller)
                    
                }
                
                
                // ContainerView Methode--------------------------------------
                
                let contaninerVC = TNContainerViewController.init(controllers: self.controllerArray as NSArray, topBarHeight: 0, parentViewController: self)
                contaninerVC.menuItemFont = UIFont.systemFont(ofSize: 13)
                contaninerVC.menuIndicatorColor = UIColor.white
                contaninerVC.menuItemTitleColor = UIColor.white
                contaninerVC.menuItemSelectedTitleColor = UIColor.white
                self.view.addSubview(contaninerVC.view)
            }
            
        }
            
            
            
        catch
        {
            
        }
        
}
    
    
    
    
    
    
    
// TABLE VIEW METHODE----------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrNew[0][self.controllerTitle[tableView.tag]].count
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "containerid", for: indexPath)as! containerCell
    
            Cell.selectionStyle = .none


        
//            Cell.newsHeading.frame = UILabel(frame: CGRect(x:10, y: 22, width: self.arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["source"] .stringValue.characters.count*5+3, height: 9))
        

        
        Cell.newsHeading.text = self.arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["source"] .stringValue
        
        
            labelPower = UILabel(frame: CGRect(x:Int(Cell.newsHeading.bounds.size.width+15), y: 21, width: self.arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["type"] .stringValue.characters.count*5, height: 9))

            labelPower.layer.masksToBounds = true
            labelPower.layer.cornerRadius = 4.0
            labelPower.textColor = #colorLiteral(red: 0.9901960784, green: 1, blue: 1, alpha: 1)
            labelPower.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            labelPower.textAlignment = .center
            labelPower.font = labelPower.font.withSize(7)
            labelPower.text = self.arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["type"] .stringValue
        
        
        Cell.addSubview(labelPower)
        
  
        Cell.labelHeading.text = self.arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["title"] .stringValue

        
        Cell.labelDateTime.text = "\(self.arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["time"] .stringValue)- \(self.arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["pubdate"] .stringValue)"
        
      
        return Cell
    }
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {

        let newView:WebbrowserControllerViewController = storyboard?.instantiateViewController(withIdentifier: "webBrowse") as! WebbrowserControllerViewController
        
        newView.strWebUrl = arrNew[0][self.controllerTitle[tableView.tag]][indexPath.row]["link"] .stringValue
        
        self.navigationController?.pushViewController(newView, animated: true)
        
    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}





