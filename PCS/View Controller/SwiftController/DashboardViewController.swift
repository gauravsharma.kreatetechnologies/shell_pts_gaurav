//
//  DashboardViewController.swift
//  ptsApp swift
//
//  Created by pavan yadav on 04/01/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts

class DashboardViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegateFlowLayout
    
{
    
    
    @IBOutlet weak var bottomPickerConstraints: NSLayoutConstraint!
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet weak var typePickerView: UIPickerView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelmarquee: MarqueeLabel!
    @IBOutlet weak var viewhight: NSLayoutConstraint!
    
    
    var bounds = UIScreen.main.bounds
    var currentViewController: UIViewController?
    var ContainerCall: ViewContainertn?
    
    var animate = true
    var pickerSelection = true
    var pickerType = " "
    var pickerLoc = " "
    var pickerStateid = " "
    
    let arrayiex = ["IEX","PXIL"]
    var date = [String]()
    
    var minvalue = [String]()
    var avgvalue = [String]()
    var maxvalue = [String]()
    
    var Lognotification = ""
    
    var dictsDashboard = [["name": "Realtime", "icon": "ic_realtime.png", "storyboardid": "RealTimeScheduling"], ["name": "Transmission Corridor", "icon": "ic_corridor", "storyboardid": "TransmissionCorridor"],  ["name": "Monthely ATC", "icon": "ic_atc", "storyboardid": "MonthlyATC"] ,["name": "StateLoad", "icon": "stateLoad", "storyboardid": "StateLoadID"] ,["name": "Notification", "icon": "ic_notification", "storyboardid": "notificationID"]]
    
    
    var Locationtype = [JSON]()
    var responseTable = [JSON]()
    var collectionGraphRes = [JSON]()
    var allRegionData:JSON?
    var allursDetailsData:JSON?
    var stateName = [String]()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.separatorColor = UIColor .clear
        labelmarquee.type = .continuous
        labelmarquee.animationCurve = .easeInOut
        typePickerView.isHidden = true
        bottomPickerConstraints.constant = -181
        
        ContainerCall = self.storyboard!.instantiateViewController(withIdentifier: "idTNcontainer") as? ViewContainertn
        currentViewController = self.ContainerCall
        self.displayContentController(ContainerCall!)
        
        
        
        
        //SVProgressHUD.show(withStatus: "Please wait...")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        
        
        //1. Calling Initial api and getting response..
        
        let someDict =  ["latitude" : "-1","logtitude" : "-1"]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: DashboardLocation, parameters:parameters){(APIData) -> Void in
                
                
                if APIData["type"] == "ERROR"
                {
                    SVProgressHUD.showError(withStatus: APIData["message"] .stringValue)
                }
                else
                {
                    self.Locationtype = APIData ["states"].arrayValue
                    
                    self.stateName.removeAll()
                    
                    for i in 0..<self.Locationtype.count
                    {
                        self.stateName.append("\(self.Locationtype[i]["state_name"] .stringValue)\("(")\(self.self.Locationtype[i]["iexregion"] .stringValue)\(")")")
                    }
                    
                    
                    
                    if (AppHelper.userDefaults(forKey: "StateId") == nil)
                    {
                        self.pickerStateid =  self.Locationtype[0]["id"] .stringValue
                        self.pickerLoc =  self.stateName[0]
                        self.pickerType = self.arrayiex[0]
                        
                        AppHelper.save(toUserDefaults: self.Locationtype[0]["region"].stringValue, withKey: "regionName")
                    }
                    else
                    {
                        self.pickerStateid =  AppHelper.userDefaults(forKey: "StateId")
                        self.pickerLoc =  AppHelper.userDefaults(forKey: "StateName")
                        self.pickerType =  AppHelper.userDefaults(forKey: "BidType")
                    }
                    
                    AppHelper.save(toUserDefaults: self.pickerLoc, withKey: "StateName")
                    AppHelper.save(toUserDefaults: self.pickerStateid, withKey: "StateId")
                    AppHelper.save(toUserDefaults: self.pickerType, withKey: "BidType")
                    
                    
                    
                    
                    self.CallSecApi()
                    self.graphApi()
                    
                }
            }
        }
        catch
        {
            
        }
        
        self.calluserApi()
        self.newURSDetailsApi()
        
       // SVProgressHUD.dismiss()
        CustomHUD.shredObject.removeXib(view: self.view)
    }
    
    
    
    
    
    //2. Calling second api and getting response..
    
    func CallSecApi()
    {
        CustomHUD.shredObject.loadXib(view: self.view)
        let someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : pickerStateid,"type" : pickerType]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: DashboardLocationSec, parameters:parameters){(APIData) -> Void in
                 CustomHUD.shredObject.removeXib(view: self.view)
                
                if APIData.count>0
                {
                    
                    self.responseTable = APIData .arrayValue
                    
                    
                    for i in 0..<self.responseTable.count
                    {
                        self.minvalue.append(self.responseTable [i]["min"] .stringValue)
                        self.avgvalue.append(self.responseTable[i]["avg"] .stringValue)
                        self.maxvalue.append(self.responseTable[i]["max"] .stringValue)
                        self.date.append(self.responseTable[i]["date"] .stringValue)
                        
                    }
                    self.tableView.reloadData()
                }
                    
                else
                {
                   
                    let alert = UIAlertController(title: "Data Not found", message: "Please select another state", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
                
            }
            
        }
        catch
        {
            
        }
        
    }
    
    
    
    
    //3. Api calling  graph..-------------------------------------
    func graphApi()
    {
        
        let someDict =  ["state_id" : pickerStateid,"type" : pickerType]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL:"http://www.mittalpower.com/pts-client/api/index.php/mobile/getdashboarddata", parameters:parameters){(APIData) -> Void in
                
                
                
                let combination = NSMutableAttributedString()
                let lineArr = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 15.0)!]
                let line = NSMutableAttributedString(string: "|", attributes: lineArr)
                
                let nameAttribyte = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 10.0)!]
                let valueAttribyte = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: 10.0)!]
                
                
                
                for j in 0..<APIData["losses"].count
                {
                    
                    combination.append(NSAttributedString(string:APIData["losses"][j]["name"] .stringValue, attributes: nameAttribyte))
                    
                    combination.append(NSAttributedString(string:" "))
                    
                    combination.append(NSAttributedString(string: APIData["losses"][j]["value"] .stringValue, attributes: valueAttribyte ))
                    
                    combination.append(NSAttributedString(string:" "))
                    
                    combination.append(line)
                    combination.append(NSAttributedString(string:"  "))
                    
                }
                
                self.labelmarquee.attributedText = combination
                self.labelmarquee.layer.speed = 0.2
                //self.labelmarquee.labelWasTapped(<#T##recognizer: UIGestureRecognizer##UIGestureRecognizer#>)
                
                self.collectionGraphRes = [APIData]
                self.tableView.reloadData()
            }
        }
        catch
        {
        }
    }
    
    
    
    //4. Api calling  user details..
    func calluserApi()
    {
        
        let someDict =  ["type" : "REGION","id" : "NRLDC","device_id" : AppHelper.userDefaults(forKey: "deviceId"),"access_key" : AppHelper.userDefaults(forKey: "access_key")]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: currentURS, parameters:parameters)
            {(APIData) -> Void in
                
                
              //  print(APIData)
                
                self.allRegionData = APIData
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        } 
        catch
        {
            
        }
        
    }
    
    
    //MARK:- New Api For all URS Detils Data
    
    func newURSDetailsApi()
    {
        
        let someDict =  ["type" : "REGION","region" : "NRLDC","device_id" : AppHelper.userDefaults(forKey: "deviceId")]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: newURSDetails, parameters:parameters)
            {(APIData) -> Void in
                
                
                
                //print(APIData)
                
                self.allursDetailsData = APIData
                self.tableView.reloadData()
              
            }
        }
        catch
        {
            
        }
        
    }
    
    
    
    
    
    
    // ContainerView Controller---------------------------------------------
    
    func displayContentController(_ content: UIViewController)
    {
        self.addChildViewController(content)
        content.view.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.containerView.frame.size.width), height: CGFloat(self.containerView.frame.size.height))
        self.containerView.addSubview(content.view)
        content.didMove(toParentViewController: self)
         self.containerView.isHidden = true
    }
    
    
    
    // ContainerView Height---------------------------------------------
    
    @IBAction func buttonAnimationview(_ sender: UIButton)
    {
        if animate == true
        {
             self.containerView.isHidden = false
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: [], animations: {
                self.viewhight.constant = self.bounds.size.height-130;
                self.view.layoutIfNeeded()
                self.animate = false
                
            }, completion: { (finished: Bool) in
                
                
            })
            
         
        }
        else
        {
            
            
            UIView.animate(withDuration: 1.5, delay: 0.0, options: [], animations: {
                self.viewhight.constant = 1;
                self.view.layoutIfNeeded()
                self.animate = true
                
            }, completion: { (finished: Bool) in
                
                self.containerView.isHidden = true
            })
            
            
        }
    }
    
    
    
    @IBAction func ButtonDate(_ sender: Any)
    {
        date = []
        minvalue = []
        avgvalue = []
        maxvalue = []
        typePickerView.isHidden = false
        pickerSelection = true
        typePickerView.reloadAllComponents()
        
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.bottomPickerConstraints.constant = 0
            self.view.layoutIfNeeded()
           
        })
       
        
    }
    
    @IBAction func buttonAction(_ sender: Any)
    {
        date = []
        minvalue = []
        avgvalue = []
        maxvalue = []
        typePickerView.isHidden = false
        pickerSelection = false
        typePickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.bottomPickerConstraints.constant = 0
            self.view.layoutIfNeeded()
            
        })
    }
    
    
    
    
    
    
    
    // PICKER VIEW METHODE CALLING-----------------------------------------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerSelection == true
        {
            return stateName[row]
        }
        else
        {
            
            return arrayiex[row]
        }
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerSelection == true
        {
            return stateName.count
        }
        else
        {
            
            return arrayiex.count
        }
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        
        if pickerSelection == true
        {
            
            pickerLoc =  stateName[row]
            pickerStateid =  Locationtype[row]["id"] .stringValue
            
            AppHelper.removeFromUserDefaults(withKey:  "StateId")
            AppHelper.save(toUserDefaults: self.pickerStateid, withKey: "StateId")
            AppHelper.removeFromUserDefaults(withKey:  "StateName")
            AppHelper.save(toUserDefaults: self.pickerLoc, withKey: "StateName")
            AppHelper.removeFromUserDefaults(withKey:  "regionName")
            AppHelper.save(toUserDefaults: self.Locationtype[row]["region"].stringValue, withKey: "regionName")
            
        }
        else
        {
            pickerType = arrayiex[row]
            
            AppHelper.removeFromUserDefaults(withKey: "BidType")
            AppHelper.save(toUserDefaults: self.pickerType, withKey: "BidType")
        }
        
        
        
        self.CallSecApi()
        self.graphApi()
        
      
        tableView.reloadData()
       
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            self.bottomPickerConstraints.constant = -181
            self.view.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            self.typePickerView.isHidden = true
        })
      
    }
    
    
    
    
    // TABLE VIEW METHODE----------------------------------------------
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 330
        }
        return 280
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section==0
        {
            let Cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)as! cellFirstTableViewCell
            
            Cell.selectionStyle = .none
            Cell.ButtonType.layer.borderColor = UIColor (red: 55 / 255.0, green: 180 / 255.0, blue: 190 / 255.0, alpha: 1.0) .cgColor
            Cell.buttonDate.layer.borderColor = UIColor (red: 55 / 255.0, green: 180 / 255.0, blue: 190 / 255.0, alpha: 1.0) .cgColor
            Cell.ButtonType.setTitle(pickerType, for: .normal)
            Cell.buttonDate.setTitle(pickerLoc, for: .normal)
            Cell.buttonDate.titleLabel?.adjustsFontSizeToFitWidth = true
            // Cell.buttonDate.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            // Cell.buttonDate.titleLabel!.textAlignment = .left
            Cell.buttonDate.titleEdgeInsets = UIEdgeInsetsMake(0,0, 0, 8)
            
            Cell.ResponseData = self.responseTable
            Cell.tableViewcellfirst.reloadData()
            
            Cell.responseGraph = self.collectionGraphRes
            Cell.graphCollction.reloadData()
            
            
            
            
            // LineChart in cell methode MCP Last 3 year ---------------------------------------
            
            Cell.lineGraphView.chartDescription.text = "Values in Rupees"
            Cell.lineGraphView.chartDescription.font = UIFont .systemFont(ofSize: 8.0)
            Cell.lineGraphView.chartDescription.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.lineGraphView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            
            Cell.lineGraphView.rightAxis.drawLabelsEnabled = false
            Cell.lineGraphView.xAxis.drawLabelsEnabled = true
            Cell.lineGraphView.drawGridBackgroundEnabled = true
            Cell.lineGraphView.xAxis.drawGridLinesEnabled = true
            Cell.lineGraphView.drawGridBackgroundEnabled = true
            
            Cell.lineGraphView.xAxis.enabled = true
            
            Cell.lineGraphView.rightAxis.enabled = true
            Cell.lineGraphView.drawGridBackgroundEnabled = false
            Cell.lineGraphView.borderColor = UIColor .gray
            Cell.lineGraphView.borderLineWidth = 0.5
            Cell.lineGraphView.drawBordersEnabled = true
            Cell.lineGraphView.dragEnabled = false
            Cell.lineGraphView.setScaleEnabled(true)
            Cell.lineGraphView.pinchZoomEnabled = true
            Cell.lineGraphView.legend.enabled = true
            Cell.lineGraphView.legend.textHeightMax = 1.0
            
            
            
            
            // chart leftAxis Detail
            Cell.lineGraphView.leftAxis.enabled = true
            Cell.lineGraphView.leftAxis.drawAxisLineEnabled = true
            Cell.lineGraphView.leftAxis.drawGridLinesEnabled = true
            
            
            let leftxis = Cell.lineGraphView.leftAxis
            leftxis.axisMinimum = 0.0
            //            leftxis.axisMaximum = 5.0
            leftxis.labelFont =  UIFont .systemFont(ofSize: 8.0)
            
            Cell.lineGraphView.xAxis.gridColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            Cell.lineGraphView.xAxis.enabled = true
            Cell.lineGraphView.xAxis.drawAxisLineEnabled = true
            Cell.lineGraphView.xAxis.drawGridLinesEnabled = true
            
            let xaxis = Cell.lineGraphView.xAxis
            
            xaxis.labelFont = UIFont .systemFont(ofSize: 7.0)
            xaxis.granularity = 2.0
            xaxis.labelPosition = .top
            xaxis.drawLabelsEnabled = true
            xaxis.drawLimitLinesBehindDataEnabled = true
            xaxis.avoidFirstLastClippingEnabled = true
            
            
            xaxis.labelPosition = .top
            xaxis.centerAxisLabelsEnabled = true
            
            
            xaxis.valueFormatter = IndexAxisValueFormatter(values: date)
            
            
            setChart(Cell.lineGraphView,dataPoints:date, values: date)
            
            
            
            return Cell
        }
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)as! cellSecond
        Cell.selectionStyle = .none
        
        
        // myLabel.text!.replacingOccurrences(of: "\", with: "")
        
        Cell.labelNRLDC.text = self.allursDetailsData?["NRLDC"]["reshedule_urs_remaining"] .stringValue
        Cell.labelWRLDC.text = self.allursDetailsData?["WRLDC"]["reshedule_urs_remaining"] .stringValue
        Cell.labelSRLDC.text = self.allursDetailsData?["SRLDC"]["reshedule_urs_remaining"] .stringValue
        Cell.labelNERLDC.text = self.allursDetailsData?["NERLDC"]["reshedule_urs_remaining"] .stringValue
        Cell.labelERLDC.text = self.allursDetailsData?["ERLDC"]["reshedule_urs_remaining"] .stringValue
        
       // Cell.labelERLDC.text = self.allRegionData?["region"]["ERLDC"]["available"] .stringValue.replacingOccurrences(of: "-", with: "")
        
        return Cell
    }
    
    
    
    
    
    
    
    
    
    //COLLECTION VIEW METHODE-----------------------------------
    
    @objc(collectionView:layout:insetForSectionAtIndex:) func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        
        let viewWidth: Int = Int(collectionView.bounds.width)
        let totalCellWidth: Int = 56 * dictsDashboard.count+1
        let totalSpacingWidth: Int = 3 * (dictsDashboard.count+1 - 1)
        let leftInset: Int = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2
        let rightInset: Int = leftInset
        return UIEdgeInsetsMake(0, CGFloat(leftInset), 0, CGFloat(rightInset))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return dictsDashboard.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
        
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollection", for: indexPath) as! collectionViewCollectionViewCell
        cell.imageView.image = UIImage(named:dictsDashboard[indexPath.row]["icon"]!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let login = storyboard?.instantiateViewController(withIdentifier:dictsDashboard[indexPath.row]["storyboardid"]!)
        
        self.navigationController?.pushViewController(login!, animated: true)
    }
    
    
    
    // LineChart  methode MCP Last 3 year------
    
    fileprivate func setChart(_ lineChartView: LineChartView, dataPoints: [String], values: [String])
    {
        
        var dataEntries: [ChartDataEntry] = []
        var dataEntrires1: [ChartDataEntry] = []
        var dataEntrires2: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = ChartDataEntry(x: Double(i+1), y: Double(minvalue[i])!)
            let dataEntry1 = ChartDataEntry(x: Double(i+1), y: Double(avgvalue[i])!)
            let dataEntry2 = ChartDataEntry(x: Double(i+1), y: Double(maxvalue[i])!)
            
            
            dataEntries.append(dataEntry)
            dataEntrires1.append(dataEntry1)
            dataEntrires2.append(dataEntry2)
        }
        
        
        lineChartView.animate(yAxisDuration: 2.5)
        
        
        //arrMin Line Data Setting------------------------
        
        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "Min")
        
        lineChartDataSet.lineWidth = 1.0
        lineChartDataSet.circleHoleRadius = 1.0
        lineChartDataSet.circleRadius = 2.0
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.colors = [#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)]
        lineChartDataSet.circleColors = [#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)]
        lineChartDataSet.drawValuesEnabled = false
        
        let lineChartDataSet1 = LineChartDataSet(entries: dataEntrires1, label: "Avg")
        lineChartDataSet1.lineWidth = 1.0
        lineChartDataSet1.circleHoleRadius = 1.0
        lineChartDataSet1.circleRadius = 2.0
        lineChartDataSet1.mode = .cubicBezier
        lineChartDataSet1.colors = [#colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1)]
        lineChartDataSet1.circleColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        lineChartDataSet1.drawValuesEnabled = false
        
        
        let lineChartDataSet2 = LineChartDataSet(entries: dataEntrires2, label: "Max")
        lineChartDataSet2.lineWidth = 1.0
        lineChartDataSet2.circleHoleRadius = 1.0
        lineChartDataSet2.circleRadius = 2.0
        lineChartDataSet2.mode = .cubicBezier
        lineChartDataSet2.colors = [#colorLiteral(red: 0.3077597327, green: 0.467019067, blue: 0.8503822143, alpha: 1)]
        lineChartDataSet2.drawValuesEnabled = false
        
        
        var dataSets = [ChartDataSet]()
        
        dataSets.append(lineChartDataSet)
        dataSets.append(lineChartDataSet1)
        dataSets.append(lineChartDataSet2)
        
        let lineChartData = LineChartData(dataSets: dataSets)
        lineChartView.data = lineChartData
        
    }
    
    
    
    
    //    @IBAction func Landscape2(_ sender: Any) {
    //        let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "Landscape") as! LandscapeViewController
    //        self.navigationController?.pushViewController(loginPageView, animated: true)
    //    }
    //}
    //
    
    
    
    
    // GraphView touch go next graphView------
    
    @IBAction func GraphPushView(_ sender: Any)
    {
        
        let login:BlockViewController = storyboard?.instantiateViewController(withIdentifier: "blockviewId") as! BlockViewController
        
        login.StateID = pickerStateid
        login.ExcType = pickerType
        
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    
    //pragma mark - SlideNavigationController Methods -
    
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
    }
    
    @IBAction func mcpComparisonButton(_ sender: UIButton)
    {
        let pushView: MCPViewController? = storyboard?.instantiateViewController(withIdentifier: "MCPID") as! MCPViewController?
        navigationController?.pushViewController(pushView!, animated: true)
    }
   
}






