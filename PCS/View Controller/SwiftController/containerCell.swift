//
//  containerCell.swift
//  PCS
//
//  Created by pavan yadav on 22/03/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit

class containerCell: UITableViewCell {
    
    
// cellFirstCell Property.----------------------------------------------------
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelMAX: UILabel!
    @IBOutlet weak var labelMIIN: UILabel!
    @IBOutlet weak var labelAVG: UILabel!
    
// secondcell & container news property Property.------------------------------
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet var newsHeading: UILabel!
    @IBOutlet var newsheadingConstraint: NSLayoutConstraint!
    @IBOutlet var typeEnergy: UILabel!
    @IBOutlet var labelDateTime: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    
}
