//
//  setupAccTableViewCell.swift
//  PCS
//
//  Created by Pawan Yadav on 26/03/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit

class setupAccTableViewCell: UITableViewCell
{

    @IBOutlet weak var serialNo: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var region: UILabel!
    @IBOutlet weak var revision: UILabel!
    @IBOutlet weak var Buyer: UILabel!
    @IBOutlet weak var seller: UILabel!
    @IBOutlet weak var approval: UILabel!
    @IBOutlet weak var qtm: UILabel!
    @IBOutlet weak var diffrence: UILabel!
    @IBOutlet weak var importType: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
