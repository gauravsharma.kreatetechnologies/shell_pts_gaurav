//
//  MyScheduleViewController.swift
//  MySchedule
//
//  Created by pavan yadav on 24/04/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MyScheduleViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate
    
{
    var condi = Bool()
   @objc var arrResponse = [String:AnyObject]()
    var Lognotification = ""
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet var btnLogin: UIButton!
    
    
    
    
    
    var responseTable = [JSON]()
    var Arrfilterd = [""]
    var responseFilterCount: Int = 0
    var scro = true
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        dataView.isHidden = true
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        
        
        
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        
        self.FirstApiCalling()
    }
    
    
    
    
    //MARK: 1. Calling Initial api and getting response..
    
    func FirstApiCalling()
    {
       // SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["access_key" : "wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv"]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: MyscheduleGet, parameters:parameters)
            {
                (APIData) -> Void in
                
                if APIData.count>0
                {
                    //SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    
                    
                    if APIData["type"].stringValue == "ERROR"
                    {
                        
                        self.dataView.isHidden = true
                        
                    }
                    else
                    {
                        
                        self.dataView.isHidden = false
                        self.responseTable = APIData .arrayValue
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    
                    self.dataView.isHidden = true
                    
                }
                //SVProgressHUD.dismiss()
                CustomHUD.shredObject.removeXib(view: self.view)
            }
        }
        catch
        {
        }
    }
    
    
    
    
    
    
    // TABLE VIEW METHODE----------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 24
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return responseTable.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "cellCommon", for: indexPath)as! setupAccTableViewCell
        Cell.selectionStyle = UITableViewCellSelectionStyle .none
        
        
        
        Cell.serialNo.text = "\(indexPath.row + 1)"
        Cell.region.text = self.responseTable[indexPath.row]["region"] .stringValue
        Cell.revision.text = self.responseTable[indexPath.row]["revision"] .stringValue
        Cell.Buyer.text = self.responseTable[indexPath.row]["buyer"] .stringValue
        Cell.seller.text = self.responseTable[indexPath.row]["seller"] .stringValue
        Cell.approval.text = self.responseTable[indexPath.row]["app_no"] .stringValue
        
        Cell.qtm.text = self.responseTable[indexPath.row]["qtm"] .stringValue
        
        
        
        
        return Cell
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let realView: myScheduleGraph? = storyboard?.instantiateViewController(withIdentifier: "myschedulegraph") as? myScheduleGraph
        
        realView?.dataId = self.responseTable[indexPath.row]["id"] .stringValue as NSString
        navigationController?.pushViewController(realView!, animated: true)
        
        
        self.view.endEditing(true)
    }
    
    
    
    
    
    
    
    
    
    //pragma mark - SlideNavigationController Methods -
    
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    
}



