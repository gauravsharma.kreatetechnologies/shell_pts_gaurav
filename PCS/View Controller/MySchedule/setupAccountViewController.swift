//
//  setupAccountViewController.swift
//  PCS
//
//  Created by Pawan Yadav on 26/03/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class setupAccountViewController: UIViewController ,UITextFieldDelegate ,UIPickerViewDelegate,UIPickerViewDataSource ,UITableViewDataSource,UITableViewDelegate
    
{
    
    @IBOutlet weak var setupViewConstant: NSLayoutConstraint!
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet weak var slctRegionbtn: UIButton!
    @IBOutlet weak var clientTextField: UITextField!
    
    
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet weak var tableView: UITableView!
    var Lognotification = ""
    var selectedRegion = "WR"
    
    
    var apiResponse = [JSON]()
    var apiDataRes = [JSON]()
    let regionsType = ["WR","ER","NR","NER","SR"]
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupViewConstant.constant = 0
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        pickerView.isHidden = true
        
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        self.FirstApiCalling()
    }
    
    
    
    
    //MARK: 1. Calling Initial api and getting response..
    
    func FirstApiCalling()
    {
       // SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["access_key" : "lxrqjkxjtradkkvqaszphurmdbygnknyheprombieblplgfmzfbgzttdvrjjbxhj"]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: MyscheduleSetup, parameters:parameters)
            {
                (APIData) -> Void in
                print(APIData)
                
                self.apiDataRes = [APIData]
                
                if APIData.count>0
                {
                    //SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    
                    
                    if APIData["type"].stringValue == "ERROR"
                    {
                        
                        // self.dataView.isHidden = true
                        
                    }
                    else
                    {
                        
                        //self.dataView.isHidden = false
                        self.apiResponse = APIData["data"].arrayValue
                        self.apiDataRes = [APIData]
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    
                    //self.dataView.isHidden = true
                    
                }
                //SVProgressHUD.dismiss()
                CustomHUD.shredObject.removeXib(view: self.view)
            }
        }
        catch
        {
        }
    }
    
    
    
    
    
    
    func saveApiMethode()
    {
        
        let email = apiDataRes[0]["email"] .stringValue
        let Mobile = apiDataRes[0]["mobile"] .stringValue
        let company = apiDataRes[0]["company"] .stringValue
        let code = clientTextField.text!
        
        //SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["access_key" : "lxrqjkxjtradkkvqaszphurmdbygnknyheprombieblplgfmzfbgzttdvrjjbxhj","company" : company,"mobile" : Mobile,"email" : email,"detail" : ["region" : selectedRegion,"code" : code]] as [String : Any]
        
        
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: MyscheduleSave, parameters:parameters)
            {
                (APIData) -> Void in
                print(APIData)
                
                if APIData.count>0
                {
                   // SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    
                    
                    if APIData["type"].stringValue == "ERROR"
                    {
                        //SVProgressHUD.dismiss()
                        CustomHUD.shredObject.removeXib(view: self.view)
                        // self.dataView.isHidden = true
                    }
                    else
                    {
                        SVProgressHUD.showSuccess(withStatus: self.apiResponse[0]["message"].stringValue)
                        
                    }
                    self.FirstApiCalling()
                }
                else
                {
                    //self.dataView.isHidden = true
                }
                
            }
        }
        catch
        {
        }
    }
    
    
    
    
    
    
    
    
    @IBAction func deleteButtonRow(_ sender: UIButton)
    {
        
        
        let alert = UIAlertController(title: "Are you sure want to Delete?", message: "Delete", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Confirm", style: .destructive, handler:{ action in
            self.deleteMethode(tag: sender.tag)
            print()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    
    
    func deleteMethode(tag : Int)
    {
        
        let region = apiResponse[tag]["region"] .stringValue
        let id = apiResponse[tag]["id"] .stringValue
        let code = apiResponse[tag]["code"] .stringValue
        
        
        
        
       // SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["access_key" : "lxrqjkxjtradkkvqaszphurmdbygnknyheprombieblplgfmzfbgzttdvrjjbxhj","id" : id,"region" : region,"code" : code]
        
        
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: MyscheduleSetupDelete, parameters:parameters)
            {
                (APIData) -> Void in
                
                print(APIData)
                if APIData.count>0
                {
                    //SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    
                    
                    if APIData["type"].stringValue == "ERROR"
                    {
                        //SVProgressHUD.dismiss()
                        CustomHUD.shredObject.removeXib(view: self.view)
                        // self.dataView.isHidden = true
                    }
                    else
                    {
                        SVProgressHUD.showSuccess(withStatus: self.apiResponse[0]["message"].stringValue)
                        
                    }
                    self.FirstApiCalling()
                }
                else
                {
                    //self.dataView.isHidden = true
                }
                
            }
        }
        catch
        {
        }
    }
    
    
    
    @IBAction func setupButton(_ sender: UIButton)
    {
        setupViewConstant.constant = 200
        clientTextField.text = ""
    }
    
    @IBAction func cancelButton(_ sender: UIButton)
    {
        pickerView.isHidden = true
        setupViewConstant.constant = 0
    }
    
    @IBAction func saveButton(_ sender: UIButton)
    {
        pickerView.isHidden = true
        self.saveApiMethode()
        setupViewConstant.constant = 0
    }
    
    
    
    @IBAction func selectRegionButt(_ sender: UIButton)
    {
        pickerView.isHidden = false
        pickerView.reloadAllComponents()
    }
    
    
    
    
    
    
    
    // PICKER VIEW METHODE CALLING-------------------------------------------------------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    //
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        return regionsType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return regionsType[row]
    }
    
    
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        pickerView.isHidden = true
        slctRegionbtn.setTitle(regionsType[row], for: .normal)
        selectedRegion = regionsType[row]
        
    }
    
    
    
    
    
    
    
    
    // TABLE VIEW METHODE----------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 26
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return apiResponse.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! setupAccTableViewCell
        Cell.selectionStyle = UITableViewCellSelectionStyle .none
        self.tableView.layoutMargins = UIEdgeInsets .zero
        
        
        
        Cell.serialNo.text = "    \(indexPath.row + 1)."
        Cell.code.text = apiResponse[indexPath.row]["code"] .stringValue
        Cell.region.text = apiResponse[indexPath.row]["region"] .stringValue
        Cell.cancelButton.tag = indexPath.row
        
        
        return Cell
    }
    
    
    
    
    
    
    
    
    
    //pragma mark - SlideNavigationController Methods -
    
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        pickerView.isHidden = true
        self.view.endEditing(true)
    }
    
    
}

