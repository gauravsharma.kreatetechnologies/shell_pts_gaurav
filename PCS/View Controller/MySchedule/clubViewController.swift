//
//  clubViewController.swift
//  PCS
//
//  Created by Pawan Yadav on 27/03/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class clubViewController:  UIViewController ,UITextFieldDelegate ,UIPickerViewDelegate,UIPickerViewDataSource ,UITableViewDataSource,UITableViewDelegate
    
{
    
    @IBOutlet weak var setupViewConstant: NSLayoutConstraint!
    @IBOutlet weak var viewConstant: NSLayoutConstraint!
    @IBOutlet weak var dataView: UIView!
    
    @IBOutlet var btnLogin: UIButton!
    
    @IBOutlet weak var slctViewbtn: UIButton!
    @IBOutlet weak var slctTermtbtn: UIButton!
    
    @IBOutlet weak var lblViewType: UILabel!
    @IBOutlet weak var lblTerms: UILabel!
    
    
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet weak var tableView: UITableView!
    
    var Lognotification = ""
    let viewType = ["BOTH","TERMS","REGIONS"]
    
    let termsType = ["STOA","MTOA","LTA","INTRA","ALL"]
    let regionsType = ["ER","WR","NR","SR","NER","ALL"]
    
    var buttontYPE = "view"
    var termsTy = ""
    var responseTable = [JSON]()
    
    var apiPaTYPE = "BOTH"
    var apiPaREGION = "term"
    
    var indexValue = 0
    
    
    
    var unclubbed  = [Dictionary<String, AnyObject>]()
    var clubbed  = [Dictionary<String, AnyObject>]()
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupViewConstant.constant = 0
        viewConstant.constant = 60
        lblTerms.isHidden = true
        slctTermtbtn.isHidden = true
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        pickerView.isHidden = true
        
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        self.FirstApiCalling()
    }
    
    
    
    //MARK: 1. Calling Initial api and getting response..
    
    func FirstApiCalling()
    {
        //SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["access_key" : "wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv","type": apiPaTYPE ,"region": apiPaREGION]
        
        
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: ClubbedViewGet, parameters:parameters)
            {
                (APIData) -> Void in
                
                if APIData.count>0
                {
                    self.responseTable = [APIData]
                    
                    //SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    
                    if APIData["type"].stringValue == "ERROR"
                    {
                        //self.dataView.isHidden = true
                    }
                    else
                    {
                        
                        
                        //MARK:- clubed data Get.
                        for i in 0 ..< self.responseTable[0]["clubbed"].count
                        {
                            
                            for j in 0 ..< self.responseTable[0]["clubbed"][i]["data"].count
                            {
                                
                                var nsdict = self.responseTable[0]["clubbed"][i]["data"][j].dictionary! as [String : AnyObject]
                                
                                nsdict["color"] = "false" as AnyObject
                                
                                
                                if i % 2 == 0
                                {
                                    nsdict["clubedDiffrence"] = "true" as AnyObject
                                }
                                else
                                {
                                    nsdict["clubedDiffrence"] = "false" as AnyObject
                                }
                                
                                
                                
                                self.clubbed.append(nsdict)
                            }
                            
                        }
                        
                        
                        
                        //MARK:- unclubed data Get.
                        for i in 0 ..< self.responseTable[0]["unclubbed"].count
                        {
                            
                            var Dictunclubbed = self.responseTable[0]["unclubbed"][i]["data"].dictionary! as [String : AnyObject]
                            
                            Dictunclubbed["color"] = "true" as AnyObject
                            
                            self.unclubbed.append(Dictunclubbed)
                        }
                        
                        
                        
                        
                        
                        
                        
                        
                        //MARK:- MERGE UNclubed & clubed data Get.
                        
                        for k in 0 ..< self.unclubbed.count
                        {
                            
                            self.clubbed.append(self.unclubbed[k])
                            
                        }
                        
                        
                        
                        self.tableView.reloadData()
                    }
                }
                
               // SVProgressHUD.dismiss()
                CustomHUD.shredObject.removeXib(view: self.view)
            }
        }
        catch
        {
        }
    }
    
    
    
    
    
    @IBAction func setupButton(_ sender: UIButton)
    {
        setupViewConstant.constant = 200
    }
    
    @IBAction func cancelButton(_ sender: UIButton)
    {
        pickerView.isHidden = true
        setupViewConstant.constant = 0
    }
    
    @IBAction func saveButton(_ sender: UIButton)
    {
        pickerView.isHidden = true
        setupViewConstant.constant = 0
        self.FirstApiCalling()
    }
    
    
    
    @IBAction func viewTypeButton(_ sender: UIButton)
    {
        pickerView.isHidden = false
        buttontYPE = "view"
        pickerView.reloadAllComponents()
    }
    
    @IBAction func selectClientButt(_ sender: UIButton)
    {
        pickerView.isHidden = false
        buttontYPE = "term"
        pickerView.reloadAllComponents()
    }
    
    
    
    
    
    // PICKER VIEW METHODE CALLING-------------------------------------------------------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    //
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        
        if  buttontYPE == "view"
        {
            return viewType[row]
        }
        else
        {
            
            if termsTy == "term"
            {
                return termsType[row]
            }
            else
            {
                return regionsType[row]
            }
            
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        if buttontYPE == "view"
        {
            return viewType.count
        }
        else
        {
            
            if termsTy == "term"
            {
                return termsType.count
            }
            else
            {
                return regionsType.count
            }
            
        }
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        if buttontYPE == "view"
        {
            indexValue = row
            slctViewbtn.setTitle(viewType[row], for: .normal)
            if row == 0
            {
                slctViewbtn.setTitle(viewType[row], for: .normal)
                viewConstant.constant = 60
                lblTerms.isHidden = true
                slctTermtbtn.isHidden = true
            }
            else if row == 1
            {
                slctTermtbtn.setTitle(termsType[row], for: .normal)
                termsTy = "term"
                viewConstant.constant = 100
                lblTerms.isHidden = false
                slctTermtbtn.isHidden = false
                lblTerms.text = "Select Terms"
            }
            else if row == 2
            {
                slctTermtbtn.setTitle(regionsType[row], for: .normal)
                termsTy = "region"
                viewConstant.constant = 100
                lblTerms.isHidden = false
                slctTermtbtn.isHidden = false
                lblTerms.text = "Select Region"
            }
            
            apiPaTYPE = viewType[row]
        }
        else
        {
            if indexValue == 1
            {
                slctTermtbtn.setTitle(termsType[row], for: .normal)
                termsTy = "term"
                apiPaREGION = termsType[row]
            }
            else if indexValue == 2
            {
                slctTermtbtn.setTitle(regionsType[row], for: .normal)
                termsTy = "region"
                apiPaREGION = regionsType[row]
            }
            
            viewConstant.constant = 100
            lblTerms.isHidden = false
            slctTermtbtn.isHidden = false
        }
        
        
        pickerView.isHidden = true
        pickerView.reloadAllComponents()
        
        
    }
    
    
    
    
    
    
    
    
    // TABLE VIEW METHODE----------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 26
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if clubbed.count > 0
        {
            return clubbed.count
        }
        else
        {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let Cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! setupAccTableViewCell
        
        
        if clubbed.count > 0
        {
            
            Cell.selectionStyle = UITableViewCellSelectionStyle .none
            self.tableView.layoutMargins = UIEdgeInsets .zero
            
            
            
            if String(describing: clubbed[indexPath.row]["color"] as AnyObject) == "true"
            {
                Cell.serialNo.textColor = UIColor .red
                Cell.region.textColor = UIColor .red
                Cell.Buyer.textColor = UIColor .red
                Cell.seller.textColor = UIColor .red
                Cell.approval.textColor = UIColor .red
                Cell.qtm.textColor = UIColor .red
                Cell.diffrence.textColor = UIColor .red
                Cell.revision.textColor = UIColor .red
                
                
                Cell.serialNo.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                Cell.region.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                Cell.Buyer.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                Cell.seller.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                Cell.approval.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                Cell.qtm.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                Cell.diffrence.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                Cell.revision.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                
                Cell.diffrence.text = String(describing: clubbed[indexPath.row]["difference"] as AnyObject)
            }
            else
            {
                Cell.serialNo.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                Cell.region.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                Cell.Buyer.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                Cell.seller.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                Cell.approval.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                Cell.qtm.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                Cell.diffrence.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                Cell.revision.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                
                if String(describing: clubbed[indexPath.row]["clubedDiffrence"] as AnyObject) == "true"
                {
                    
                    Cell.serialNo.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    Cell.region.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    Cell.Buyer.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    Cell.seller.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    Cell.approval.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    Cell.qtm.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    Cell.diffrence.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    Cell.revision.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                else
                {
                    Cell.serialNo.backgroundColor = UIColor .blue
                    Cell.region.backgroundColor = UIColor .blue
                    Cell.Buyer.backgroundColor = UIColor .blue
                    Cell.seller.backgroundColor = UIColor .blue
                    Cell.approval.backgroundColor = UIColor .blue
                    Cell.qtm.backgroundColor = UIColor .blue
                    Cell.diffrence.backgroundColor = UIColor .blue
                    Cell.revision.backgroundColor = UIColor .blue
                }
                Cell.diffrence.text =  "0"
            }
            
            
            Cell.serialNo.text = "\(indexPath.row + 1)"
            Cell.region.text = String(describing: clubbed[indexPath.row]["region"] as AnyObject)
            Cell.Buyer.text = String(describing: clubbed[indexPath.row]["buyer"] as AnyObject)
            Cell.seller.text = String(describing: clubbed[indexPath.row]["seller"] as AnyObject)
            Cell.approval.text = String(describing: clubbed[indexPath.row]["app_no"] as AnyObject)
            Cell.qtm.text = String(describing: clubbed[indexPath.row]["qtm"] as AnyObject)
            Cell.revision.text = String(describing: clubbed[indexPath.row]["revision"] as AnyObject)
            
            
        }
        
        
        
        return Cell
    }
    
    
    
    
    
    
    
    //pragma mark - SlideNavigationController Methods -
    
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        pickerView.isHidden = true
        self.view.endEditing(true)
    }
    
    
}

