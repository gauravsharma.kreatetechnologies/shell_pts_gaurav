//  myScheduleGraph.swift
//  PCS
//
//  Created by pavan yadav on 09/06/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts

class myScheduleGraph: UIViewController ,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dataId: NSString!
    
    @IBOutlet weak var buttonRvsn25: UIButton!
    @IBOutlet weak var buttonRvsn24: UIButton!
    
    var responseTable = [JSON]()
    var Revision31 = [String]()
    var Revision32 = [String]()
    @objc var isPresented = Bool()
    
    var window: UIWindow?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.isPresented = true
        
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        self.apiCall()
    }
    
    
    
    @IBAction func BackButton(_ sender: UIButton)
    {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        navigationController?.popViewController(animated: true)
    }
    
    
    // First api calling..............
    
    func apiCall()
    {
       // SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"data_id":dataId] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: MyscheduleGraph, parameters:parameters)
            {
                
                (APIData) -> Void in
                //SVProgressHUD.dismiss()
                CustomHUD.shredObject.removeXib(view: self.view)
                
                self.responseTable = APIData["detail"] .arrayValue
                
                for i in 0..<self.responseTable.count
                {
                    self.Revision31.append(self.responseTable[i]["p_qtm"] .stringValue)
                    self.Revision32.append(self.responseTable[i]["c_qtm"] .stringValue)
                    
                }
                
                self.tableView.reloadData()
                
            }
        }
        catch
        {
        }
    }
    
    
    
    
    
    
    // TABLE VIEW METHODE----------------------------------------------
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 255
        }
        else if indexPath.section == 1
        {
            return 27
        }
        
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 1
        }
        return responseTable.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        if indexPath.section==0
        {
            let Cell = tableView.dequeueReusableCell(withIdentifier: "LineChart", for: indexPath)as! commonTableViewCell
            
            Cell.selectionStyle = .none
            
            
            // LineChart in cell methode MCP Last 3 year ---------------------------------------
            
            Cell.lineChartView.chartDescription.text="Discription";
            Cell.lineChartView.chartDescription.font = UIFont .systemFont(ofSize: 8.0)
            Cell.lineChartView.chartDescription.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            
        
            Cell.lineChartView.xAxis.drawLabelsEnabled = true
            Cell.lineChartView.drawGridBackgroundEnabled = true
            Cell.lineChartView.xAxis.drawGridLinesEnabled = true
            Cell.lineChartView.drawGridBackgroundEnabled = true
            
            Cell.lineChartView.xAxis.enabled = true
            
    
            Cell.lineChartView.drawGridBackgroundEnabled = false
            Cell.lineChartView.borderColor = UIColor .gray
            Cell.lineChartView.borderLineWidth = 0.5
            Cell.lineChartView.drawBordersEnabled = true
            Cell.lineChartView.dragEnabled = false
            Cell.lineChartView.setScaleEnabled(true)
            Cell.lineChartView.pinchZoomEnabled = true
            Cell.lineChartView.legend.enabled = true
            Cell.lineChartView.legend.textHeightMax = 1.0
            
            
            
            
            // chart leftAxis Detail
            Cell.lineChartView.leftAxis.enabled = true
            Cell.lineChartView.leftAxis.drawAxisLineEnabled = true
            Cell.lineChartView.leftAxis.drawGridLinesEnabled = true
            
            
            let leftxis = Cell.lineChartView.leftAxis
            leftxis.axisMinimum = 0.0
            //leftxis.axisMaximum = 5.0
            leftxis.labelFont =  UIFont .systemFont(ofSize: 8.0)
            
            
            // chart leftAxis Detail
            Cell.lineChartView.rightAxis.enabled = true
            Cell.lineChartView.rightAxis.drawAxisLineEnabled = true
            Cell.lineChartView.rightAxis.drawGridLinesEnabled = true
            
            let rightxis = Cell.lineChartView.rightAxis
            rightxis.axisMinimum = 0.0
            //            leftxis.axisMaximum = 5.0
            rightxis.labelFont =  UIFont .systemFont(ofSize: 8.0)
            
            
            
            Cell.lineChartView.xAxis.gridColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            Cell.lineChartView.xAxis.enabled = true
            Cell.lineChartView.xAxis.drawAxisLineEnabled = true
            Cell.lineChartView.xAxis.drawGridLinesEnabled = true
            
            let xaxis = Cell.lineChartView.xAxis
            
            xaxis.labelFont = UIFont .systemFont(ofSize: 7.0)
            xaxis.granularity = 2.0
            xaxis.labelPosition = .top
            xaxis.drawLabelsEnabled = true
            xaxis.drawLimitLinesBehindDataEnabled = true
            xaxis.avoidFirstLastClippingEnabled = true
            
            
            xaxis.labelPosition = .top
            xaxis.centerAxisLabelsEnabled = true
           // xaxis.valueFormatter = IndexAxisValueFormatter(values: date)
            
            
            setChart(Cell.lineChartView,dataPoints:Revision31, values: Revision32)
            
            return Cell
        }
        else if indexPath.section == 1
        {
            
            let Cell = tableView.dequeueReusableCell(withIdentifier: "lblDatecell", for: indexPath)as! commonTableViewCell
            Cell.selectionStyle = .none
            
            return Cell
        }
        let Cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath)as! commonTableViewCell
        Cell.selectionStyle = .none
    
        Cell.SrNo.text = "    \(indexPath.row + 1)"
        Cell.time.text = self.responseTable[indexPath.row]["time"] .stringValue
        Cell.R31label.text = self.responseTable[indexPath.row]["p_qtm"] .stringValue
        Cell.R32label.text = self.responseTable[indexPath.row]["c_qtm"] .stringValue
        
       
        return Cell
        
        
    }
    
    
    // LineChart  methode MCP Last 3 year------
    
        fileprivate func setChart(_ lineChartView: LineChartView, dataPoints: [String], values: [String])
        {
    
            var dataEntries: [ChartDataEntry] = []
            var dataEntrires1: [ChartDataEntry] = []
        
    
            for i in 0..<dataPoints.count
            {
                let dataEntry = ChartDataEntry(x: Double(i+1), y: Double(Revision31[i])!)
                let dataEntry1 = ChartDataEntry(x: Double(i+1), y: Double(Revision32[i])!)
                
    
                if buttonRvsn25.tag == 2
                {
                    dataEntries.append(dataEntry)
                }
                if buttonRvsn24.tag == 0
                {
                    dataEntrires1.append(dataEntry1)
                }
            
                
            }
    
    
            lineChartView.animate(yAxisDuration: 2.5)
    
    
            //arrMin Line Data Setting------------------------
    
            let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "")
    
            lineChartDataSet.lineWidth = 1.0
            lineChartDataSet.circleHoleRadius = 1.0
            lineChartDataSet.circleRadius = 2.0
            lineChartDataSet.mode = .cubicBezier
            lineChartDataSet.colors = [#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)]
            lineChartDataSet.circleColors = [#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)]
            lineChartDataSet.drawValuesEnabled = false
    
            let lineChartDataSet1 = LineChartDataSet(entries: dataEntrires1, label: "")
            lineChartDataSet1.lineWidth = 1.0
            lineChartDataSet1.circleHoleRadius = 1.0
            lineChartDataSet1.circleRadius = 2.0
            lineChartDataSet1.mode = .cubicBezier
            lineChartDataSet1.colors = [#colorLiteral(red: 0.2997276328, green: 1, blue: 0.3350572799, alpha: 1)]
            lineChartDataSet1.circleColors = [#colorLiteral(red: 0.2997276328, green: 1, blue: 0.3350572799, alpha: 1)]
            lineChartDataSet1.drawValuesEnabled = false
    
            var dataSets = [ChartDataSet]()
    
            dataSets.append(lineChartDataSet)
            dataSets.append(lineChartDataSet1)
  
    
            let lineChartData = LineChartData(dataSets: dataSets)
            lineChartView.data = lineChartData
    
        }
    
  
    
    @IBAction func buttonRvsn25(_ sender: UIButton)
    {
        
        if buttonRvsn25.tag == 2
        {
         buttonRvsn25.tag = 3
         buttonRvsn25.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        }
        else
        {
           buttonRvsn25.tag = 2
           buttonRvsn25.setTitleColor(#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), for: .normal)
        }
        
        if buttonRvsn25.tag == 3 &&  buttonRvsn24.tag == 1
        {
            buttonRvsn24.tag = 0
            buttonRvsn25.tag = 2
            buttonRvsn25.setTitleColor(#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), for: .normal)
            buttonRvsn24.setTitleColor(#colorLiteral(red: 0.2997276328, green: 1, blue: 0.3350572799, alpha: 1), for: .normal)
        }
       self.tableView.reloadData()
    }
    
    
    @IBAction func buttonRvsn24(_ sender: UIButton)
    {
        
        if buttonRvsn24.tag == 0
        {
            buttonRvsn24.tag = 1
            buttonRvsn24.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        }
        else
        {
            buttonRvsn24.tag = 0
            buttonRvsn24.setTitleColor(#colorLiteral(red: 0.2997276328, green: 1, blue: 0.3350572799, alpha: 1), for: .normal)
        }
        
        if buttonRvsn25.tag == 3 &&  buttonRvsn24.tag == 1
        {
            buttonRvsn24.tag = 0
            buttonRvsn25.tag = 2
            buttonRvsn25.setTitleColor(#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), for: .normal)
            buttonRvsn24.setTitleColor(#colorLiteral(red: 0.2997276328, green: 1, blue: 0.3350572799, alpha: 1), for: .normal)
        }
        
        self.tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
