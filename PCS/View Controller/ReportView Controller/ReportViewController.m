//
//  ReportViewController.m
//  PCS
//
//  Created by lab4code on 06/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

//#import "ReportViewController.h"
////#import "ReportTableViewCell.h"
//#import "Services.h"
//#import "AppHelper.h"
//#import "SVProgressHUD.h"
//#import "AppDelegate.h"
//#import "CustmObj.h"
//
//@class PDFVC;
//
//@interface ReportViewController ()
//{
//    BOOL isLblFromTouch;
//}
//@property(weak,nonatomic)IBOutlet UILabel *lblCompanyName;
//@property(weak,nonatomic)IBOutlet UILabel *lblFrom;
//@property(weak,nonatomic)IBOutlet UILabel *lblTo;
//@property(weak,nonatomic)IBOutlet UITableView *myTableView;
//
//@property (weak, nonatomic) IBOutlet UIView *datePickerView;
//@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;
//
//@property(strong,nonatomic)NSMutableArray *arrData;
//@property(strong,nonatomic)NSMutableArray *arrDate;
//@property(strong,nonatomic)NSMutableArray *arrAmount;
//@property(strong,nonatomic)NSMutableArray *arrBalance;
//
//
//
//@property(nonatomic,strong)NSString *strFromDate;
//@property(nonatomic,strong)NSString *strToDate;
//
//@property (weak, nonatomic) IBOutlet UIView *viewNoData;
//
//@end
//
//@implementation ReportViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//    [self.viewNoData setHidden:YES];
//    // Do any additional setup after loading the view.
//    self.arrDate = [NSMutableArray array];
//
//    if ([AppHelper userDefaultsForKey:@"companyName"]) {
//        self.lblCompanyName.text=[AppHelper userDefaultsForKey:@"companyName"];
//    }
//    
//    self.strFromDate=@"";
//    self.strToDate=@"";
//    
//    self.datePicker.datePickerMode=UIDatePickerModeDate;
//    
//    self.tableHeightConstraint.constant= - (self.datePickerView.frame.size.height+80);
//    [self.datePickerView needsUpdateConstraints];
//    
//    
//    UITapGestureRecognizer *lblFromTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblFromTap:)];
//    lblFromTap.numberOfTapsRequired=1;
//    [self.lblFrom addGestureRecognizer:lblFromTap];
//    
//    
//    UITapGestureRecognizer *lblToTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblToTap:)];
//    lblToTap.numberOfTapsRequired=1;
//    [self.lblTo addGestureRecognizer:lblToTap];
//    
//    
//    [self oneTimeServiceHitData];
//    
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//#pragma mark - date tap gesture
//
//- (void)lblFromTap:(UIGestureRecognizer*)gestureRecognizer
//{
//    
//    isLblFromTouch=YES;
//    
//    if (self.tableHeightConstraint.constant !=0) {
//        
//        [UIView animateWithDuration:0.5 animations:^{
//            
//            
//            self.tableHeightConstraint.constant=0;
//            [self.datePickerView needsUpdateConstraints];
//            
//            [self.view setNeedsLayout];
//            [self.view layoutIfNeeded];
//            
//            
//        }];
//        
//    }
//    
//}
//
//- (void)lblToTap:(UIGestureRecognizer*)gestureRecognizer
//{
//    isLblFromTouch=NO;
//    
//    if (self.tableHeightConstraint.constant !=0)
//    {
//        [UIView animateWithDuration:0.5 animations:^{
//            
//            
//            self.tableHeightConstraint.constant=0;
//            [self.datePickerView needsUpdateConstraints];
//            
//            [self.view setNeedsLayout];
//            [self.view layoutIfNeeded];
//            
//            
//        }];
//    }
//    
//}
//- (IBAction)btnExportPDF:(id)sender
//{
//    PDFVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PDFVC"];
//    
//    [self presentViewController:vc animated:YES completion:nil];
//
//}
//
//
//
//- (IBAction)cancelBtnClcik:(id)sender
//{
//    
//    
//    [UIView animateWithDuration:0.5 animations:^{
//        
//        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
//        [self.datePickerView needsUpdateConstraints];
//        [self.view setNeedsLayout];
//        [self.view layoutIfNeeded];
//        
//        
//    }];
//    
//}
//
//- (IBAction)doneBtnClick:(id)sender
//{
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    
//    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//    NSString *dateOfBirth=[dateFormatter stringFromDate:self.datePicker.date];
//    
//    
//    
//    if (isLblFromTouch) {
//        
//        self.lblFrom.text=dateOfBirth;
//    }
//    else
//    {
//        self.lblTo.text=dateOfBirth;
//    }
//    
//    
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSString *savedate=[dateFormatter stringFromDate:self.datePicker.date];
//    
//    if (isLblFromTouch) {
//        
//        self.strFromDate=savedate;
//    }
//    else
//    {
//        self.strToDate=savedate;
//    }
//    
//    
//    
//    
//    [UIView animateWithDuration:0.5 animations:^{
//        
//        
//        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
//        [self.datePickerView needsUpdateConstraints];
//        
//        [self.view setNeedsLayout];
//        [self.view layoutIfNeeded];
//        
//        
//    }];
//    
//    
//}
//
//
//-(IBAction)menuButtonClick:(id)sender
//{
//  // [SVProgressHUD dismiss];
//    [CustmObj removeHud:self.view];
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//-(IBAction)goButtonClick:(id)sender
//{
//    if (self.strFromDate.length>0 && self.strToDate.length>0)
//    {
//        NSDateFormatter *f = [[NSDateFormatter alloc] init];
//        [f setDateFormat:@"yyyy-MM-dd"];
//        NSDate *startDate = [f dateFromString:self.strFromDate];
//        NSDate *endDate = [f dateFromString:self.strToDate];
//        
//        NSTimeInterval timeDifference = [startDate timeIntervalSinceDate:endDate];
//        
//       // NSLog(@"diff %f",timeDifference);
//        
//        if (timeDifference <=0.000000)
//        {
//            [self initialServiceHitData];
//        }
//        else
//        {
//            [SVProgressHUD showErrorWithStatus:@"To date should be greater than From date"];
//        }
//        
//       
//    }
//}
//
//-(IBAction)exportToExcelButtonClick:(id)sender
//{
//  
//}
//
//-(void)initialServiceHitData
//{
//   
//    
//    
//    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:self.strFromDate,@"fromdate",self.strToDate,@"todate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
//    
//    NSError * err;
//    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
//    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
//    
//    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
//    
//    //NSLog(@"dictFinaldata %@",dictFinaldata);
//    
//    //[SVProgressHUD showWithStatus:@"Please wait.."];
//    [CustmObj addHud:self.view];
//    [[Services sharedInstance]serviceCallbyPost:@"html/accountsummary.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
//        
//        // NSLog(@"Response %@",response);
//        
//        if (type == kResponseTypeFail)
//        {
//            // NSLog(@"fail Response:----> %@",response);
//            self.view.userInteractionEnabled=YES;
//            //[SVProgressHUD dismiss];
//            [CustmObj removeHud:self.view];
//            
//            
//            NSError *error=(NSError*)response;
//            
//            if (error.code == -1009) {
//                
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
//                                                                               message:@"The Internet connection appears to be offline."
//                                                                        preferredStyle:UIAlertControllerStyleAlert];
//                
//                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
//                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                                          
//                                                                          // NSLog(@"You pressed button three");
//                                                                      }];
//                
//                [alert addAction:thirdAction];
//                [self presentViewController:alert animated:YES completion:nil];
//            }
//            
//            
//            
//        }
//        else if (type == kresponseTypeSuccess)
//        {
//        
//        NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
//        if ([arrResponsedata objectForKey:@"detail"]>0)
//        {
//            
//            self.arrData=[arrResponsedata objectForKey:@"detail"];
//            
//            if([self.arrData count] >0){
//                [self.myTableView reloadData];
//                [self.myTableView setHidden:NO];
//                self.viewNoData.hidden=YES;
//            }else{
//                self.viewNoData.hidden=NO;
//                [self.myTableView setHidden:YES];
//            }
//            
//            
//        }
//        //[SVProgressHUD dismiss];
//        [CustmObj removeHud:self.view];
//        
//        }
//    }];
//}
//
//-(void)oneTimeServiceHitData
//{
//    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:self.strFromDate,@"fromdate",self.strToDate,@"todate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",@"LASTTRANSACTION",@"lasttransaction",nil];
//    
//    NSError * err;
//    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
//    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
//    
//    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
//    
//   
//    
//    //[SVProgressHUD showWithStatus:@"Please wait.."];
//    [CustmObj addHud:self.view];
//    [[Services sharedInstance]serviceCallbyPost:@"html/accountsummary.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
//        
//       // NSLog(@"Response %@",response);
//        
//        if (type == kResponseTypeFail)
//        {
//            // NSLog(@"fail Response:----> %@",response);
//            self.view.userInteractionEnabled=YES;
//           // [SVProgressHUD dismiss];
//            [CustmObj removeHud:self.view];
//            
//            
//            NSError *error=(NSError*)response;
//            
//            if (error.code == -1009) {
//                
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
//                                                                               message:@"The Internet connection appears to be offline."
//                                                                        preferredStyle:UIAlertControllerStyleAlert];
//                
//                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
//                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                                          
//                                                                          // NSLog(@"You pressed button three");
//                                                                      }];
//                
//                [alert addAction:thirdAction];
//                [self presentViewController:alert animated:YES completion:nil];
//            }
//            
//            
//            
//        }
//        else if (type == kresponseTypeSuccess)
//        {
//            
//            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
//            if ([arrResponsedata objectForKey:@"detail"]>0)
//            {
//                
//                self.arrData=[arrResponsedata objectForKey:@"detail"];
//                
////                  NSLog(@"Response arrdata %@",self.arrData);
//                for (int i = 0; i < [self.arrData count]; i++)
//                {
//                    NSMutableDictionary *dict=[self.arrData objectAtIndex:i];
//                    
//                    if ([dict objectForKey:[NSString stringWithFormat:@"%d",0]]) {
//                        NSString *date = [dict objectForKey:[NSString stringWithFormat:@"%d",0]];
//                        NSLog(@"%@",date);
//                        [self.arrDate addObject:[dict objectForKey:[NSString stringWithFormat:@"%d",0]]];
//
//                    }
//
//                }
//                NSLog(@"%@", self.arrDate);
//
//                [self.myTableView reloadData];
//                
//            }
//            //[SVProgressHUD dismiss];
//            [CustmObj removeHud:self.view];
//            
//        }
//    }];
//}
//
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//#pragma mark - Table View Delegate and datasource
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 2;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    if (section == 0)
//    {
//        return 1;
//    }
//    return [self.arrData count];
//    
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.section == 0)
//    {
//        return 47;
//    }
//    return 47;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (indexPath.section==0)
//    {
//        static NSString *simpleTableIdentifier = @"firstNoBid";
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
//        return cell;
//    }
//    else
//    {
//        static NSString *simpleTableIdentifier = @"ReportScreenCell";
//        ReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
//        
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        
//        
//        
//        NSMutableDictionary *dict=[self.arrData objectAtIndex:indexPath.row];
//        
//        if ([dict objectForKey:[NSString stringWithFormat:@"%d",0]]) {
//            cell.lblDate.text=[dict objectForKey:[NSString stringWithFormat:@"%d",0]];
//        }
//        
//        if ([dict objectForKey:[NSString stringWithFormat:@"%d",1]]) {
//            cell.lblUnitName.text=[dict objectForKey:[NSString stringWithFormat:@"%d",1]];
//        }
//        
//        if ([dict objectForKey:[NSString stringWithFormat:@"%d",6]]) {
//            cell.lblAmntRecievable.text=[NSString stringWithFormat:@"%.02f",[[dict objectForKey:[NSString stringWithFormat:@"%d",6]]floatValue]];
//        }
//        
//        if ([dict objectForKey:[NSString stringWithFormat:@"%d",7]]) {
//            cell.lblAmntyRecieved.text=[NSString stringWithFormat:@"%.02f",[[dict objectForKey:[NSString stringWithFormat:@"%d",7]]floatValue]];
//        }
//        
//        if ([dict objectForKey:[NSString stringWithFormat:@"%d",8]]) {
//            cell.lblbalance.text=[NSString stringWithFormat:@"%.02f",[[dict objectForKey:[NSString stringWithFormat:@"%d",8]]floatValue]];
//        }
//        return cell;
//    }
//}
//
//
//-(IBAction)btnLogout_Click:(id)sender
//{
//    [AppDelegate logout];
//    [self.navigationController popToRootViewControllerAnimated:YES];
//}
//
//@end
