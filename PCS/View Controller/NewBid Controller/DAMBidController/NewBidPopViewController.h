//
//  NewBidPopViewController.h
//  PCS
//
//  Created by lab4code on 14/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserActionDelegate <NSObject>

- (void)serviceHitForBid;

@end

@interface NewBidPopViewController : UIViewController
@property(strong,nonatomic)NSMutableDictionary *dictData;
@property(strong,nonatomic)NSString *strBidType;
@property(strong,nonatomic)NSString *strDate;
@property(strong,nonatomic)NSString *strType;
@property(strong,nonatomic)NSString *Editselected;
@property (weak, nonatomic) id <UserActionDelegate> delegate;
@end
