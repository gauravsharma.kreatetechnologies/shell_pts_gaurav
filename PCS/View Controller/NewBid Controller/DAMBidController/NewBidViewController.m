//
//  NewBidViewController.m
//  PCS
//

//  Created by lab4code on 14/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "NewBidViewController.h"
#import "NoBidTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "NewBidPopViewController.h"
#import "Copy Bid Area.h"
#import "BidsummaryViewController.h"
#import "CustmObj.h"
#import "AppDelegate.h"


@interface NewBidViewController ()<UIActionSheetDelegate,UserActionDelegate>
{
    
#pragma mark DAM button boolean Value.
    
    BOOL isFutureDateSelected;
    BOOL blockBidChecked;
    BOOL isSubmitedButton;
    BOOL isViewAppearCall;
    
    NSDateFormatter *dateFormatter;
    UITapGestureRecognizer *dobTap;
    
    int whichBidButtonChecked;
    int whichFutureDateButtonChecked;
    UIToolbar *numberToolbar;
}

#pragma mark DAM button Property
@property (weak, nonatomic) IBOutlet UIImageView *Datelblimage;
@property(weak,nonatomic)IBOutlet UILabel *lblTradingdate;

@property(weak,nonatomic)IBOutlet UILabel *lblSingleOrBlockText;
@property (weak, nonatomic) IBOutlet UIImageView *labelborderimg;
@property (weak, nonatomic) IBOutlet UIButton *NewBidDAM;

@property(weak,nonatomic)IBOutlet UILabel *lblCompanyName;
@property(weak,nonatomic)IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

@property(weak,nonatomic)IBOutlet UIButton* btnIEX;
@property(weak,nonatomic)IBOutlet UIButton* btnPXIL;
@property(weak,nonatomic)IBOutlet UIButton* btnBlock;
@property(weak,nonatomic)IBOutlet UIButton* btnSingle;
@property(weak,nonatomic)IBOutlet UIButton* btnSubmitted;
@property(weak,nonatomic)IBOutlet UIButton* btnFutureDate;
@property(weak,nonatomic)IBOutlet UIButton* btnTodayDate;

@property(strong,nonatomic)NSMutableArray *arrData;
@property(strong,nonatomic)NSString *strSaveDate;
@end


@implementation NewBidViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (_dictEitcontainerValue.count >=1)
    {
        _buttonBidsummary.hidden = YES;
        _buttonCopy.hidden = YES;
    }

#pragma mark HIdden containt
    _Datelblimage.hidden = YES;
    _lblTradingdate.hidden =YES;
    
    _lblSingleOrBlockText.hidden=YES;
    _labelborderimg.hidden=YES;
    _NewBidDAM.hidden=YES;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    whichBidButtonChecked=0;
    whichFutureDateButtonChecked=0;
    
    _myTableView.hidden = YES;
    
    
    if (_selectionType.length > 0)
    {    _whichIEXButtonChecked = 1;
    
        if ([_selectionType isEqualToString:@"IEX"])
        {
            _iexchecked =YES;
        }
        else
        {
            _iexchecked = NO;
        }
    }
    
#pragma mark edit button call from BidSummary.
    
    if (_dictEitcontainerValue > 0)
    {
        _whichIEXButtonChecked = 1;
        whichBidButtonChecked =1;
        whichFutureDateButtonChecked =1;
        _lblTradingdate.hidden =NO;
        _Datelblimage.hidden = NO;
        
        if ([[_dictEitcontainerValue objectForKey:@"Exchange"] isEqualToString:@"IEX"])
        {
            _iexchecked =YES;
        }
        else
        {
            _iexchecked = NO;
        }
        
        if ([[_dictEitcontainerValue objectForKey:@"bidType"] isEqualToString:@"Block Bid"])
        {
            
            blockBidChecked=NO;
        }
        else
        {
            blockBidChecked=YES;
        }
        
        [self blockBidButtonClick:nil];
        
        NSDate *todayDate = [NSDate date];
        
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];
        
        
        if ([[_dictEitcontainerValue objectForKey:@"date"] isEqualToString:convertedDateString])
        {
            [self.btnFutureDate setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.btnTodayDate setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            
            _strSaveDate =[NSString stringWithFormat:@"%@", convertedDateString];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormatter dateFromString: convertedDateString];
            dateFormatter = [[NSDateFormatter alloc] init] ;
            [dateFormatter setDateFormat:@"dd/MM/yyyy"];
            NSString *convertedString = [dateFormatter stringFromDate:date];
            
            self.lblTradingdate.text =convertedString;
        }
        else
        {
            [self.btnFutureDate setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.btnTodayDate setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            
            _strSaveDate =[_dictEitcontainerValue objectForKey:@"date"];
            
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormatter dateFromString: [_dictEitcontainerValue objectForKey:@"date"]];
            dateFormatter = [[NSDateFormatter alloc] init] ;
            [dateFormatter setDateFormat:@"dd/MM/yyyy"];
            NSString *convertedString = [dateFormatter stringFromDate:date];
            
            self.lblTradingdate.text =convertedString;
            
        }
        
        
        [self goButtonClick:nil];
    }
    
    
    
    

    
    
    
#pragma mark REC DateObject Memmory Alloc.
    
    if ([AppHelper userDefaultsForKey:@"companyName"]) {
        self.lblCompanyName.text=[AppHelper userDefaultsForKey:@"companyName"];
    }
    
    isViewAppearCall=NO;
    
    
    if (_whichIEXButtonChecked > 0)
    {
        if (_iexchecked) {
            
            [self.btnIEX setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.btnPXIL setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            
        }
        else
        {
            [self.btnIEX setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.btnPXIL setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            
        }
    }
    else
    {
        [self.btnPXIL setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        [self.btnIEX setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
    }
    
    self.btnSubmitted.hidden=YES;
    self.lblSingleOrBlockText.text=@"BLOCK BID DETAILS:";
    self.datePicker.datePickerMode=UIDatePickerModeDate;
    self.datePicker.minimumDate=[NSDate date];
    self.tableHeightConstraint.constant= - (self.datePickerView.frame.size.height+80);
    [self.datePickerView needsUpdateConstraints];
    
    
    dobTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dobTap:)];
    dobTap.numberOfTapsRequired=1;
    [self.lblTradingdate addGestureRecognizer:dobTap];
    dobTap.enabled=NO;
    
    
    
//    
//    if (<#condition#>)
//    
//    {
//        _iexchecked=NO;
//        
//        
//        [self.btnIEX setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
//        [self.btnPXIL setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
//    
//    }
//    else if ()
//    {
//        _iexchecked=YES;
//        [self.btnIEX setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
//        [self.btnPXIL setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
//    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (isViewAppearCall) {
        [self initialServiceHitData];
    }
    
}

- (void)serviceHitForBid
{
    isViewAppearCall=YES;
}


#pragma mark IBAction Methods

-(IBAction)menuButtonClick:(id)sender
{
    
    if (isSubmitedButton) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PTS"
                                                                       message:@"Are you sure want to leave without submitting bid?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  
                                                                  
                                                                  //[SVProgressHUD dismiss];
                                                                  [CustmObj removeHud:self.view];
                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                                  
                                                              }];
        
        UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                    
                                                                    // NSLog(@"You pressed button three");
                                                                }];
        
        [alert addAction:thirdAction];
        [alert addAction:thirdActionNO];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
       // [SVProgressHUD dismiss];
        [CustmObj removeHud:self.view];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

-(IBAction)goButtonClick:(id)sender
{
    
    isViewAppearCall=NO;
    
    UIAlertController *alert;
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { }];
    [alert addAction:firstAction];
    
    
    if (whichBidButtonChecked == 0 && whichFutureDateButtonChecked == 0 && _whichIEXButtonChecked == 0)
    {
        alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please fill all details." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:firstAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if(_whichIEXButtonChecked==0){
        alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please select exchange." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:firstAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if(whichBidButtonChecked==0){
        alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please select bid." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:firstAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if(whichFutureDateButtonChecked==0){
        alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please select trading date." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:firstAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    
    if (whichBidButtonChecked > 0 && whichFutureDateButtonChecked > 0 && _whichIEXButtonChecked > 0)
    {
        [self initialServiceHitData];
        
        _lblSingleOrBlockText.hidden=NO;
        _labelborderimg.hidden=NO;
        _NewBidDAM.hidden=NO;
    }
    
}


-(IBAction)iexButtonClick:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int tag=(int)[btn tag];
    
    
    if (_whichIEXButtonChecked > 0) {
        
        _whichIEXButtonChecked=1;
        if (_iexchecked) {
            _iexchecked=NO;
            
            [self.btnIEX setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.btnPXIL setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }
        else
        {
            _iexchecked=YES;
            [self.btnIEX setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.btnPXIL setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        }
    }
    else
    {
        _whichIEXButtonChecked=1;
        
        if (tag == 1)
        {
            _iexchecked=YES;
            [self.btnIEX setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }
        else
        {
            _iexchecked=NO;
            [self.btnPXIL  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }
    }
    
    
    
}

-(IBAction)blockBidButtonClick:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int tag=(int)[btn tag];
    
    if (whichBidButtonChecked > 0)
    {
        whichBidButtonChecked=1;
        if (blockBidChecked) {
            
            blockBidChecked=NO;
            
            self.lblSingleOrBlockText.text=@"SINGLE BID DETAILS:";
            
            [self.btnBlock setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.btnSingle setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }
        else
        {
            blockBidChecked=YES;
            self.lblSingleOrBlockText.text=@"BLOCK BID DETAILS:";
            
            [self.btnBlock  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.btnSingle setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        }
    }
    else
    {
        whichBidButtonChecked=1;
        
        if (tag == 1)
        {
            blockBidChecked=YES;
            
            self.lblSingleOrBlockText.text=@"BLOCK BID DETAILS:";
            [self.btnBlock setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }
        else
        {
            blockBidChecked=NO;
            self.lblSingleOrBlockText.text=@"SINGLE BID DETAILS:";
            [self.btnSingle  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            
        }}}

-(IBAction)futureButtonClick:(id)sender
{
    _Datelblimage.hidden = NO;
    _lblTradingdate.hidden =NO;
    
    
    UIButton *btn=(UIButton *)sender;
    int tag=(int)[btn tag];
    
    if (whichFutureDateButtonChecked > 0)
    {
        whichFutureDateButtonChecked=1;
        
        if (isFutureDateSelected) {
            isFutureDateSelected=NO;
            
            dobTap.enabled=NO;
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *savedate=[dateFormatter stringFromDate:[NSDate date]];
            self.strSaveDate=savedate;
            
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            NSString *dateOfBirth=[dateFormatter stringFromDate:[NSDate date]];
            self.lblTradingdate.text=dateOfBirth;
            
            [self.btnFutureDate setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.btnTodayDate setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }
        else
        {
            isFutureDateSelected=YES;
            
            dobTap.enabled=YES;
            
            
            NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
            [deltaComps setDay:1];
            NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
            NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc]init];
            [dateFormat1 setDateFormat:@"dd-MM-yyyy"];
            
            self.lblTradingdate.text = [dateFormat1 stringFromDate:tomorrow];
            
            [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
            NSString *savedate=[dateFormatter stringFromDate:tomorrow];
            self.strSaveDate=savedate;
          //  NSLog(@"else : %@",savedate);
            
            // self.lblTradingdate.text=@"SELECT TRADING DATE";
            
            
            
            [self.btnFutureDate  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.btnTodayDate setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        }
    }
    else
    {
        whichFutureDateButtonChecked=1;
        
        if (tag == 1)
        {
            isFutureDateSelected=YES;
            dobTap.enabled=YES;
            
            NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
            [deltaComps setDay:1];
            NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
            NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc]init];
            [dateFormat1 setDateFormat:@"dd-MM-yyyy"];
            
            self.lblTradingdate.text = [dateFormat1 stringFromDate:tomorrow];
            
            [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
            NSString *savedate=[dateFormat1 stringFromDate:tomorrow];
            self.strSaveDate=savedate;
            
            // self.lblTradingdate.text=@"SELECT TRADING DATE";
            
            //  NSLog(@"else else : %@",savedate);
            
            [self.btnFutureDate  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }
        else
        {
            isFutureDateSelected=NO;
            dobTap.enabled=NO;
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *savedate=[dateFormatter stringFromDate:[NSDate date]];
            self.strSaveDate=savedate;
            
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            NSString *dateOfBirth=[dateFormatter stringFromDate:[NSDate date]];
            self.lblTradingdate.text=dateOfBirth;
            
            
            [self.btnTodayDate setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        }}}



-(IBAction)newBidButtonClick:(id)sender
{
    
    if (whichBidButtonChecked > 0 && whichFutureDateButtonChecked > 0 && _whichIEXButtonChecked > 0)
    {
        NSString *strBlock;//=@"BLOCK";
        NSString *strType;//=@"IEX";
        
        if (blockBidChecked) {
            
            strBlock=@"BLOCK";
        }
        else
        {
            strBlock=@"SINGLE";
        }
        
        if (_iexchecked) {
            strType=@"IEX";
        }
        else
        {
            strType=@"PXIL";
        }
        
        
        isViewAppearCall=NO;
        NewBidPopViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"newbidpop"];
        
        newView.strBidType=strBlock;
        newView.strDate=self.strSaveDate;
        newView.strType=strType;
        newView.delegate=self;
        
        [self.navigationController pushViewController:newView animated:NO];
    }
    else
    {
        UIAlertController *alert;// = [UIAlertController alertControllerWithTitle:@"" message:@"Please select trading date" preferredStyle:UIAlertControllerStyleAlert];
        
        if(_whichIEXButtonChecked == 0)
        {
            alert = [UIAlertController alertControllerWithTitle:@""
                                                        message:@"Please select exchange type"
                                                 preferredStyle:UIAlertControllerStyleAlert];
        }else if (whichBidButtonChecked == 0)
        {
            alert = [UIAlertController alertControllerWithTitle:@""
                                                        message:@"Please select bid"
                                                 preferredStyle:UIAlertControllerStyleAlert];
        }else if (whichFutureDateButtonChecked == 0)
        {
            alert = [UIAlertController alertControllerWithTitle:@""
                                                        message:@"Please select trading date"
                                                 preferredStyle:UIAlertControllerStyleAlert];
        }
        
        
        
        
        
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  // NSLog(@"You pressed button one");
                                                                  
                                                                  
                                                                  
                                                              }];
        
        
        
        
        
        
        
        [alert addAction:firstAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}



-(IBAction)cancelBidButtonClick:(id)sender
{
    if (isSubmitedButton)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PTS"
                                                                       message:@"Are you sure want to Submit all bid?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  // NSLog(@"You pressed button three");
                                                                  
                                                                  [self submitServiceHit];
                                                                  
                                                              }];
        
        UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                    
                                                                    // NSLog(@"You pressed button three");
                                                                }];
        
        [alert addAction:thirdAction];
        [alert addAction:thirdActionNO];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PTS"
                                                                       message:@"Are you sure want to cancel all bid?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  // NSLog(@"You pressed button three");
                                                                  
                                                                  [self cancelAllBid];
                                                                  
                                                              }];
        
        UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                    
                                                                    // NSLog(@"You pressed button three");
                                                                }];
        
        [alert addAction:thirdAction];
        [alert addAction:thirdActionNO];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
}
-(IBAction)btnBackAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnLogoutAction:(id)sender{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Service Hit

-(void)initialServiceHitData
{
    NSString *strBlock=nil;
    NSString *strType=@"IEX";
    
    if (blockBidChecked) {
        
        strBlock=@"BLOCK";
    }
    else
    {
        strBlock=@"SINGLE";
    }
    
    if (_iexchecked) {
        strType=@"IEX";
    }
    else
    {
        strType=@"PXIL";
    }
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strType,@"type",strBlock,@"bidtype",self.strSaveDate,@"biddate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    //  NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/getbiddetail.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        //  NSLog(@"Response %@",response);
        
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            self.view.userInteractionEnabled=YES;
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableArray *arrResponsedata=(NSMutableArray *)response;
            // NSLog(@"%@",arrResponsedata);
            if ([arrResponsedata count]>0)
            {
                NSMutableDictionary *dictt=[arrResponsedata objectAtIndex:0];
                if ([dictt objectForKey:@"biddetail"])
                {
                    self.arrData=[[dictt objectForKey:@"biddetail"] mutableCopy];
                    //NSLog(@"%@",self.arrData);
                    
                }
                
                if ([self.arrData count]>0)
                {
                    
                    [self.myTableView setHidden:NO];
                    self.btnSubmitted.hidden=NO;
                    [self.myTableView reloadData];
                    
                    if ([[dictt objectForKey:@"submitstatus"] isEqualToString:@"FALSE"])
                    {
                        self->isSubmitedButton=NO;
                        [self.btnSubmitted setImage:[UIImage imageNamed:@"CancelBidButton"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        self->isSubmitedButton=YES;
                        // [self.btnSubmitted setAlpha:0.0f];
                        [self.btnSubmitted setImage:[UIImage imageNamed:@"SubmitBid"] forState:UIControlStateNormal];
                        
                        
                        //                    if (self.timer)
                        //                    {
                        //                        [self.timer invalidate];
                        //                        self.timer=nil;
                        //                    }
                        //                   self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(calculateTimeForBid:) userInfo:nil repeats:YES];
                        
                        
                    }
                    // [SVProgressHUD dismiss];
                    [CustmObj removeHud:self.view];
                }
                else
                {
                    [self.myTableView setHidden:YES];
                    self.btnSubmitted.hidden=YES;
                }
                
                
                // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                
                
            }
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
        }
        
    }];
}

-(void)submitServiceHit
{
    NSString *strBlock=nil;
    NSString *strType=@"IEX";
    
    if (blockBidChecked) {
        
        strBlock=@"BLOCK";
    }
    else
    {
        strBlock=@"SINGLE";
    }
    
    if (_iexchecked) {
        strType=@"IEX";
    }
    else
    {
        strType=@"PXIL";
    }
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strType,@"type",strBlock,@"bidtype",self.strSaveDate,@"biddate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    // NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/submitbid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        //  NSLog(@"Response %@",response);
        
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            self.view.userInteractionEnabled=YES;
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if (arrResponsedata)
            {
                
                if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                {
                    [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"message"] ];
                    
                    self->isSubmitedButton=NO;
                    [self.btnSubmitted setImage:[UIImage imageNamed:@"CancelBidButton"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"message"]];
                }
                
                
            }
            else
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
            
        }
        
        
    }];
}

-(void)deleteBid:(NSMutableDictionary *)dictBid index:(int)tag
{
    NSString *strBlock=nil;
    NSString *strType=@"IEX";
    
    if (blockBidChecked) {
        
        strBlock=@"BLOCK";
    }
    else
    {
        strBlock=@"SINGLE";
    }
    
    if (_iexchecked) {
        strType=@"IEX";
    }
    else
    {
        strType=@"PXIL";
    }
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictBid objectForKey:@"id"],@"id",strType,@"type",strBlock,@"bidtype",[dictBid objectForKey:@"date"],@"biddate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    //  NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/deletebid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        //  NSLog(@"Response %@",response);
        
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            self.view.userInteractionEnabled=YES;
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if (arrResponsedata)
            {
                
                if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                {
                    if ([self.arrData count]>tag) {
                        
                        [self.arrData removeObjectAtIndex:tag];
                        if([self.arrData count]>0)
                        {
                        self.btnSubmitted.hidden=NO;
                        [self.myTableView setHidden:NO];
                        [self.myTableView reloadData];
                        }
                        
                        else
                        {
                           [self.myTableView setHidden:YES];
                            self.btnSubmitted.hidden=YES;
                        }
                        
                    }
                    
                    
                    [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"message"] ];
                    
                    
                    
                }
                else
                {
                    [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"message"]];
                }
                
                
            }
            else
            {
                //[SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
        }
        
    }];
}

-(void)cancelAllBid
{
    NSString *strBlock=nil;
    NSString *strType=@"IEX";
    
    if (blockBidChecked) {
        
        strBlock=@"BLOCK";
    }
    else
    {
        strBlock=@"SINGLE";
    }
    
    if (_iexchecked) {
        strType=@"IEX";
    }
    else
    {
        strType=@"PXIL";
    }
    
    NSString *striDs=@"";
    
    for (NSMutableDictionary *dictd in self.arrData)
    {
        
        striDs=[striDs stringByAppendingFormat:@"%@,",[dictd objectForKey:@"id"]];
    }
    
    if ([striDs length]>0)
    {
        striDs=[striDs substringToIndex:[striDs length]-1];
    }
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:striDs,@"id",strType,@"type",strBlock,@"bidtype",self.strSaveDate,@"biddate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
     NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/deletebid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        //  NSLog(@"Response %@",response);
        
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            self.view.userInteractionEnabled=YES;
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if (arrResponsedata)
            {
                
                if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                {
                    if ([self.arrData count]>0) {
                        
                        [self.arrData removeAllObjects];
                        [self.myTableView reloadData];
                        [self.myTableView setHidden:YES];
                        self.btnSubmitted.hidden=YES;
                    }
                    
                    
                    [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"message"] ];
                    
                    
                    
                }
                else
                {
                    [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"message"]];
                }
                
                
            }
            else
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
        }
        
    }];
}

#pragma mark - Timer
-(void)calculateTimeForBid:(NSTimer *)theTimer
{
    if (isSubmitedButton)
    {
        
        
        //fade in
        [UIView animateWithDuration:2.0f animations:^{
            
            [self.btnSubmitted setAlpha:1.0f];
            
        } completion:^(BOOL finished) {
            
            //fade out
            [UIView animateWithDuration:2.0f animations:^{
                
                [self.btnSubmitted setAlpha:0.0f];
                
            } completion:nil];
            
        }];
    }
    else
    {
        
    }
}

#pragma mark - date tap gesture

- (void)dobTap:(UIGestureRecognizer*)gestureRecognizer
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=0;
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
    
}

- (IBAction)cancelBtnClcik:(id)sender
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
}

- (IBAction)doneBtnClick:(id)sender
{
    
    
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *dateOfBirth=[dateFormatter stringFromDate:self.datePicker.date];
    self.lblTradingdate.text=dateOfBirth;
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *savedate=[dateFormatter stringFromDate:self.datePicker.date];
    self.strSaveDate=savedate;
    
    
    isFutureDateSelected=YES;
    
    dobTap.enabled=YES;
    
    [self.btnFutureDate  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
    [self.btnTodayDate setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant = - (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
    
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (section == 0)
    {
        return 1;
    }
    
    else
    {
        return [self.arrData count];
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==self.myTableView)
        
    {
        
        if (indexPath.section == 0)
        {
            return 30;
        }else{
            return 42;
        }
        
        
    }
    
    else
    {
        return YES;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        static NSString *simpleTableIdentifier = @"firstNoBid";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"NoBidScreenCell";
        NoBidTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        NSMutableDictionary *dict=[self.arrData objectAtIndex:indexPath.row];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblSerialNo.text=[NSString stringWithFormat:@"%d",(int)indexPath.row+1];
        
        cell.lblBid.text=[NSString stringWithFormat:@"%.1f",[[dict objectForKey:@"bid"]floatValue]];
        
        cell.lblDateTime.text=[NSString stringWithFormat:@"%@-%@",[dict objectForKey:@"blockfrom"],[dict objectForKey:@"blockto"]];
        cell.lblPrice.text=[NSString stringWithFormat:@"%d",[[dict objectForKey:@"price"]intValue]];
        
        cell.btnCross.tag=indexPath.row;
        [cell.btnCross addTarget:self action:@selector(crossButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
        
    }
    
    
}


#pragma mark- TAM REC BTN

-(void)crossButtonClick:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int tag=(int)btn.tag;
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Bid"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Edit"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        // NSLog(@"You pressed button one");
        
        NSString *strBlock=@"BLOCK";
        NSString *strType=@"IEX";
        
        if (self->blockBidChecked) {
            
            strBlock=@"BLOCK";
        }
        else
        {
            strBlock=@"SINGLE";
        }
        
        if (self->_iexchecked) {
            strType=@"IEX";
        }
        else
        {
            strType=@"PXIL";
        }
        
        
        self->isViewAppearCall=NO;
        NewBidPopViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"newbidpop"];
        newView.strBidType=strBlock;
        newView.strDate=self.strSaveDate;
        newView.strType=strType;
        newView.delegate=self;
        NSMutableDictionary *dictt=[self.arrData objectAtIndex:tag];
        newView.dictData=dictt;
        
        [self.navigationController pushViewController:newView animated:YES];
        
        newView.Editselected=@"yes";
        // [SVProgressHUD dismiss];
        [CustmObj removeHud:self.view];
        
    }];
    
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Delete"
                                                           style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        
        //  NSLog(@"You pressed button two");
        
        NSMutableDictionary *dictt=[self.arrData objectAtIndex:tag];
        [self deleteBid:dictt index:tag];
        // [SVProgressHUD dismiss];
        [CustmObj removeHud:self.view];
        
    }];
    
    
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        
        // NSLog(@"You pressed button three");
    }];
    
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:thirdAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


- (IBAction)CopyBidArea:(id)sender {
    
    Copy_Bid_Area *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CopyBid"];
    [self.navigationController pushViewController:newView animated:YES];
    
}


- (IBAction)Bidsummery:(id)sender {
    
    BidsummaryViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"bidsummary"];
    [self.navigationController pushViewController:newView animated:YES];
    
}



@end
