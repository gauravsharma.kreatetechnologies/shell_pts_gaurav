//
//  NewBidPopViewController.m
//  PCS
//
//  Created by lab4code on 14/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "NewBidPopViewController.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "CustmObj.h"


@interface NewBidPopViewController ()
{
    BOOL isLblFromTouch;
    BOOL isLblTOTouch;
    BOOL isBuySelected;
    BOOL isHourPicker;
    
    UITextField *activeField;
    
    UIToolbar* numberToolbar;
    
    NSMutableArray *pickerHourArray;
    NSMutableArray *pickerMinuteArray;
    NSString *bidQtm;
    NSString *buyStr;
    
}

@property(weak,nonatomic)IBOutlet UILabel *lblFromTime;
@property(weak,nonatomic)IBOutlet UILabel *lblToTime;
@property(weak,nonatomic)IBOutlet UILabel *lblFromTimeTo;
@property(weak,nonatomic)IBOutlet UILabel *lblToTimeTo;
@property(weak,nonatomic)IBOutlet UITextField *txtRupees;
@property(weak,nonatomic)IBOutlet UITextField *txtBid;
@property(weak,nonatomic)IBOutlet UITextField *txtBlock;

@property(weak,nonatomic)IBOutlet UIButton *btnBuy;
@property(weak,nonatomic)IBOutlet UIButton *btnSell;
@property(weak,nonatomic)IBOutlet UIButton *btnSaveOrUpdate;

@property(weak,nonatomic)IBOutlet UIScrollView *myScrollView;
@property(weak,nonatomic)IBOutlet UIView *containerView;

@property(nonatomic,weak)IBOutlet NSLayoutConstraint *constrainViewWidth;

@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *hourPicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;



@property (weak, nonatomic) IBOutlet UILabel *LblNoofBlock;
@property (weak, nonatomic) IBOutlet UIImageView *imgBlock;
@property (weak, nonatomic) IBOutlet UIImageView *lblblockExmp;



-(IBAction)savBidButtonClick:(id)sender;

-(IBAction)buyBtnClick:(id)sender;

@end

@implementation NewBidPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //  isBuySelected=YES;
    
    _txtBlock.text = @"1";
    
    self.tableHeightConstraint.constant= - (185+80);
    [self.datePickerView needsUpdateConstraints];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    _constrainViewWidth.constant=self.view.frame.size.width;
    self.myScrollView.contentSize=CGSizeMake(self.containerView.frame.size.width, self.containerView.frame.size.height);
    
    pickerHourArray=[[NSMutableArray alloc]initWithObjects:@"00",@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24", nil];
    
    pickerMinuteArray=[[NSMutableArray alloc]initWithObjects:@"00",@"15",@"30",@"45",nil];
    
    
    UITapGestureRecognizer *dobTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblFromTap:)];
    dobTap.numberOfTapsRequired=1;
    [self.lblFromTime addGestureRecognizer:dobTap];
    
    UITapGestureRecognizer *lblToTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblToTap:)];
    lblToTap.numberOfTapsRequired=1;
    [self.lblToTime addGestureRecognizer:lblToTap];
    
    
    UITapGestureRecognizer *lblFromTimeToTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblFromTimeToTap:)];
    lblFromTimeToTap.numberOfTapsRequired=1;
    [self.lblFromTimeTo addGestureRecognizer:lblFromTimeToTap];
    
    UITapGestureRecognizer *lblToTapTo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblToTapTo:)];
    lblToTapTo.numberOfTapsRequired=1;
    [self.lblToTimeTo addGestureRecognizer:lblToTapTo];
    
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(touchPadDown)],
                           nil];
    [numberToolbar sizeToFit];
    self.txtRupees.inputAccessoryView = numberToolbar;
    self.txtBid.inputAccessoryView = numberToolbar;
    self.txtBlock.inputAccessoryView = numberToolbar;
    //    self.txtBlock.text=@"1";
    
    if (self.dictData)
    {
        [self.btnSaveOrUpdate setImage:[UIImage imageNamed:@"updateBtn"] forState:UIControlStateNormal];
        // [self.btnSaveOrUpdate setBackgroundImage:[UIImage imageNamed:@"updateBtn"] forState:UIControlStateNormal];
        
        //{"id":"269822","date":"2015-10-20","price":"4000","bid":"8","blockfrom":"00:00","blockto":"12:00","status":"FALSE"},
        
        
        self.txtBlock.userInteractionEnabled=NO;
        
        if ([self.dictData objectForKey:@"price"])
        {
            //self.txtRupees.text=[NSString stringWithFormat:@"%d",[[self.dictData objectForKey:@"price"]intValue]];
            self.txtRupees.text=[self.dictData objectForKey:@"price"];
        }
        
        //
        //        if ([self.dictData objectForKey:@"price"])
        //        {
        //            //self.txtRupees.text=[NSString stringWithFormat:@"%d",[[self.dictData objectForKey:@"price"]intValue]];
        //            self.txtRupees.text=[self.dictData objectForKey:@"price"];
        //        }
        
        
        
        if ([self.dictData objectForKey:@"bid"])
        {
            
            bidQtm = [NSString stringWithFormat:@"%.1f",[[self.dictData objectForKey:@"bid"]floatValue]];
            
            bidQtm = [bidQtm substringFromIndex:1];
            //              NSLog(@"msg %@",bidQtm);
            
            
            if ([[self.dictData objectForKey:@"bid"]floatValue]<0)
            {
                
                isBuySelected=NO;
                [self.btnBuy setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
                [self.btnSell setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
                self.txtBid.text=bidQtm;
                
            
                buyStr=@"SELL";
                
                
            }
            else
            {
                self.txtBid.text=[NSString stringWithFormat:@"%.1f",[[self.dictData objectForKey:@"bid"]floatValue]];
                
                   isBuySelected=YES;
                [self.btnBuy setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
                [self.btnSell setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
                
                    buyStr=@"BUY";
            }
            
            
        }
        if ([self.dictData objectForKey:@"blockfrom"])
        {
            
            NSArray *items = [[self.dictData objectForKey:@"blockfrom"] componentsSeparatedByString:@":"];
            
            if ([items count]>0)
            {
                self.lblFromTime.text=[items objectAtIndex:0];
            }
            if ([items count]>1) {
                self.lblFromTimeTo.text=[items objectAtIndex:1];
            }
            
            
        }
        if ([self.dictData objectForKey:@"blockto"])
        {
            
            
            NSArray *items = [[self.dictData objectForKey:@"blockto"] componentsSeparatedByString:@":"];
            
            if ([items count]>0)
            {
                self.lblToTime.text=[items objectAtIndex:0];
            }
            if ([items count]>1) {
                self.lblToTimeTo.text=[items objectAtIndex:1];
            }
        }
        
    }
    else
    {
        self.txtBlock.userInteractionEnabled=YES;
    }
    
    // NSLog(@"type bid and date %@ %@ %@",self.strType,self.strBidType,self.strDate);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    if ([_Editselected isEqualToString:@"yes"])
    {
        _LblNoofBlock.hidden=YES;
        _imgBlock.hidden=YES;
        _lblblockExmp.hidden=YES;
        _txtBlock.hidden=YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}

-(void)touchPadDown
{
    [activeField resignFirstResponder];
    
    
    
    //    if (activeField == self.txtRupees)
    //    {
    //        [self.txtBid becomeFirstResponder];
    //    }
    //    else if (activeField == self.txtBid)
    //    {
    //        [self.txtBlock becomeFirstResponder];
    //    }
    //    else
    //    {
    //        [activeField resignFirstResponder];
    //
    //    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)buyBtnClick:(id)sender
{
    if (isBuySelected) {
        isBuySelected=NO;
        self.btnBuy.selected = FALSE;
        self.btnSell.selected = TRUE;
        [self.btnBuy setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        [self.btnSell setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
    }
    else
    {
        self.btnBuy.selected = TRUE;
        self.btnSell.selected = FALSE;
        isBuySelected=YES;
        [self.btnBuy  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        [self.btnSell setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
    }
    
}

-(IBAction)CancelBtnpopclick:(id)sender
{
    //[SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)savBidButtonClick:(id)sender
{
   
    
    if(self.btnBuy.selected){
        buyStr=@"BUY";
    }else if(self.btnSell.selected){
        buyStr=@"SELL";
    }
    
    //Checking if all value are not filled
    if(buyStr == NULL && [self.lblFromTime.text isEqualToString:@""] && [self.lblFromTime.text isEqualToString:@""] && [self.lblToTime.text isEqualToString:@""] && [self.txtRupees.text isEqualToString:@""] && [self.txtBid.text isEqualToString:@""] && [self.txtBlock.text isEqualToString:@""])
    {
        [SVProgressHUD showInfoWithStatus:@"Please fill all details."];
        return;
    }
    
    //Checking if bid type is selected
    if(buyStr == NULL){
        [SVProgressHUD showInfoWithStatus:@"Please select transaction type."];
        return;
    }
  //  NSLog(@"ttype : %@",buyStr);
    
    //Checking if slot to time selected
    if([self.lblFromTime.text isEqualToString:@""]){
        [SVProgressHUD showInfoWithStatus:@"Plese fill the slot from HH."];
        return;
    }
    
    //Checking if slot from time selected
    if([self.lblToTime.text isEqualToString:@""]){
        [SVProgressHUD showInfoWithStatus:@"Plese fill the slot to HH."];
        return;
    }
    
    //Checking if price is filled
    if([self.txtRupees.text isEqualToString:@""]){
        [SVProgressHUD showInfoWithStatus:@"Please fill price."];
        return;
    }else if ([self.txtRupees.text integerValue] <= 0 || [self.txtRupees.text integerValue] > 20000) {
        //Checking if price is within range
        [SVProgressHUD showInfoWithStatus:@"Price should be less than 20,000 and greater than 0"];
        return;
    }
  //  NSLog(@"price : %@",self.txtRupees.text);
    
    //Checking if bid is filled
    if([self.txtBid.text isEqualToString:@""]){
        [SVProgressHUD showInfoWithStatus:@"Please fill bid."];
        return;
    }
    else if ([self.txtBid.text floatValue] <= 0.0) {
        //Checking if bid is greater than 0
        [SVProgressHUD showInfoWithStatus:@"Bid must be greater than 0"];
        return;
    }
  //  NSLog(@"bid : %@",self.txtBid.text);
    
    //Checking if no of block is filled
//    if([self.txtBlock.text isEqualToString:@""]){
//        [SVProgressHUD showInfoWithStatus:@"Please fill no of block."];
//        return;
// if ([self.txtBlock.text integerValue] <= 0) {
//        //Checking if no of block is greater than 0
//        [SVProgressHUD showInfoWithStatus:@"No of block must be greater than 0."];
//        return;
//    }
  //  NSLog(@"block : %@",self.txtBlock.text);
    
   // return;
    
    
    
    
    if (self.dictData)
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:self.strBidType,@"bidtype",buyStr,@"btype",[NSString stringWithFormat:@"%@:%@",self.lblFromTime.text,self.lblFromTimeTo.text],@"blockfrom",[NSString stringWithFormat:@"%@:%@",self.lblToTime.text,self.lblToTimeTo.text],@"blockto",self.txtRupees.text,@"price",self.txtBid.text,@"bid",self.txtBlock.text,@"noofblock",self.strType,@"type",[self.dictData objectForKey:@"date"],@"biddate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",[self.dictData objectForKey:@"id"],@"where", nil];
        
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
        
        
        NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
        
//         NSLog(@"dictData %@",self.dictData);
        
       NSLog(@"dict %@",dict);
        
        //[SVProgressHUD showWithStatus:@"Please wait.."];
        [CustmObj addHud:self.view];
        
        [[Services sharedInstance]serviceCallbyPost:@"service/newbid/updateclientbid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
            
            
            
            NSLog(@"Response %@",response);
            
            if (type == kResponseTypeFail)
            {
                // NSLog(@"fail Response:----> %@",response);
                self.view.userInteractionEnabled=YES;
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                
                
                NSError *error=(NSError*)response;
                
                if (error.code == -1009) {
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                   message:@"The Internet connection appears to be offline."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                              
                                                                              // NSLog(@"You pressed button three");
                                                                          }];
                    
                    [alert addAction:thirdAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
                
                
            }
            else if (type == kresponseTypeSuccess)
            {
                
                
                NSMutableDictionary *dict=(NSMutableDictionary *)response;
                
                if ([[dict objectForKey:@"status"] isEqualToString:@"ERR"])
                {
                    [SVProgressHUD showErrorWithStatus:[dict objectForKey:@"message"]];
                }
                else
                {
                    [SVProgressHUD showSuccessWithStatus:[dict objectForKey:@"message"]];
                    [self.delegate serviceHitForBid];
                }
                
                
                
                [self.navigationController popViewControllerAnimated:YES];
                
                // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
        }];
    }
    else
    {
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:self.strBidType,@"bidtype",buyStr,@"btype",[NSString stringWithFormat:@"%@:%@",self.lblFromTime.text,self.lblFromTimeTo.text],@"blockfrom",[NSString stringWithFormat:@"%@:%@",self.lblToTime.text,self.lblToTimeTo.text],@"blockto",self.txtRupees.text,@"price",self.txtBid.text,@"bid",self.txtBlock.text,@"noofblock",self.strType,@"type",self.strDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];
        
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
         NSLog(@"string %@",myString);
        
        NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
        
        // NSLog(@"dict %@",dictFinaldata);
        
       // [SVProgressHUD showWithStatus:@"Please wait.."];
        [CustmObj addHud:self.view];
        
        [[Services sharedInstance]serviceCallbyPost:@"service/newbid/saveclientbid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
        {
            
            
            
              NSLog(@"Response %@",response);
            
            if (type == kResponseTypeFail)
            {
                // NSLog(@"fail Response:----> %@",response);
                self.view.userInteractionEnabled=YES;
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                
                
                NSError *error=(NSError*)response;
                
                if (error.code == -1009) {
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                   message:@"The Internet connection appears to be offline."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                              
                                                                              // NSLog(@"You pressed button three");
                                                                          }];
                    
                    [alert addAction:thirdAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
                
                
            }
            else if (type == kresponseTypeSuccess)
            {
                
                
                NSMutableDictionary *dict=(NSMutableDictionary *)response;
                
                if ([[dict objectForKey:@"status"] isEqualToString:@"ERR"])
                {
                    [SVProgressHUD showErrorWithStatus:[dict objectForKey:@"message"]];
                }
                else
                {
                    [SVProgressHUD showSuccessWithStatus:[dict objectForKey:@"message"]];
                    
                    [self.delegate serviceHitForBid];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }
                // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                
            }
        }];
    }
    
    
    
    
    
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    
    if (isHourPicker)
    {
        return [pickerHourArray count];
    }
    else
    {
        return [pickerMinuteArray count];
    }
    
    
}

#pragma mark- Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    //[myTextField setText:[pickerArray objectAtIndex:row]];
    
    
}

- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated
{
    
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component{
    
    if (isHourPicker)
    {
        return [pickerHourArray objectAtIndex:row];
    }
    else
    {
        return [pickerMinuteArray objectAtIndex:row];
    }
    
    
}

#pragma mark - label tap gesture

- (void)lblFromTap:(UIGestureRecognizer*)gestureRecognizer
{
    
    isHourPicker=YES;
    isLblFromTouch=YES;
    [self.hourPicker reloadAllComponents];
    
    
    
    
    
    if (self.tableHeightConstraint.constant !=0) {
        
        [UIView animateWithDuration:0.5 animations:^{
            
            
            self.tableHeightConstraint.constant=0;
            [self.datePickerView needsUpdateConstraints];
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
            
            
        }];
        
    }
    
}

- (void)lblToTap:(UIGestureRecognizer*)gestureRecognizer
{
    isHourPicker=YES;
    
    isLblFromTouch=NO;
    
    [self.hourPicker reloadAllComponents];
    
    if (self.tableHeightConstraint.constant !=0)
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            
            self.tableHeightConstraint.constant=0;
            [self.datePickerView needsUpdateConstraints];
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
            
            
        }];
    }
    
}

- (void)lblFromTimeToTap:(UIGestureRecognizer*)gestureRecognizer
{
    
    isHourPicker=NO;
    isLblTOTouch=YES;
    
    [self.hourPicker reloadAllComponents];
    
    if (self.tableHeightConstraint.constant !=0) {
        
        [UIView animateWithDuration:0.5 animations:^{
            
            
            self.tableHeightConstraint.constant=0;
            [self.datePickerView needsUpdateConstraints];
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
            
            
        }];
        
    }
    
}

- (void)lblToTapTo:(UIGestureRecognizer*)gestureRecognizer
{
    isHourPicker=NO;
    isLblTOTouch=NO;
    
    [self.hourPicker reloadAllComponents];
    
    if (self.tableHeightConstraint.constant !=0)
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            
            self.tableHeightConstraint.constant=0;
            [self.datePickerView needsUpdateConstraints];
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
            
            
        }];
    }
    
}




#pragma mark - picker button click
- (IBAction)cancelBtnClcik:(id)sender
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
}



- (IBAction)doneBtnClick:(id)sender
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
    
    //    if (isHourPicker)
    //    {
    //        NSLog(@"selected row %@",[self pickerView:self.hourPicker titleForRow:[self.hourPicker selectedRowInComponent:0] forComponent:0]);
    //    }
    //    else
    //    {
    //        NSLog(@"selected row %@",[self pickerView:self.hourPicker titleForRow:[self.hourPicker selectedRowInComponent:0] forComponent:0]);
    //    }
    
    int row=(int)[self.hourPicker selectedRowInComponent:0];
    
    // NSLog(@"row %d",(int)[self.hourPicker selectedRowInComponent:0]);
    
    if (isHourPicker)
    {
        if (isLblFromTouch) {
            
            self.lblFromTime.text=[pickerHourArray objectAtIndex:row];
            
            
            int lblTo=(int)[pickerHourArray indexOfObject:self.lblToTime.text];
            int lblFromTo=(int)[pickerMinuteArray indexOfObject:self.lblFromTimeTo.text];
            int lblToMinTo=(int)[pickerMinuteArray indexOfObject:self.lblToTimeTo.text];
            
            if (row < lblTo)
            {
                self.lblFromTime.text=[pickerHourArray objectAtIndex:row];
            }
            else if (row == lblTo)
            {
                if (lblFromTo <= lblToMinTo)
                {
                    self.lblFromTime.text=[pickerHourArray objectAtIndex:row];
                }
            }
            
            // NSLog(@"row %ld and lblTo %d and lblFromTo %d and lblToMinTo %d",(long)row,lblTo,lblFromTo,lblToMinTo);
            
            if ([self.lblFromTime.text isEqualToString:@"24"])
            {
                self.lblFromTimeTo.text=@"00";
            }
        }
        else
        {
            
            
            
            int lblFrom=(int)[pickerHourArray indexOfObject:self.lblFromTime.text];
            int lblFromTo=(int)[pickerMinuteArray indexOfObject:self.lblFromTimeTo.text];
            int lblToMinTo=(int)[pickerMinuteArray indexOfObject:self.lblToTimeTo.text];
            
            if (row > lblFrom)
            {
                self.lblToTime.text=[pickerHourArray objectAtIndex:row];
            }
            else if (row == lblFrom)
            {
                if (lblFromTo <= lblToMinTo)
                {
                    self.lblToTime.text=[pickerHourArray objectAtIndex:row];
                }
            }
            
            
            
            if ([self.lblToTime.text isEqualToString:@"24"])
            {
                self.lblToTimeTo.text=@"00";
            }
        }
    }
    else
    {
        if (isLblTOTouch)
        {
            if ([self.lblFromTime.text isEqualToString:@"24"])
            {
                self.lblFromTimeTo.text=@"00";
            }
            else
            {
                
                
                
                int lblFrom=(int)[pickerHourArray indexOfObject:self.lblFromTime.text];
                int lblTo=(int)[pickerHourArray indexOfObject:self.lblToTime.text];
                int lblToMinTo=(int)[pickerMinuteArray indexOfObject:self.lblToTimeTo.text];
                
                if (lblFrom < lblTo)
                {
                    self.lblFromTimeTo.text=[pickerMinuteArray objectAtIndex:row];
                }
                else if (lblFrom == lblTo)
                {
                    if (row <= lblToMinTo)
                    {
                        self.lblFromTimeTo.text=[pickerMinuteArray objectAtIndex:row];
                    }
                }
                
            }
            
        }
        else
        {
            if ([self.lblToTime.text isEqualToString:@"24"])
            {
                self.lblToTimeTo.text=@"00";
            }
            else
            {
                
                
                
                int lblFrom=(int)[pickerHourArray indexOfObject:self.lblFromTime.text];
                int lblTo=(int)[pickerHourArray indexOfObject:self.lblToTime.text];
                int lblFromTo=(int)[pickerMinuteArray indexOfObject:self.lblFromTimeTo.text];
                
                if (lblFrom < lblTo)
                {
                    self.lblToTimeTo.text=[pickerMinuteArray objectAtIndex:row];
                }
                else if (lblFrom == lblTo)
                {
                    if (row >= lblFromTo)
                    {
                        self.lblToTimeTo.text=[pickerMinuteArray objectAtIndex:row];
                    }
                }
            }
            
        }
    }
    
}



#pragma mark - Keyboard Activity
- (void)keyboardWillShow:(NSNotification *)notif
{
    CGSize keyboardSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //  NSLog(@"keyboard Height %f",keyboardSize.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height-50, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    
    // NSLog(@"active field %f and %f",activeField.frame.origin.y,aRect.size.height);
    
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        
        [self.myScrollView scrollRectToVisible:activeField.frame animated:YES];
    }
    
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    // move the toolbar frame down as keyboard animates into view
    //    NSDictionary *info = [notification userInfo];
    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // Write code to adjust views accordingly using kbSize.height
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    
}

#pragma mark -TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField=textField;
    
    
    if (self.tableHeightConstraint.constant ==0) {
        
        [UIView animateWithDuration:0.5 animations:^{
            
            
            self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
            [self.datePickerView needsUpdateConstraints];
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
            
            
        }];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField=nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.txtBid]) {
        
        // NSLog(@"string and text field and range %@ %@ %lu",string,textField.text,(unsigned long)range.length);
        
        if ([string length]>0) {
            
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            // NSLog(@"new string %@",newString);
            
            NSString *expression = @"^-?[0-9]*((\\.|,)[0-9]{0,1})?$";
            NSError *error = nil;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
            NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
            return numberOfMatches != 0;
        }
        else
        {
            
            return YES;
            
        }
        
        
    }
    
    return YES;
}

//-(void)textFieldDidChange:(UITextField *)textField
//{
//   // textField.text=[textField.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
//
//    if ([textField isEqual:self.txtBid])
//    {
//        self.txtBid.text=textField.text;
//
//        NSLog(@"self.txtBid %@",self.txtBid.text);
//    }
//
//
//
//}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtRupees)
    {
        [self.txtBid becomeFirstResponder];
    }
    else if (textField == self.txtBid)
    {
        [self.txtBlock becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        
    }
    
    return YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
