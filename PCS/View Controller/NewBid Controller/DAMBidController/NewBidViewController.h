//
//  NewBidViewController.h
//  PCS
//
//  Created by lab4code on 14/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBidViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign)BOOL iexchecked;
@property(nonatomic,assign)int whichIEXButtonChecked;


@property(nonatomic,assign)BOOL RECiexcheck;
@property(nonatomic,assign)int RECCIEXButtonChecked;

@property (weak, nonatomic) IBOutlet UIButton *buttonCopy;
@property (weak, nonatomic) IBOutlet UIButton *buttonBidsummary;
@property (weak,nonatomic) IBOutlet UIButton *btnBack;
@property (weak,nonatomic) IBOutlet UIButton *btnLogout;

@property(strong, nonatomic) NSDictionary *dictEitcontainerValue;
@property(strong, nonatomic) NSString *selectionType;
@end
