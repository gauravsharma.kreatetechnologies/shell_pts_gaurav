//
//  GDAMTableViewCell.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/15/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift

class GDAMTableViewCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet weak var lblSlotTo: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewSlot: UIView!
    
    @IBOutlet weak var viewSlotFrom: UIView!
    @IBOutlet weak var lblbid: UILabel!
    @IBOutlet weak var lblSlotFrom: UILabel!

    var GDAMFromToTime = TamFromToTimeData()
    var pickerView = ToolbarPickerView()
    var slotToCompletion:((String)->Void)?
    var slotFromCompletion:((String)->Void)?
    var priceCompletion:((String)->Void)?
    var bidCompletion:((String)->Void)?

    var fromSlot:[String] = []
    var toSlot:[String] = []
    var completion:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
    }
    
    @IBAction func btnDlt(_ sender: UIButton) {
        self.completion?()
    }
    
    
}
