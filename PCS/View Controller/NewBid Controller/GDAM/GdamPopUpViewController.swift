//
//  GdamPopUpViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/29/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class GdamPopUpViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var viewSlotFrom: UIView!
    @IBOutlet weak var viewSlotTo: UIView!
    @IBOutlet weak var txtBid: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtSlotTo: UITextField!
    @IBOutlet weak var txtSlotFrom: UITextField!
    var bidDetailData:BidDetailData?
    
    var Completion:((String,String,String,String)->Void)?
    var pickerView = ToolbarPickerView()
    var fromSlot:[String] = []
    var toSlot:[String] = []
    var tamFromToTime:TamFromToTimeData?
    var isEdit:Bool?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtBid.delegate =  self
        self.txtPrice.delegate =  self
        self.txtSlotTo.delegate =  self
        self.txtSlotFrom.delegate =  self
       
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtSlotTo.inputView =  pickerView
        self.txtSlotFrom.inputView = pickerView
        self.pickerView.toolbarDelegate = self
        self.txtSlotTo.inputAccessoryView = self.pickerView.toolbar
        self.txtSlotFrom.inputAccessoryView =  self.pickerView.toolbar
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        roundCorner(viewSlotTo)
        roundCorner(viewSlotFrom)
        self.fromSlot = tamFromToTime?.fromslot ?? []
        self.toSlot =  tamFromToTime?.toslot ?? []
        if isEdit == true{
            self.txtPrice.text =  self.bidDetailData?.price
            self.txtSlotTo.text =  self.bidDetailData?.blockto
            self.txtSlotFrom.text =  self.bidDetailData?.blockfrom
            self.txtBid.text  = self.bidDetailData?.bid
        }
    }
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }
    @IBAction func btnDissmiss(_ sender: UIButton) {
    }
    
    @IBAction func btnCross(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        if  txtSlotFrom.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter Slot From Time")
            return
        }else if self.txtSlotTo.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter Slot To Time")

            return
        }else if self.txtPrice.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter Price")

         return
        }else if self.txtBid.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter Bid")

         return
        }
       
        self.dismiss(animated: true) {
            if let from =  self.txtSlotFrom.text,let to =  self.txtSlotTo.text,let price =  self.txtPrice.text,let bid =  self.txtBid.text{
                self.Completion?(from,to,price,bid)
            }
        }
       
       
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == self.txtSlotFrom{
//            if let slotFrom = self.txtSlotFrom.text{
//                self.bidDetailData?.blockfrom =  slotFrom
//                self.bidDetailData?.blockfrom?.append(slotFrom)
//            }
//        }else if textField ==  self.txtSlotTo{
//            if let slotTo =  self.txtSlotTo.text{
//                self.bidDetailData?.blockto = slotTo
//            }
//        }else if textField ==  self.txtPrice{
//            if let price  = self.txtPrice.text{
//                self.bidDetailData?.price = price
//            }
//        }else{
//            if let bid =  self.txtBid.text{
//                self.bidDetailData?.bid =  bid
//            }
//        }
//    }
}

extension GdamPopUpViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtSlotTo.isFirstResponder{
            return self.toSlot.count
        }
        
        return fromSlot.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtSlotTo.isFirstResponder{
            return self.toSlot[row]
        }
        return fromSlot[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtSlotTo.isFirstResponder{
            txtSlotTo.text =  self.toSlot[row]

        }
        txtSlotFrom.text =  fromSlot[row]

    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
extension GdamPopUpViewController:ToolbarPickerViewDelegate{
    func didTapDone() {
        if txtSlotTo.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtSlotTo.text = self.toSlot[row]
            self.txtSlotTo.resignFirstResponder()

        }
        let row = self.pickerView.selectedRow(inComponent: 0)
        self.pickerView.selectRow(row, inComponent: 0, animated: false)
        self.txtSlotFrom.text = fromSlot[row]
        self.txtSlotFrom.resignFirstResponder()
    }
    
    func didTapCancel() {
        if txtSlotTo.isFirstResponder{
            self.txtSlotTo.text = nil
            self.txtSlotTo.resignFirstResponder()
        }
        self.txtSlotFrom.text = nil
        self.txtSlotFrom.resignFirstResponder()
    }
}
