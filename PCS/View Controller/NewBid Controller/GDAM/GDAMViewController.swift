//
//  GDAMViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/12/22.
//  Copyright © 2022 lab4code. All rights reserved.
//
import IQKeyboardManagerSwift
import UIKit
import SwiftyJSON
import Alamofire



class GDAMViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtDeliverDate: UITextField!
    @IBOutlet weak var txtSelectClientType: UITextField!
    @IBOutlet weak var txtDiscPremPrice: UITextField!
    @IBOutlet weak var txtPremiumDiscount: UITextField!
    @IBOutlet weak var viewSource: UIView!
    @IBOutlet weak var viewClient: UIView!
    @IBOutlet weak var viewDelivery: UIView!
    @IBOutlet weak var btnNewBid: UIButton!
    @IBOutlet weak var txtSelectOCF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSubmitBid: UIButton!
    @IBOutlet weak var txtSolar: UITextField!
    @IBOutlet weak var viewOCF: UIView!
    @IBOutlet weak var viewOuterOCF: UIView!
    @IBOutlet weak var viewButtons:UIView!
    @IBOutlet weak var viewStack:UIStackView!
    @IBOutlet weak var viewOuterPremDiscPrice: UIView!
    @IBOutlet weak var viewOuterPremDisc: UIView!
    @IBOutlet weak var viewDiscPrem: UIView!
    @IBOutlet weak var txtSelectDate: UITextField!
    @IBOutlet weak var btnIEX:UIButton!
    @IBOutlet weak var btnPXIL:UIButton!
    @IBOutlet weak var btnBlockBid:UIButton!
    @IBOutlet weak var btnSingleBid:UIButton!
    @IBOutlet weak var btnFutureDate:UIButton!
    @IBOutlet weak var btnToday:UIButton!
    @IBOutlet weak var viewDiscPremPrice: UIView!


    var selectDate:String?
    var pickerView = ToolbarPickerView()
    var sellerTypeArray:[BuyerType] = []
    var selectOCF:[OCFType] = []
    var discountType:[DiscountType] = []
    var sourceArray = [String]()
    var buyerType:String = "BUYER"
    var ocfType:String = ""
    var solorType:String = ""
    var disPremType:String = ""
    var exchnageType:String = ""
    var blockType:String = ""
    var isSubmit:String = ""
    var tamFromToTime = TamFromToTimeData()

    var gdamData:GdamData?
    var bidIdArrData:BidIdArrData?
    var bidDetailData = [BidDetailData]()
    var index:Int?
    var bidData:BidDetailData?
    var isGo:Bool?
    let Exchange: RadioButtonController = RadioButtonController()
    let BidType: RadioButtonController = RadioButtonController()
    let TradeDate: RadioButtonController = RadioButtonController()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtSelectDate.setInputViewDatePicker(target: self, selector: #selector(dateToPressed))
//        dateToPressed()
        Exchange.buttonsArray = [btnIEX,btnPXIL]
        BidType.buttonsArray = [btnBlockBid,btnSingleBid]
        TradeDate.buttonsArray = [btnFutureDate,btnToday]
        
        self.sellerTypeArray.insert(BuyerType(key: "Buyer", value: "BUYER"), at: 0)
        self.sellerTypeArray.insert(BuyerType(key: "Seller", value: "SELLER"), at: 1)
        
        self.selectOCF.insert(OCFType(key: "Yes", value: "YES"), at: 0)
        self.selectOCF.insert(OCFType(key: "No", value: "NO"), at: 1)

        self.discountType.insert(DiscountType(key: "DISCOUNT", value: "DISCOUNT"), at: 0)
        self.discountType.insert(DiscountType(key: "PREMIUM", value: "PREMIUM"), at: 0)

        self.txtSelectClientType.delegate =  self
        self.txtSelectOCF.delegate =  self
        self.txtPremiumDiscount.delegate =  self
        self.txtPremiumDiscount.delegate =  self
        self.txtSolar.delegate =  self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtSelectClientType.inputView =  pickerView
        self.txtSelectOCF.inputView =  pickerView
        self.txtSolar.inputView =  pickerView
        self.txtPremiumDiscount.inputView = pickerView

        self.pickerView.toolbarDelegate = self
        self.txtSelectClientType.inputAccessoryView = self.pickerView.toolbar
        self.txtSelectOCF.inputAccessoryView = self.pickerView.toolbar
        self.txtSolar.inputAccessoryView = self.pickerView.toolbar
        self.txtPremiumDiscount.inputAccessoryView =  self.pickerView.toolbar

        
        self.txtSelectDate.delegate =  self
        self.txtDeliverDate.isUserInteractionEnabled = false
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.callTamfromtotime()
        
       
        
        self.roundCorner(viewDelivery)
        self.roundCorner(viewClient)
        self.roundCorner(viewSource)
        self.roundCorner(viewOCF)
        self.roundCorner(viewDiscPrem)
        self.roundCorner(viewDiscPremPrice)
        self.btnSubmitBid.isHidden =  true
        self.viewOuterPremDisc.isHidden =  true
        self.viewOuterPremDiscPrice.isHidden =  true
        self.viewButtons.isHidden = true
        self.viewStack.isHidden = true
        self.tableView.isHidden = true
        
        
    }
    
    func callTamfromtotime()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let someDict =  ["access_key" : access_key,"product_type":"ITD"] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamfromtotime, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamFromToTimeModel.self, from: productData)
                   print(decodedData)
                    if let data = decodedData.data{
                        self.tamFromToTime  = data

                    }
                    
                   
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
       
    }
   
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }
    
    @objc func dateToPressed() {
        if let  datePicker = self.txtSelectDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            self.txtSelectDate.text = dateFormatter.string(from: datePicker.date)
            self.txtDeliverDate.text = dateFormatter.string(from: datePicker.date)
            self.selectDate =  formatter.string(from: datePicker.date)
//            self.callGdamBiddata()
        }
        self.txtSelectDate.resignFirstResponder()
    }
    
    func callGdamBiddata()
    {
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["date"] = selectDate
        param["clientid"] = client_id
        var disc:[[String:Any]] = []
        
        for i in 0..<bidDetailData.count {
            let bidDetaiData:[String:Any] = ["blockfrom":bidDetailData[i].blockfrom ?? "","blockto":bidDetailData[i].blockto ?? "","bid":bidDetailData[i].bid ?? "","btype":"","price":bidDetailData[i].price ?? ""]
            disc.append(bidDetaiData)
        }
        
        if isGo == true{
            param["buyer_seller"] = ""
            param["source_type"] = ""
            param["ocf"] = ""
            param["p_d"] = ""
            param["pd_price"] = ""
            param["exchange"] = ""
            param["biddingtype"] = ""
            param["bidDetailArr"] = disc
        }
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: gdamgetGdamBiddata, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(GdamData.self, from: productData)
                   print(decodedData)
                    self.gdamData =  decodedData
                    self.bidIdArrData =  decodedData.bidIdArr
                    self.bidDetailData = []
                    self.bidDetailData.append(contentsOf: decodedData.bidDetailArr ?? [])
                    self.tableView.isHidden = self.bidDetailData.count == 0 ? true:false
//                    self.viewStack.isHidden = self.bidIdArrData?.id == nil ? true:false
//                    self.viewButtons.isHidden = self.bidIdArrData?.id == nil ? true:false
                    self.btnSubmitBid.isHidden = self.bidIdArrData?.id == nil ? true:false
                    if self.bidDetailData.count != 0{
                        self.btnSubmitBid.setImage(UIImage(named: "CancelBid"), for: .normal)
                        self.isSubmit = "cancel"
                    }
                    self.setupUI()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func callSubmitGdamBiddata()
    {
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["date"] = selectDate
        param["clientid"] = client_id
        var disc:[[String:Any]] = []
        
        for i in 0..<bidDetailData.count {
            let bidDetaiData:[String:Any] = ["blockfrom":bidDetailData[i].blockfrom ?? "","blockto":bidDetailData[i].blockto ?? "","bid":bidDetailData[i].bid ?? "","btype":"","price":bidDetailData[i].price ?? ""]
            disc.append(bidDetaiData)
        }
        
        param["buyer_seller"] = buyerType
        param["source_type"] = solorType
        param["ocf"] = ocfType
        param["p_d"] = disPremType
        param["pd_price"] = self.txtDiscPremPrice.text
        param["exchange"] = exchnageType
        param["biddingtype"] = blockType
        param["bidDetailArr"] = disc
        param["type"] = exchnageType
       
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: gdamsubmitGdamBiddata, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(GdamData.self, from: productData)
                   print(decodedData)
//                    self.gdamData =  decodedData
//                    self.bidIdArrData =  decodedData.bidIdArr
//                    self.bidDetailData = []
//                    self.bidDetailData.append(contentsOf: decodedData.bidDetailArr ?? [])
//                    self.tableView.isHidden = self.bidDetailData.count == 0 ? true:false
//
//                    self.setupUI()
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func callDeleteGdamBiddata(bid_id:String,id:String)
    {
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["date"] = selectDate
        param["clientid"] = client_id
        var disc:[[String:Any]] = []
        
        for i in 0..<bidDetailData.count {
            let bidDetaiData:[String:Any] = ["blockfrom":bidDetailData[i].blockfrom ?? "","blockto":bidDetailData[i].blockto ?? "","bid":bidDetailData[i].bid ?? "","btype":"","price":bidDetailData[i].price ?? "","id":id,"bid_id":bid_id  ,"timestamp":bidDetailData[i].timestamp ?? "" , "entry_by":bidDetailData[i].entry_by ?? ""]
            disc.append(bidDetaiData)
        }
        
        param["buyer_seller"] = buyerType
        param["source_type"] = solorType
        param["ocf"] = ocfType
        param["p_d"] = disPremType
        param["pd_price"] = self.txtDiscPremPrice.text
        param["exchange"] = exchnageType
        param["biddingtype"] = blockType
        param["bidDetailArr"] = disc
        param["type"] = exchnageType
       
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: gdamdeleteGdamBiddata, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(DeleteGdamData.self, from: productData)
                   print(decodedData)
                    if decodedData.status == "SUCCESS"{
                        self.showToast(controller: self, message: decodedData.message ?? "", seconds: 2)
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func callGdamSouceType(buyer:String)
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
       
        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["buyer_seller"] = buyer
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: gdamgetGdamSouceType, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(SouceTypeDataModel.self, from: productData)
                   print(decodedData)
                    self.sourceArray = decodedData.source_type ?? []
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        
    }
    func setupUI(){
        let data  = bidIdArrData
        self.txtSolar.text = data?.source_type
        self.txtSelectOCF.text =  data?.ocf
        self.txtPremiumDiscount.text = data?.p_d
        self.txtSelectClientType.text =  data?.buyer_seller
        self.txtDiscPremPrice.text = data?.pd_price
        self.txtDeliverDate.text = gdamData?.deliveryDt
        if data?.exchange == "PXIL"{
            Exchange.defaultButton = btnPXIL
        }else{
            Exchange.defaultButton = btnIEX
        }
        if data?.biddingtype == "BLOCK"{
            BidType.defaultButton = btnBlockBid
        }else{
            BidType.defaultButton = btnSingleBid
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPXILAction(_ sender: UIButton) {
        Exchange.buttonArrayUpdated(buttonSelected: sender)
        self.exchnageType = "PXIL"

    }
    @IBAction func btnNewBid(_ sender: UIButton) {
        if  txtSelectClientType.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Client Type")
            return
        }else if self.txtSolar.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Source Type")

            return
        }else if self.txtSelectOCF.text?.isEmpty ?? true{
            self.alertView(message: "Please Select OCF")

         return
        }
        if ocfType == "YES"{
         if self.txtPremiumDiscount.text?.isEmpty ?? true{
                self.alertView(message: "Please Select Premium/Discount")

                return
            }
            else if self.txtDiscPremPrice.text?.isEmpty ?? true{
                self.alertView(message: "Please Enter Price")

                return
            }
        }
        
        let vc: GdamPopUpViewController? = self.storyboard?.instantiateViewController(withIdentifier: "GdamPopUpViewController") as! GdamPopUpViewController?
        vc?.modalPresentationStyle = .overFullScreen
        vc?.tamFromToTime =  self.tamFromToTime
        vc?.Completion = { from , to , price , bid  in
            self.bidDetailData.insert(BidDetailData.init(id: "", bid_id: "", blockfrom: from, blockto: to, price: price, bid: bid, entry_by: "", timestamp: ""), at:0)
            self.tableView.isHidden = self.bidDetailData.count == 0 ? true:false
            self.btnSubmitBid.isHidden = self.bidDetailData.count == 0 ? true:false //submitBidButton
            self.btnSubmitBid.setImage(UIImage(named: "submitBidButton"), for: .normal)
            self.isSubmit = "submit"
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        self.present(vc!, animated: true, completion: nil)

    }
    @IBAction func btnsubmitBid(_ sender: UIButton) {
        if  txtSelectClientType.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Client Type")
            return
        }else if self.txtSolar.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Source Type")

            return
        }else if self.txtSelectOCF.text?.isEmpty ?? true{
            self.alertView(message: "Please Select OCF")

         return
        }
        if ocfType == "YES"{
         if self.txtPremiumDiscount.text?.isEmpty ?? true{
                self.alertView(message: "Please Select Premium/Discount")

                return
            }
            else if self.txtDiscPremPrice.text?.isEmpty ?? true{
                self.alertView(message: "Please Enter Price")

                return
            }
        }
        if isSubmit == "cancel"{

            self.alert(message:"Are you sure want to cancel bid")
        }else{
            self.callSubmitGdamBiddata()
        }
        
    }
    func alert(message:String){
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default) {
            UIAlertAction in
            print("Delete Pressed")
            
            if self.isSubmit == "cancel"{
                self.callDeleteGdamBiddata(bid_id: self.bidData?.bid_id ?? "", id: self.bidData?.id ?? "")
            }
            if self.bidDetailData.count == 1{
                self.btnSubmitBid.setImage(UIImage(named: "CancelBid"), for: .normal)
            }
            self.bidDetailData.remove(at: self.index ?? 0)
            self.tableView.isHidden = self.bidDetailData.count == 0 ? true:false
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }

        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            print("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func btnToday(_ sender: UIButton) {
        TradeDate.buttonArrayUpdated(buttonSelected: sender)
            self.dateToPressed()
    }
    @IBAction func btnFutureDate(_ sender: UIButton) {
        TradeDate.buttonArrayUpdated(buttonSelected: sender)
        let today = Date()
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat =  "dd-MM-yyyy"
        let nextDate = calendar.date(byAdding: .day, value: 1, to: today)
        self.txtSelectDate.text =  formatter.string(from: nextDate ?? Date())
        self.selectDate =  formatter.string(from: nextDate ?? Date())
//        self.callGdamBiddata()
    }
    
    @IBAction func btnIEXLAction(_ sender: UIButton) {
        Exchange.buttonArrayUpdated(buttonSelected: sender)
        self.exchnageType = "IEX"
    }
    
    
    @IBAction func btnBlockBidLAction(_ sender: UIButton) {
        BidType.buttonArrayUpdated(buttonSelected: sender)
            self.blockType = "BLOCK"
    }
    
    @IBAction func btnSingleBidLAction(_ sender: UIButton) {
        BidType.buttonArrayUpdated(buttonSelected: sender)
        self.blockType = "SINGLE"

    }
    @IBAction func btnGoAction(_ sender: UIButton) {
//        self.isGo = true
        self.viewButtons.isHidden =  false
        self.viewStack.isHidden =  false
        self.callGdamBiddata()

    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
       
    }
    func editButtonAction(){
        let optionMenuController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let addAction = UIAlertAction(title: NSLocalizedString("Edit", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File has been Add")
            self.edit()
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File has been Edit")
            self.delete()
        })

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        optionMenuController.addAction(addAction)
        optionMenuController.addAction(saveAction)
        optionMenuController.addAction(cancelAction)
        self.present(optionMenuController, animated: true, completion: nil)
    }
    
    func delete(){
        if self.bidDetailData.count == 1{
           if self.isSubmit == "submit" {
            self.showToast(controller: self, message: "Please click on Submit bid  to submit bid first", seconds: 2)

           }else{
            self.showToast(controller: self, message: "Please click on cancel to delete this bid", seconds: 2)

           }
        }else{
            self.alert(message:"Are you sure want to Delete")
        }
    }
    func edit(){
        let vc: GdamPopUpViewController? = self.storyboard?.instantiateViewController(withIdentifier: "GdamPopUpViewController") as! GdamPopUpViewController?
        vc?.modalPresentationStyle = .overFullScreen
        vc?.tamFromToTime =  self.tamFromToTime
        vc?.bidDetailData =  self.bidData
        vc?.isEdit =  true
        vc?.Completion = { from , to , price , bid  in
//            self.bidDetailData.insert(BidDetailData.init(id: self.bidData?.id, bid_id: self.bidData?.id, blockfrom: from, blockto: to, price: price, bid: bid, entry_by: self.bidData?.id, timestamp: self.bidData?.id), at:self.index ?? 0)
            self.bidDetailData[self.index ?? 0].blockfrom =  from
            self.bidDetailData[self.index ?? 0].blockto =  to
            self.bidDetailData[self.index ?? 0].price =  price
            self.bidDetailData[self.index ?? 0].bid =  bid
            self.isSubmit = "submit"
            self.tableView.isHidden = self.bidDetailData.count == 0 ? true:false
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        self.present(vc!, animated: true, completion: nil)
    }
}


extension GDAMViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableView{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            return 1;
        }
        
        else
        {
            return self.bidDetailData.count
        }

       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 0)
        {
            return 40;
        }else{
            return 50;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section==0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "firstGDam")!
            return cell
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "GDAMTableViewCell")as! GDAMTableViewCell
            let data =  bidDetailData[indexPath.row]
            self.bidData = data
            cell.lblPrice.text = data.price
            cell.lblbid.text =  data.bid
            cell.lblSlotTo.text =  data.blockto
            cell.lblSlotFrom.text =  data.blockfrom
            cell.selectionStyle = .none
            cell.completion = {
                
                self.index = indexPath.row
                self.editButtonAction()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                if self.bidDetailData.count == 0{
                    self.tableView.isHidden = true
                    self.btnSubmitBid.isHidden =  true
                }
                
            }
            
            return cell
        }
        
    }
    
}

extension GDAMViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtSelectClientType.isFirstResponder {
            return sellerTypeArray.count
        }else if txtSolar.isFirstResponder{
            return sourceArray.count
        }else if txtPremiumDiscount.isFirstResponder{
            return discountType.count
        }
        return selectOCF.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtSelectClientType.isFirstResponder {
            return sellerTypeArray[row].key
        }else if txtSolar.isFirstResponder{
            return sourceArray[row]
        }else if txtPremiumDiscount.isFirstResponder{
            return discountType[row].key
        }
        return selectOCF[row].key
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtSelectClientType.isFirstResponder {
            txtSelectClientType.text =  sellerTypeArray[row].key
            self.buyerType =  sellerTypeArray[row].value ?? ""
            self.callGdamSouceType(buyer:self.buyerType)
        }
        else if txtSolar.isFirstResponder{
            if sourceArray != []{
                self.txtSolar.text  = sourceArray[row]
                self.solorType = sourceArray[row]
            }
        } else if txtPremiumDiscount.isFirstResponder {
            txtPremiumDiscount.text =  discountType[row].key
            self.disPremType =  discountType[row].value ?? ""
        }
        else{
            txtSelectOCF.text =  selectOCF[row].key
            self.ocfType =  selectOCF[row].value ?? ""
            
            if ocfType == "YES"{
                self.viewOuterPremDisc.isHidden =  false
            }else{
                self.viewOuterPremDisc.isHidden =  true
            }
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
extension GDAMViewController:ToolbarPickerViewDelegate{
    func didTapDone() {
        if txtSelectClientType.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtSelectClientType.text = sellerTypeArray[row].key
            self.buyerType =  sellerTypeArray[row].value ?? ""
            self.txtSelectClientType.resignFirstResponder()
            self.callGdamSouceType(buyer:self.buyerType)

        }else if txtSolar.isFirstResponder{
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            if sourceArray != []{
                self.txtSolar.text  = sourceArray[row]
                self.solorType = sourceArray[row]
            }
            
            self.txtSolar.resignFirstResponder()
        }
        else if txtPremiumDiscount.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtPremiumDiscount.text = discountType[row].key
            self.disPremType =  discountType[row].value ?? ""
            self.txtPremiumDiscount.resignFirstResponder()

        }
        else{
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtSelectOCF.text = selectOCF[row].key
            self.ocfType =  selectOCF[row].value ?? ""
            self.txtSelectOCF.resignFirstResponder()
            if ocfType == "YES"{
                self.viewOuterPremDisc.isHidden =  false
            }else{
                self.viewOuterPremDisc.isHidden =  true
            }
        }
    }
    
    func didTapCancel() {
        if txtSelectClientType.isFirstResponder{
            self.txtSelectClientType.text = nil
            self.txtSelectClientType.resignFirstResponder()
        }else if txtSolar.isFirstResponder{
            self.txtSolar.text = nil
            self.txtSolar.resignFirstResponder()
        }
        else if txtPremiumDiscount.isFirstResponder{
            self.txtPremiumDiscount.text = nil
            self.txtPremiumDiscount.resignFirstResponder()
        }
        else{
            self.txtSelectOCF.text = nil
            self.txtSelectOCF.resignFirstResponder()
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) {
        if textField == self.txtPremiumDiscount{
            self.viewOuterPremDiscPrice.isHidden =  false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtPremiumDiscount{
            self.viewOuterPremDiscPrice.isHidden =  false
        }
    }
}


