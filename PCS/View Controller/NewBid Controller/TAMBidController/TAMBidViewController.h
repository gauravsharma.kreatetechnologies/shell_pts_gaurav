//
//  TAMBidViewController.h
//  PCS
//
//  Created by lab4code on 30/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAMBidViewController : UIViewController
@property (weak,nonatomic) IBOutlet UIButton *btnBack;
@property (weak,nonatomic) IBOutlet UIButton *btnLogout;
@end
