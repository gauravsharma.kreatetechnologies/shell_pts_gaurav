//
//  TamBidDailyPopUpVC.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/10/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

struct BidSlot {
    var key:String?
    var value:String?
}

import UIKit
import SwiftyJSON
import Alamofire

class TamBidDailyPopUpVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewBidSlot: UIView!
    @IBOutlet weak var txtDate:UITextField!
    @IBOutlet weak var txtBidSlot:UITextField!
    @IBOutlet weak var txtPrice:UITextField!
    @IBOutlet weak var txtBid:UITextField!
    @IBOutlet weak var viewdate:UIView!
    
    var tamFromToTime = TamFromToTimeData()
    var pickerView = ToolbarPickerView()
    var productType:String = ""
    var fromDate:String = ""
    var toDate:String = ""
    var buyerType:String = ""
    var type:String = ""
    var product:String = "TAM"
    var ordernature:String = "BUY"
    var id:String = ""
    var client_id:String = AppHelper.userDefaults(forKey: "client_id")
    var isEdit:Bool?
    var tamDate:String = ""
    var bidSlotArray:[BidSlot] = []

    var tamData:TamData?
    
    
    var completion:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtDate.isUserInteractionEnabled =  false
        self.txtBidSlot.delegate =  self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtBidSlot.inputView = pickerView
        self.pickerView.toolbarDelegate = self
        self.txtBidSlot.inputAccessoryView =  self.pickerView.toolbar
        self.roundCorner(self.viewDate)
        self.roundCorner(self.viewBidSlot)
        if isEdit ==  true{
            self.steupUI()
        }
        if productType == "WEEKLY"{
            self.viewdate.isHidden =  true
        }
        if tamDate == ""{
            self.txtDate.text = "\(fromDate) To \(toDate)"
        }else{
            self.txtDate.text =  tamDate
        }
        self.bidSlotArray.insert(BidSlot(key: "BASE", value: "BASE"), at: 0)
        self.bidSlotArray.insert(BidSlot(key: "NIGHT", value: "NIGHT"), at: 1)
        self.bidSlotArray.insert(BidSlot(key: "DAY", value: "DAY"), at: 2)
        self.bidSlotArray.insert(BidSlot(key: "PEAK", value: "PEAK"), at: 3)
    }
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }
    
    func steupUI(){
        self.txtDate.text =  tamData?.fromtime
        self.txtBid.text =  tamData?.totime
        self.txtPrice.text =  tamData?.price
        self.txtBid.text =  tamData?.bidquantum
        self.productType =  tamData?.producttype ?? ""
        self.fromDate =  tamData?.date ?? ""
        self.toDate =  tamData?.todate ?? ""
        self.buyerType =  tamData?.client_type ?? ""
        self.type =  tamData?.type ?? ""
        self.product = tamData?.product ?? ""
        self.ordernature =  tamData?.ordernature ?? ""
        self.id =  tamData?.id ?? ""
        self.client_id = tamData?.clientid ?? ""
        if tamDate == ""{
            self.txtBidSlot.text = "\(tamData?.date ?? "") To \(tamData?.todate ?? "")"
        }else{
            self.txtBidSlot.text =  tamDate
        }
        
    }
    
    func savetamgtambidAPI(){
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
//        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")
        let dateArray = [""]
        
        let someDict =  ["access_key" : access_key,"date":fromDate,"type":type,"price":self.txtPrice.text ?? "","noofcertificates":"","ordernature":ordernature,"product":product,"producttype":productType,"dailydates":tamDate,"DailyDateArr":dateArray,"appno":"","todate":toDate ,"dailybuyer":buyerType,"optType":"NEWBID","fromtime":"","totime":"","clientid":self.client_id ,"bidquantum":self.txtBid.text ?? "" ,"id":self.id,"bidslot": self.txtBidSlot.text ?? ""] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
           print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamsavetamgtambid, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamBidModel.self, from: productData)
                    print(decodedData)
                    if decodedData.status == "ERR"{
                        if let msg = decodedData.message{
                            DispatchQueue.main.async {
                                self.alertView(message: msg)
                            }
                        }
                    }else if decodedData.status == "SUCCESS"{
                        self.dismiss(animated: true) {
                            self.completion?()
                        }
                    }
                   
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        
    }

    @IBAction func btnCross(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnDissmiss(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        if txtBidSlot.text?.isEmpty ?? true{
            
            return
        }else if txtPrice.text?.isEmpty ?? true{
            return
        }else if txtBid.text?.isEmpty ?? true{
            return
        }
        self.savetamgtambidAPI()
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
}


extension TamBidDailyPopUpVC:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return bidSlotArray.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return bidSlotArray[row].key
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
        txtBidSlot.text =  bidSlotArray[row].value
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
extension TamBidDailyPopUpVC:ToolbarPickerViewDelegate{
    func didTapDone() {
        
        let row = self.pickerView.selectedRow(inComponent: 0)
        self.pickerView.selectRow(row, inComponent: 0, animated: false)
        self.txtBidSlot.text = bidSlotArray[row].value
        self.txtBidSlot.resignFirstResponder()
    }
    
    func didTapCancel() {
        
        self.txtBidSlot.text = nil
        self.txtBidSlot.resignFirstResponder()
    }
}
