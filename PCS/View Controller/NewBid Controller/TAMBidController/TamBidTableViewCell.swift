//
//  TamBidTableViewCell.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/3/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class TamBidTableViewCell: UITableViewCell {

    @IBOutlet weak var lblFromTime:UILabel!
    
    @IBOutlet weak var lblToTime: UILabel!
    @IBOutlet weak var lblMWH: UILabel!
    @IBOutlet weak var lblOrderNature: UILabel!
   
    var completion:(()->Void)?
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func btnAction(_ sender: UIButton) {
        self.completion?()
    }
    

}
