//
//  TamBidViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/3/22.
//  Copyright © 2022 lab4code. All rights reserved.
//
import IQKeyboardManagerSwift
import UIKit
import SwiftyJSON
import Alamofire

struct BuyerType {
    var key:String?
    var value:String?
}


class TamBidViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnIEX: UIButton!
    @IBOutlet weak var viewAddDate: UIView!
    @IBOutlet weak var viewDateFrom: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewDateTo: UIView!
    @IBOutlet weak var btnPXIL: UIButton!
    @IBOutlet weak var btnSubmit:UIButton!
    @IBOutlet weak var btnNewBid:UIButton!
    @IBOutlet weak var viewProductType: UIView!
    @IBOutlet weak var txtDateFrom:UITextField!
    @IBOutlet weak var txtDateTo:UITextField!
    @IBOutlet weak var viewSellerType: UIView!
    @IBOutlet weak var txtSellerType:UITextField!
    @IBOutlet weak var txtProductType:UITextField!
    @IBOutlet weak var viewScroll:UIScrollView!
    @IBOutlet weak var dropDownTable: UITableView!
    
    let radioController: RadioButtonController = RadioButtonController()
    
   
    var pickerView = ToolbarPickerView()
    var productArray = [ProductData]()
    var tamDataArray = [TamData]()
    var tamData:TamData?
    var sellerTypeArray:[BuyerType] = []
    var toDate:String = ""
    var fromDate:String = ""
    var type:String = ""
    var productType:String = ""
    var buyerType:String = ""
    var isSubmit:String = ""
    var tamDate:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.steupUI()
        self.sellerTypeArray.insert(BuyerType(key: "Buyer", value: "BUYER"), at: 0)
        self.sellerTypeArray.insert(BuyerType(key: "Seller", value: "SELLER"), at: 1)
        self.type =  "IEX"
        radioController.buttonsArray = [btnIEX,btnPXIL]
        radioController.defaultButton = btnIEX
        self.txtSellerType.delegate =  self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtSellerType.inputView =  pickerView
        self.pickerView.toolbarDelegate = self
        self.viewAddDate.isHidden = true
        self.txtProductType.isUserInteractionEnabled =  false
        self.txtSellerType.inputAccessoryView = self.pickerView.toolbar
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        let gestureEarthType = UITapGestureRecognizer(target: self, action:  #selector(self.myProvince))
        self.viewProductType.addGestureRecognizer(gestureEarthType)
        txtDateFrom.setInputViewDatePicker(target: self, selector: #selector(dateFromPressed))
        self.txtDateTo.setInputViewDatePicker(target: self, selector: #selector(dateToPressed))
            self.callsellerProductType()
        
    }
    
    @IBAction func btnAddDate(_ sender: UIButton) {
        if self.txtSellerType.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter Sller Type")
            return
        }else if self.txtDateFrom.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter From Date")
         return
        }else if self.txtDateTo.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter To Date")
            return
        }
        
        self.callAddDateAPI()
    }
    @IBAction func btnNewBid(_ sender: UIButton) {
        if self.productType == "DAILY" || self.productType == "WEEKLY"{
            let vc: TamBidDailyPopUpVC? = self.storyboard?.instantiateViewController(withIdentifier: "TamBidDailyPopUpVC") as! TamBidDailyPopUpVC?
            vc?.modalPresentationStyle = .overFullScreen
            vc?.productType =  self.productType
            vc?.fromDate =  self.fromDate
            vc?.toDate =  self.toDate
            vc?.buyerType =  self.buyerType
            vc?.type = self.type
            if self.productType == "DAILY"{
                vc?.tamDate =  self.tamDate
            }
            vc?.completion = {
                self.getfulltambiddetail()
            }
            self.present(vc!, animated: true, completion: nil)
        }
        let vc: TamBidPopUpVC? = self.storyboard?.instantiateViewController(withIdentifier: "TamBidPopUpVC") as! TamBidPopUpVC?
        vc?.modalPresentationStyle = .overFullScreen
        vc?.productType =  self.productType
        vc?.fromDate =  self.fromDate
        vc?.toDate =  self.toDate
        vc?.buyerType =  self.buyerType
        vc?.type = self.type
        vc?.completion = {
            self.getfulltambiddetail()
        }
        self.present(vc!, animated: true, completion: nil)

    }
    
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        let data = tamData
        if isSubmit == "cancel"{
            self.callTamCancelAPI(product: data?.product ?? "", client_id: data?.clientid ?? "", type: type, product_type: data?.producttype ?? "", date: fromDate, todate: toDate, ordernature: "Buy",  action: "CANCEL")
        }else{
            self.CallTamSubmitbidAPI(date: fromDate, type: type, price: "", ordernature: "Buy", product: data?.product ?? "", producttype: data?.producttype ?? "", clientid: data?.clientid ?? "", dailybuyer: buyerType, todate: toDate)
        }
        
    }
    
    
    @objc func myProvince(sender : UITapGestureRecognizer) {
//        self.callsellerProductType()
        DispatchQueue.main.async {
            self.dropDownTable.reloadData()
        }
        self.dropDownTable.isHidden =  false
    }
    @objc func dateFromPressed() {
        if let  datePicker = self.txtDateFrom.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            let formatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            formatter.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            self.txtDateFrom.text = dateFormatter.string(from: datePicker.date)
            self.fromDate =  formatter.string(from: datePicker.date)
            print(fromDate)
            dateFormatter.dateFormat = "EEEE"
            let dayOfTheWeekString = dateFormatter.string(from: datePicker.date)
            print(dayOfTheWeekString)
            if self.productType == "WEEKLY"{
                if dayOfTheWeekString != "Monday"{
                    
                    self.showToast(controller: self, message: "Delivery date must be from Monday for Weekly product ", seconds: 2)
                    return
                }
                let calendar = Calendar.current
                let addOneWeekToCurrentDate = calendar.date(byAdding: .day, value: 6, to: datePicker.date)
                print(addOneWeekToCurrentDate)
                self.toDate =  formatter.string(from: addOneWeekToCurrentDate ?? Date())
                
                let formter = DateFormatter()
                formter.dateStyle = .medium
                self.txtDateTo.text = formter.string(from: addOneWeekToCurrentDate ?? Date())
                self.txtDateTo.isUserInteractionEnabled =  false
            }
        }
        
        self.txtDateFrom.resignFirstResponder()
    }
    
    @objc func dateToPressed() {
        if let  datePicker = self.txtDateTo.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            self.txtDateTo.text = dateFormatter.string(from: datePicker.date)
            self.toDate =  formatter.string(from: datePicker.date)
            print(toDate)
        }
        self.txtDateTo.resignFirstResponder()
    }
    
    
    func steupUI(){
        self.roundCorner(self.viewSellerType)
        self.roundCorner(self.viewProductType)
        self.btnSubmit.isHidden = true
        self.btnNewBid.isHidden =  true
        self.dropDownTable.isHidden =  true
        self.tableView.isHidden = true
        
    }

    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnIEXAction(_ sender: UIButton) {
        radioController.buttonArrayUpdated(buttonSelected: sender)
        self.type =  "IEX"
    }
    @IBAction func btnGo(_ sender: UIButton) {
        if  txtProductType.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter Product Type")
            return
        }else if self.txtSellerType.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter Sller Type")

            return
        }else if self.txtDateFrom.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter From Date")

         return
        }else if self.txtDateTo.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter To Date")

            return
        }
        self.getfulltambiddetail()
    }
    
    
    func editButtonAction(){
        let optionMenuController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let addAction = UIAlertAction(title: NSLocalizedString("Edit", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File has been Add")
            self.edit()
        })
        let saveAction = UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File has been Edit")
            self.delete()
        })

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        optionMenuController.addAction(addAction)
        optionMenuController.addAction(saveAction)
        optionMenuController.addAction(cancelAction)
        self.present(optionMenuController, animated: true, completion: nil)
    }
    
    
    func edit(){
        if self.productType == "DAILY" || self.productType == "WEEKLY"{
            let vc: TamBidDailyPopUpVC? = self.storyboard?.instantiateViewController(withIdentifier: "TamBidDailyPopUpVC") as! TamBidDailyPopUpVC?
            vc?.modalPresentationStyle = .overFullScreen
            vc?.productType =  self.productType
            vc?.fromDate =  self.fromDate
            vc?.toDate =  self.toDate
            vc?.buyerType =  self.buyerType
            vc?.type = self.type
            vc?.isEdit =  true
            vc?.tamData =  self.tamData
            self.present(vc!, animated: true, completion: nil)
        }
        let vc: TamBidPopUpVC? = self.storyboard?.instantiateViewController(withIdentifier: "TamBidPopUpVC") as! TamBidPopUpVC?
        vc?.modalPresentationStyle = .overFullScreen
        vc?.productType =  self.productType
        vc?.fromDate =  self.fromDate
        vc?.toDate =  self.toDate
        vc?.buyerType =  self.buyerType
        vc?.type = self.type
        vc?.isEdit =  true
        vc?.tamData =  self.tamData
        self.present(vc!, animated: true, completion: nil)

    }
    
    func delete(){
        print("delete")
        alert(message:"Are you sure want to Delete")
    }
    
    func alert(message:String){
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default) {
            UIAlertAction in
            print("Delete Pressed")
            let data = self.tamData
            self.callTamdeleteAPI(product: data?.product ?? "", client_id: data?.clientid ?? "", type: data?.type ?? "", product_type: data?.producttype ?? "", date: data?.date ?? "", todate: data?.todate ?? "", ordernature: data?.ordernature ?? "", id: data?.id ?? "")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            print("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func callTamdeleteAPI(product:String,client_id:String,type:String,product_type:String,date:String,todate:String,ordernature:String,id:String)
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let someDict =  ["access_key" : access_key,"product":product,"clientid":client_id,"type":type,"producttype":product_type,"date":date,"todate":todate,"appno":"","ordernature":ordernature,"id":id] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamdeletetamgtambid, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamBidModel.self, from: productData)
                   print(decodedData)
                    if decodedData.status == "SUCCESS"{
                        self.getfulltambiddetail()
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.dropDownTable.reloadData()
        }
    }
    func callTamCancelAPI(product:String,client_id:String,type:String,product_type:String,date:String,todate:String,ordernature:String,action:String)
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let someDict =  ["access_key" : access_key,"product":product,"clientid":client_id,"type":type,"producttype":product_type,"date":date,"todate":todate,"appno":"","ordernature":ordernature,"action":action,"optType":"EDITBID","opttype":"EDITBID","fromtime":"","totime":"","bidquantum":"","dailydates":""] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamdeletetamgtambid, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamBidModel.self, from: productData)
                   print(decodedData)
                    if decodedData.status == "SUCCESS"{
                        self.getfulltambiddetail()
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.dropDownTable.reloadData()
        }
    }
    
    func callAddDateAPI()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let dateArray = [""]
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")

        let someDict =  ["access_key" : access_key,"date":fromDate,"type":type,"price":"","noofcertificates":"","ordernature":"BUY","product":"TAM","producttype":productType,"dailydates":"","DailyDateArr":dateArray,"appno":"","todate":toDate ,"dailybuyer":buyerType,"optType":"NEWBID","clientid":client_id]  as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamgetdailydates, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamDateData.self, from: productData)
                   print(decodedData)
                    self.tamDate =  decodedData.dailydates ?? ""
                    self.showToast(controller: self, message: "Date Add Successfully", seconds: 2)
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.dropDownTable.reloadData()
        }
    }
    func callsellerProductType()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let someDict =  ["access_key" : access_key] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: sellerProductType, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(ProductDataModel.self, from: productData)
                   print(decodedData)
                    self.productArray =  decodedData.data ?? []
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.dropDownTable.reloadData()
        }
    }
    
    func CallTamSubmitbidAPI(date:String,type:String,price:String,ordernature:String,product:String,producttype:String,clientid:String,dailybuyer:String,todate:String)
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let someDict =  ["access_key" : access_key,"date":date,"type":type,"price":price,"noofcertificates":"","ordernature":ordernature,"product":product,"producttype":producttype,"optType":"EDITBID","opttype":"EDITBID","clientid":clientid,"appno":"","dailybuyer":dailybuyer,"todate":todate] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["single":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamsubmittamgtambid, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamBidModel.self, from: productData)
                   print(decodedData)
                    if decodedData.status == "SUCCESS"{
                        self.btnSubmit.setImage(UIImage(named: "CancelBid"), for: .normal)
                        self.isSubmit = "cancel"
                        self.showToast(controller: self, message: decodedData.message ?? "", seconds: 2)
                    }
                    if decodedData.status == "ERR"{
                        self.showToast(controller: self, message: decodedData.message ?? "", seconds: 2)
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.dropDownTable.reloadData()
        }
    }
    
    func getfulltambiddetail(){
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let dateArray = [""]
        let someDict =  ["access_key" : access_key,"date":fromDate,"type":type,"price":"","noofcertificates":"","ordernature":"BUY","product":"TAM","producttype":productType,"dailydates":"","DailyDateArr":dateArray,"appno":"","todate":toDate ,"dailybuyer":buyerType,"optType":"NEWBID"] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
           print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: fulltambiddetail, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamDataModel.self, from: productData)
                   print(decodedData)
                    self.tamDataArray =  decodedData.data ?? []
                    self.tableView.isHidden = self.tamDataArray.count == 0 ? true:false

                    self.btnSubmit.isHidden = self.tamDataArray.count == 0 ? true:false
                    for i in 0..<self.tamDataArray.count{
                        if self.tamDataArray[i].status == "TRUE"{
                            self.btnSubmit.setImage(UIImage(named: "CancelBid"), for: .normal)
                            self.isSubmit = "cancel"
                        }else{
                            self.btnSubmit.setImage(UIImage(named: "SubmitBid"), for: .normal)
                            self.isSubmit = "Submit"
                        }
                    }
                    self.btnNewBid.isHidden =  false
//                    if decodedData.status == true{
//                        self.btnSubmit.isHidden =  false
//                    }
                    
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @IBAction func btnPXILAction(_ sender: UIButton) {
        radioController.buttonArrayUpdated(buttonSelected: sender)
        self.type =  "PXIL"

    }
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
       
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         let touch = touches.first
         if touch?.view == self.view {
            dropDownTable.isHidden = true
        }
    }
}


extension TamBidViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableView{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView{
            if (section == 0)
            {
                return 1;
            }
            
            else
            {
                return tamDataArray.count
            }
        }
        return productArray.count
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(tableView==self.tableView)
        
        {
            if (indexPath.section == 0)
            {
                return 30;
            }else{
                return 42;
            }
        }
        else
        {
            return 45;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView{
            if (indexPath.section==0)
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "TamBid")!
                return cell
            }else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "TamBidTableViewCell", for: indexPath)as! TamBidTableViewCell
                let data =  tamDataArray[indexPath.row]
                self.tamData = data
                cell.lblFromTime.text =  data.fromtime
                cell.lblToTime.text =  data.totime
                cell.lblOrderNature.text  =  data.ordernature
                cell.lblMWH.text =  data.bidquantum
                cell.completion = {
                    self.tamData = data
                    self.editButtonAction()
                }
                return cell
            }
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "dropDownTableViewCell", for: indexPath)as! dropDownTableViewCell
            let data =  productArray[indexPath.row]
            cell.lblTitle.text =  data.value
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView ==  dropDownTable{
            let data  = productArray[indexPath.row]
            self.txtProductType.text =  data.value
            self.productType =  data.key ?? ""
            self.dropDownTable.isHidden =  true
            self.txtDateFrom.text = ""
            if self.txtProductType.text == "Daily"{
                self.viewAddDate.isHidden = false
            }else{
                self.viewAddDate.isHidden = true
            }
        }
    }
    
}

extension TamBidViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return sellerTypeArray.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return sellerTypeArray[row].key
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        txtSellerType.text =  sellerTypeArray[row].key
        self.buyerType =  sellerTypeArray[row].value ?? ""
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
extension TamBidViewController:ToolbarPickerViewDelegate{
    func didTapDone() {
        if txtSellerType.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtSellerType.text = sellerTypeArray[row].key
            self.buyerType =  sellerTypeArray[row].value ?? ""
            self.txtSellerType.resignFirstResponder()
        }
    }
    
    func didTapCancel() {
        if txtSellerType.isFirstResponder{
            self.txtSellerType.text = nil
            self.txtSellerType.resignFirstResponder()
        }
    }
}
