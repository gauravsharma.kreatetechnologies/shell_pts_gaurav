//
//  TamBidPopUpVC.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/5/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire



class TamBidPopUpVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var viewFromDate: UIView!
    @IBOutlet weak var viewToDate: UIView!
    @IBOutlet weak var txtToDate:UITextField!
    @IBOutlet weak var txtFromDate:UITextField!
    @IBOutlet weak var txtPrice:UITextField!
    @IBOutlet weak var txtBid:UITextField!
    var tamFromToTime = TamFromToTimeData()
    var pickerView = ToolbarPickerView()
    var productType:String = ""
    var fromDate:String = ""
    var toDate:String = ""
    var buyerType:String = ""
    var type:String = ""
    var product:String = "TAM"
    var ordernature:String = "BUY"
    var id:String = ""
    var client_id:String = AppHelper.userDefaults(forKey: "client_id")
    var isEdit:Bool?
    var tamDate:String = ""
    
    var tamData:TamData?
    
    
    var completion:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtToDate.delegate =  self
        self.txtFromDate.delegate =  self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtToDate.inputView =  pickerView
        self.txtFromDate.inputView = pickerView
        self.pickerView.toolbarDelegate = self
        self.txtToDate.inputAccessoryView = self.pickerView.toolbar
        self.txtFromDate.inputAccessoryView =  self.pickerView.toolbar
        self.roundCorner(self.viewFromDate)
        self.roundCorner(self.viewToDate)
        if isEdit ==  true{
            self.steupUI()
        }
        self.callTamfromtotime()
    }
    func steupUI(){
        self.txtFromDate.text =  tamData?.fromtime
        self.txtToDate.text =  tamData?.totime
        self.txtPrice.text =  tamData?.price
        self.txtBid.text =  tamData?.bidquantum
        self.productType =  tamData?.producttype ?? ""
        self.fromDate =  tamData?.date ?? ""
        self.toDate =  tamData?.todate ?? ""
        self.buyerType =  tamData?.client_type ?? ""
        self.type =  tamData?.type ?? ""
        self.product = tamData?.product ?? ""
        self.ordernature =  tamData?.ordernature ?? ""
        self.id =  tamData?.id ?? ""
        self.client_id = tamData?.clientid ?? ""
    }
    
    
    
    func callTamfromtotime()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let someDict =  ["access_key" : access_key,"product_type":productType] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamfromtotime, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamFromToTimeModel.self, from: productData)
                   print(decodedData)
                    if let data = decodedData.data{
                        self.tamFromToTime  = data
                    }
                   
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
       
    }
    
    
    func savetamgtambidAPI(){
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
//        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")
        let dateArray = [""]
        
        let someDict =  ["access_key" : access_key,"date":fromDate,"type":type,"price":self.txtPrice.text ?? "","noofcertificates":"","ordernature":ordernature,"product":product,"producttype":productType,"dailydates":"","DailyDateArr":dateArray,"appno":"","todate":toDate ,"dailybuyer":buyerType,"optType":"NEWBID","fromtime":self.txtFromDate.text ?? "","totime":self.txtToDate.text ?? "","clientid":self.client_id,"bidquantum":self.txtBid.text ?? "" ,"id":self.id] as [String : Any]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
           print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: tamsavetamgtambid, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(TamBidModel.self, from: productData)
                    print(decodedData)
                    if decodedData.status == "ERR"{
                        if let msg = decodedData.message{
                            DispatchQueue.main.async {
                                self.alertView(message: msg)
                            }
                        }
                    }else if decodedData.status == "SUCCESS"{
                        self.dismiss(animated: true) {
                            self.completion?()
                        }
                    }
                   
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        
    }
    
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }

    @IBAction func btnCross(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        if  txtFromDate.text?.isEmpty ?? true{
//            self.alertView(message: "Please Enter Product Type")
            return
        }else if txtToDate.text?.isEmpty ?? true{
            return
        }else if txtPrice.text?.isEmpty ?? true{
            return
        }else if txtBid.text?.isEmpty ?? true{
            return
        }
        self.savetamgtambidAPI()
    }
    
    @IBAction func btnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func btnCancel(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
}

extension TamBidPopUpVC:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtToDate.isFirstResponder{
            return tamFromToTime.toslot?.count ?? 1
        }
        
        return tamFromToTime.fromslot?.count ?? 1
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtToDate.isFirstResponder{
            return tamFromToTime.toslot?[row]
        }
        return tamFromToTime.fromslot?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtToDate.isFirstResponder{
            txtToDate.text =  tamFromToTime.toslot?[row]
        }
        txtFromDate.text =  tamFromToTime.fromslot?[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
extension TamBidPopUpVC:ToolbarPickerViewDelegate{
    func didTapDone() {
        if txtToDate.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtToDate.text = tamFromToTime.toslot?[row]
            self.txtToDate.resignFirstResponder()
        }
        let row = self.pickerView.selectedRow(inComponent: 0)
        self.pickerView.selectRow(row, inComponent: 0, animated: false)
        self.txtFromDate.text = tamFromToTime.fromslot?[row]
        self.txtFromDate.resignFirstResponder()
    }
    
    func didTapCancel() {
        if txtToDate.isFirstResponder{
            self.txtToDate.text = nil
            self.txtToDate.resignFirstResponder()
        }
        self.txtFromDate.text = nil
        self.txtFromDate.resignFirstResponder()
    }
}
