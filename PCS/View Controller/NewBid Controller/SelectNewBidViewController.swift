//
//  SelectNewBidViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 7/26/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class SelectNewBidViewController: UIViewController {

    @IBOutlet weak var viewDam:UIView!
    @IBOutlet weak var viewTam:UIView!
    @IBOutlet weak var viewGTam:UIView!
    @IBOutlet weak var viewRTM:UIView!
    @IBOutlet weak var viewREC:UIView!
    @IBOutlet weak var viewEscrets:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.roundCorner(self.viewDam)
        self.roundCorner(self.viewTam)
        self.roundCorner(self.viewGTam)
        self.roundCorner(self.viewRTM)
        self.roundCorner(self.viewREC)
        self.roundCorner(self.viewEscrets)
 
    }
    
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 10.0
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogout(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func btnDamAction(_ sender: UIButton) {
        
        let pushView: NewBidViewController? = storyboard?.instantiateViewController(withIdentifier: "DAMNewBidId") as! NewBidViewController?
        navigationController?.pushViewController(pushView!, animated: true)
    }
    
    @IBAction func btnTamAction(_ sender: UIButton) {
        let pushView: TamBidViewController? = storyboard?.instantiateViewController(withIdentifier: "TamBidViewController") as! TamBidViewController?
        navigationController?.pushViewController(pushView!, animated: true)
    }
    
    @IBAction func btnGtamAction(_ sender: UIButton) {
        let pushView: GTamBidViewController? = storyboard?.instantiateViewController(withIdentifier: "GTamBidViewController") as! GTamBidViewController?
        navigationController?.pushViewController(pushView!, animated: true)
    }
    
    @IBAction func BtnRtm(_ sender: UIButton) {
        let pushView: GDAMViewController? = storyboard?.instantiateViewController(withIdentifier: "GDAMViewController") as! GDAMViewController?
        navigationController?.pushViewController(pushView!, animated: true)
    }
    
    @IBAction func btnRec(_ sender: UIButton) { //
        let pushView: RECNewBidView? = storyboard?.instantiateViewController(withIdentifier: "RECNewBid") as! RECNewBidView?
        navigationController?.pushViewController(pushView!, animated: true)
    }
    
    @IBAction func btnEscrete(_ sender: UIButton) {
        let pushView: escertViewController? = storyboard?.instantiateViewController(withIdentifier: "escert") as! escertViewController?
        navigationController?.pushViewController(pushView!, animated: true)
    }
 
}
