//
//  CollectionViewCell.h
//  PCS
//
//  Created by pavan yadav on 06/11/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@end
