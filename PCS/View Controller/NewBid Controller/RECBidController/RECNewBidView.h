//
//  RECNewBidView.h
//  PCS
//
//  Created by pavan yadav on 24/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RECNewBidView : UIViewController
@property(nonatomic,assign)BOOL iexchecked;
@property(nonatomic,assign)int whichIEXButtonChecked;
@property(nonatomic,assign)BOOL RECiexcheck;
@property(nonatomic,assign)int RECCIEXButtonChecked;
@property (weak,nonatomic) IBOutlet UIButton *btnBack;
@property (weak,nonatomic) IBOutlet UIButton *btnLogout;
@end
