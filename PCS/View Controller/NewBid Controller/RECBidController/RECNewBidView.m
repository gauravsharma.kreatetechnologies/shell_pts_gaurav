//
//  RECNewBidView.m
//  PCS
//
//  Created by pavan yadav on 24/08/16.
//  Copyright © 2016 lab4code. All rights reserved.

#import "RECNewBidView.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "RECDateTableViewCell.h"
#import "RECTableViewCell.h"
#import "CustmObj.h"
#import "AppDelegate.h"



@interface RECNewBidView ()<UIActionSheetDelegate,UITextFieldDelegate>
{
#pragma mark REC button Value.
    BOOL BuyChecked;
    int BuyButtonChecked;
    BOOL UpobjectREC;
    
    BOOL CancellREC;
    BOOL isSubmitedButton;
    
    
#pragma mark RECPpoView Property.
    //BOOL isSolarSelected;
    NSString *solarsalect;
    
#pragma mark REC DateObject.
    NSString *RECtype;
    NSString *RECordernature;
    NSArray *arrRECBidDate;
    NSString *strSelectedDate;
    UIToolbar *numberToolbar;
}

#pragma mark Tableview REC Property.

@property (weak, nonatomic) IBOutlet UITableView *TableviewDate;
@property (weak, nonatomic) IBOutlet UITableView *TableViewREC;

#pragma mark label & Button REC
@property (weak, nonatomic) IBOutlet UILabel *lbldate;
@property (weak, nonatomic) IBOutlet UIButton *BtnDate;
@property (weak, nonatomic) IBOutlet UIButton *IEXBtnpro;
@property (weak, nonatomic) IBOutlet UIButton *PXILbtnpro;
@property (weak, nonatomic) IBOutlet UIButton *BUYPro;
@property (weak, nonatomic) IBOutlet UIButton *SELLBtnpro;

@property (weak, nonatomic) IBOutlet UIButton *BtnNewBid;
@property (weak, nonatomic) IBOutlet UIButton *BtnSubmitbid;

#pragma mark RECPOPVIEW.
@property(strong,nonatomic)NSMutableArray *savedataTable;
@property(strong,nonatomic)NSMutableArray *updateobject;
@property(strong,nonatomic)NSMutableArray *idd;
@property(strong,nonatomic)NSMutableString *Idall;

#pragma mark RECPpoView Property.

@property(weak,nonatomic)IBOutlet UITextField *txtRupeesREC;
@property(weak,nonatomic)IBOutlet UITextField *txtblockREC;
@property (weak, nonatomic) IBOutlet UIView *POPUPRECView;

@property (weak, nonatomic) IBOutlet UIButton *SolarPro;
@property (weak, nonatomic) IBOutlet UIButton *NonSolarpro;

@property (weak, nonatomic) IBOutlet UILabel *lblMinValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxValue;
@end

@implementation RECNewBidView

- (void)viewDidLoad {
    [super viewDidLoad];
#pragma mark HIDDEN property.
    _TableviewDate.hidden=YES;
    _TableViewREC.hidden=YES;
    
    _TableViewREC.hidden = YES;
    _BtnNewBid.hidden = YES;
    _BtnSubmitbid.hidden = YES;
    
    self.txtRupeesREC.delegate = self;
    self.txtblockREC.delegate = self;
    
    [self HiteDateApi];
    BuyButtonChecked = 0;
    CancellREC = 0;
    UpobjectREC =0;
    
    _POPUPRECView.hidden=YES;
    //    isSolarSelected = YES;
    
#pragma mark  for REC .
    
    if (_RECCIEXButtonChecked > 0)
    {
        if (_iexchecked) {
            
            [self.IEXBtnpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.PXILbtnpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            
        }
        else
        {
            [self.IEXBtnpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.PXILbtnpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            
        }
    }
    else
    {
        [self.PXILbtnpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        [self.IEXBtnpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
    }
    
    BuyButtonChecked=0;
    
    
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,220)];
    numberToolbar.barStyle = UIBarStyleDefault;
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)],
                           nil];
    [numberToolbar sizeToFit];
    self.txtRupeesREC.inputAccessoryView = numberToolbar;
    
    self.txtRupeesREC.inputAccessoryView = numberToolbar;
    self.txtblockREC.inputAccessoryView = numberToolbar;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideDateTableView)];
    
    [self.TableViewREC addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)HiteDateApi

{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/rec/getallrectradingdate.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
    {
        
        if (type == kResponseTypeFail)
        {
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            arrRECBidDate = (NSArray *) response;
            
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
        }
        
    }];
}


-(void)hideDateTableView
{
    [self.TableviewDate setHidden:YES];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.TableviewDate setHidden:YES];
}


- (IBAction)BtnDate:(id)sender
{
    _TableviewDate.hidden = NO;
    [_TableviewDate reloadData];
}

-(IBAction)btnBackAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnLogoutAction:(id)sender{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)IEXButton:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    int tag=(int)[btn tag];
    
    
    
    if (_RECCIEXButtonChecked > 0) {
        
        _RECCIEXButtonChecked=1;
        if (_RECiexcheck) {
            _RECiexcheck=NO;
            
            
            [self.IEXBtnpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.PXILbtnpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            RECtype=@"PXIL";
        }
        else
        {
            _RECiexcheck=YES;
            [self.IEXBtnpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.PXILbtnpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            RECtype=@"IEX";
        }
    }
    else
    {
        _RECCIEXButtonChecked=1;
        
        if (tag == 1)
        {
            _RECiexcheck=YES;
            [self.IEXBtnpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            
            RECtype=@"IEX";
        }
        else
        {
            _RECiexcheck=NO;
            
            [self.PXILbtnpro  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            RECtype=@"PXIL";
        }}}

- (IBAction)BUYButton:(id)sender {
    
    
    UIButton *btn=(UIButton *)sender;
    int tag=(int)[btn tag];
    
    
    if (BuyButtonChecked > 0)
        
    {
        BuyButtonChecked=1;
        if (BuyChecked) {
            BuyChecked=NO;
            
            
            [self.BUYPro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            [self.SELLBtnpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            RECordernature=@"SELL";
        }
        else
        {
            BuyChecked=YES;
            [self.BUYPro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            [self.SELLBtnpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
            RECordernature=@"BUY";
        }
    }
    else
    {
        BuyButtonChecked=1;
        
        if (tag == 1)
        {
            BuyChecked=YES;
            [self.BUYPro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            RECordernature=@"BUY";
        }
        else
        {
            BuyChecked=NO;
            
            [self.SELLBtnpro  setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
            RECordernature=@"SELL";
        }}}



#pragma mark - Table View Delegate and datasource


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.TableviewDate)
    {
        UITableViewCell *cell = [self.TableviewDate cellForRowAtIndexPath:indexPath];
        _lbldate.text = cell.textLabel.text;
        
        self.TableviewDate.hidden = YES;
        
        strSelectedDate = [NSString stringWithString:[[arrRECBidDate objectAtIndex:indexPath.row] objectForKey:@"date"]];
    }}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView==self.TableviewDate)
        
    {
        return 1;
    }
    else
        
    {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.TableviewDate)
        
    {
        return [arrRECBidDate count];
    }
    
    else
    {
        if (section == 0)
        {
            return 1;
        }else{
            return [self.savedataTable count];
        }
    }
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == self.TableviewDate)
    {
        
        RECDateTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellDate"];
        cell.textLabel.text = [NSString stringWithString:[[arrRECBidDate objectAtIndex:indexPath.row] objectForKey:@"printDate"]];
        return cell;
    }
    
    
    
    else
    {
        if (indexPath.section==0)
        {
            static NSString *simpleTableIdentifier = @"HeaderCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
            return cell;
        }else{
            static NSString *simpleTableIdentifier = @"CellREC";
            RECTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
            
            NSMutableDictionary *dict=[_savedataTable objectAtIndex:indexPath.row];
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.lblSerialNorec.text=[NSString stringWithFormat:@"%d",(int)indexPath.row+1];
            
            cell.lblNoOfRec.text=[NSString stringWithFormat:@"%d",[[dict objectForKey:@"noofrec"]intValue]];
            
            cell.lblrectype.text=[NSString stringWithFormat:@"%@",[dict objectForKey:@"rectype"]];
            cell.lblPricerec.text=[NSString stringWithFormat:@"%d",[[dict objectForKey:@"price"]intValue]];
            
            cell.btnCrossrec.tag=indexPath.row;
            [cell.btnCrossrec addTarget:self action:@selector(Crossrecbtn:) forControlEvents:UIControlEventTouchUpInside];
            
            
            return cell;
        }
    }}

-(void)Crossrecbtn:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int tag=(int)btn.tag;
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Bid"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Edit"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                              NSMutableDictionary *dictt=[self.updateobject objectAtIndex:tag];
                                                              
                                                              
                                                              UpobjectREC = 1;
                                                              _POPUPRECView.hidden=NO;
                                                              
                                                              _txtRupeesREC.text=[dictt objectForKey:@"price"];
                                                              _txtblockREC.text=[dictt objectForKey:@"noofrec"];
                                                              _idd =[dictt objectForKey:@"id"];
                                                              
                                                              
                                                              
                                                              
                                                              if ([[dictt objectForKey:@"rectype"] isEqualToString:@"SOLAR"])
                                                                  
                                                              {
                                                                  [self.SolarPro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
                                                                  [self.NonSolarpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
                                                                  solarsalect=@"SOLAR";
                                                                  
                                                              }
                                                              else
                                                              {
                                                                  [self.SolarPro  setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
                                                                  [self.NonSolarpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
                                                                  solarsalect=@"NONSOLAR";
                                                                  
                                                              }
                                                              
                                                              
                                                          }];
    
    
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Delete"
                                                           style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PTS"
                                                                                                                              message:@"Are you sure want to delete bid?"
                                                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                                               
                                                               UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                                                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                         
                                                                                                                         NSMutableDictionary *dictta=[_updateobject objectAtIndex:tag];
                                                                                                                         [self deleteRECBid:dictta index:tag];
                                                                                                                         
                                                                                                                         //[SVProgressHUD showWithStatus:@"Please wait.."];
                                                    [CustmObj addHud:self.view];
                                                                                                                         
                                                                                                                     }];
                                                               
                                                               UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                                                                                       style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                           
                                                                                                                       }];
                                                               
                                                               [alert addAction:thirdAction];
                                                               [alert addAction:thirdActionNO];
                                                               [self presentViewController:alert animated:YES completion:nil];
                                                               
                                                           }];
    
    
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                              
                                                              
                                                          }];
    
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:thirdAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)deleteRECBid:(NSMutableDictionary *)dictBid index:(int)tag
{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictBid objectForKey:@"id"],@"id",RECtype,@"type",RECordernature,@"ordernature",strSelectedDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/rec/newbid/deletebid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        
        
        
        
        //
        if (type == kResponseTypeFail)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if (arrResponsedata)
            {
                
                if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                {
                    [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"msg"] ];
                    [self initialRECServiceHitData];
                    
                   // [self initialRECServiceHitData];
                    
                }
                else
                {
                    [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"msg"]];
                }
                
                
            }
            else
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
            
            
        }
        
    }];
    
}


- (IBAction)RECGoButton:(id)sender {
    
    if (_RECCIEXButtonChecked >  0 && BuyButtonChecked > 0 && ![self.lbldate.text isEqualToString:@"Select Trading Date"])
    {
        [self initialRECServiceHitData];
        _BtnNewBid.hidden = NO;
       // [SVProgressHUD showWithStatus:@"Please wait..."];
        [CustmObj addHud:self.view];
    }
    else if (_RECCIEXButtonChecked ==  0 && BuyButtonChecked == 0 && [self.lbldate.text isEqualToString:@"Select Trading Date"])
    {
        [SVProgressHUD showInfoWithStatus:@"Please fill all details."];
        return;
    }else
    {
        if (_RECCIEXButtonChecked == 0)
        {
            [SVProgressHUD showInfoWithStatus:@"Please select type."];
            return;
        }
        else if(BuyButtonChecked == 0)
        {
            [SVProgressHUD showInfoWithStatus:@"Please select nature."];
            return;
            
        }else if ([self.lbldate.text isEqualToString:@"Select Trading Date"]){
            [SVProgressHUD showInfoWithStatus:@"Please select trading date."];
            return;
        }
    }
}
- (IBAction)NEWBid:(id)sender
{
    _POPUPRECView.hidden=NO;
    self.txtRupeesREC.text = nil;
    self.txtblockREC.text = nil;
    
}


-(void)initialRECServiceHitData
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:RECtype,@"type",RECordernature,@"ordernature",strSelectedDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/rec/newbid/getrecbiddetail.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        
        
        _updateobject=response;
        
        
        
        
        if (type == kResponseTypeFail)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            
            NSMutableArray *arrResponsedata=(NSMutableArray *)response;
            
            if ([arrResponsedata count]>0)
                
            {
                
                NSMutableDictionary *dictt=[arrResponsedata objectAtIndex:0];
                
                
                if ([dictt count]>0)
                {
                    
                    
                    self.BtnSubmitbid.hidden=NO;
                    _TableViewREC.hidden = NO;
                    
                    
                    if ([[dictt objectForKey:@"status"] isEqualToString:@"TRUE"])
                    {
                        
                        [self.BtnSubmitbid setImage:[UIImage imageNamed:@"CancelBidButton"] forState:UIControlStateNormal];
                        //
                        CancellREC =1;
                        
                    }
                    else
                    {
                        
                        // [self.btnSubmitted setAlpha:0.0f];
                        [self.BtnSubmitbid setImage:[UIImage imageNamed:@"SubmitBid"] forState:UIControlStateNormal];
                        CancellREC =0;
                        //
                        
                    }
                }
                else
                {
                    
                }
            }
            else
            {
                self.BtnSubmitbid.hidden=YES;
                [self.TableViewREC setHidden:YES];
            }
            
            
            _savedataTable=response;
            [_TableViewREC reloadData];
            //_TableViewREC.hidden = NO;
            
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            
            
        }
        //[SVProgressHUD dismiss];
        [CustmObj removeHud:self.view];
    }];
}



-(IBAction)CancellRECBid:(id)sender
{
    if (CancellREC == 0)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PTS"
                                                                       message:@"Are you sure want to Submit all bid?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  
                                                                  
                                                                  [self SumbitREChit];
                                                                  
                                                              }];
        
        UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                    
                                                                }];
        
        [alert addAction:thirdAction];
        [alert addAction:thirdActionNO];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PTS"
                                                                       message:@"Are you sure want to cancel all bid?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  // NSLog(@"You pressed button three");
                                                                  
                                                                  [self CancellAllBid];
                                                                  
                                                              }];
        
        UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                    
                                                                }];
        
        [alert addAction:thirdAction];
        [alert addAction:thirdActionNO];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}



- (IBAction)SumbitREChit
{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:RECtype,@"type",RECordernature,@"ordernature",strSelectedDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/rec/newbid/submitrecbid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        
        
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            self.view.userInteractionEnabled=YES;
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if (arrResponsedata)
            {
                
                if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                {
                    [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"msg"] ];
                    
                    isSubmitedButton=NO;
                    [self.BtnSubmitbid setImage:[UIImage imageNamed:@"CancelBidButton"] forState:UIControlStateNormal];
                    CancellREC = 1;
                    
                    
                }
                else
                {
                    [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"msg"]];
                    
                    [self.BtnSubmitbid setImage:[UIImage imageNamed:@"SubmitBid"] forState:UIControlStateNormal];
                    CancellREC =0;
                    
                }
                
                
            }
            else
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
            //
        }
        
        
    }];
    
}




-(void)CancellAllBid
{
    NSString *strDeleteItems=@"";
    
    for (int i=0; i < [_updateobject count]; i++) {
        NSDictionary *dictTemp=[_updateobject objectAtIndex:i];
        
        strDeleteItems = [strDeleteItems stringByAppendingString:[dictTemp objectForKey:@"id"]];
        
        if(i<([_updateobject count]-1)){
            strDeleteItems = [strDeleteItems stringByAppendingString:@","];
        }
        
    }
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strDeleteItems,@"id",RECtype,@"type",RECordernature,@"ordernature",strSelectedDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/rec/newbid/deletebid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        
        
        if (type == kResponseTypeFail)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if (arrResponsedata)
            {
                
                if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                {
                    [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"msg"] ];
                    [self.BtnSubmitbid setImage:[UIImage imageNamed:@"SubmitBid"] forState:UIControlStateNormal];
                    CancellREC =0;
                    [self initialRECServiceHitData];
                    
                    _TableViewREC.hidden = YES;
                    
                }
                else
                {
                    [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"msg"]];
                    
                    [self.BtnSubmitbid setImage:[UIImage imageNamed:@"CancelBidButton"] forState:UIControlStateNormal];
                    CancellREC =1;
                }
                
                
            }
            else
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
            
        }
        
    }];
    
}




#pragma mark RECPpoView Property.
- (IBAction)SolarbuttonREC:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    int tag=(int)[btn tag];
    if (tag == 1)
    {
        [self.SolarPro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        [self.NonSolarpro setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        solarsalect=@"SOLAR";
        
//        NSLog(@"%@", [[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"solor.min"]);
        
        
        [self.lblMinValue setText:[NSString stringWithFormat:@"%@%@",@"Min :",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"solor.min"]]];

        [self.lblMaxValue setText:[NSString stringWithFormat:@"%@%@",@"Max :",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"solor.max"]]];
        
    }
    else
    {     if (tag == 2)
        
        [self.SolarPro  setImage:[UIImage imageNamed:@"uncheckedImg"] forState:UIControlStateNormal];
        [self.NonSolarpro setImage:[UIImage imageNamed:@"checkedImg"] forState:UIControlStateNormal];
        solarsalect=@"NONSOLAR";
        [self.lblMinValue setText:[NSString stringWithFormat:@"%@%@",@"Min :",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"nonsolor.min"]]];
        [self.lblMaxValue setText:[NSString stringWithFormat:@"%@%@",@"Max :",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"nonsolor.max"]]];
    }
    
}

// text field delegate-------------------------------------

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _txtRupeesREC)
    {
        int limit = 3;
        return !([textField.text length]>limit && [string length] > range.length);
    }
    else if (textField == _txtblockREC)
    {
        int limit = 6;
        return !([textField.text length]>limit && [string length] > range.length);
    }
    return textField;
}


-(IBAction)savRECBidButtonClick:(id)sender

{
    [self.txtRupeesREC resignFirstResponder];
    [self.txtblockREC resignFirstResponder];
    
    //Checking all values are filled
    
    if ([self.txtblockREC.text integerValue] > 1000000  || [self.txtblockREC.text integerValue] < 0)
    {
        [SVProgressHUD showInfoWithStatus:@"Number of Rec should be greater than 0 and less than 1000000."];
        return;
    }
    
    if ([self.txtRupeesREC.text isEqualToString:@""] && [self.txtblockREC.text isEqualToString:@""] && solarsalect == nil)
    {
        [SVProgressHUD showInfoWithStatus:@"Please fill all data"];
        return;
    }
    
    //Checking REC Type selected or not
    if(solarsalect == nil){
        [SVProgressHUD showInfoWithStatus:@"Please select type"];
        return;
    }
    
    //Checking price entered or not
    if ([self.txtRupeesREC.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"Please fill the price."];
        return;
    }
    else
    {
        //Checking price range for SOLAR
    if ([solarsalect isEqualToString:@"SOLAR"]) {
    if ([self.txtRupeesREC.text integerValue] < [[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"solor.min"] integerValue] || [self.txtRupeesREC.text integerValue] > [[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"solor.max"] integerValue] )
            {
                
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@%@%@%@",@"Price should be less than or equal to ",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"solor.max"],@" and greater than or equal to ",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"solor.min"]]];
                
                return;
            }
            //Checking price range for NON-SOLAR
        }else if ([solarsalect isEqualToString:@"NONSOLAR"])
        {
            if ([self.txtRupeesREC.text integerValue] < [[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"nonsolor.min"] integerValue] || [self.txtRupeesREC.text integerValue] > [[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"nonsolor.max"] integerValue])
            {
                
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@%@%@%@",@"Price should be less than or equal to ",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"nonsolor.max"],@" and greater than or equal to ",[[AppHelper userValueForKey:@"RECprice"] valueForKeyPath:@"nonsolor.min"]]];                return;
            }
        }
    }
    
    //Checking no of REC Block
    if ([self.txtblockREC.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"Please fill the no. of rec."];
        return;
    }else{
        //Checking bid range for no of rec
        if ([self.txtblockREC.text isEqualToString:@"0"]) {
            [SVProgressHUD showInfoWithStatus:@"No of rec must be greater than 0."];
            return;
        }
    }
    
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    if (UpobjectREC == 0) {
        
        dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:RECtype,@"type",solarsalect,@"rectype",RECordernature,@"ordernature",strSelectedDate,@"date",_txtRupeesREC.text,@"price",_txtblockREC.text,@"noofrec",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    }
    else
    {
        
        dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:_idd,@"id",RECtype,@"type",solarsalect,@"rectype",RECordernature,@"ordernature",strSelectedDate,@"date",_txtRupeesREC.text,@"price",_txtblockREC.text,@"noofrec",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    }
    
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    // NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/rec/newbid/savebid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        
        
        if (type == kResponseTypeFail)
        {
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if (arrResponsedata)
            {
                
                if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                {
                    [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"msg"] ];
                    [self initialRECServiceHitData];
                    _POPUPRECView.hidden = YES;
                    _BtnSubmitbid.hidden=NO;
                    
                    _txtRupeesREC.text = nil;
                    _txtblockREC.text = nil;
                    
                    
                    
                    //                         NSLog(@"string %@",response);
                    
                }
                else
                {
                    [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"msg"]];
                }
                
                [CustmObj removeHud:self.view];
            }
            else
            {
                //[SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
            
        }
        
        
    }];
}

-(IBAction)RECCancelBtnpopclick:(id)sender
{
    [self.txtRupeesREC resignFirstResponder];
    [self.txtblockREC resignFirstResponder];
    
    _POPUPRECView.hidden = YES;
}


-(void)dismissKeyboard
{
    [_txtRupeesREC resignFirstResponder];
    [_txtblockREC resignFirstResponder];
}



@end
