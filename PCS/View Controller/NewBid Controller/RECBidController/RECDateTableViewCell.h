//
//  RECDateTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 24/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RECDateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end
