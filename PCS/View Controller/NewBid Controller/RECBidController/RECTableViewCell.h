//
//  RECTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 24/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RECTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UILabel *lblSerialNorec;
@property(weak,nonatomic)IBOutlet UILabel *lblrectype;
@property(weak,nonatomic)IBOutlet UIButton *btnCrossrec;
@property(weak,nonatomic)IBOutlet UILabel *lblPricerec;
@property(weak,nonatomic)IBOutlet UILabel *lblNoOfRec;
@end
