//
//  NewBidMainViewController.m
//  PCS
//
//  Created by pavan yadav on 26/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "NewBidMainViewController.h"
#import "NewBidViewController.h"
#import "RECNewBidView.h"
#import "TAMBidViewController.h"
#import "AppDelegate.h"
#import "CollectionViewCell.h"
#import "CustmObj.h"
@class  TAMBidViewController;
@interface NewBidMainViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIViewController *currentViewController;
    NSMutableArray *arrayData;
  
}
@property (strong, nonatomic)NewBidViewController *nBidView;
@property (strong, nonatomic)RECNewBidView *recBideView;
@property (strong,nonatomic) TAMBidViewController *vcTAMBidView;
@property (strong,nonatomic) TAMBidViewController *escertBidView;

@property(weak,nonatomic)IBOutlet UICollectionView *CollectionView;
@property (weak, nonatomic) IBOutlet UIView *containerMain;
@property (weak, nonatomic) IBOutlet UIButton *btnDAM;
@property (weak, nonatomic) IBOutlet UIButton *btnTAM;
@property (weak, nonatomic) IBOutlet UIButton *btnREC;

@property (nonatomic, assign) int selectedRow;
@end

@implementation NewBidMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [CustmObj addHud:self.view];
    self.nBidView = [self.storyboard instantiateViewControllerWithIdentifier:@"DAMNewBidId"];
    self.recBideView = [self.storyboard instantiateViewControllerWithIdentifier:@"RECNewBid"];
    self.vcTAMBidView = [self.storyboard instantiateViewControllerWithIdentifier:@"TamBidViewController"];
    self.escertBidView = [self.storyboard instantiateViewControllerWithIdentifier:@"escert"];
    
    
    arrayData = [[NSMutableArray alloc]initWithObjects:@"DAM",@"TAM",@"REC",@"ESCERTS", nil];
    
    
    self.nBidView.dictEitcontainerValue = _dictEdit;
    self.nBidView.selectionType = _TypeSelection;
    [self hideContentController:currentViewController];
    [self displayContentController:self.nBidView];
    
    currentViewController = self.nBidView;
    [CustmObj removeHud:self.view];
    
   // [self btnDAM_Click:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btnLogout_Click:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)btnBack_Click:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Adding/Hiding child view controller

- (void) hideContentController: (UIViewController*) content
{
    [content willMoveToParentViewController:nil];
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
}

- (void) displayContentController: (UIViewController*) content
{
    [self addChildViewController:content];
    content.view.frame = CGRectMake(0, 0, self.containerMain.frame.size.width, self.containerMain.frame.size.height);
    [self.containerMain addSubview:content.view];
    [content didMoveToParentViewController:self];
}


#pragma mark Collection view Delegate and datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayData count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"collectionCell";
    
    CollectionViewCell *cell = (CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.labelText.text = [arrayData objectAtIndex:indexPath.row];
    
    if (indexPath.row == _selectedRow)
    {
        cell.labelText.backgroundColor = [UIColor colorWithRed:33.0f/255.0f green:145.0f/255.0f blue:135.0f/255.0f alpha:1.0];
    }
    else
    {
            cell.labelText.backgroundColor = [UIColor darkGrayColor];
    }

    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    if (indexPath.row == 0)
    {
        [CustmObj addHud:self.view];
        self.nBidView.dictEitcontainerValue = _dictEdit;
        self.nBidView.selectionType = _TypeSelection;
        
        [self hideContentController:currentViewController];
        [self displayContentController:self.nBidView];
        currentViewController = self.nBidView;
        [CustmObj removeHud:self.view];
        _selectedRow = 0;
        
    }
    else if (indexPath.row == 1)
    {
        [CustmObj addHud:self.view];
        [self hideContentController:currentViewController];
        [self displayContentController:self.vcTAMBidView];
        currentViewController = self.vcTAMBidView;
        [CustmObj removeHud:self.view];
        _selectedRow = 1;
        
    }
    else if (indexPath.row == 2)
    {
        [CustmObj addHud:self.view];
        [self hideContentController:currentViewController];
        [self displayContentController:self.recBideView];
        currentViewController = self.recBideView;
        [CustmObj removeHud:self.view];
        _selectedRow = 2;
    }
    else
    {
        [CustmObj addHud:self.view];
        [self hideContentController:currentViewController];
        [self displayContentController:self.escertBidView];
        currentViewController = self.escertBidView;
        [CustmObj removeHud:self.view];
        _selectedRow = 3;
    }
    
    [_CollectionView reloadData];
    
}



@end
