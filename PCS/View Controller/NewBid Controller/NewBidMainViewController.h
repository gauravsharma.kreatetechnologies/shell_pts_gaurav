//
//  NewBidMainViewController.h
//  PCS
//
//  Created by pavan yadav on 26/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBidMainViewController : UIViewController

@property(strong, nonatomic) NSDictionary *dictEdit;
@property(strong, nonatomic) NSString *TypeSelection;
@end
