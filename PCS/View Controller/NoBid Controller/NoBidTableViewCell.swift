//
//  NoBidTableViewCell.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/17/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class NoBidTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    var completion:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBAction func btnDelete(_ sender: UIButton) {
        self.completion?()
    }
    
}
