//
//  NoBIdViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/17/22.
//  Copyright © 2022 lab4code. All rights reserved.
//
import SwiftyJSON
import Alamofire
import UIKit
import IQKeyboardManagerSwift

class NoBIdViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewProduct: UIView!
    @IBOutlet weak var txtProductType: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var viewBid:UIView!
    @IBOutlet weak var btnIEX:UIButton!
    @IBOutlet weak var btnPXIL:UIButton!
    
    var pickerView = ToolbarPickerView()
    var sellerTypeArray:[BuyerType] = []
    var nobidData = [NoBidData]()
    var index:Int?
    var productType:String?
    var dateFrom:String?
    var platformType:String?
    let platform: RadioButtonController = RadioButtonController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtProductType.delegate =  self
        self.txtDate.delegate = self
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtProductType.inputView =  pickerView
        self.pickerView.toolbarDelegate = self
        self.viewBid.isHidden = true
        self.tableView.isHidden = true
        platform.buttonsArray = [btnIEX,btnPXIL]

        self.txtProductType.inputAccessoryView = self.pickerView.toolbar
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.sellerTypeArray.insert(BuyerType(key: "Dam", value: "DAM"), at: 0)
        self.sellerTypeArray.insert(BuyerType(key: "Rec", value: "REC"), at: 1)
        self.sellerTypeArray.insert(BuyerType(key: "Escerts", value: "ESCERTS"), at: 2)
        self.sellerTypeArray.insert(BuyerType(key: "Gdam", value: "GDAM"), at: 3)
        self.sellerTypeArray.insert(BuyerType(key: "Tam", value: "TAM"), at: 4)
        self.sellerTypeArray.insert(BuyerType(key: "Gtam", value: "GTAM"), at: 5)
        self.txtDate.setInputViewDatePicker(target: self, selector: #selector(dateToPressed))
        self.roundCorner(self.viewProduct)
    }
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }
    @objc func dateToPressed() {
        if let  datePicker = self.txtDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.txtDate.text = dateFormatter.string(from: datePicker.date)
            self.dateFrom =  formatter.string(from: datePicker.date)
            print(dateFrom)
        }
        self.txtDate.resignFirstResponder()
    }
    @IBAction func btnPXILAction(_ sender: UIButton) {
        platform.buttonArrayUpdated(buttonSelected: sender)
        self.platformType = "PXIL"
    }
    @IBAction func btnIEXLAction(_ sender: UIButton) {
        platform.buttonArrayUpdated(buttonSelected: sender)
        self.platformType = "IEX"
    }
    func callCommongetNoBidData(buyer:String)
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")

        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["product"] = buyer
        param["client_id"] = client_id
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: nobidcommongetNoBidData, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(NoBidDataModel.self, from: productData)
                   print(decodedData)
                    self.nobidData = decodedData.value ?? []
                    self.tableView.isHidden = self.nobidData.count == 0 ? true:false
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func callAddNoBidData()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")

        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["product"] = self.productType
        param["client_id"] = client_id
        param["type"] = self.platformType
        param["date"] = self.dateFrom
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: nobidcommonaddNoBidData, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(AddNoBidDataModel.self, from: productData)
                   print(decodedData)
                    if decodedData.status == "SUCCESS"{
                        self.showToast(controller: self, message: decodedData.msg ?? "", seconds: 2)
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func callDeleteNoBidData(id:String)
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")

        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["product"] = self.productType
        param["client_id"] = client_id
        param["type"] = self.platformType
        param["date"] = self.dateFrom
        param["id"] = id
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: nobidcommondeleteNoBidData, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(AddNoBidDataModel.self, from: productData)
                   print(decodedData)
                    if decodedData.status == "SUCCESS"{
                        self.showToast(controller: self, message: decodedData.msg ?? "", seconds: 2)
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func alert(id:String){
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: "Are you sure want to delete", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default) {
            UIAlertAction in
            print("Delete Pressed")
            self.callDeleteNoBidData(id:id)
            self.nobidData.remove(at: self.index ?? 0)
            self.tableView.isHidden = self.nobidData.count == 0 ? true:false
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }

        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            print("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnPlaceNoBid(_ sender: UIButton) {
        if  txtProductType.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Product Type")
            return
        }else if self.txtDate.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Date")
            return
        }
        self.callAddNoBidData()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension NoBIdViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return sellerTypeArray.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return sellerTypeArray[row].key
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        txtProductType.text =  sellerTypeArray[row].key
        self.productType =  sellerTypeArray[row].value ?? ""
        if self.productType == "DAM" || self.productType == "REC" || self.productType == "GDAM"{
            self.viewBid.isHidden = false
        }else{
            self.viewBid.isHidden = true
        }
        if let pType = productType{
            self.callCommongetNoBidData(buyer:pType)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
extension NoBIdViewController:ToolbarPickerViewDelegate{
    func didTapDone() {
        if txtProductType.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtProductType.text = sellerTypeArray[row].key
            self.productType =  sellerTypeArray[row].value ?? ""
            self.txtProductType.resignFirstResponder()
            if self.productType == "DAM" || self.productType == "REC" || self.productType == "GDAM"{
                self.viewBid.isHidden = false
            }else{
                self.viewBid.isHidden = true
            }
        }
    }
    
    func didTapCancel() {
        if txtProductType.isFirstResponder{
            self.txtProductType.text = nil
            self.txtProductType.resignFirstResponder()
        }
    }
}

extension NoBIdViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableView{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            return 1;
        }
        
        else
        {
            return self.nobidData.count
        }

       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 0)
        {
            return 40;
        }else{
            return 50;
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if (indexPath.section==0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoBidHeaderCell")!
            return cell
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "NoBidTableViewCell", for: indexPath)as! NoBidTableViewCell
            cell.selectionStyle = .none
            let data =  self.nobidData[indexPath.row]
            cell.lblNo.text =  data.id
            cell.lblDate.text =  data.date
            self.index = indexPath.row
            
            cell.completion = {
                if let id = data.id{
                    self.alert(id:id)
                }
            }
            return cell
        }
    }
    
    
}
