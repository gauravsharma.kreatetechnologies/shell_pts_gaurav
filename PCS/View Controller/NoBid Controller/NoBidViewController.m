//
//  NoBidViewController.m
//  PCS
//
//  Created by lab4code on 29/12/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import "NoBidViewController.h"
#import "NoBidTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "RECDateCellTbl.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "CustmObj.h"

@interface NoBidViewController ()
{

NSMutableArray *recdatepri;   //table date object use
NSMutableArray *dateindexget;   //date responce object
NSString *daetvalueatindex;  //at index get adet

NSString *APIUrl;
NSString *salecttradingdate;
NSString *message;
NSString *strSelectedBidType;
NSString *dateapiUrl;
NSString * differTable;
    
}




@property(weak,nonatomic)IBOutlet UILabel *lblTradingdate;
@property (weak, nonatomic) IBOutlet UILabel *DamNoBidLabel;

@property (weak, nonatomic) IBOutlet UIView *viewNoData;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;

@property (weak, nonatomic) IBOutlet UIImageView *imagetradingdatedam;
@property(weak,nonatomic)IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UITableView *DatetableRec;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

@property(strong,nonatomic)NSMutableArray *arrData;
@property(strong,nonatomic)NSMutableArray *arrEscertData;
@property(strong,nonatomic)NSString *strSaveDate;

#pragma mark - RECView pro.

@property (weak, nonatomic) IBOutlet UIButton *DmButtonpro;
@property (weak, nonatomic) IBOutlet UIButton *RECButtonpro;
@property (weak, nonatomic) IBOutlet UIButton *escertsButoon;
@property (weak, nonatomic) IBOutlet UIButton *btndatedam;


@end

@implementation NoBidViewController

- (void)viewDidLoad
{
[super viewDidLoad];
[self DmButton];

UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideDateTableView)];

[self.myTableView addGestureRecognizer:tap];

recdatepri=[[NSMutableArray alloc] init];
dateindexget=[[NSMutableArray alloc]init];



self.datePicker.datePickerMode=UIDatePickerModeDate;
self.datePicker.minimumDate=[NSDate date];

_DatetableRec.hidden=YES;

self.tableHeightConstraint.constant= - (self.datePickerView.frame.size.height+80);
[self.datePickerView needsUpdateConstraints];


UITapGestureRecognizer *dobTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dobTap:)];
dobTap.numberOfTapsRequired=1;
[self.lblTradingdate addGestureRecognizer:dobTap];
}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
// Dispose of any resources that can be recreated.
}

-(void)hideDateTableView
{
[self.DatetableRec setHidden:YES];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
[self.DatetableRec setHidden:YES];
}

-(IBAction)menuButtonClick:(id)sender
{
//[SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
[self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)DmButton
{
self.arrData = nil;
self.arrEscertData = nil;
differTable = @"DAMTABLE";
strSelectedBidType = @"DAM";

_RECButtonpro.backgroundColor =[UIColor colorWithRed:78.0f/255.0f green:83.0f/255.0f blue:87.0f/255.0f alpha:1.0f];
_DmButtonpro.backgroundColor = [UIColor colorWithRed:41.0f/255.0f green:171.0f/255.0f blue:162.0f/255.0f alpha:1.0f];
_escertsButoon.backgroundColor =[UIColor colorWithRed:78.0f/255.0f green:83.0f/255.0f blue:87.0f/255.0f alpha:1.0f];

_DamNoBidLabel.text=@"DAM NO BID";

//Hiding the REC Elements

_btnrecdate.hidden=YES;
_DatetableRec.hidden=YES;

//Showing the DAM Elements
_imagetradingdatedam.hidden=NO;
_btndatedam.hidden=NO;

APIUrl =@"service/nobid/get_nobid.php";
[self initialServiceHitData];

}



- (IBAction)RECButton:(id)sender
{


differTable = @"DAMTABLE";
self.arrData = nil;
self.arrEscertData = nil;
strSelectedBidType = @"REC";
self.lblTradingdate.text=@"SELECT TRADING DATE";


_DamNoBidLabel.text=@"REC NO BID";
_DmButtonpro.backgroundColor =[UIColor colorWithRed:78.0f/255.0f green:83.0f/255.0f blue:87.0f/255.0f alpha:1.0f];
_RECButtonpro.backgroundColor = [UIColor colorWithRed:41.0f/255.0f green:171.0f/255.0f blue:162.0f/255.0f alpha:1.0f];
_escertsButoon.backgroundColor =[UIColor colorWithRed:78.0f/255.0f green:83.0f/255.0f blue:87.0f/255.0f alpha:1.0f];


_imagetradingdatedam.hidden=YES;
_btndatedam.hidden=YES;


_btnrecdate.hidden=NO;

dateapiUrl = @"service/rec/getallrectradingdate.php";
APIUrl =@"service/rec/nobid/getrecdatedetails.php";
//    apiurlGo=@"service/rec/nobid/saverecnobid.php";
//    apiurlDelete=@"service/rec/nobid/deletenobid.php";
//    message=@"msg";
//

[self HiteDateApi];
[self initialServiceHitData];

}




//PRGMA MARK:- ESCERTS MODULE...........
- (IBAction)escertButton:(UIButton *)sender
{
differTable = @"ESCERTS";
_arrData = nil;
_arrEscertData = nil;
_DmButtonpro.backgroundColor =[UIColor colorWithRed:78.0f/255.0f green:83.0f/255.0f blue:87.0f/255.0f alpha:1.0f];
_RECButtonpro.backgroundColor = [UIColor colorWithRed:78.0f/255.0f green:83.0f/255.0f blue:87.0f/255.0f alpha:1.0f];
_escertsButoon.backgroundColor =  [UIColor colorWithRed:41.0f/255.0f green:171.0f/255.0f blue:162.0f/255.0f alpha:1.0f];

self.arrData = nil;

strSelectedBidType = @"ESCERTS";
self.lblTradingdate.text=@"SELECT TRADING DATE";
_DamNoBidLabel.text=@"ESCERTS NO BID";

_imagetradingdatedam.hidden=YES;
_btndatedam.hidden=YES;
_btnrecdate.hidden=NO;

dateapiUrl = @"service/escert/getallescerttradingdate.php";
[self HiteDateApi];
[self escertHit];
}






#pragma mark-REC View..

-(void)HiteDateApi
{

NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];

NSError * err;
NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];



NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];

[[Services sharedInstance]serviceCallbyPost:dateapiUrl param:dictFinaldata andCompletion:^(ResponseType type, id response)
 {
     
     if (type == kResponseTypeFail)
     {
         //[SVProgressHUD dismiss];
         [CustmObj removeHud:self.view];
         
         NSError *error=(NSError*)response;
         
         if (error.code == -1009) {
             
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                            message:@"The Internet connection appears to be offline."
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                   }];
             
             [alert addAction:thirdAction];
             
         }
         
     }
     else if (type == kresponseTypeSuccess)
     {
         recdatepri=[response valueForKeyPath:@"printDate"];
         dateindexget=response;
         [_DatetableRec reloadData];
         
     }
     
 }];
}



-(void)initialServiceHitData
{

    
    
//[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];


NSError * err;
NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];

// NSLog(@"string %@",myString);

NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];


[[Services sharedInstance]serviceCallbyPost:APIUrl param:dictFinaldata andCompletion:^(ResponseType type, id response)
 {
     
     // NSLog(@"Response %@",response);
     
     if (type == kResponseTypeFail)
     {
         //[SVProgressHUD dismiss];
         [CustmObj removeHud:self.view];
         
         NSError *error=(NSError*)response;
         NSLog(@"pkkkkk");
         if (error.code == -1009)
         {
             
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                            message:@"The Internet connection appears to be offline."
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                       
                                                                       // NSLog(@"You pressed button three");
                                                                   }];
             
             [alert addAction:thirdAction];
             [self presentViewController:alert animated:YES completion:nil];
         }
         
         
         
     }
     else if (type == kresponseTypeSuccess)
     {
         
         NSArray *arrResponsedata=(NSArray *)response;
         if ([arrResponsedata count]>0)
         {
             self.arrData=[arrResponsedata mutableCopy];
             [self.myTableView reloadData];
             [self.viewNoData setHidden:YES];
             [self.myTableView setHidden:NO];
             //[SVProgressHUD dismiss];
             [CustmObj removeHud:self.view];
         }
         else
         {
             [self.viewNoData setHidden:NO];
             [self.myTableView setHidden:YES];
             //[SVProgressHUD dismiss];
             [CustmObj removeHud:self.view];
         }
     }
 }];
}




-(void)escertHit
{

//[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];


NSError * err;
NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];

// NSLog(@"string %@",myString);

NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];


[[Services sharedInstance]serviceCallbyPost:@"service/escert/nobid/getescertdatedetails.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
 {
     
     // NSLog(@"Response %@",response);
     
     if (type == kResponseTypeFail)
     {
        // [SVProgressHUD dismiss];
         [CustmObj removeHud:self.view];
         
         NSError *error=(NSError*)response;
         
         if (error.code == -1009) {
             
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                            message:@"The Internet connection appears to be offline."
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                       
                                                                       // NSLog(@"You pressed button three");
                                                                   }];
             
             [alert addAction:thirdAction];
             [self presentViewController:alert animated:YES completion:nil];
         }
         
         
         
     }
     else if (type == kresponseTypeSuccess)
     {
         
         NSArray *arrResponsedata=(NSArray *)[response valueForKeyPath:@"NoBid"];
         if ([arrResponsedata count]>0)
         {
             self.arrEscertData=[arrResponsedata mutableCopy];
             [self.myTableView reloadData];
             [self.viewNoData setHidden:YES];
             [self.myTableView setHidden:NO];
             //[SVProgressHUD dismiss];
             [CustmObj removeHud:self.view];
         }
         else
         {
             [self.viewNoData setHidden:NO];
             [self.myTableView setHidden:YES];
            // [SVProgressHUD dismiss];
             [CustmObj removeHud:self.view];
         }
     }
 }];
}



#pragma mark- DAM REC View....

-(IBAction)goButtonClick:(id)sender
{

if (![self.lblTradingdate.text isEqualToString:@"SELECT TRADING DATE"])
{
    
    NSString *strNOBidSaveUrl;
    NSString *strResponseMessageKey;
    
    if([strSelectedBidType isEqualToString:@"DAM"])
    {
        
        APIUrl = @"service/nobid/get_nobid.php";
        strNOBidSaveUrl = @"service/nobid/save_nobid.php";
        strResponseMessageKey = @"message";
        
    }
    else if([strSelectedBidType isEqualToString:@"REC"])
    {
        APIUrl = @"service/rec/nobid/getrecdatedetails.php";
        strNOBidSaveUrl = @"service/rec/nobid/saverecnobid.php";
        strResponseMessageKey = @"msg";
    }
    else if([strSelectedBidType isEqualToString:@"ESCERTS"])
    {
        APIUrl = @"service/escert/nobid/getescertdatedetails.php";
        strNOBidSaveUrl = @"service/escert/nobid/saveescertnobid.php";
        strResponseMessageKey = @"message";
    }
    
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"PTS"
                                                                   message:@"Are you sure want to save no bid?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {

                                     // [SVProgressHUD showWithStatus:@"Please wait .."];
                                      [CustmObj addHud:self.view];
                                      NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:self.strSaveDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];


                                      NSError * err;
                                      NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
                                      NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];

                                      NSLog(@"string %@",myString);
                                      //                                                                  return;
                                      NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];


                                      [[Services sharedInstance]serviceCallbyPost:strNOBidSaveUrl param:dictFinaldata andCompletion:^(ResponseType type, id response)
                                       {

                                          
                                          
                                          
                                           if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                                           {
                                               [SVProgressHUD showErrorWithStatus:@"Network Connection not Available"];
                                           }
                                           else
                                           {

                                               if ([[response objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                                               {
                                                   [SVProgressHUD showSuccessWithStatus:[response objectForKey:strResponseMessageKey]];
//
                        if ([differTable isEqualToString:@"DAMTABLE"])
                                                   {
        [self performSelector:@selector(initialServiceHitData) withObject:nil afterDelay:2.0];
                                                   }
                                                   else
                                                   {
    [self performSelector:@selector(escertHit) withObject:nil afterDelay:2.0];
                                                   }
                                                   
                                                   [CustmObj removeHud:self.view];
                                               }
                                               else
                                               {
                                                   [SVProgressHUD showErrorWithStatus:[response objectForKey:strResponseMessageKey]];
                                                   [CustmObj removeHud:self.view];

                                               }
//
//
                                           }
                                       }];
//
                                  }];
    
    UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                
                                                                // NSLog(@"You pressed button three");
                                                            }];
    
    [alert addAction:thirdAction];
    [alert addAction:thirdActionNO];
    [self presentViewController:alert animated:YES completion:nil];
    
}
else
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Please select trading date"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      
                                      //NSLog(@"You pressed button one");
                                      
                                      
                                      
                                  }];
    
    
    [alert addAction:firstAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

}







- (IBAction)btnrecdate:(id)sender
{


_DatetableRec.hidden=NO;
[_DatetableRec reloadData];
}



#pragma mark - date tap gesture

- (void)dobTap:(UIGestureRecognizer*)gestureRecognizer
{
[UIView animateWithDuration:0.5 animations:^{
    self.tableHeightConstraint.constant=0;
    [self.datePickerView needsUpdateConstraints];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}];
}

- (IBAction)cancelBtnClcik:(id)sender
{

[UIView animateWithDuration:0.5 animations:^{
    
    self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
    [self.datePickerView needsUpdateConstraints];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}];

}

- (IBAction)doneBtnClick:(id)sender
{

NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

[dateFormatter setDateFormat:@"dd-MM-yyyy"];
NSString *dateOfBirth=[dateFormatter stringFromDate:self.datePicker.date];
self.lblTradingdate.text=dateOfBirth;

// NSLog(@"%@",self.lblTradingdate.text);


[dateFormatter setDateFormat:@"yyyy-MM-dd"];
NSString *savedate=[dateFormatter stringFromDate:self.datePicker.date];
self.strSaveDate=savedate;


[UIView animateWithDuration:0.5 animations:^{
    
    
    self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
    [self.datePickerView needsUpdateConstraints];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    
}];
}





-(void)crossButtonClick:(id)sender
{
[self.viewNoData setHidden:YES];

UIButton *btn=(UIButton *)sender;
int tag=(int)btn.tag;

NSMutableDictionary *dictinory=[[NSMutableDictionary alloc]init];

if([strSelectedBidType isEqualToString:@"ESCERTS"])
{
    dictinory = [self.arrEscertData objectAtIndex:tag];
}
else
{
    dictinory=[self.arrData objectAtIndex:tag];
}

NSArray *items = [[dictinory objectForKey:@"date"] componentsSeparatedByString:@"-"];

NSString *strMsg=[NSString stringWithFormat:@"%@ %@",@"Are you sure, you want to cancel NO BID for",[dictinory objectForKey:@"date"]];

if ([items count]==3)
{
    
    NSString *strDate=[NSString stringWithFormat:@"%@-%@-%@",[items objectAtIndex:2],[items objectAtIndex:1],[items objectAtIndex:0]];
    strMsg=[NSString stringWithFormat:@"%@ %@",@"Are you sure, you want to cancel NO BID for",strDate];
}




UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                               message:strMsg
                                                        preferredStyle:UIAlertControllerStyleAlert];

UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Yes"
                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                          
                                                          // NSLog(@"You pressed button one");
                                                          
                                                          [self deleteBid:tag];
                                                          
                                                      }];

UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                           
                                                           //  NSLog(@"You pressed button two");
                                                           
                                                           
                                                           
                                                       }];

[alert addAction:firstAction];
[alert addAction:secondAction];


[self presentViewController:alert animated:YES completion:nil];
}



-(void)deleteBid:(int)tag
{

[self.viewNoData setHidden:YES];
NSString *strDeleteUrl;

if([strSelectedBidType isEqualToString:@"DAM"])
{
    strDeleteUrl =@"service/nobid/delete_nobid.php";
}
else if([strSelectedBidType isEqualToString:@"REC"])
{
    strDeleteUrl =@"service/rec/nobid/deletenobid.php";
}
else if([strSelectedBidType isEqualToString:@"ESCERTS"])
{
    strDeleteUrl =@"service/escert/nobid/deletenobid.php";
}


NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];

if (tag < [self.arrData count])
{
    NSMutableDictionary *dictinory=[self.arrData objectAtIndex:tag];
    
    [dict setObject:[dictinory objectForKey:@"id"] forKey:@"id"];
    [dict setObject:[dictinory objectForKey:@"date"] forKey:@"date"];
    [dict setObject:[AppHelper userDefaultsForKey:@"access_key"] forKey:@"access_key"];
    
}
else  if (tag < [self.arrEscertData count])
{
    NSMutableDictionary *dictinoryESCERTS=[self.arrEscertData objectAtIndex:tag];
    
    [dict setObject:[dictinoryESCERTS objectForKey:@"id"] forKey:@"id"];
    [dict setObject:[dictinoryESCERTS objectForKey:@"date"] forKey:@"date"];
    [dict setObject:[AppHelper userDefaultsForKey:@"access_key"] forKey:@"access_key"];
}

NSError * err;
NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];

NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];




//[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
[[Services sharedInstance]serviceCallbyPost:strDeleteUrl param:dictFinaldata andCompletion:^(ResponseType type, id response)
 {
     
     if (type == kResponseTypeFail)
     {
         // NSLog(@"fail Response:----> %@",response);
         self.view.userInteractionEnabled=YES;
        // [SVProgressHUD dismiss];
         [CustmObj removeHud:self.view];
         
         
         NSError *error=(NSError*)response;
         
         if (error.code == -1009) {
             
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                            message:@"The Internet connection appears to be offline."
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                       
                                                                       // NSLog(@"You pressed button three");
                                                                   }];
             
             [alert addAction:thirdAction];
             [self presentViewController:alert animated:YES completion:nil];
         }
         
         
         
     }
     else if (type == kresponseTypeSuccess)
     {
         
         if ([[response objectForKey:@"status"] isEqualToString:@"SUCCESS"])
         {
             if([strSelectedBidType isEqualToString:@"DAM"])
             {
                 [SVProgressHUD showSuccessWithStatus:[response objectForKey:@"message"]];
                 [self.arrData removeObjectAtIndex:tag];
                 
             }
             else if([strSelectedBidType isEqualToString:@"REC"])
             {
                 
                 [SVProgressHUD showSuccessWithStatus:[response objectForKey:@"msg"]];
                 [self.arrData removeObjectAtIndex:tag];
                 
             }
             else if([strSelectedBidType isEqualToString:@"ESCERTS"])
             {
                 
                 [SVProgressHUD showSuccessWithStatus:[response objectForKey:@"message"]];
                 [self.arrEscertData removeObjectAtIndex:tag];
             }
             
             
             
             if([self.arrData count]>0)
             {
                 [self.myTableView setHidden:NO];
                 [self.myTableView reloadData];
             }
             else if([self.arrEscertData count]>0)
             {
                 [self.myTableView setHidden:NO];
                 [self.myTableView reloadData];
             }
             else
             {
                 [self.myTableView setHidden:YES];
                 [self.viewNoData setHidden:NO];
             }
//              [CustmObj removeHud:self.view];
         }
         else
         {
             [SVProgressHUD showErrorWithStatus:[response objectForKey:@"message"]];
         }
         
          [CustmObj removeHud:self.view];
         
     }
     else
     {
        // [SVProgressHUD dismiss];
         [CustmObj removeHud:self.view];
     }
 }];

}




-(IBAction)btnLogout_Click:(id)sender
{
[AppDelegate logout];
[self.navigationController popToRootViewControllerAnimated:YES];
}







#pragma mark - Table View Delegate and datasource

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
if (tableView == _DatetableRec)
{
    
    UITableViewCell *cell = [self.DatetableRec cellForRowAtIndexPath:indexPath];
    _lblTradingdate.text = cell.textLabel.text;
    
    _DatetableRec.hidden=YES;
    
    
    _strSaveDate =[[dateindexget valueForKeyPath:@"date"] objectAtIndex:(long)indexPath.row];
    
}
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

if(tableView==self.DatetableRec)
{
    return 1;
}
else
{
    return 2;
}
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

if(tableView==self.DatetableRec)
{
    return recdatepri.count;
}
else
{
    if (section == 0)
    {
        return 1;
    }
    
    if ([differTable  isEqual: @"DAMTABLE"])
    {
        return [self.arrData count];
    }
    else
    {
        return [self.arrEscertData count];
        
    }
}
}







- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


if(tableView == self.DatetableRec)
{
    RECDateCellTbl *cella=[tableView dequeueReusableCellWithIdentifier:@"DateCell"];
    {
        
        cella.textLabel.text = [recdatepri objectAtIndex:indexPath.row];
        
    }
    return cella;
}
else
{
    
    
    if (indexPath.section==0)
    {
        static NSString *simpleTableIdentifier = @"firstNoBid";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        return cell;
    }
    else
    {
        
        static NSString *simpleTableIdentifier = @"NoBidScreenCell";
        NoBidTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if ([differTable  isEqual: @"DAMTABLE"])
        {
            cell.lblSerialNo.text=[NSString stringWithFormat:@"%d",(int)indexPath.row+1];
            
            NSMutableDictionary *dict=[self.arrData objectAtIndex:indexPath.row];
            
            if ([dict objectForKey:@"date"])
            {
                
                NSArray *items = [[dict objectForKey:@"date"] componentsSeparatedByString:@"-"];
                
                if ([items count]==3)
                {
                    cell.lblDateTime.text=[NSString stringWithFormat:@"%@-%@-%@",[items objectAtIndex:2],[items objectAtIndex:1],[items objectAtIndex:0]];
                }
                else
                {
                    cell.lblDateTime.text=[dict objectForKey:@"date"];
                }
                
            }
        }
        else
        {
            cell.lblSerialNo.text=[NSString stringWithFormat:@"%d",(int)indexPath.row+1];
            
            NSMutableDictionary *dict=[self.arrEscertData objectAtIndex:indexPath.row];
            if ([dict objectForKey:@"date"])
            {
                //
                
                NSArray *items = [[dict objectForKey:@"date"] componentsSeparatedByString:@"-"];
                
                if ([items count]==3)
                {
                    cell.lblDateTime.text=[NSString stringWithFormat:@"%@-%@-%@",[items objectAtIndex:0],[items objectAtIndex:1],[items objectAtIndex:2]];
                }
                else
                {
                    cell.lblDateTime.text=[dict objectForKey:@"date"];
                }
                
            }
            
        }
        
        cell.btnCross.tag=indexPath.row;
        [cell.btnCross addTarget:self action:@selector(crossButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
}
}





@end
