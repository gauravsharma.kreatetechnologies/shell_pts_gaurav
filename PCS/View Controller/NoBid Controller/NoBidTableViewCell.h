//
//  NoBidTableViewCell.h
//  PCS
//
//  Created by lab4code on 15/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoBidTableViewCell : UITableViewCell
@property(weak,nonatomic)IBOutlet UILabel *lblSerialNo;
@property(weak,nonatomic)IBOutlet UILabel *lblDateTime;
@property(weak,nonatomic)IBOutlet UIButton *btnCross;
@property(weak,nonatomic)IBOutlet UILabel *lblPrice;
@property(weak,nonatomic)IBOutlet UILabel *lblBid;


@end
