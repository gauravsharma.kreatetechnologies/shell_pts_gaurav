//
//  NewsLetterViewController.m
//  PCS
//
//  Created by lab4code on 21/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "NewsLetterViewController.h"
#import "NoBidTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "WebbrowserControllerViewController.h"
#import "AppDelegate.h"
#import "CustmObj.h"

#ifdef __IPHONE_8_0
#define GregorianCalendar NSCalendarIdentifierGregorian
#else
#define GregorianCalendar NSGregorianCalendar
#endif

@interface NewsLetterViewController ()
{
    BOOL isLblFromTouch;
    NSDateFormatter *dateFormat;
    
}
@property(weak,nonatomic)IBOutlet UILabel *lblFrom;
@property(weak,nonatomic)IBOutlet UILabel *lblTo;

@property(weak,nonatomic)IBOutlet UITableView *myTableView;

@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

@property(nonatomic,strong)NSString *strFromDate;
@property(nonatomic,strong)NSString *strToDate;
@property(strong,nonatomic)NSMutableArray *arrData;

@end

@implementation NewsLetterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self.viewNoData setHidden:YES];
    self.datePicker.datePickerMode=UIDatePickerModeDate;
    
    self.tableHeightConstraint.constant= - (self.datePickerView.frame.size.height+80);
    [self.datePickerView needsUpdateConstraints];
    
    
    UITapGestureRecognizer *lblFromTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblFromTap:)];
    lblFromTap.numberOfTapsRequired=1;
    [self.lblFrom addGestureRecognizer:lblFromTap];
    
    
    UITapGestureRecognizer *lblToTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblToTap:)];
    lblToTap.numberOfTapsRequired=1;
    [self.lblTo addGestureRecognizer:lblToTap];
    
    
    
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    
    NSString *strCurrentDate=[NSString stringWithFormat:@"%@",theDate];
    
    NSTimeInterval secondsPerDay = 24 * 60 * 60 * 7;
    NSDate *tomorrow = [[NSDate alloc]
                        initWithTimeIntervalSinceNow:-secondsPerDay];
    
    
    NSString *thePastDate = [dateFormat stringFromDate:tomorrow];
    
    
    
    self.strFromDate=thePastDate;
    
    self.strToDate=strCurrentDate;
    
    
   // NSLog(@"current date %@",strCurrentDate);
    //NSLog(@"past date %@",thePastDate);
    
    
    [self initialServiceHitData];

    
    
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
   
    NSString *theDateAgain = [dateFormat stringFromDate:now];
    
    NSString *strCurrentdateFormat=[NSString stringWithFormat:@"%@",theDateAgain];
    
    NSTimeInterval secondsPerDayAgain = 24 * 60 * 60 * 7;
    NSDate *pastday = [[NSDate alloc]
                        initWithTimeIntervalSinceNow:-secondsPerDayAgain];
    
    
    NSString *thePastDateAgain = [dateFormat stringFromDate:pastday];
    
        self.lblFrom.text=thePastDateAgain;
        self.lblTo.text=strCurrentdateFormat;

    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)menuButtonClick:(id)sender
{
  //[SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - date tap gesture

- (void)lblFromTap:(UIGestureRecognizer*)gestureRecognizer
{
    
    isLblFromTouch=YES;
    
    if (self.tableHeightConstraint.constant !=0) {
        
        [UIView animateWithDuration:0.5 animations:^{
            
            
            self.tableHeightConstraint.constant=0;
            [self.datePickerView needsUpdateConstraints];
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
            
            
        }];
        
    }
    
}

- (void)lblToTap:(UIGestureRecognizer*)gestureRecognizer
{
    isLblFromTouch=NO;
    
    if (self.tableHeightConstraint.constant !=0)
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            
            self.tableHeightConstraint.constant=0;
            [self.datePickerView needsUpdateConstraints];
            
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
            
            
        }];
    }
    
}


- (IBAction)cancelBtnClcik:(id)sender
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
}

- (IBAction)doneBtnClick:(id)sender
{
    
    
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *dateOfBirth=[dateFormat stringFromDate:self.datePicker.date];
    
    
    
    if (isLblFromTouch) {
        
        self.lblFrom.text=dateOfBirth;
    }
    else
    {
        self.lblTo.text=dateOfBirth;
    }
    
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *savedate=[dateFormat stringFromDate:self.datePicker.date];
    
    if (isLblFromTouch) {
        
        self.strFromDate=savedate;
    }
    else
    {
        self.strToDate=savedate;
    }
    
    
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
    
    
    
}




-(IBAction)goButtonClick:(id)sender
{
    if (self.strFromDate.length>0 && self.strToDate.length>0) {
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd"];
        NSDate *startDate = [f dateFromString:self.strFromDate];
        NSDate *endDate = [f dateFromString:self.strToDate];
        
        NSTimeInterval timeDifference = [startDate timeIntervalSinceDate:endDate];
        
        // NSLog(@"diff %f",timeDifference);
        
        if (timeDifference <=0.000000)
        {
            [self initialServiceHitData];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:@"To date should be greater than From date"];
        }
        
        
    }
    
}


#pragma mark - Table View Delegate and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    return [self.arrData count];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 30;
    }
    return 48;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section==0)
    {
        static NSString *simpleTableIdentifier = @"firstNoBid";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"NoBidScreenCell";
        NoBidTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblSerialNo.text=[NSString stringWithFormat:@"%d",(int)indexPath.row+1];
        
        NSMutableDictionary *dict=[self.arrData objectAtIndex:indexPath.row];
        
        if ([dict objectForKey:@"text"]) {
            cell.lblDateTime.text=[dict objectForKey:@"text"];
        }
        
        cell.btnCross.tag=indexPath.row;
        [cell.btnCross addTarget:self action:@selector(downloadButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    
    
}

-(void)downloadButtonClick:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    int tag=(int)btn.tag;
    
    if (tag < [self.arrData count])
    {
        NSMutableDictionary *dictinory=[self.arrData objectAtIndex:tag];
        
        if ([dictinory objectForKey:@"url"]) {
            
            NSString *strUrl=[dictinory objectForKey:@"url"];
            
            WebbrowserControllerViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"webBrowse"];
            newView.strWebUrl=strUrl;
            [self.navigationController pushViewController:newView animated:YES];
        }
        
        
        
    }
    
    
}

-(void)initialServiceHitData
{
   
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:self.strFromDate,@"fromdate",self.strToDate,@"todate",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    //  NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
//    [manager POST:@"https://www.mittalsgroup.com/newsletter/services/newsletter_for_app.php" parameters:dictFinaldata success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject)
    
    NSString*getNewsLetterBaseUrl = [AppHelper userDefaultsForKey:@"newsletterBaseUrl"];
    NSString*strWebUrl = @"services/newsletter_for_app.php";
    
    NSString*combineUrl = [NSString stringWithFormat:@"%@/%@",getNewsLetterBaseUrl,strWebUrl];
    
//    NSLog(@"combineUrl  %@",combineUrl);

    [[manager POST:combineUrl parameters:dictFinaldata success:^(NSURLSessionDataTask *task, id responseObject)
      {
        
//
//
//        NSLog(@"Response Data  %@",responseObject);
//
//
        
        NSMutableArray *arrResponsedata=(NSMutableArray *)responseObject;
        if ([arrResponsedata count]>0)
        {
            [self.myTableView setHidden:NO];
            [self.viewNoData setHidden:YES];
            self.arrData=arrResponsedata;
            [self.myTableView reloadData];
        }
        else
        {
            [self.myTableView setHidden:YES];
            [self.viewNoData setHidden:NO];
        }
        
        // [SVProgressHUD dismiss];
        [CustmObj removeHud:self.view];
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        
        // [SVProgressHUD dismiss];
        [CustmObj removeHud:self.view];
        
        if (error.code == -1009) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                           message:@"The Internet connection appears to be offline."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                
                // NSLog(@"You pressed button three");
            }];
            
            [alert addAction:thirdAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
        
    }]resume];
    
}

-(IBAction)btnLogout_Click:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
