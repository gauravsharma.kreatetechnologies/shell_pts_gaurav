//
//  monthlyATCviewcontroller.m
//  PCS
//
//  Created by pavan yadav on 17/01/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

#import "monthlyATCviewcontroller.h"
#import "Services.h"
#import "AppHelper.h"
#import "ACTTableViewCell.h"
#import "WebbrowserControllerViewController.h"
#import "SVProgressHUD.h"
#import "LogonViewController.h"
#import "MenuViewController.h"
#import "SlideNavigationController.h"
#import "CustmObj.h"
#import "Power_Trading_Solutions-Swift.h"
@class SideMenuViewController;

@interface monthlyATCviewcontroller ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIPickerView *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *datebutton;
@end
NSMutableArray *datearr;
NSString *region;
NSDictionary *arrResponse;
NSArray *arrHeader;
NSString *Lognotification;

NSString *sectionZero;
NSString *sectionOne;
NSString *sectionTwo;
NSString *sectionThree;
NSString *sectionFoure;
NSString *selectedDate;


@implementation monthlyATCviewcontroller

- (void)viewDidLoad
{
    [super viewDidLoad];

   //NSLog(@"%@ getAcces",[AppHelper userDefaultsForKey:@"deviceId"]);
    
    datearr = [[NSMutableArray alloc]init];
    
    _datePicker.hidden = true;
    self.tableView.separatorColor = [UIColor clearColor];
    

    NSDateFormatter *formate = [[NSDateFormatter alloc]init];
   [formate setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* component = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components

    NSInteger currentYear = [component year];
    
     NSInteger startFY = 2016;
     NSInteger endFY = startFY + 1;
    
    NSString *arrDate = [NSString stringWithFormat:@"%ld-%ld",(long)startFY,(long)endFY];
    [datearr insertObject:arrDate atIndex:0];
    int i = 0;
    
    for (i; endFY <= currentYear ;i++ )
    {
        
        NSString *arrDate = [NSString stringWithFormat:@"%ld-%ld",(long)endFY,(long)endFY+1];
        [datearr insertObject:arrDate atIndex:(i+1)];
        
        
        endFY= endFY+1;
    }
    
    
    if ([component month] == 1 || [component month] == 2 || [component month] == 3)
    {
        [_datebutton setTitle:[datearr  objectAtIndex:i-1]forState:UIControlStateNormal];
        
    }
    else
    {
        [_datebutton setTitle:[datearr  objectAtIndex:i]forState:UIControlStateNormal];
    }
    selectedDate = _datebutton.titleLabel.text;

    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }

    
   [self InterCountry:nil];
    
    if ([_buttonType isEqualToString:@"intra-regional"])
    {
         [self IntraRegional:nil];
    }
    if ([_buttonType isEqualToString:@"inter-country"])
    {
        [self InterCountry:nil];
    }
    if ([_buttonType isEqualToString:@"inter-regional"])
    {
        [self InterRegional:nil];
    }
    
}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
}


- (IBAction)dateButton:(id)sender
{
    [self.datePicker setHidden:NO];
}

- (IBAction)InterCountry:(id)sender
{
    sectionZero =nil;
    sectionOne=nil;
    sectionTwo=nil;
    sectionThree=nil;
    sectionFoure=nil;
    
    [_buttoninter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_buttoninterregional setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_buttonIntra setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    region =@"InterCountry";
    [self callApi];
    
}
- (IBAction)InterRegional:(id)sender
{
    sectionZero =nil;
    sectionOne=nil;
    sectionTwo=nil;
    sectionThree=nil;
    sectionFoure=nil;
    
    [_buttoninter setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_buttoninterregional setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_buttonIntra setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
     region =@"InterRegional";
     [self callApi];
}
- (IBAction)IntraRegional:(id)sender
{
    sectionZero =nil;
    sectionOne=nil;
    sectionTwo=nil;
    sectionThree=nil;
    sectionFoure=nil;
    
    [_buttoninter setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_buttoninterregional setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_buttonIntra setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     region =@"IntraRegional";
     [self callApi];
}

#pragma mark - Api call -
- (void)callApi
{
   
   // [SVProgressHUD showWithStatus:@"Please wait..."];
      [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",selectedDate,@"fy",region,@"type",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/market/monthly_atc.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {


        
        if (type == kResponseTypeFail)
        {
             //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            if ([response count]>0)
            {
            //[SVProgressHUD dismiss];
              [CustmObj removeHud:self.view];
            _tableView.hidden = NO;
            arrResponse= response;
            arrHeader =[arrResponse allKeys];
            [_tableView reloadData];
            }
            else
            {
                //[SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                _tableView.hidden = YES;
            }
        }
    }];
}


#pragma mark - PickerView Methods -

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return datearr.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   
    return [datearr objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    selectedDate = [datearr  objectAtIndex:row];
    
    [_datebutton setTitle:selectedDate forState:UIControlStateNormal];
    [self callApi];
    [self.datePicker setHidden:YES];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.datePicker setHidden:YES];
}


#pragma mark - TableHeaderview Methods -

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 23;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
      CGRect frame = CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width,23);
    
    UIView *View=[[UIView alloc]initWithFrame:frame];
    View.backgroundColor = [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.backgroundColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15];
     [View addSubview:label];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-25,1,21,20);
   [button setBackgroundImage:[UIImage imageNamed:@"buttonhifhtRemove"] forState:UIControlStateNormal];
  

    
    
    [button addTarget:self
               action:@selector(buttonhightReduce:)
     forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    
    switch (section) {
        case 0:
            
            if ([sectionZero isEqualToString:@"returnTotalCount"])
            {
                [View addSubview:button];
            }
            label.text =[arrHeader objectAtIndex:0];
            button.tag = 0;
            break;
            
        case 1:
            if ([sectionOne isEqualToString:@"returnTotalCount"])
            {
                [View addSubview:button];
            }
            label.text = [arrHeader objectAtIndex:1];
            button.tag = 1;
            break;
            
        case 2:
            if ([sectionTwo isEqualToString:@"returnTotalCount"])
            {
                [View addSubview:button];
            }
            label.text = [arrHeader objectAtIndex:2];
            button.tag = 2;
            break;
            
        case 3:
            if ([sectionThree isEqualToString:@"returnTotalCount"])
            {
                [View addSubview:button];
            }
            
            label.text = [arrHeader objectAtIndex:3];
            button.tag = 3;
            break;
            
        case 4:
            if ([sectionFoure isEqualToString:@"returnTotalCount"])
            {
                [View addSubview:button];
            }
            label.text = [arrHeader objectAtIndex:4];
            button.tag = 4;
            break;
        default:
            break;
    }
    
    return View;
}

-(void)buttonhightReduce:(UIButton *)sender
{
    if (sender.tag == 0)
    {
        sectionZero = nil;
    }
    else if (sender.tag == 1)
    {
        sectionOne = nil;
    }
    else if (sender.tag == 2)
    {
        sectionTwo = nil;
    }
    else if (sender.tag == 3)
    {
        sectionThree = nil;
    }
    else if (sender.tag == 4)
    {
        sectionFoure = nil;
    }
    [_tableView reloadData];
}



#pragma mark - TableFooterview Methods -

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (section == 0)
{
    if ([sectionZero isEqualToString:@"returnTotalCount"])
    {
        return 0;
    }
        
    else
    {
        if ([arrResponse[[arrHeader objectAtIndex:0]] count]> 4)
        {
            return 20;
        }
        else
        {
            return 0;
        }
    }
}
    
else if (section == 1)
{
        if ([sectionOne isEqualToString:@"returnTotalCount"])
        {
            return 0;
        }
        
        else
        {
            if ([arrResponse[[arrHeader objectAtIndex:1]] count]> 4)
            {
                return 20;
            }
            else
            {
                return 0;
            }
        }
}
    
else if (section == 2)
{
        if ([sectionTwo isEqualToString:@"returnTotalCount"])
        {
            return 0;
        }
        
        else
        {
            if ([arrResponse[[arrHeader objectAtIndex:2]] count]> 4)
            {
                return 20;
            }
            else
            {
                return 0;
            }
        }
}
    
else if (section == 3)
{
        if ([sectionThree isEqualToString:@"returnTotalCount"])
        {
            return 0;
        }
        
        else
        {
            if ([arrResponse[[arrHeader objectAtIndex:3]] count]> 4)
            {
                return 20;
            }
            else
            {
                return 0;
            }
        }
}
    
   
if ([sectionFoure isEqualToString:@"returnTotalCount"])
    {
            return 0;
    }
        
        else
        {
            if ([arrResponse[[arrHeader objectAtIndex:4]] count]> 4)
            {
                return 20;
            }
            else
            {
                return 0;
            }
    }

}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width,30);
    
    button.backgroundColor = [UIColor whiteColor];
    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0]];
    [button setTitle:@"View More"
             forState:(UIControlState)UIControlStateNormal];
    
    [button addTarget:self
               action:@selector(buttonViewMore:)
      forControlEvents:(UIControlEvents)UIControlEventTouchDown];
 
    
    switch (section)
    {
        case 0:
            button.tag = 0;
            break;
            
        case 1:
            button.tag = 1;
            break;
            
        case 2:
            button.tag = 2;
            break;
        case 3:
            button.tag = 3;
            break;
        case 4:
           button.tag = 4;
            break;
        default:
            break;
    }
    return button;
}


-(void)buttonViewMore:(UIButton *)sender
{
    if (sender.tag == 0)
    {
      sectionZero = @"returnTotalCount";
    }
    else if (sender.tag == 1)
    {
      sectionOne = @"returnTotalCount";
    }
    else if (sender.tag == 2)
    {
      sectionTwo = @"returnTotalCount";
    }
    else if (sender.tag == 3)
    {
      sectionThree = @"returnTotalCount";
    }
    else if (sender.tag == 4)
    {
      sectionFoure = @"returnTotalCount";
    }
    [_tableView reloadData];
}

#pragma mark :Tableview.

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrHeader.count;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0)
{
        if ([sectionZero isEqualToString:@"returnTotalCount"])
    {
             return [arrResponse[[arrHeader objectAtIndex:0]] count];
    }
        
        else
    {
        if ([arrResponse[[arrHeader objectAtIndex:0]] count]>= 4)
        {
            return 4;
        }
        else
        {
             return [arrResponse[[arrHeader objectAtIndex:0]] count];
        }
    }
}
    
else if (section == 1)
{
        
        if ([sectionOne isEqualToString:@"returnTotalCount"])
    {
            return [arrResponse[[arrHeader objectAtIndex:1]] count];
    }
    else
    {
        
        if ([arrResponse[[arrHeader objectAtIndex:1]] count]>= 4)
        {
            return 4;
        }
        else
        {
            return [arrResponse[[arrHeader objectAtIndex:1]] count];
        }
    }
}

else if (section == 2)
{
    if ([sectionTwo isEqualToString:@"returnTotalCount"])
    {
            return [arrResponse[[arrHeader objectAtIndex:2]] count];
    }
        else
    {
        if ([arrResponse[[arrHeader objectAtIndex:2]] count]>= 4)
        {
            return 4;
        }
        else
        {
            return [arrResponse[[arrHeader objectAtIndex:2]] count];
        }
    }
}
    
else if (section == 3)
{
    if ([sectionThree isEqualToString:@"returnTotalCount"])
    {
            return [arrResponse[[arrHeader objectAtIndex:3]] count];
    }
        else
    {
        if ([arrResponse[[arrHeader objectAtIndex:3]] count]>= 4)
        {
            return 4;
        }
        else
        {
            return [arrResponse[[arrHeader objectAtIndex:3]] count];
        }
    }
}
   
    
    if ([sectionFoure isEqualToString:@"returnTotalCount"])
    {
    return [arrResponse[[arrHeader objectAtIndex:4]] count];
    }
    else
    {
    
        if ([arrResponse[[arrHeader objectAtIndex:4]] count]>= 4)
        {
            return 4;
        }
        else
        {
            return [arrResponse[[arrHeader objectAtIndex:4]] count];
        }
    }

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WebbrowserControllerViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"webBrowse"];
    
    newView.strWebUrl=[[[[arrResponse objectForKey:[arrHeader objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"file_address"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self.navigationController pushViewController:newView animated:YES];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ACTTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.section==0)
        
    {
    cell.labeltitle.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:0]] objectAtIndex:indexPath.row] objectForKey:@"file_name"];
        
        cell.labeltime.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:0]] objectAtIndex:indexPath.row] objectForKey:@"timestamp"];
      
    }
    
    
    else if (indexPath.section==1)
    {
 cell.labeltitle.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:1]] objectAtIndex:indexPath.row] objectForKey:@"file_name"];
 cell.labeltime.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:1]] objectAtIndex:indexPath.row] objectForKey:@"timestamp"];
    }
    
   else if (indexPath.section==2)
    {
    cell.labeltitle.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:2]] objectAtIndex:indexPath.row] objectForKey:@"file_name"];
     cell.labeltime.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:2]] objectAtIndex:indexPath.row] objectForKey:@"timestamp"];
    }

     else if (indexPath.section==3)
    {
        
    cell.labeltitle.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:3]] objectAtIndex:indexPath.row] objectForKey:@"file_name"];
    cell.labeltime.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:3]] objectAtIndex:indexPath.row] objectForKey:@"timestamp"];
    }
    
     else if (indexPath.section==4)
    {
      cell.labeltitle.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:4]] objectAtIndex:indexPath.row] objectForKey:@"file_name"];
      cell.labeltime.text =[[[arrResponse objectForKey:[arrHeader objectAtIndex:4]] objectAtIndex:indexPath.row] objectForKey:@"timestamp"];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
     return cell;
}



#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}


- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
    
}

- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    
    
}



@end
