//
//  ACTTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 18/01/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACTTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labeltitle;
@property (weak, nonatomic) IBOutlet UILabel *labeltime;
@end
