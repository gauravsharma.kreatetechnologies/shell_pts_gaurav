//
//  monthlyATCviewcontroller.h
//  PCS
//
//  Created by pavan yadav on 17/01/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface monthlyATCviewcontroller : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *buttoninter;
@property (weak, nonatomic) IBOutlet UIButton *buttoninterregional;
@property (weak, nonatomic) IBOutlet UIButton *buttonIntra;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property(strong,nonatomic)NSString *buttonType;
@end
