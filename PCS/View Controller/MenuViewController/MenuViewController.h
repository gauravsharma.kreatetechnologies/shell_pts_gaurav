//
//  MenuViewController.h
//  PCS
//
//  Created by lab4code on 09/12/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController
@property(nonatomic,strong)NSTimer *timer;

@property(nonatomic,strong)NSMutableArray *arrayMenues;
@property(nonatomic,strong)NSString *strLastTimeBid;
@property(nonatomic,strong)NSString *strBidTime;
@property(weak,nonatomic)IBOutlet UILabel *lblLastTextBid;
@property(weak,nonatomic)IBOutlet UILabel *lblLastBid;
@property(weak,nonatomic)IBOutlet UILabel *lblCompanyName;

@end
