//
//  MenuViewCollectionViewCell.h
//  PCS
//
//  Created by lab4code on 26/12/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewCollectionViewCell : UICollectionViewCell

@property(weak,nonatomic)IBOutlet UILabel *lblMenuName;
@property(weak,nonatomic)IBOutlet UIImageView *imgIcon;
@end
