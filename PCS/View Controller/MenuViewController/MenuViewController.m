//
//  MenuViewController.m
//  PCS
//
//  Created by lab4code on 09/12/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuViewCollectionViewCell.h"
#import "MainScreenViewController.h"
#import "NoBidViewController.h"
#import "NotificationsViewController.h"
#import "ReportViewController.h"
#import "DownloadsmainViewController.h"
#import "NewBidMainViewController.h"
#import "MarketPriceViewController.h"
#import "NewsLetterViewController.h"
#import "AppHelper.h"
#import "CurrentAvailableURSViewController.h"
#import "SlideNavigationController.h"
#import "AppDelegate.h"

@class DashboardViewController; 
@class CurtailmentViewController;
@class SelectNewBidViewController;
@class NoBIdViewController;
@class MarKetPriceViewController;
@class BillAndReportViewController;
@class ReportsViewController;
@class ReportViewController;
#ifdef __IPHONE_8_0
#define GregorianCalendar NSCalendarIdentifierGregorian
#else
#define GregorianCalendar NSGregorianCalendar
#endif


@interface MenuViewController ()
{
    NSDateFormatter *dateFormat;
    NSCalendar *calendar;
    NSString *dateString;
}
@property(weak,nonatomic)IBOutlet UICollectionView *menuCollectionView;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(![AppHelper userDefaultsForKey:@"client_id"]){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    // Do any additional setup after loading the view.
    
    //  dataArray=[[NSMutableArray alloc]initWithObjects:@"Home",@"No Bid",@"New Bid",@"Market Place",@"Notifications",@"Download",@"Report",@"Logout", nil];
    
    /*
     
     If today time is less than or equal to 10:30 AM then run timer for end time 10:30(HH:MM)
     If today time is greater than 10:30 AM then run timer for tommarrow 10:30 aM
     
     BID time left for Today  Trading bid :01:30:20
     BID time left for tomorrow Trading bid: 22:43:01
     
     lastBidTime = "10:30:00";
     
     */
    
    dateFormat = [[NSDateFormatter alloc] init];
    calendar = [[NSCalendar alloc] initWithCalendarIdentifier:GregorianCalendar];
    NSTimer *theTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(calculateTimeForBid:) userInfo:nil repeats:YES];
    // Assume a there's a property timer that will retain the created timer for future reference.
    self.timer = theTimer;
    
    
    //  NSLog(@"%@",self.arrayMenues);
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc]init];
    [dateFormat1 setDateFormat:@"dd-MM-yyyy"];
     dateString = [dateFormat1 stringFromDate:tomorrow];
    
    
    
    
    NSMutableAttributedString *strWelcomeCompany = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *attrDict = @{
                               NSFontAttributeName : [UIFont boldSystemFontOfSize:15],
                               NSForegroundColorAttributeName : [UIColor blackColor]
                               };
    
    NSAttributedString *strWelcome = [[NSAttributedString alloc]initWithString:@"Welcome, " attributes:attrDict];
    NSAttributedString *strComapnyName = [[NSAttributedString alloc]initWithString:[AppHelper userDefaultsForKey:@"companyName"] attributes:nil];
    
    [strWelcomeCompany appendAttributedString:strWelcome];
    [strWelcomeCompany appendAttributedString:strComapnyName];
    [self.lblCompanyName setAttributedText:strWelcomeCompany];
    
    
    //    if ([AppHelper userDefaultsForKey:@"companyName"]) {
    //        self.lblCompanyName.text=[AppHelper userDefaultsForKey:@"companyName"];
    //    }
    
    
    [_menuCollectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)calculateTimeForBid:(NSTimer *)theTimer
{
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString *strLastBidTime = [AppHelper userDefaultsForKey:@"lastBidTime"];
    
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDate = [dateFormat stringFromDate:now];
    
    
    NSString *strFixedDateAndTime=[NSString stringWithFormat:@"%@ %@",theDate,strLastBidTime];
    
    
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSString *theTime = [dateFormat stringFromDate:now];
    
    NSString *strCurrentDateAndTime=[NSString stringWithFormat:@"%@ %@",theDate,theTime];
    
    //NSLog(@"fixed date and time %@",strFixedDateAndTime);
    // NSLog(@"current date and time %@",strCurrentDateAndTime);
    
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate *tomorrow = [[NSDate alloc]
                        initWithTimeIntervalSinceNow:secondsPerDay];
    
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString *theFutureDate = [dateFormat stringFromDate:tomorrow];
    NSString *strFutureDateAndTime=[NSString stringWithFormat:@"%@ %@",theFutureDate,strLastBidTime];
    // NSLog(@"tomorrow date and time %@",strFutureDateAndTime);
    
    [dateFormat setDateFormat:@"yyyy-MM-dd H:mm:ss"];
    NSDate *currentDate = [dateFormat dateFromString:strCurrentDateAndTime];
    NSDate *fixedDate = [dateFormat dateFromString:strFixedDateAndTime];
    NSDate *futureDate = [dateFormat dateFromString:strFutureDateAndTime];
    
    
    NSCalendarUnit unitFlags = NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:currentDate toDate:fixedDate options:0];
    
    
    NSInteger hour = [dateComponents hour];
    NSInteger minute = [dateComponents minute];
    NSInteger second = [dateComponents second];
    
    // NSLog(@"hour = %.2ld, minutes = %.0ld,seconds = %.0ld",(long)hour, (long)minute, (long)second);
    
    NSString *strBidTimeTh;
    
    if (second >= 0)
    {
        self.strBidTime=[NSString stringWithFormat:@"%.2ld:%.2ld:%.2ld",(long)hour, (long)minute, (long)second];
        
        strBidTimeTh=@"Bid Time Left for Today Trading : ";
        
    }
    else
    {
        NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:currentDate toDate:futureDate options:0];
        
        NSInteger hour1 = [dateComponent hour];
        NSInteger minute1 = [dateComponent minute];
        NSInteger second1 = [dateComponent second];
        
        self.strBidTime=[NSString stringWithFormat:@"%.2ld:%.2ld:%.2ld",(long)hour1, (long)minute1, (long)second1];
        strBidTimeTh=[NSString stringWithFormat:@"Bid Time Left for Tomorrow (%@) Trading : ",dateString];
    }
    
    NSMutableAttributedString *strBidTimeLeft = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *attrDict = @{
                               NSFontAttributeName : [UIFont boldSystemFontOfSize:11],
                               NSForegroundColorAttributeName : [UIColor whiteColor]
                               };
    
    NSAttributedString *strBidTimeLeftTh = [[NSAttributedString alloc]initWithString:strBidTimeTh attributes:attrDict];
    NSAttributedString *strBidTimeValue = [[NSAttributedString alloc]initWithString:self.strBidTime attributes:nil];
    
    [strBidTimeLeft appendAttributedString:strBidTimeLeftTh];
    [strBidTimeLeft appendAttributedString:strBidTimeValue];
    
    [self.lblLastBid setAttributedText:strBidTimeLeft];
    
    if ([[UIScreen mainScreen] bounds].size.width >320)
    {
        _lblLastBid.font=[_lblLastBid.font fontWithSize:11];
    }
    else
    {
        _lblLastBid.font=[_lblLastBid.font fontWithSize:8];
    }
}


-(IBAction)logoutButtonClick:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark Collection view Delegate and datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    
//    NSLog(@"%@",self.arrayMenues);
    return [self.arrayMenues count];
    
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size for orientation
    if ([[UIScreen mainScreen] bounds].size.width >320)
    {
        return CGSizeMake([[UIScreen mainScreen]bounds].size.width/3-5, 108.0f);
    }
    else
    {
        return CGSizeMake([[UIScreen mainScreen]bounds].size.width/2-5, 108.0f);
    }
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    
    MenuViewCollectionViewCell *cell = (MenuViewCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSMutableDictionary *dict=[self.arrayMenues objectAtIndex:indexPath.row];
    
    if ([dict objectForKey:@"submenu"])
    {
        if ([[dict objectForKey:@"submenu"] count]>0)
        {
            if ([[[dict objectForKey:@"submenu"]objectAtIndex:0] objectForKey:@"Caption"]) {
                cell.lblMenuName.text=[[[dict objectForKey:@"submenu"]objectAtIndex:0] objectForKey:@"Caption"];
            }
            
            if ([[[dict objectForKey:@"submenu"]objectAtIndex:0] objectForKey:@"activty_icon"]) {
                cell.imgIcon.image=[UIImage imageNamed:[[[dict objectForKey:@"submenu"]objectAtIndex:0] objectForKey:@"activty_icon"]];
                
            }
        }
    }
    
    else
    {
        NSString *strReport = @"Notifications";
        NSString *strCompare = [dict objectForKey:@"Caption"];
//        NSLog(@"%@",strCompare);

        if ([strCompare isEqualToString:strReport]) {
                        
            cell.lblMenuName.text= @"Report";
            
        }else{
            cell.lblMenuName.text=[dict objectForKey:@"Caption"];
        }
        if ([dict objectForKey:@"activty_icon"]) {
            cell.imgIcon.image=[UIImage imageNamed:[dict objectForKey:@"activty_icon"]];
        }
    }
    
    
    return cell;
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    //self.arrayMenues =[[NSArray alloc]initWit];
    self.arrayMenues = (NSMutableArray *)[AppHelper userDefaultsForKey:@"menuitems"];
    NSString*getNewsLetterBaseUrl = [AppHelper userDefaultsForKey:@"newsletterBaseUrl"];
    
//     getNewsLetterBaseUrl = nil;
       if (getNewsLetterBaseUrl == nil)
        {
              NSMutableArray *newArrayTwo = [[NSMutableArray alloc]init];
              for (NSDictionary *objDic1 in _arrayMenues)
              {
                  if (![objDic1[@"activity_name"] isEqualToString:@"newsletter"])
                   {
                      [newArrayTwo addObject:objDic1];
                      
                   }
       
              }
          _arrayMenues = newArrayTwo;
        }
    [self.menuCollectionView reloadData];
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dict=[self.arrayMenues objectAtIndex:indexPath.row];
    
    NSLog(@"%@",dict);
    
    if ([dict objectForKey:@"submenu"])
    {
        if ([[dict objectForKey:@"submenu"] count]>0)
        {
            if ([[[dict objectForKey:@"submenu"]objectAtIndex:0] objectForKey:@"activty_icon"]) {
                
                NSString *activictyName=[[[dict objectForKey:@"submenu"]objectAtIndex:0] objectForKey:@"activty_icon"];
                
                
                if ([activictyName isEqualToString:@"menureport"])
                {
                    ReportViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportViewController"];
                    [self.navigationController pushViewController:newView animated:YES];
                }
            }
            
            
        }
    }
    else
    {
        if ([dict objectForKey:@"activty_icon"])
        {
            NSString *activictyName=[dict objectForKey:@"activty_icon"];
            
            
            
            if ([activictyName isEqualToString:@"menuhome"])
            {
                MainScreenViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"menuhome"];
                [self.navigationController pushViewController:newView animated:YES];
            }
            else if ([activictyName isEqualToString:@"menunewbid"])
            {
//                NewBidMainViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"menunewbididd"];
////                newView.iexchecked=YES;
////                newView.whichIEXButtonChecked=0;
//                [self.navigationController pushViewController:newView animated:YES];
                SelectNewBidViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectNewBidViewController"];
//                newView.iexchecked=YES;
//                newView.whichIEXButtonChecked=0;
                [self.navigationController pushViewController:newView animated:YES];
                
            }
            else if ([activictyName isEqualToString:@"menunobid"])
            {
//                NoBidViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"menunobid"];
//                [self.navigationController pushViewController:newView animated:YES];
                NoBIdViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"NoBIdViewController"];
                [self.navigationController pushViewController:newView animated:YES];
            }
            else if ([activictyName isEqualToString:@"menumarketprice"])
            {
                MarKetPriceViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"MarKetPriceViewController"];
                [self.navigationController pushViewController:newView animated:YES];
            }
            else if ([activictyName isEqualToString:@"menudownload"])
            {
//                DownloadsmainViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"menudownload"];
//                [self.navigationController pushViewController:newView animated:YES];
                BillAndReportViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"BillAndReportViewController"];
                [self.navigationController pushViewController:newView animated:YES];
                
            }
            else if ([activictyName isEqualToString:@"notification"])
            {
                
                ReportsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportsViewController"];
                [self.navigationController pushViewController:newView animated:YES];
                
            }
            else if([activictyName isEqualToString:@"newsletter"])
            {
                NewsLetterViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"newsletter"];
                [self.navigationController pushViewController:newView animated:YES];
            }
            
            else if([activictyName isEqualToString:@"newsupdate"])
            {
                
                 DashboardViewController*newsView = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardId"];
                
               // newsView.isFromMenu = YES;
                [self.navigationController pushViewController:newsView animated:YES];
            }
            else if([activictyName isEqualToString:@"curtailment"])
            {
                CurtailmentViewController *pushView = [self.storyboard instantiateViewControllerWithIdentifier:@"CurtailmentView"];

                [self.navigationController pushViewController:pushView animated:YES];
            }
         
        }
        
    }
    
}


- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return NO;
}

-(IBAction)btnToggleSideMenu_Click:(id)sender{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
//    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"MENU",@"REQUEST", nil];
//    [[NSNotificationCenter defaultCenter] postNotificationName: @"MyNotification" object:nil userInfo:userInfo];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController]. agogomedia@icloud.com  Mvcampb4168!
 // Pass the selected object to the new view controller.
 }
 */

@end
