
#import "NotificationSettingViewController.h"
#import "cellFirstTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "LogonViewController.h"
#import "MenuViewController.h"
#import "SlideNavigationController.h"
#import "CustmObj.h"
@class SideMenuViewController;

@interface NotificationSettingViewController ()
{
    
    NSDictionary *Dictresponse;
    NSString *BidNotification;
    NSString *curtailmentsNotification;
    NSString *newsNotification;
    NSString *newsLetter;
    NSArray *labelHeading;
    NSArray *labelData;
    NSString *Lognotification;
    BOOL switcha;
    NSMutableArray *arrresponseall;
    NSMutableDictionary *dictionary;
    NSMutableDictionary *dictResponse;
    NSMutableArray *arrNotificationHead;
    
    
}
@end

@implementation NotificationSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self InitialApiHit];
    NSDictionary *dTmp = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"NotificationList" ofType:@"plist"]];
    
    arrNotificationHead = dTmp[@"Objects"];
    _mainSwitch.transform = CGAffineTransformMakeScale(0.9, 0.7);
    [_mainSwitch setOnTintColor:[UIColor colorWithRed:0.0f/255.0f
                                                green:181.0f/255.0f
                                                 blue:178.0f/255.0f
                                                alpha:1.0f]];
    [_mainSwitch setThumbTintColor:[UIColor colorWithRed:0.0f/255.0f
                                                   green:118.0f/255.0f
                                                    blue:117.0f/255.0f
                                                   alpha:1.0f]];
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
    
}

-(void)updateMainSwitchOnLoad
{
    BOOL checkAllSetting = YES;
    NSArray *arrAllActivity = [dictResponse allKeys];
    for (int i=0; i< arrAllActivity.count; i++) {
        if([dictResponse[[arrAllActivity objectAtIndex:i]] isEqualToString:@"NO"]){
            checkAllSetting=NO;
        }
    }
    [_mainSwitch setOn:checkAllSetting animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 38;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrNotificationHead.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cellFirstTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"first"];
    NSMutableDictionary *dictTemp = [arrNotificationHead objectAtIndex:indexPath.row];
    cell.labelHeading.text = dictTemp[@"heading"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.cellSwitch.transform = CGAffineTransformMakeScale(0.9, 0.7);
    [cell.cellSwitch setOnTintColor:[UIColor colorWithRed:0.0f/255.0f
                                                    green:181.0f/255.0f
                                                     blue:178.0f/255.0f
                                                    alpha:1.0f]];
    [cell.cellSwitch setThumbTintColor:[UIColor colorWithRed:0.0f/255.0f
                                                       green:118.0f/255.0f
                                                        blue:117.0f/255.0f
                                                       alpha:1.0f]];
    NSArray *arrTemp = dictTemp[@"items"];
    BOOL checkAllSetting=YES;
    for (int i=0; i< arrTemp.count; i++) {
        if([dictResponse[[arrTemp objectAtIndex:i]] isEqualToString:@"NO"]){
            checkAllSetting=NO;
        }
    }
    [cell.cellSwitch setOn:checkAllSetting animated:YES];
    return cell;
}

- (IBAction)mainSwitchClick:(UISwitch *)sender
{
    NSString *strUpdateValue;
    if (sender.on)
    {
        strUpdateValue = @"YES";
    }
    else
    {
        strUpdateValue = @"NO";
    }
    NSMutableDictionary *dictMainResponce = [[NSMutableDictionary alloc]init];
    for (int i=0;  i < arrNotificationHead.count; i++)
    {
        NSMutableDictionary *dictTemp = [arrNotificationHead objectAtIndex:i];
        NSArray *arrTemp = dictTemp[@"items"];
        for (int j=0; j<arrTemp.count; j++)
        {
            [dictMainResponce setObject:strUpdateValue forKey:arrTemp[j]];
        }
    }
    [self updateNotificationOnServer:dictMainResponce];
}

-(void)updateNotificationOnServer:(NSDictionary*)dictActivity
{
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",dictActivity,@"activity",nil];
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    [[Services sharedInstance]serviceCallbyPost:@"service/notification/updatenotificationsetting.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
    {
        [CustmObj removeHud:self.view];
        if (type == kResponseTypeFail)
        {
            [CustmObj removeHud:self.view];
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                [alert addAction:thirdAction];
            }
        }
        else if (type == kresponseTypeSuccess)
        {
            [self viewDidLoad];
        }
    }];
}
- (IBAction)cellSwitchClick:(UISwitch *)sender
{
    UITableViewCell *theParentCell = [[sender superview]superview];
    int row = (int)[self.tableView indexPathForCell:theParentCell].row;
    NSString *strUpdateValue;
    if (sender.on)
    {
        strUpdateValue = @"YES";
    }
    else
    {
        strUpdateValue = @"NO";
    }
    NSMutableDictionary *dictTemp = [arrNotificationHead objectAtIndex:row];
    NSArray *arrTemp = dictTemp[@"items"];
    NSMutableDictionary *dictMainResponce = [[NSMutableDictionary alloc]initWithDictionary:dictResponse];
    for (int j=0; j<arrTemp.count; j++)
    {
        [dictMainResponce setObject:strUpdateValue forKey:arrTemp[j]];
    }
    
    //    NSLog(@"%@",dictMainResponce);
    [self updateNotificationOnServer:dictMainResponce];
    
}




#pragma mark - Api call -
-(void)InitialApiHit
{
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    [[Services sharedInstance]serviceCallbyPost:@"service/notification/getnotificationsetting.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        [CustmObj removeHud:self.view];
        if (type == kResponseTypeFail)
        {
            [CustmObj removeHud:self.view];
            NSError *error=(NSError*)response;
            if (error.code == -1009) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                [alert addAction:thirdAction];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            dictResponse = response;
            [_tableView reloadData];
            [self updateMainSwitchOnLoad];
        }
    }];
}

#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}


- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
    
}

- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    
}

@end

