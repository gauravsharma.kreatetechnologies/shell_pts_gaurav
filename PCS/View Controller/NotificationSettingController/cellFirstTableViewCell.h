//
//  cellFirstTableViewCell.h
//  NotificationSetting
//
//  Created by pavan yadav on 02/01/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cellFirstTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelHeading;

@property (weak, nonatomic) IBOutlet UISwitch *cellSwitch;


@end
