//
//  NotificationSettingViewController.h
//  NotificationSetting
//
//  Created by pavan yadav on 02/01/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSettingViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UISwitch *mainSwitch;


@end
