//
//  DisclaimerViewController.m
//  PtsDisclaimer
//
//  Created by pavan yadav on 29/09/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import "DisclaimerViewController.h"
#import "LogonViewController.h"
#import "AppHelper.h"
#import "MenuViewController.h"
#import "SlideNavigationController.h"
@class SideMenuViewController;


@interface DisclaimerViewController ()
{
    NSString *Lognotification;
}
@end

@implementation DisclaimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
    
    
    self.WebViewDisclaimer.layer.borderWidth = 1;
    [[_WebViewDisclaimer layer] setBorderColor:
     [[UIColor colorWithRed:0 green:0 blue:0 alpha:1] CGColor]];
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"Disclaimer" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [_WebViewDisclaimer loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType

{
    if (navigationType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    return YES;
}

#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
    
}




- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
}
    

@end
