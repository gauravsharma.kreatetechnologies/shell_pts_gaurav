//
//  DisclaimerViewController.h
//  PtsDisclaimer
//
//  Created by pavan yadav on 29/09/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisclaimerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *WebViewDisclaimer;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@end
