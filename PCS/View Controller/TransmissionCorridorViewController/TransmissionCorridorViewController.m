//
//  TransmissionCorridorViewController.m
//  PCS
//
//  Created by pavan yadav on 06/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "TransmissionCorridorViewController.h"
#import "SlideNavigationController.h"
#import "ImportTableViewCell.h"
#import "ExportTableViewCell.h"
#import "CorridorTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "LogonViewController.h"
#import "MenuViewController.h"
#import "CustmObj.h"
#import "Power_Trading_Solutions-Swift.h"

@interface TransmissionCorridorViewController ()
{
    NSMutableDictionary *dictResponse;
    UILabel *lbltransmission;
    UILabel *lbltime;
    NSString *Lognotification;
}

@property (weak, nonatomic) IBOutlet UIView *HedrView;
@property(weak,nonatomic)IBOutlet UITableView *myTableView;
@end

@implementation TransmissionCorridorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myTableView.hidden = YES;
    //[SVProgressHUD showWithStatus:@"Please wait..."];
    //[CustmObj addHud:self.view];
    
    
    

    [self initialServiceHitData];
    self.activityIndicator.hidden=YES;
    
    
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
    
    
    lbltransmission=[[UILabel alloc]init];
    [lbltransmission setFrame:CGRectMake(3,3,140,20)];
    [lbltransmission setText:@"Transmission Corridor"];
    lbltransmission.textColor=[UIColor whiteColor];
    [self.HedrView addSubview:lbltransmission];
    
    
    lbltime=[[UILabel alloc]init];
    [lbltime setFrame:CGRectMake(125,4,150,25)];
    lbltime.textColor=[UIColor whiteColor];
    [self.HedrView addSubview:lbltime];
    
    
    if ([[UIScreen mainScreen] bounds].size.width >320)
        
    {
         [lbltime setFrame:CGRectMake(135,4,170,25)];
        
        lbltime.font = [UIFont fontWithName:@"Helvetica Light" size:9];
        lbltransmission.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
    }
    else
    {
        
        lbltime.font = [UIFont fontWithName:@"Helvetica Light" size:8];
        lbltransmission.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initialServiceHitData
{
    
     [CustmObj addHud:self.view];
     [[Services sharedInstance]serviceCallbyPost:@"service/coridoravailibility.php" param:nil andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
            
            
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            //[SVProgressHUD dismiss];
           [CustmObj removeHud:self.view];
            dictResponse=response[@"data"];
            
            
            lbltime.text =[response valueForKey:@"date"];
            //            NSLog(@"%@",response);
            
            self.myTableView.hidden = NO;
            [_myTableView reloadData];
            [_activityIndicator stopAnimating];
            [self performSelector:@selector(tablereload) withObject:nil afterDelay:60.0];
            self.activityIndicator.hidden=YES;
            
        }
        
    }];
}

#pragma mark :Table Header.

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        
        
        return 8;
    }
    else if (section == 1)
    {
        
        return 8;
    }
    
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 20;
    }
    else if (section == 1)
    {
        
        return 20;
    }
    else if (section == 2)
    {
        
        return 20;
    }
    
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 21;
    }
    
    if (indexPath.section == 1)
    {
        return 21;
    }
    
    if (indexPath.section == 2)
    {
        return 21;
    }
    
    return 21;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width,30);
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    
    label.backgroundColor = [UIColor colorWithRed:32/255.0f green:140/255.0f blue:134/255.0f alpha:1.0f];
    
    switch (section) {
        case 0:
            label.text = @"Import";
            label.textColor = [UIColor whiteColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont systemFontOfSize:10];
            return label;
            break;
            
        case 1:
            label.text = @"Export";
            label.textColor = [UIColor whiteColor];
            label.font = [UIFont systemFontOfSize:10];
            label.textAlignment = NSTextAlignmentCenter;
            return label;
            break;
            
        case 2:
            label.text = @"Corridor";
            label.font = [UIFont systemFontOfSize:10];
            label.textColor = [UIColor whiteColor];
            label.textAlignment = NSTextAlignmentCenter;
            return label;
            break;
        default:
            break;
    }
    
    return label;
}


- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    
    
    view.tintColor = [UIColor whiteColor];
    
}

#pragma mark :Table View.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [dictResponse[@"IMPORT"] count];
        
    }
    else if (section == 1)
    {
        
        return [dictResponse[@"EXPORT"] count];
    }
    else if (section == 2)
    {
        
        return [dictResponse[@"CORRIDOR"] count];
    }
    else
    {
        return 0;
    }
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.section==0)
    {
        static NSString *simpleTableIdentifier = @"importCell";
        ImportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        NSArray *arrTemp=[dictResponse[@"IMPORT"] objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
     
            //[NSCharacterSet whitespaceCharacterSet]].length
        
            if ([[arrTemp objectAtIndex:2] isEqualToString:@""] && [[arrTemp objectAtIndex:3] isEqualToString:@""])
                
        {
            cell.lblSerialNo.text =[arrTemp objectAtIndex:0];
            cell.lblCorridor.text =[arrTemp objectAtIndex:1];
            cell.lblTTC.text =@"-";
            cell.lblATC.text =@"-";
            cell.lblActualflow.text =[arrTemp objectAtIndex:4];
        }
        else
        {
        
        cell.lblSerialNo.text =[arrTemp objectAtIndex:0];
        cell.lblCorridor.text =[arrTemp objectAtIndex:1];
        cell.lblTTC.text =[arrTemp objectAtIndex:2];
        cell.lblATC.text =[arrTemp objectAtIndex:3];
        cell.lblActualflow.text =[arrTemp objectAtIndex:4];
        }
        
        if ([[arrTemp objectAtIndex:5] isEqualToString:@"Green"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:10/255.0f green:190/255.0f blue:34/255.0f alpha:1.0f];
            cell.lblActualflow.textColor=[UIColor whiteColor];
            
            
        }
        else if ([[arrTemp objectAtIndex:5] isEqualToString:@"Yellow"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:252/255.0f green:203/255.0f blue:0/255.0f alpha:1.0f];
            
            cell.lblActualflow.textColor=[UIColor whiteColor];
        }
        
        else if ([[arrTemp objectAtIndex:5] isEqualToString:@"Red"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:255/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f];
            
            cell.lblActualflow.textColor=[UIColor whiteColor];
        }
        
        else
        {
            cell.lblActualflow.backgroundColor=[UIColor whiteColor];
        }
        
        return cell;
    }
    
    
    
    
    else if (indexPath.section==1)
    {
        static NSString *simpleTableIdentifier = @"ExportCell";
        ExportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        NSArray *arrTemp=[dictResponse[@"EXPORT"] objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([[arrTemp objectAtIndex:2] isEqualToString:@""] && [[arrTemp objectAtIndex:3] isEqualToString:@""])
            
        {
            cell.lblSerialNo.text =[arrTemp objectAtIndex:0];
            cell.lblCorridor.text =[arrTemp objectAtIndex:1];
            cell.lblTTC.text =@"-";
            cell.lblATC.text =@"-";
            cell.lblActualflow.text =[arrTemp objectAtIndex:4];
        }
        else
        {
        cell.lblSerialNo.text =[arrTemp objectAtIndex:0];
        cell.lblCorridor.text =[arrTemp objectAtIndex:1];
        cell.lblTTC.text =[arrTemp objectAtIndex:2];
        cell.lblATC.text =[arrTemp objectAtIndex:3];
        cell.lblActualflow.text =[arrTemp objectAtIndex:4];
        }
        
        if ([[arrTemp objectAtIndex:5] isEqualToString:@"Green"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:10/255.0f green:190/255.0f blue:34/255.0f alpha:1.0f];
            cell.lblActualflow.textColor=[UIColor whiteColor];
            
        }
        else if ([[arrTemp objectAtIndex:5] isEqualToString:@"Yellow"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:252/255.0f green:203/255.0f blue:0/255.0f alpha:1.0f];
            
            cell.lblActualflow.textColor=[UIColor whiteColor];
        }
        else if ([[arrTemp objectAtIndex:5] isEqualToString:@"Red"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:255/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f];
            
            cell.lblActualflow.textColor=[UIColor whiteColor];
        }
        
        else
        {
            cell.lblActualflow.backgroundColor=[UIColor whiteColor];
        }
        
        
        return cell;
    }
    
    
    
    
    else if (indexPath.section==2)
    {
        static NSString *simpleTableIdentifier = @"CorridorCell";
        CorridorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        NSArray *arrTemp=[dictResponse[@"CORRIDOR"] objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if ([[arrTemp objectAtIndex:2] isEqualToString:@""] && [[arrTemp objectAtIndex:3] isEqualToString:@""])
            
        {
            cell.lblSerialNo.text =[arrTemp objectAtIndex:0];
            cell.lblCorridor.text =[arrTemp objectAtIndex:1];
            cell.lblTTC.text =@"-";
            cell.lblATC.text =@"-";
            cell.lblActualflow.text =[arrTemp objectAtIndex:4];
        }
         else
        {
            cell.lblSerialNo.text =[arrTemp objectAtIndex:0];
            cell.lblCorridor.text =[arrTemp objectAtIndex:1];
            cell.lblTTC.text =[arrTemp objectAtIndex:2];
            cell.lblATC.text =[arrTemp objectAtIndex:3];
            cell.lblActualflow.text =[arrTemp objectAtIndex:4];
        }
        
        if ([[arrTemp objectAtIndex:5] isEqualToString:@"Green"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:10/255.0f green:190/255.0f blue:34/255.0f alpha:1.0f];
            
            cell.lblActualflow.textColor=[UIColor whiteColor];
        }
        
        else if ([[arrTemp objectAtIndex:5] isEqualToString:@"Yellow"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:252/255.0f green:203/255.0f blue:0/255.0f alpha:1.0f];
            
            cell.lblActualflow.textColor=[UIColor whiteColor];
        }
        else if ([[arrTemp objectAtIndex:5] isEqualToString:@"Red"])
        {
            cell.lblActualflow.backgroundColor=[UIColor colorWithRed:255/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f];
            
            cell.lblActualflow.textColor=[UIColor whiteColor];
        }
        
        else
        {
            cell.lblActualflow.backgroundColor=[UIColor whiteColor];
        }
        
        
        return cell;
    }
    
    else
    {
        return 0;
    }
    
}


-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
    
}

-(void)tablereload
{
    self.activityIndicator.hidden=NO;
    [_activityIndicator startAnimating];
    [self performSelector:@selector(initialServiceHitData) withObject:nil afterDelay:2.0];
    
}

- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }


}


@end
