//
//  ExportTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 06/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExportTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSerialNo;
@property (weak, nonatomic) IBOutlet UILabel *lblCorridor;
@property (weak, nonatomic) IBOutlet UILabel *lblTTC;
@property (weak, nonatomic) IBOutlet UILabel *lblATC;
@property (weak, nonatomic) IBOutlet UILabel *lblActualflow;
@end
