//
//  landedcostListViewController.m
//  pcs newM
//
//  Created by pavan yadav on 26/09/16.
//  Copyright © 2016 Invetech. All rights reserved.
//
#import "landedcostListViewController.h"
#import "ApplicstionTableViewCell.h"


@interface landedcostListViewController ()
@end

@implementation landedcostListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _MytableView.separatorStyle=NO;
    
    _MytableView.estimatedRowHeight = 550;
    _MytableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0 && section == 1)
    {
        return 16;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width,20)];
    headerView.backgroundColor=[UIColor colorWithRed:32/255.0f green:140/255.0f blue:134/255.0f alpha:1.0f];
    
    
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 3,200,15)];
    UILabel *headerLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(200, 3,[[UIScreen mainScreen] bounds].size.width -210,15)];
    
    
    switch (section) {
        case 0:
            
            headerLabel.textAlignment = NSTextAlignmentLeft;
            headerLabel.text = @"Applicable Charges ";
            headerLabel.textColor=[UIColor whiteColor];
            headerLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
            [headerView addSubview:headerLabel];
            
            
            headerLabel1.textAlignment = NSTextAlignmentRight;
            headerLabel1.text = @"(Rs./kWh)";
            headerLabel1.textColor=[UIColor whiteColor];
            headerLabel1.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
            [headerView addSubview:headerLabel1];
            
            return headerView;
            break;
            
        case 1:
            
            headerLabel.textAlignment = NSTextAlignmentLeft;
            headerLabel.text = @"Applicable Losses ";
            headerLabel.textColor=[UIColor whiteColor];
            headerLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
            [headerView addSubview:headerLabel];
            
            
            headerLabel1.textAlignment = NSTextAlignmentRight;
            headerLabel1.text = @"(Rs./kWh)";
            headerLabel1.textColor=[UIColor whiteColor];
            headerLabel1.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
            [headerView addSubview:headerLabel1];
            
            return headerView;
            break;
        case 2:
            
            headerLabel.textAlignment = NSTextAlignmentLeft;
            headerLabel.text = @"Calculation";
            headerLabel.textColor=[UIColor whiteColor];
            headerLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
            [headerView addSubview:headerLabel];
            
            
            headerLabel1.textAlignment = NSTextAlignmentRight;
            headerLabel1.text = @"(Rs./kWh)";
            headerLabel1.textColor=[UIColor whiteColor];
            headerLabel1.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
            [headerView addSubview:headerLabel1];
            
            return headerView;
            break;

            
    }
    
    return headerView;
}


#pragma mark :Table View.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [_dicttResponse[@"applicable_charges"] count];
        
        
    }
    else if (section == 1)
    {
        
        return [_dicttResponse[@"applicable_losses"] count];
        
    }
    else if (section == 2)
    {
        return [_dicttResponse[@"calculation"] count];
    }
    
    
    else
    {
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = [UIColor colorWithRed:220/255.0f green:220/255.0f blue:220/255.0f alpha:1.0f];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        static NSString *simpleTableIdentifier = @"lossescell";
        ApplicstionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        
        NSArray *arrTemp=[_dicttResponse[@"applicable_charges"]  objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.labelrght.text =[arrTemp valueForKey:@"name"];
        
        if([arrTemp valueForKey:@"value"] == [NSNull null]){
            cell.lblleft.text =@"null";
        }else{
//            NSString* val=[arrTemp valueForKey:@"value"];
//            float fCost = [val floatValue];
//            NSString *test = [NSString stringWithFormat:@"%.2f", fCost];
            cell.lblleft.text = [NSString stringWithFormat: @"%@", [arrTemp valueForKey:@"value"]];
        }
        return cell;
    }
    else if (indexPath.section==1)
    {
        static NSString *simpleTableIdentifier = @"lossescell";
        ApplicstionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        NSArray *arrTemp=[_dicttResponse[@"applicable_losses"] objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.labelrght.text =[arrTemp valueForKey:@"name"] ;
        
        if([arrTemp valueForKey:@"value"] == [NSNull null]){
            cell.lblleft.text =@"";
        }else{
            NSString* val=[arrTemp valueForKey:@"value"];
            float fCost = [val floatValue];
            NSString *test = [NSString stringWithFormat:@"%.2f", fCost];
            cell.lblleft.text =test;
        }
        
        return cell;
    }
    else if (indexPath.section==2)
    {
        static NSString *simpleTableIdentifier = @"lossescell";
        ApplicstionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        NSArray *arrTemp=[_dicttResponse[@"calculation"] objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        
        cell.labelrght.text =[arrTemp valueForKey:@"name"] ;
        
        if([arrTemp valueForKey:@"value"] == [NSNull null]){
            cell.lblleft.text =@"";
        }else{
            NSString* val=[arrTemp valueForKey:@"value"];
            float fCost = [val floatValue];
            NSString *test = [NSString stringWithFormat:@"%.2f", fCost];
            cell.lblleft.text =test;
        }
        
        return cell;
    }
    else
    {
        return 0;
    }
}
- (IBAction)BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
