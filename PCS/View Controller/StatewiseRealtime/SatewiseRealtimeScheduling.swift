//
//  SatewiseRealtimeScheduling.swift
//  PCS
//
//  Created by pavan yadav on 28/09/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SatewiseRealtimeScheduling: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate
    
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var revisionNumber: UIButton!
    var region: String = ""
    var stateId: String = ""
    var importType: String = ""
    var revison: String = ""
    var responseApi = [JSON]()
    
    
    
    
    
    var isSearchEnabled = Bool()
    var searchArray = [wonlead]()
    var filteredArray = [wonlead]()
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        revisionNumber.setTitle(revison, for: .normal)
        self.initialHit()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    // MARK:- apiCalling--
    func initialHit()
    {
        
        //1. Calling Initial api and getting response..
        //SVProgressHUD.show(withStatus: "Please wait...")
        CustomHUD.shredObject.loadXib(view: self.view)

        

        
        let someDict =  ["region": region ,"stateid": stateId ,"importtype" : importType ,"device_id": AppHelper.userDefaults(forKey: "deviceId")]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: stateRealtime, parameters:parameters)
            {
                (APIData) -> Void in
                
                
                if APIData["type"] == "ERROR"
                {
                    SVProgressHUD.showError(withStatus: APIData["message"] .stringValue)
                }
                else
                {
                   // SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    self.responseApi = APIData["data"] .arrayValue
                    
                    for i in 0..<self.responseApi.count
                    {
                        let labelTrader = self.responseApi[i]["trader"].stringValue
                        let labelBuyer = self.responseApi[i]["buyer"].stringValue
                        let labelSeller = self.responseApi[i]["seller"].stringValue
                        let labelApprovalNo = self.responseApi[i]["approval_no"].stringValue
                        let labelQuantum = self.responseApi[i]["totalqtm"].stringValue
                        
                        let card = wonlead(titleTrader: labelTrader, Buyer: labelBuyer, Seller: labelSeller, ApprovalNo: labelApprovalNo, Quantum: labelQuantum)
                        
                        self.searchArray.append(card)
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }
        catch
        {
            
        }
        
    }
    
    
    
    // MARK:- tableViewDelegate.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearchEnabled ==  false
        {
            return responseApi.count
        }
        else
        {
            
            return filteredArray.count
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! SatewiseSchedulingCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle .none

        if !isSearchEnabled
        {
            cell.labelIndexNo.text =  "\(indexPath.row + 1)"
            cell.labelTrader.text = responseApi[indexPath.row]["trader"].stringValue
            cell.labelBuyer.text = responseApi[indexPath.row]["buyer"].stringValue
            cell.labelSeller.text = responseApi[indexPath.row]["seller"].stringValue
            cell.labelApprovalNo.text = responseApi[indexPath.row]["approval_no"].stringValue
            cell.labelQuantum.text = responseApi[indexPath.row]["totalqtm"].stringValue
        }
        else
        {
            let card = filteredArray[indexPath.row]
      
            cell.labelIndexNo.text =  "\(indexPath.row + 1)"
            cell.labelTrader.text = card.std_Trader
            cell.labelBuyer.text = card.std_Buyer
            cell.labelSeller.text = card.std_Seller
            cell.labelApprovalNo.text = card.std_ApprovalNo
            cell.labelQuantum.text = card.std_Quantum
        }
        
  
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let push = self.storyboard?.instantiateViewController(withIdentifier: "RealTimeApproval") as! RealtimeApproval
        
        push.quantumNo = revison
        push.approvalNo = responseApi[indexPath.row]["approval_no"].stringValue
        push.dataid = responseApi[indexPath.row]["data_id"].stringValue
        self.navigationController?.pushViewController(push, animated: true)

    }
    
    
    
    
    
    // MARK:- searchBarDelegate.
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        if searchBar.text?.count == 0
        {
            isSearchEnabled =  false
        }
        else
        {
            filteredArray.removeAll()
            isSearchEnabled =  true
//            let searchPredicate = NSPredicate(format: "SELF.std_Seller CONTAINS[c] %@",searchText)
//            let array = (self.searchArray as NSArray).filtered(using: searchPredicate)
//            filteredArray = array  as! [wonlead]
            
            let array = self.searchArray.filter({$0.std_Seller.uppercased().contains(searchBar.text!.uppercased())})
            filteredArray = array
        }
        
        
        DispatchQueue.main.async
            {
            self.tableView.reloadData()
        }
    }
    
    
    @IBAction func backButton(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    
    
}





class SatewiseSchedulingCell: UITableViewCell
{
    
    @IBOutlet weak var labelIndexNo: UILabel!
    @IBOutlet weak var labelTrader: UILabel!
    @IBOutlet weak var labelBuyer: UILabel!
    @IBOutlet weak var labelSeller: UILabel!
    @IBOutlet weak var labelApprovalNo: UILabel!
    @IBOutlet weak var labelQuantum: UILabel!
    
}




// MARK:- model class for student cards

class wonlead:NSObject
{
    
    let std_Trader:String
    let std_Buyer:String
    let std_Seller:String
    let std_ApprovalNo : String
    let std_Quantum :String
    
    
    
    
    init(titleTrader:String, Buyer:String, Seller:String, ApprovalNo:String,Quantum:String)
    {
        self.std_Trader = titleTrader
        self.std_Buyer = Buyer
        self.std_Seller = Seller
        self.std_ApprovalNo = ApprovalNo
        self.std_Quantum = Quantum
        
    }
    
}

