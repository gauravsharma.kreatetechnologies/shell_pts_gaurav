//
//  StatewiseRealtimeViewController.swift
//  PCS
//
//  Created by pavan yadav on 26/07/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts


class StatewiseRealtimeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout ,ChartViewDelegate,UIPickerViewDelegate, UIPickerViewDataSource
{
    @IBOutlet weak var viewDatanotfound: UIView!
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet weak var labelDate: UILabel!
    
    var color: [UIColor] = []
    var Lognotification = ""
    
    var Locationtype = [JSON]()
    var responseGraph = [JSON]()
    
    
    var statId = ""
    var regionName = ""
    var stateName = [String]()
    var Scheduling = [String]()
    var demand = [String]()
    var newsTitle = [String]()
    
    
    var values = [Double]()
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewDatanotfound.isHidden = true
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        selectionButton.layer.cornerRadius = 17
        selectionButton.layer.borderWidth = 1
        selectionButton.layer.borderColor = UIColor(red: CGFloat(39 / 255.0), green: CGFloat(174 / 255.0), blue: CGFloat(171 / 255.0), alpha: CGFloat(1.0)) .cgColor
        
        
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 10
        collectionView!.collectionViewLayout = layout
        
        pickerView.isHidden = true
        
        
        self.stateCallApi()
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectioButtonMethode(_ sender: UIButton)
    {
        pickerView.isHidden = false
    }
    
 
    
    
    func stateCallApi()
    {
        
        //1. Calling Initial api and getting response..
       // SVProgressHUD.show(withStatus: "Please wait...")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        
        
        let someDict =  ["latitude" : "-1","logtitude" : "-1"]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: DashboardLocation, parameters:parameters)
            {
                
                (APIData) -> Void in
                
                if APIData["type"] == "ERROR"
                {
                    SVProgressHUD.showError(withStatus: APIData["message"] .stringValue)
                    
                }
                else
                {
                   
                   // SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    self.Locationtype = APIData["states"] .arrayValue
                    
                    
                    
                    self.stateName.removeAll()
                    for i in 0..<APIData ["states"].count
                    {
                        self.stateName.append("\(self.Locationtype[i]["state_name"] .stringValue)\("(")\(self.self.Locationtype[i]["iexregion"] .stringValue)\(")")")
                    }
                    
                    
                    if (AppHelper.userDefaults(forKey: "StateId") == nil)
                    {
                        self.statId = self.Locationtype[0]["id"].stringValue
                        self.selectionButton.setTitle(self.stateName[0], for: UIControlState .normal)
                        self.regionName = self.Locationtype[0]["region"].stringValue
                    }
                    else
                    {
                        self.statId =  AppHelper.userDefaults(forKey: "StateId")
                        self.selectionButton.setTitle(AppHelper.userDefaults(forKey: "StateName"), for: UIControlState .normal)
                        self.regionName = AppHelper.userDefaults(forKey: "regionName")
                        
                    }
                    
                    self.pickerView.reloadAllComponents()
                    self.getChartData()
                    
                }
            }
        }
        catch
        {
            
        }
        
    }
    
    
    func getChartData()
    {
        
       // SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        
        let someDict =  ["region" : regionName,"stateid" : statId,"device_id" : AppHelper.userDefaults(forKey: "deviceId")]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: statewiseData, parameters:parameters)
            {
                
                
                (APIData) -> Void in
                
                if APIData["status"] == "ERROR"
                {
                    SVProgressHUD.showError(withStatus: APIData["message"] .stringValue)
                }
                else
                {
                  //  SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    
                    if APIData["value"].count == 0
                    {
                        self.collectionView.isHidden = true
                        self.pieChartView.isHidden = true
                        self.viewDatanotfound.isHidden = false
                        self.labelDate.text = ""
                        
                        
                    }
                    else
                    {
                        self.viewDatanotfound.isHidden = true
                        self.labelDate.text = "   Schedule For Dated :\(APIData["date"].stringValue)"
                        
                        self.collectionView.isHidden = false
                        self.pieChartView.isHidden = false
                        self.newsTitle.removeAll()
                        
                        self.responseGraph = [APIData]
                        
                        for j in 0..<self.responseGraph[0]["value"].count
                        {
                            for (key, subJson) in self.responseGraph[0]["value"][j]
                            {
                                self.newsTitle.append(key)
                            }
                        }
                        self.setChart()
                        
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        catch
        {
            
        }
    }
    
    
    
    //PieChart Methode
    func setChart()
    {
        values.removeAll()
        
        pieChartView.legend.enabled = false
        pieChartView.delegate = self
        pieChartView.animate(xAxisDuration: 1, easingOption: ChartEasingOption.easeInCirc)
        pieChartView.chartDescription.enabled = false
        pieChartView.usePercentValuesEnabled = true
        pieChartView.holeRadiusPercent = 0.40
        pieChartView.usePercentValuesEnabled = true
        pieChartView.drawSlicesUnderHoleEnabled = true
        pieChartView.drawHoleEnabled = true
        pieChartView.transparentCircleRadiusPercent = 0.30;
        //pieChartView.rotationEnabled = true
        pieChartView.isUserInteractionEnabled = false
        // pieChartView.rotationWithTwoFingers = false
        
        if responseGraph[0]["value"].count>0
        {
            for j in 0..<responseGraph[0]["value"].count
            {
                self.values.append(responseGraph[0]["value"][j][newsTitle[j]]["per"].doubleValue)
            }
        }
        
        var dataEntries: [ChartDataEntry] = []
        dataEntries.removeAll()
        
        for i in 0..<values.count
        {
            let dataEntry1 = PieChartDataEntry(value: values[i])
            dataEntries.append(dataEntry1)
        }
        
        print(dataEntries)
        
        
    
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        pieChartDataSet.entryLabelColor = UIColor .black
        pieChartDataSet.valueFont = UIFont .systemFont(ofSize: 8.0)
        pieChartDataSet.valueColors = [UIColor .black]
        pieChartDataSet.sliceSpace = 0.0
        pieChartDataSet.selectionShift = 0.0
        
        pieChartDataSet.valueLinePart1OffsetPercentage = 0.7;
        pieChartDataSet.valueLinePart1Length = 0.4;
        pieChartDataSet.valueLinePart2Length = 0.3;
        pieChartDataSet.yValuePosition = .outsideSlice
        pieChartDataSet.valueLineWidth = 0.3
        
        
        
        self.color.removeAll()
        
        //  pieChart Slice color ----------------------
        let colora = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        let colorb = #colorLiteral(red: 0.7855715989, green: 0.09544268242, blue: 1, alpha: 1)
        let colorc = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        let colord = #colorLiteral(red: 0.9129520939, green: 0.4871667879, blue: 0.02246789451, alpha: 1)
        let colore = #colorLiteral(red: 0.3374590468, green: 0.597596764, blue: 0.1782938772, alpha: 1)
        let colorf = #colorLiteral(red: 0.8270527124, green: 0.7168708456, blue: 0.1448201212, alpha: 1)
        let colorg = #colorLiteral(red: 0.7393080798, green: 0.1591297183, blue: 0.06216391465, alpha: 1)
        let colorh = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        
        //colors.append(color)
        
        color.insert(colora, at: 0)
        color.insert(colorb, at: 1)
        color.insert(colorc, at: 2)
        color.insert(colord, at: 3)
        color.insert(colore, at: 4)
        color.insert(colorf, at: 5)
        color.insert(colorg, at: 6)
        color.insert(colorh, at: 7)
        
        pieChartDataSet.colors = color
        
        
        
        //  pieChartDataSet.
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        
        
        // pieChart Slice label value % formater----------------------
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        
        
        pieChartView.data = pieChartData
        //self.pieChartView.reloadInputViews()
    }
    
    
    
    
    
    //MARK:- Collection View methode------
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: screenWidth/3.1, height: screenWidth/4.5);
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        if responseGraph.count>0
        {
            return responseGraph[0]["value"].count
        }
        return 0
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! powerDemCollectionViewCell
        
        
        cell.containerView.layer.borderWidth = 1
        cell.containerView.layer.borderColor = UIColor .gray.cgColor
        
        if responseGraph.count>0
        {
            cell.labelHeading.text = newsTitle[indexPath.row]
            cell.labelHeading.backgroundColor = color[indexPath.row % color.count]
            
            let energy = String(responseGraph[0]["value"][indexPath.row][newsTitle[indexPath.row]]["sum"].stringValue.characters.prefix(8))
            
            let Avg = String(responseGraph[0]["value"][indexPath.row][newsTitle[indexPath.row]]["avg"].stringValue.characters.prefix(8))
            
            if energy == "0"
            {
                cell.labelEner.text = "0.0 MW"
            }
            else
            {
                cell.labelEner.text = "\(energy)\("MWH")"
            }
            
        }
        
        
        return cell
    }
    
    
    
    // MARK:- PICKER VIEW METHODE CALLING-----------------------------------------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return stateName.count
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return stateName[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        pickerView.isHidden = true
        
        self.selectionButton.setTitle(stateName[row], for: UIControlState .normal)
        self.statId = Locationtype[row]["id"].stringValue
        self.regionName = Locationtype[row]["region"].stringValue
        
        AppHelper.removeFromUserDefaults(withKey:  "StateId")
        AppHelper.save(toUserDefaults: self.statId, withKey: "StateId")
        
        AppHelper.removeFromUserDefaults(withKey:  "StateName")
        AppHelper.save(toUserDefaults: stateName[row], withKey: "StateName")
        
        AppHelper.removeFromUserDefaults(withKey:  "regionName")
        AppHelper.save(toUserDefaults: regionName, withKey: "regionName")
        
        self.getChartData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let push = self.storyboard?.instantiateViewController(withIdentifier: "stateRealtime") as! SatewiseRealtimeScheduling
        
        push.region = self.regionName
        push.stateId = self.statId
        push.importType = newsTitle[indexPath.row]
        push.revison = responseGraph[0]["value"][indexPath.row][newsTitle[indexPath.row]]["revision"].stringValue
        
        self.navigationController?.pushViewController(push, animated: true)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        pickerView.isHidden = true
    }
    
    //pragma mark - SlideNavigationController Methods -
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
    }
}


// MARK:- CollectionView class.
class powerDemCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelEner: UILabel!
    
}

