//
//  RealTimeTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 08/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealTimeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSerialNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTrader;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyer;
@property (weak, nonatomic) IBOutlet UILabel *lblSeller;
@property (weak, nonatomic) IBOutlet UILabel *lblapprovalNo;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantum;
@end
