//
//  Real Time SchedulingViewController.h
//  PCS
//
//  Created by pavan yadav on 08/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Real_Time_SchedulingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>


#pragma mark img prop...
@property (weak, nonatomic) IBOutlet UIButton *DropdounRevipro;

@property (weak, nonatomic) IBOutlet UIImageView *TraderUp;
@property (weak, nonatomic) IBOutlet UIImageView *TraderDoun;

@property (weak, nonatomic) IBOutlet UIImageView *BuyerUp;
@property (weak, nonatomic) IBOutlet UIImageView *BuyerDoun;

@property (weak, nonatomic) IBOutlet UIImageView *SellerUp;
@property (weak, nonatomic) IBOutlet UIImageView *SellerDoun;

@property (weak, nonatomic) IBOutlet UIImageView *ApprovalUp;
@property (weak, nonatomic) IBOutlet UIImageView *ApprovalDoun;

@property (weak, nonatomic) IBOutlet UIImageView *QuantumUp;
@property (weak, nonatomic) IBOutlet UIImageView *QuantumDoun;

@property(weak, nonatomic) NSDictionary *dicttResponse;
@property (weak, nonatomic) IBOutlet UILabel *LabelRegion;

@property (weak, nonatomic) IBOutlet UIPickerView *Regionpickerview;
@property (weak, nonatomic) IBOutlet UISearchBar *SearchBar;
@property (weak, nonatomic) IBOutlet UIView *pickerView;

@end
