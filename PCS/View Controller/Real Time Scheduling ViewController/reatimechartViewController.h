//
//  reatimechartViewController.h
//  PCS
//
//  Created by pavan yadav on 13/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Real Time SchedulingViewController.h"
#import "SVProgressHUD.h"

@interface reatimechartViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewPieChart;
@property (weak, nonatomic) IBOutlet UIView *ViewAnimation;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@end


