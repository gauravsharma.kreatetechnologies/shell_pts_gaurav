//
//  RealtimeaprvlTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 12/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealtimeaprvlTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblblockfirst;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantumfirst;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantumSec;
@property (weak, nonatomic) IBOutlet UILabel *lblblockSec;
@end
