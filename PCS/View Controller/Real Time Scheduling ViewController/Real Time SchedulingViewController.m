//
//  Real Time SchedulingViewController.m
//  PCS
//
//  Created by pavan yadav on 08/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "Real Time SchedulingViewController.h"
#import "RealTimeTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "RealtimeApproval.h"

@interface Real_Time_SchedulingViewController ()
{
    NSString  *strrevision;
    BOOL TraderChecked;

    BOOL Sortingg;
    NSString *strascending;
    
    NSMutableDictionary *dictResponse;
    NSArray *filteredString;
    BOOL isFilltered;
}

@property (strong, nonatomic) NSMutableArray *arrTemp;
@property (weak, nonatomic) IBOutlet UITableView *MyTableView;
@property (weak, nonatomic) IBOutlet UIView *lblview;

@end

@implementation Real_Time_SchedulingViewController

- (void)viewDidLoad
{
 
    
    [super viewDidLoad];
    _pickerView.hidden=YES;
    _MyTableView.separatorStyle =NO;
    
    self.arrTemp=[[NSMutableArray alloc]init];
    [_Regionpickerview reloadAllComponents];
    
    CGRect frame = _lblview.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    frame.origin.x += 100; // slide right
    _lblview.frame = frame;
    [UIView commitAnimations];
    
    strascending=@"Default";
    
    _LabelRegion.text = [NSString stringWithFormat:@"%@%@",@"Real Time Scheduling of ",_dicttResponse[@"region"]];
    
    strrevision=[_dicttResponse[@"revision"] lastObject];
    [_DropdounRevipro setTitle:[_dicttResponse[@"revision"] lastObject] forState:UIControlStateNormal];
    
    [self initialServiceHitData];
   
}


- (IBAction)doneBtnClick:(id)sender
{
    
    [self initialServiceHitData];
    [self.pickerView setHidden:true];
    
}

- (IBAction)cancelBtnClcik:(id)sender
{
    [self.pickerView setHidden:true];
}



-(void)initialServiceHitData
{
    NSString *currentDate=_dicttResponse[@"date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:currentDate];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringDate = [dateFormatter stringFromDate:dateFromString];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:stringDate,@"date",_dicttResponse[@"region"],@"region",_dicttResponse[@"importtype"],@"importtype"
                        ,strrevision,@"revision",[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    
    [[Services sharedInstance]serviceCallbyPost:@"service/scheduling/getqtm_rldcwise.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            dictResponse=response;
           
            
            for (int i=0; i<[dictResponse[@"data"] count]; i++)
            {
                NSMutableDictionary *tempDicts = [[NSMutableDictionary alloc]init];
                tempDicts = (NSMutableDictionary *)[dictResponse[@"data"] objectAtIndex:i];
                [self.arrTemp addObject:tempDicts];
            }
            
            [_MyTableView reloadData];
        }
        
    }];
}


#pragma--SearchBar.
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_MyTableView resignFirstResponder];
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length==0)
    {
        isFilltered = NO;
        
        strascending=@"Default";
    }
    else
    {
        isFilltered = YES;
        filteredString = [[NSMutableArray alloc]init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"(trader contains[cd] %@) OR (buyer contains[cd] %@) OR (seller contains[cd] %@) OR (approval_no contains[cd] %@)", searchText, searchText, searchText,searchText];
    
        
        filteredString = [dictResponse[@"data"] filteredArrayUsingPredicate:predicate];
    }
    [_MyTableView reloadData];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isFilltered)
    {
        return [filteredString count];
    }
    return [dictResponse[@"data"] count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==_MyTableView)
    {
        RealtimeApproval *realView = [self.storyboard instantiateViewControllerWithIdentifier:@"RealTimeApproval"];
        
        NSArray *arrTemp=[dictResponse[@"data"] objectAtIndex:indexPath.row];
        
        realView.QuantumNo = strrevision;
        
        realView.Dataid=[arrTemp valueForKeyPath:@"data_id"];
        realView.ApprovalNo=[arrTemp valueForKeyPath:@"approval_no"];
        [self.navigationController pushViewController:realView animated:YES];
        
    }
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    RealTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Reallid" forIndexPath:indexPath];
    
    cell.lblSerialNo.text =[NSString stringWithFormat:@"%ld",indexPath.row+1];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if(!isFilltered)
    {
        NSDictionary *arrTemp=[dictResponse[@"data"] objectAtIndex:indexPath.row];
        
        if ([strascending isEqualToString:@"Default"])
        {
            
            if ([arrTemp valueForKeyPath:@"trader"] != [NSNull null])
            {
                cell.lblTrader.text=[arrTemp valueForKeyPath:@"trader"];
            }
            cell.lblBuyer.text=[arrTemp valueForKeyPath:@"buyer"];
            cell.lblSeller.text=[arrTemp valueForKeyPath:@"seller"];
            cell.lblapprovalNo.text=[arrTemp valueForKeyPath:@"approval_no"];
            cell.lblQuantum.text=[arrTemp valueForKeyPath:@"totalqtm"];
        }
        
#pragma mark - Trader condition's -
        
        else if([strascending isEqualToString:@"traderascending"])
        {
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"trader"ascending:YES];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
        }
        
        else if([strascending isEqualToString:@"traderDescending"])
        {
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"trader"ascending:NO];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
        }
        
#pragma mark - Buyer condition's -
        
        else if([strascending isEqualToString:@"buyerascending"])
        {
            
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"buyer"ascending:YES];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];

            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
            
        }
        
        else if([strascending isEqualToString:@"buyerDescending"])
        {
            
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"buyer"ascending:NO];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
            
        }
        
#pragma mark - Seller condition's -
        
        else if([strascending isEqualToString:@"sellerascending"])
        {
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"seller"ascending:YES];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
            
        }
        
        else if([strascending isEqualToString:@"sellerDescending"])
        {
            
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"seller"ascending:NO];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
            
        }
        
        
#pragma mark - Approval condition's -
        
        else if([strascending isEqualToString:@"Approvalascending"])
        {
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"approval_no"ascending:YES];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
            
        }
        
        else if([strascending isEqualToString:@"ApprovalDescending"])
        {
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"approval_no"ascending:NO];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
            
        }
        
#pragma mark - Quantum condition's -
        
        else if([strascending isEqualToString:@"Quantumascending"])
        {
            
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"totalqtm"ascending:YES];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
        }
        
        
        
        else if([strascending isEqualToString:@"Quantumdescending"])
        {
            
            NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"totalqtm"ascending:NO];
            
            [self.arrTemp sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
            
            cell.lblTrader.text=[[_arrTemp valueForKeyPath:@"trader"] objectAtIndex:indexPath.row];
            cell.lblBuyer.text=[[_arrTemp valueForKeyPath:@"buyer"] objectAtIndex:indexPath.row];
            
            cell.lblSeller.text=[[_arrTemp valueForKeyPath:@"seller"] objectAtIndex:indexPath.row];
            
            cell.lblapprovalNo.text=[[_arrTemp valueForKeyPath:@"approval_no"] objectAtIndex:indexPath.row];;
            cell.lblQuantum.text=[[_arrTemp valueForKeyPath:@"totalqtm"] objectAtIndex:indexPath.row];
        }
        
     
        return cell;
    }
    
    else
    
    {
        if ([strascending isEqualToString:@"Default"])
        {
            
            NSDictionary *arrTemp=[filteredString objectAtIndex:indexPath.row];
            
            
            cell.lblTrader.text=[arrTemp valueForKeyPath:@"trader"];
            cell.lblBuyer.text=[arrTemp valueForKeyPath:@"buyer"];
            cell.lblSeller.text=[arrTemp valueForKeyPath:@"seller"];
            cell.lblapprovalNo.text=[arrTemp valueForKeyPath:@"approval_no"];
            cell.lblQuantum.text=[arrTemp valueForKeyPath:@"totalqtm"];
        }
        return cell;
    }
}



-(IBAction)menuButtonClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)BtnTrader:(id)sender
{
    
    if (Sortingg==NO)
    {
        
        _TraderUp.image = [UIImage imageNamed:@"up black.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        Sortingg=YES;
        strascending=@"traderascending";
    }
    else
    {
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down black.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        Sortingg=NO;
        strascending=@"traderDescending";
    }
    isFilltered =NO;
    [_MyTableView reloadData];
}


- (IBAction)BtnBuyer:(id)sender

{
    
    if (Sortingg==NO)
    {
        _BuyerUp.image = [UIImage imageNamed:@"up black.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        Sortingg=YES;
        strascending=@"buyerascending";
 
    }
    else
    {
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down black.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        Sortingg=NO;
        strascending=@"buyerDescending";
    }
    isFilltered =NO;
    [_MyTableView reloadData];
}


- (IBAction)BtnSeller:(id)sender {
    
    if (Sortingg==NO)
    {
        _SellerUp.image = [UIImage imageNamed:@"up black.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        Sortingg=YES;
        strascending=@"sellerascending";
    }
    else
    {
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down black.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        Sortingg=NO;
        strascending=@"sellerDescending";
    }
    isFilltered =NO;
    [_MyTableView reloadData];
}



- (IBAction)BtnApproval:(id)sender {
    
    if (Sortingg==NO)
    {
        _ApprovalUp.image = [UIImage imageNamed:@"up black.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        Sortingg=YES;
        strascending=@"Approvalascending";
    }
    else
    {
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down black.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        Sortingg=NO;
        strascending=@"ApprovalDescending";
    }
    isFilltered =NO;
    [_MyTableView reloadData];
}

- (IBAction)BtnQuantum:(id)sender {
    if (Sortingg==NO)
    {
        _QuantumUp.image = [UIImage imageNamed:@"up black.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        Sortingg=YES;
        strascending=@"Quantumascending";
    }
    else
    {
        _QuantumUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _QuantumDoun.image = [UIImage imageNamed:@"down black.png"];
        
        _TraderUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _TraderDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _SellerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _SellerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _BuyerUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _BuyerDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        
        _ApprovalUp.image = [UIImage imageNamed:@"upp-arrow.png"];
        _ApprovalDoun.image = [UIImage imageNamed:@"down-arrow.png"];
        Sortingg=NO;
        strascending=@"Quantumdescending";
    }
    isFilltered =NO;
    [_MyTableView reloadData];
}

#pragma mark - PickerView Methods -

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    
    return [_dicttResponse [@"revision"] count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return [_dicttResponse [@"revision"]  objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    [_DropdounRevipro setTitle:[_dicttResponse[@"revision"]  objectAtIndex:row]forState:UIControlStateNormal];
    
    strrevision=[_dicttResponse[@"revision"]  objectAtIndex:row];
    
}



- (IBAction)DropdounRevisionbtn:(id)sender
{
    _pickerView.hidden=NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
