//
//  RealtimeApproval.m
//  PCS
//
//  Created by pavan yadav on 12/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "RealtimeApproval.h"
#import "RealtimeaprvlTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"

@interface RealtimeApproval ()
{
    NSMutableArray *dictresponse;
}

@end

@implementation RealtimeApproval

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lblaprvlNo.text= [NSString stringWithFormat:@"%@%@%@%@%@",@"RealTime Scheduling of Approval No-",_ApprovalNo,@" (R",_QuantumNo,@")"];

        [self initialServiceHitData];
    CGRect frame = _ViewAnimation.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    frame.origin.x += 100; // slide right
    _ViewAnimation.frame = frame;
    
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)menuButtonClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)initialServiceHitData
{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:_Dataid,@"data_id",[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    
    [[Services sharedInstance]serviceCallbyPost:@"service/scheduling/getallblockqtmofapproval.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            
            
            
     dictresponse=response;
    [_MyTableView reloadData];
            
            
        }
        
    }];
}






- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [[dictresponse valueForKey:@"block"] count]/2;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    RealtimeaprvlTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"realApproval" forIndexPath:indexPath];
 
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
cell.lblblockfirst.text=[NSString stringWithFormat:@"%@",[[dictresponse valueForKeyPath:@"block"] objectAtIndex:indexPath.row]];
    
cell.lblQuantumfirst.text=[[dictresponse valueForKeyPath:@"qtm"] objectAtIndex:indexPath.row];
   
 
cell.lblblockSec.text=[NSString stringWithFormat:@"%@",[[dictresponse valueForKeyPath:@"block"] objectAtIndex:(NSUInteger)indexPath.row+48]];
    
cell.lblQuantumSec.text=[[dictresponse valueForKeyPath:@"qtm"] objectAtIndex:(NSUInteger)indexPath.row+48];

    return cell;
    
}


@end
