//
//  RealtimeApproval.h
//  PCS
//
//  Created by pavan yadav on 12/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealtimeApproval : UIViewController
@property(strong, nonatomic) NSString *Dataid;
@property(strong, nonatomic) NSString *ApprovalNo;
@property(strong, nonatomic) NSString *QuantumNo;

@property (weak, nonatomic) IBOutlet UILabel *lblaprvlNo;
@property (weak, nonatomic) IBOutlet UIView *ViewAnimation;

@property (weak, nonatomic) IBOutlet UITableView *MyTableView;
@end
