//
//  reatimechartViewController.m
//  PCS
//
//  Created by pavan yadav on 13/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "reatimechartViewController.h"
@import Charts;
#import "PieChartCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "LogonViewController.h"
#import "MenuViewController.h"
#import "SlideNavigationController.h"
#import "CustmObj.h"
@class SideMenuViewController;


@interface reatimechartViewController ()  <ChartViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSArray *months;
    NSArray *unitsSold;
    NSArray *unitQty;
    NSMutableDictionary *dictResponse;
    NSMutableArray *arrImportType;
    NSString *strDate;
    NSString *Lognotification;
    NSString *selectedDate;

}
@property (weak, nonatomic) IBOutlet UIButton *dateButton;

@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation reatimechartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tableViewPieChart.delegate = self;
    

    selectedDate = nil;
    [self.datePickerView setHidden:true];
    self.datePicker.maximumDate=[NSDate date];

    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
        
        
    }
    
    [self initialServiceHitData];
    
    
    CGRect frame = _ViewAnimation.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    frame.origin.x += 100; // slide right
    _ViewAnimation.frame = frame;
    [UIView commitAnimations];
    
}


- (IBAction)datePickerValue:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    selectedDate =[dateFormatter stringFromDate:sender.date];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
   NSString* getDate =[dateFormatter stringFromDate:sender.date];
    
    [self.dateButton setTitle:getDate forState:UIControlStateNormal];
}



- (IBAction)doneBtnClick:(id)sender
{
    
    [self initialServiceHitData];
    [self.datePickerView setHidden:true];

}

- (IBAction)cancelBtnClcik:(id)sender
{
    [self.datePickerView setHidden:true];
}



- (IBAction)buttonDate:(UIButton *)sender
{
     [self.datePickerView setHidden:false];
}



-(void)initialServiceHitData
{
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",selectedDate,@"date",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    
    [[Services sharedInstance]serviceCallbyPost:@"service/scheduling/getqtm_importtypewise.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        
        NSLog(@"responceValue %@",response);
        
        
        if (type == kResponseTypeFail)
        {
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            if ([response isEqual:[NSNull null]])
            {
                [SVProgressHUD setStatus: @"Data not avilabele"];
                //[SVProgressHUD dismissWithDelay:3];
                [CustmObj removeHud:self.view];
                
                
            }
        else
           {
            strDate=response[@"date"];
               [self.dateButton setTitle:strDate forState:UIControlStateNormal];
            
            dictResponse=[response valueForKey:@"data"];
               
            arrImportType = [response valueForKey:@"order"];

               
            [_tableViewPieChart reloadData];
            //[SVProgressHUD dismiss];
               [CustmObj removeHud:self.view];
           }
    }
        
    }];
}

#pragma mark :Table Header.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 155;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrImportType count];
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row==0)
//    {
//        CATransform3D rotationTransform = CATransform3DTranslate(CATransform3DIdentity,0,-500,0);
//        cell.layer.transform = rotationTransform;
//        cell.alpha = 0;
//
//        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//
//            cell.layer.transform = CATransform3DIdentity;
//            cell.alpha = 1;
//        }
//                         completion:^ (BOOL completed) {} ];
//    }
//    else if (indexPath.row==1)
//    {
//        CATransform3D rotationTransform = CATransform3DTranslate(CATransform3DIdentity,0,500,0);
//        cell.layer.transform = rotationTransform;
//        cell.alpha = 0;
//
//        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//
//            cell.layer.transform = CATransform3DIdentity;
//            cell.alpha = 1;
//        }
//                         completion:^ (BOOL completed) {} ];
//    }
//    else if (indexPath.row==2)
//    {
//        CATransform3D rotationTransform = CATransform3DTranslate(CATransform3DIdentity,0,500,0);
//        cell.layer.transform = rotationTransform;
//        cell.alpha = 0;
//
//        [UIView animateWithDuration:1.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//
//            cell.layer.transform = CATransform3DIdentity;
//            cell.alpha = 1;
//        }
//                         completion:^ (BOOL completed) {} ];
//    }
//}
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    NSString *strImportType = [arrImportType objectAtIndex:(int)indexPath.row];
    NSArray *arrRegionName = [dictResponse valueForKeyPath:[NSString stringWithFormat:@"%@.regionname",strImportType]];
    
    NSArray *arrPercentageValue = [dictResponse valueForKeyPath:[NSString stringWithFormat:@"%@.percentage",strImportType]];
    
    NSArray *arrRegionQtm = [dictResponse valueForKeyPath:[NSString stringWithFormat:@"%@.totalqtm",strImportType]];
    
    
    NSMutableArray *arrTempQtmForLegend = [[NSMutableArray alloc]init];
    
    for (int i=0; i<[arrRegionQtm count]; i++)
    {
        NSString *strNewValue = [NSString stringWithFormat:@"%@ MW/h",[arrRegionQtm objectAtIndex:i]];
        
        [arrTempQtmForLegend addObject:strNewValue];
        
    }
    
    NSArray *arrMaxRevision = [dictResponse valueForKeyPath:[NSString stringWithFormat:@"%@.maxrevision",strImportType]];
    
    
    NSArray *Revision = [dictResponse valueForKeyPath:[NSString stringWithFormat:@"%@.revision",strImportType]];
    
    
    PieChartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"piecell" forIndexPath:indexPath];
    cell.ViewPiechart.delegate = self;
    cell.LabelFoter.text=[arrImportType objectAtIndex:(int)indexPath.row];
    [cell.ViewPiechart setNoDataText:@"No data available."];
    
    [cell.ViewPiechart animateWithXAxisDuration:2 easingOption:ChartEasingOptionEaseInCirc];
    

    
    NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < arrRegionName.count; i++)
    {
        NSDictionary *dictDataForSegue=@{@"date":strDate,@"region":[arrRegionName objectAtIndex:i],@"importtype":strImportType,@"Maxrevision":[arrMaxRevision objectAtIndex:i],@"revision":[Revision objectAtIndex:i]};
        
        PieChartDataEntry *myPieChartDataEntry = [[PieChartDataEntry alloc]initWithValue:[[arrPercentageValue objectAtIndex:i]doubleValue] label:[arrRegionName objectAtIndex:i]data:dictDataForSegue];
        
        [arrTemp addObject:myPieChartDataEntry];
        
    }
    
   // PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:arrTemp label:@""];
    
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc]initWithEntries:arrTemp label:@""];
    //dataSet.sliceSpace = 2.0;
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    [colors addObject:[UIColor colorWithRed:193/255.f green:37/255.f blue:82/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:255/255.f green:102/255.f blue:0/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:245/255.f green:199/255.f blue:0/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:106/255.f green:150/255.f blue:31/255.f alpha:1.f]];
    [colors addObject:[UIColor colorWithRed:179/255.f green:100/255.f blue:53/255.f alpha:1.f]];
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:6.f]];
    [data setValueTextColor:UIColor.blackColor];
    dataSet.selectionShift =8;
    
    
    //MARK :- Commented Code By LOKESH(1-3) line
  //  cell.ViewPiechart.descriptionText =@"";
    //[cell.ViewPiechart.legend setCustomWithColors:colors labels:arrTempQtmForLegend];
   // cell.ViewPiechart.legend.position = ChartLegendPositionLeftOfChartCenter;
    
    
    
    cell.ViewPiechart.legend.yEntrySpace = 12.0f;
    //    cell.ViewPiechart.legend.textColor=[UIColor whiteColor];
    cell.ViewPiechart.data = data;
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight
{
    
    Real_Time_SchedulingViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"RealTimeid"];
    newView.dicttResponse=entry.data;
    [self.navigationController pushViewController:newView animated:YES];
    
    
}

- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    
    
}



@end
