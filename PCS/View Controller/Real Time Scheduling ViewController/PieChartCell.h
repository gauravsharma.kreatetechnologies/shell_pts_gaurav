//
//  PieChartCell.h
//  PCS
//
//  Created by pavan yadav on 12/10/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;

@interface PieChartCell : UITableViewCell
@property (weak, nonatomic) IBOutlet PieChartView *ViewPiechart;
@property (weak, nonatomic) IBOutlet UILabel *LabelFoter;
@property (weak, nonatomic) IBOutlet UILabel *labelquantumfirst;

@end
