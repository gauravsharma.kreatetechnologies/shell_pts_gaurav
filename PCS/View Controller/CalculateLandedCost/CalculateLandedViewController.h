//
//  CalculateLandedViewController.h
//  pcs newM
//
//  Created by pavan yadav on 26/09/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculateLandedViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *BtnStatebtn;
@property (weak, nonatomic) IBOutlet UIButton *Btnvoltagel;


@property (weak, nonatomic) IBOutlet UIButton *Btnclear;
@property (weak, nonatomic) IBOutlet UIButton *BtnCalculate;


@property (weak, nonatomic) IBOutlet UITextField *textQuantity;
@property (weak, nonatomic) IBOutlet UITextField *texttotalhr;
@property (strong, nonatomic) IBOutlet UITextField *textPrice;



@property (weak, nonatomic) IBOutlet UIButton *BtnBuy;
@property (weak, nonatomic) IBOutlet UIButton *BtnSell;
@property (weak, nonatomic) IBOutlet UIButton *BtnState;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerview;



@property (weak, nonatomic) IBOutlet UIButton *BtnView;
@property (weak, nonatomic) IBOutlet UIView *ViewAnimation;
@property (weak, nonatomic) IBOutlet UIImageView *arrowimage;

@property (weak, nonatomic) IBOutlet UILabel *lbllangedCost;

@property (weak, nonatomic) IBOutlet UIButton *BtnVoltageLevel;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;


@end
