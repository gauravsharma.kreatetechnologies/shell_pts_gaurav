//
//  CalculateLandedViewController.m
//  pcs newM
//
//  Created by pavan yadav on 26/09/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import "CalculateLandedViewController.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "landedcostListViewController.h"
#import "LogonViewController.h"
#import "MenuViewController.h"
#import "SlideNavigationController.h"
#import "CustmObj.h"
@class SideMenuViewController;

@interface CalculateLandedViewController ()<UITextFieldDelegate>
{
    BOOL blinkStatus;
    NSTimer *timer;
    UIAlertAction *firstAction;
    
    NSMutableArray *ArrState;
    NSMutableArray *ArrVoltage;
    NSArray *arrResponse;
    
    NSMutableArray *arrlevel;
    
    Boolean level;
    NSString *TypeOfEntity;
    NSString *dicmid;
    NSString *Voltagelevel;
    NSString *Lognotification;
    NSDictionary *Dicttpassdataresp;
    
}
@property(nonatomic,retain) UIToolbar *keyboardToolbar;

@end


@implementation CalculateLandedViewController
@synthesize keyboardToolbar;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    if(keyboardToolbar==nil)
        
    {
        keyboardToolbar=[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,self.view.bounds.size.width,35)];
        
        UIBarButtonItem *previousButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"765-arrow-left.png"] style:UIBarButtonItemStyleDone target:self action:@selector(previousField:)];
        
        UIBarButtonItem *nextButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"766-arrow-right.png"] style:UIBarButtonItemStyleDone target:self action:@selector(nextField:)];
        
        UIBarButtonItem *extraspace=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard:)];
        
        [keyboardToolbar setItems:[[NSArray alloc]initWithObjects:previousButton,nextButton,extraspace,doneButton, nil]];
        
        previousButton.width=70.0;
        
        nextButton.width=70.0;
        
        [keyboardToolbar setBarStyle:UIBarStyleDefault];
    }
    
    _textQuantity.inputAccessoryView=keyboardToolbar;
    _texttotalhr.inputAccessoryView=keyboardToolbar;
    _textPrice.inputAccessoryView=keyboardToolbar;
    
    
    
    
    
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
    
    blinkStatus = NO;
    _pickerview.hidden=YES;
    _ViewAnimation.hidden=YES;
      [_ViewAnimation setAlpha:0];
    
    [self StateinitialServiceHitData];
    
    _BtnStatebtn.layer.cornerRadius = 5; // this value vary as per your desire
    _BtnStatebtn.clipsToBounds = YES;
    _Btnvoltagel.layer.cornerRadius = 5;
    _Btnvoltagel.clipsToBounds = YES;
    
    _Btnclear.layer.cornerRadius = 10;
    _Btnclear.clipsToBounds = YES;
    _BtnCalculate.layer.cornerRadius = 10;
    _BtnCalculate.clipsToBounds = YES;
    
    _BtnView.layer.cornerRadius = 10;
    _BtnView.clipsToBounds = YES;
    _BtnVoltageLevel.userInteractionEnabled = false;
    
    
    timer = [NSTimer
             scheduledTimerWithTimeInterval:(NSTimeInterval)(0.50)
             target:self
             selector:@selector(blink)
             userInfo:nil
             repeats:TRUE];
    
    
    
    firstAction = [UIAlertAction actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                               
                                           }];
}




-(void) fadeOut{
    [UILabel beginAnimations:NULL context:nil];
    [UILabel setAnimationDuration:2.0];
    [_ViewAnimation setAlpha:1];
    [UILabel commitAnimations];
}




-(void)blink{
    if(blinkStatus == NO){
        
        _arrowimage.hidden=NO;
        blinkStatus = YES;
    }else {
        
        blinkStatus = NO;
        _arrowimage.hidden=YES;
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _pickerview.hidden=YES;
//    _ViewAnimation.hidden=YES;
    
    [_textQuantity resignFirstResponder];
    [_texttotalhr resignFirstResponder];
    [_textPrice resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)StateinitialServiceHitData
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/landed_cost/getstatediscom.php"
                                          param:dictFinaldata andCompletion:^(ResponseType type, id response) {
                                              
                                              if (type == kResponseTypeFail)
                                              {
                                                 // [SVProgressHUD dismiss];
                                                  [CustmObj removeHud:self.view];
                                                  
                                                  NSError *error=(NSError*)response;
                                                  
                                                  if (error.code == -1009) {
                                                      
                                                      UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                                                     message:@"The Internet connection appears to be offline."
                                                                                                              preferredStyle:UIAlertControllerStyleAlert];
                                                      
                                                      UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                            }];
                                                      
                                                      [alert addAction:thirdAction];
                                                      
                                                  }
                                                  
                                              }
                                              else if (type == kresponseTypeSuccess)
                                              {
                                                  
                                                  arrResponse=response;
                                                  
                                                  
                                                  ArrState=[[NSMutableArray alloc]init];
                                                  
                                                  
                                                  int i;
                                                  
                                                  for ( i=0;i<[response count]; i++)
                                                      
                                                  {
                                                      
                                                      
                                                      [ArrState insertObject:[[response valueForKey:@"code"] objectAtIndex:i] atIndex:i];
                                                      
                                                      
                                                      ;
                                                  }
        // To set the title & value.-----------------------------------------
                                                  [_BtnState setTitle:[ArrState objectAtIndex:0] forState:UIControlStateNormal];
                                                  
                                                  
                                                  [_BtnVoltageLevel setTitle:[[[arrResponse objectAtIndex:0] valueForKey:@"voltage"] objectAtIndex:0]  forState:UIControlStateNormal];
                                                  
                                                  Voltagelevel = [[[arrResponse objectAtIndex:0] valueForKey:@"voltage"] objectAtIndex:0];
                                                 
   dicmid=[[arrResponse objectAtIndex:0]  valueForKey:@"discom_id"] ;
                                                  
                                                  [_pickerview reloadAllComponents];
                                                  
                                              }
                                              
                                          }];
}



# pragma mark-  Button Action.

- (IBAction)Buybtn:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    int tag=(int)[btn tag];
    {
        
        if (tag==1)
        {
            [self.BtnBuy setImage:[UIImage imageNamed:@"Checkedimg.png"] forState:UIControlStateNormal];
            [self.BtnSell setImage:[UIImage imageNamed:@"uncheckedimg.png"] forState:UIControlStateNormal];
            
            TypeOfEntity=@"BUYER";
        }
        else
        {
            [self.BtnSell setImage:[UIImage imageNamed:@"Checkedimg.png"] forState:UIControlStateNormal];
            [self.BtnBuy setImage:[UIImage imageNamed:@"uncheckedimg.png"] forState:UIControlStateNormal];
            TypeOfEntity=@"SELLER";
        }
    }
}


- (IBAction)BtnState:(id)sender
{
    _BtnVoltageLevel.userInteractionEnabled = true;
    [_textQuantity resignFirstResponder];
    [_texttotalhr resignFirstResponder];
    [_textPrice resignFirstResponder];
    level=0;
    _pickerview.hidden=NO;
    [_pickerview reloadAllComponents];
    
}




- (IBAction)BtnVoltageLevel:(id)sender
{
    [_textQuantity resignFirstResponder];
    [_texttotalhr resignFirstResponder];
    [_textPrice resignFirstResponder];
    level=1;
    
    [_pickerview reloadAllComponents];
    _pickerview.hidden=NO;
    
    
}

# pragma mark-picker View

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (level==0)
    {
        return [ArrState count];
    }
    return [arrlevel count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (level==0)
    {
        [_BtnState setTitle:[ArrState objectAtIndex:row] forState:UIControlStateNormal]; // To set the title
        [_BtnState setEnabled:YES];
        
        arrlevel= [[arrResponse valueForKey:@"voltage"] objectAtIndex:row];
        
        dicmid=[[arrResponse valueForKey:@"discom_id"] objectAtIndex:row];
        
        
        
        if (arrlevel.count>0) {
            
            [_Btnvoltagel setTitle:@"Select Voltage" forState:UIControlStateNormal];
            [_Btnvoltagel setEnabled:YES];
        }
        else
        {
            arrlevel=[[NSMutableArray alloc]init];
            [arrlevel insertObject:@"Voltage" atIndex:0];
            
            [_Btnvoltagel setTitle:@"Voltage" forState:UIControlStateNormal]; // To set the title
            [_Btnvoltagel setEnabled:NO];
        }
        
        
        
    }
    else
    {
        
        [_BtnVoltageLevel setTitle:[arrlevel objectAtIndex:row] forState:UIControlStateNormal]; // To set the title
        [_BtnVoltageLevel setEnabled:YES];
        
        Voltagelevel=[arrlevel objectAtIndex:row];
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    if (level==0)
    {
        return [ArrState objectAtIndex:row];
    }
    
    return [arrlevel objectAtIndex:row];
}


- (IBAction)BtnClear:(id)sender
{
    [_ViewAnimation setAlpha:0];
    _textQuantity.text=nil;
    _texttotalhr.text=nil;
    _textPrice.text=nil;
    _ViewAnimation.hidden=YES;
    
    [self.BtnSell setImage:[UIImage imageNamed:@"uncheckedimg.png"] forState:UIControlStateNormal];
    [self.BtnBuy setImage:[UIImage imageNamed:@"uncheckedimg.png"] forState:UIControlStateNormal];
}


- (IBAction)BtnCalculate:(id)sender
{ [_textPrice resignFirstResponder];
    if ([TypeOfEntity isEqualToString:@"BUYER"]||[TypeOfEntity isEqualToString:@"SELLER"])
    {
        if (!([_BtnStatebtn.titleLabel.text isEqual:@"Select State"]))
        {
            
            
            if (!([_Btnvoltagel.titleLabel.text isEqual:@"Select Voltage"]))
            {
                
                if (!([_Btnvoltagel.titleLabel.text isEqual:@"Voltage"]))
                {
                    
                    if ([_textQuantity.text integerValue] >0 && [_texttotalhr.text integerValue] >0 && [_textPrice.text integerValue] >0)
                        
                    {
                        [self initialServiceHitData];
                       _ViewAnimation.hidden=NO;
                        
//                        CGRect frame = _ViewAnimation.frame;
//                        [UIView beginAnimations:nil context:nil];
//                        [UIView setAnimationDuration:1];
//                        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//                        frame.origin.x +=45; // slide right
//                        _ViewAnimation.frame = frame;
//                        [UIView commitAnimations];
//                        
                        
                        
                            [NSTimer scheduledTimerWithTimeInterval:.80  target:self selector:@selector(fadeOut) userInfo:nil
                                                            repeats:NO];
                        
                    }
                    else
                    {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                                       message:@"Please fill all the details"
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:firstAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                }
                else
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                                   message:@"There are not found Voltage in the Selected State,Please select another state "
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:firstAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                
            }
            else
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                               message:@"Please select Voltage"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:firstAction];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }
        else
        {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:@"Please select State"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:firstAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:@"Please select Type of Entity"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alert addAction:firstAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}




-(void)initialServiceHitData
{
 
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",TypeOfEntity,@"client_type",dicmid,@"discom_id",Voltagelevel,@"voltage_level",_textQuantity.text,@"quantum",_texttotalhr.text,@"no_of_hours",_textPrice.text,@"price",@"",@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/landed_cost/landedcost_calc.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
//            double result;
//            NSString* val=[NSString stringWithFormat:@"%@",response[@"landed_cost"]];
//            float fCost = [val floatValue];
//            result=[_textIEX.text integerValue] +fCost;
//            NSString *test = [NSString stringWithFormat:@"%.2f", result];
            _lbllangedCost.text= [NSString stringWithFormat:@"%@",response[@"landed_cost"]];;
            
            Dicttpassdataresp=response;
         // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
        }
        
    }];
}


- (IBAction)BtnLandedCost:(id)sender {
    
    landedcostListViewController *vcDetailedURS = [self.storyboard instantiateViewControllerWithIdentifier:@"LandedId"];

    vcDetailedURS.dicttResponse=Dicttpassdataresp;
    
    [self.navigationController pushViewController:vcDetailedURS animated:YES];

}

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
    [_ViewAnimation setAlpha:0];
    _textQuantity.text=nil;
    _texttotalhr.text=nil;
    _textPrice.text=nil;
    _ViewAnimation.hidden=YES;
    
    [self.BtnSell setImage:[UIImage imageNamed:@"uncheckedimg.png"] forState:UIControlStateNormal];
    [self.BtnBuy setImage:[UIImage imageNamed:@"uncheckedimg.png"] forState:UIControlStateNormal];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
    
}





- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _pickerview.hidden=YES;
    return YES;
}


- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    
    
}

//Pragma Mark KeyBoard ------------------

-(void)resignKeyboard:(id)sender
{
    if ([_textQuantity isFirstResponder])
        [_textQuantity resignFirstResponder];
    if ([_texttotalhr isFirstResponder])
        [_texttotalhr resignFirstResponder];
    if ([_textPrice isFirstResponder])
        [_textPrice resignFirstResponder];
    
}
- (IBAction)nextField:(id)sender
{
    if ([_textQuantity isFirstResponder])
        [_texttotalhr becomeFirstResponder];
    else if ([_texttotalhr isFirstResponder])
        [_textPrice becomeFirstResponder];
//    else if ([_secondTxt isFirstResponder])
//        [_firstTxt becomeFirstResponder];
    
}

-(IBAction)previousField:(id)sender
{
    if([_texttotalhr isFirstResponder])
        [_textQuantity becomeFirstResponder];
    else if([_textPrice isFirstResponder])
        [_texttotalhr becomeFirstResponder];
    
}





@end
