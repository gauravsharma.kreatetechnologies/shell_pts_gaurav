//
//  stateLoadViewController.swift
//  ptsNewModule
//
//  Created by pavan yadav on 05/04/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts

class stateLoadViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource ,UIPickerViewDelegate ,UIPickerViewDataSource ,ChartViewDelegate
{
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var buttonRegional: UIButton!
    @IBOutlet var lineChartView: LineChartView!
    @IBOutlet var labelDate: UILabel!
    
    var typeStr = ""
    
    
    
    var Lognotification = ""
    var arrPicker = [String]()
    var arrResponse = [JSON]()
    var arrValue = [JSON]()
    var State = [String]()
    
    var demand = [String]()
    var Scheduling = [String]()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.separatorColor = UIColor .clear
        pickerView.isHidden = true
        
    
        
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        if typeStr.characters.count>0
        {
            buttonRegional.setTitle(typeStr, for: UIControlState .normal)
        }
        else
        {
            typeStr = "NRLDC"
        }
                
        self.ApiCalling()
        
    }
    
    //1. Calling Initial api and getting response..
    func ApiCalling()
    {
        //SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["state_id" : AppHelper.userDefaults(forKey: "StateId") ,"type" : AppHelper.userDefaults(forKey: "BidType")]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL:"http://www.mittalpower.com/pts-client/api/index.php/mobile/getdashboarddata", parameters:parameters){(APIData) -> Void in
                
                
                self.arrResponse = [APIData]
                self.arrValue = self.arrResponse[0]["stateload"][self.typeStr].arrayValue
                self.labelDate.text = "\("Updated at:")\(self.arrResponse[0]["stateload"]["NRLDC"][0]["time"].stringValue)"
                
                
                for (key, subJson) in self.arrResponse[0]["stateload"]
                {
                    
                    self.arrPicker.append(key)
                }
                
                self.pickerView .reloadAllComponents()
                self.tableView.reloadData()
                self.chartMethode()
                //SVProgressHUD.dismiss()
                CustomHUD.shredObject.removeXib(view: self.view)
            }
            
        }
            
        catch
        {
            
        }
     
    }
    
    @IBAction func buttonRegional(_ sender: Any)
    {
        pickerView.isHidden = false
    }
    
    
    
    
    // PICKER VIEW METHODE CALLING-----------------------------------------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        
        return 1
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        return arrPicker.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        
        return arrPicker[row]
    }
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        
        pickerView.isHidden = true
        buttonRegional.setTitle(arrPicker[row], for: UIControlState .normal)
        self.arrValue = self.arrResponse[0]["stateload"][arrPicker[row]].arrayValue
        
        tableView.reloadData()
        self.chartMethode()
        
    }
    
    
    
    
    
    // TABLE VIEW METHODE----------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrValue.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "stateCell", for: indexPath)as! commonTableViewCell
        Cell.selectionStyle = .none
        
        
        
        Cell.labelSerial.text = "\(indexPath.row + 1)"
        
        if  self.arrValue[indexPath.row]["generation"].stringValue.characters.count == 0
        {
            Cell.labelGeneration.text = "-"
        }
        else
        {
            Cell.labelGeneration.text = self.arrValue[indexPath.row]["generation"] .stringValue
        }
        
        Cell.labelState.text = self.arrValue[indexPath.row]["state"] .stringValue
        
        Cell.labelDemand.text = self.arrValue[indexPath.row]["demand"] .stringValue
        Cell.labelSCHDrawal.text = self.arrValue[indexPath.row]["sch_drawal"] .stringValue
        Cell.labelACTDrawal.text = self.arrValue[indexPath.row]["act_drawal"] .stringValue
        Cell.labelDeviation.text = self.arrValue[indexPath.row]["deviation"] .stringValue
        
        return Cell
    }
    
    
    
    
    // LineChart  methode--------------------------------------------------------
    
    func chartMethode()
    {
        demand.removeAll()
        Scheduling.removeAll()
        State.removeAll()
        
        for i in 0..<arrValue.count
        {
            self.demand.append(self.arrValue[i]["demand"] .stringValue)
            self.Scheduling.append(self.arrValue[i]["sch_drawal"] .stringValue)
            
            
            self.State.append(self.arrValue[i]["state"] .stringValue)
            
        }
        
        let xAxis: XAxis? = lineChartView.xAxis
        
        xAxis?.labelFont = UIFont .systemFont(ofSize: 7.0)
        xAxis?.labelPosition = .top
        xAxis?.granularity = 2.0
        xAxis?.valueFormatter = IndexAxisValueFormatter(values: State)

        
        lineChartView.chartDescription.text="";
        
        lineChartView.leftAxis.drawLabelsEnabled = true
        lineChartView.leftAxis.enabled = true
        lineChartView.xAxis.drawLabelsEnabled = true
        lineChartView.xAxis.enabled = true
        lineChartView.rightAxis.drawLabelsEnabled = true
        lineChartView.rightAxis.enabled = true
        
        lineChartView.borderColor = UIColor .gray
        lineChartView.borderLineWidth = 0.5
        lineChartView.legend.enabled = true
        lineChartView.legend.textHeightMax=1.0;
        
        lineChartView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        
        setChart(lineChartView,dataPoints:Scheduling, values: demand)
    }
    
    
    
    
    fileprivate func setChart(_ lineChartView: LineChartView, dataPoints: [String], values: [String])
    {
        
        let leftLabel = lineChartView.leftAxis
        leftLabel.labelFont = UIFont .systemFont(ofSize: 7.0)
        
        
        
        var dataEntries: [ChartDataEntry] = []
        var dataEntrires2: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = ChartDataEntry(x: Double(i+1), y: Double(demand[i])!)
            let dataEntry2 = ChartDataEntry(x: Double(i+1), y: Double(Scheduling[i])!)
            
            dataEntries.append(dataEntry)
            dataEntrires2.append(dataEntry2)
        }
        
        let xaxis = lineChartView.xAxis
        
        xaxis.labelFont = UIFont .systemFont(ofSize: 5.0)
        xaxis.granularity = 2.0
        xaxis.labelPosition = .top
        xaxis.drawLabelsEnabled = true
        xaxis.drawLimitLinesBehindDataEnabled = true
        xaxis.avoidFirstLastClippingEnabled = true
        
        
        xaxis.labelPosition = .top
        xaxis.centerAxisLabelsEnabled = true
        
        
        xaxis.valueFormatter = IndexAxisValueFormatter(values: State)
        
        //arrMin Line Data Setting
        
        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "Demand")
        lineChartDataSet.setColor(UIColor.green)
        lineChartDataSet.mode = .linear
        lineChartDataSet.fillColor  = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lineChartDataSet.drawCirclesEnabled = true
        lineChartDataSet.lineWidth = 1.0
      
        lineChartDataSet.circleRadius = 3.0
        lineChartDataSet.circleColors = [#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)]
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = true
        lineChartDataSet.drawFilledEnabled = true
        lineChartDataSet.colors = [#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)]
        
        
        let lineChartDataSet2 = LineChartDataSet(entries: dataEntrires2, label: "SCH.Drawal")
        lineChartDataSet2.setColor(UIColor.blue)
        lineChartDataSet2.mode = .linear
        lineChartDataSet2.drawCirclesEnabled = true
        lineChartDataSet2.lineWidth = 1.0
//        lineChartDataSet2.circleHoleRadius = 1.0
        lineChartDataSet2.circleRadius = 3.0
        lineChartDataSet2.circleColors = [#colorLiteral(red: 0.8550179037, green: 1, blue: 0.4034725341, alpha: 1)]
        lineChartDataSet2.fillColor  = #colorLiteral(red: 0.8550179037, green: 1, blue: 0.4034725341, alpha: 1)
        lineChartDataSet2.drawHorizontalHighlightIndicatorEnabled = true
        lineChartDataSet2.drawFilledEnabled = true
        lineChartDataSet2.colors = [#colorLiteral(red: 0.8550179037, green: 1, blue: 0.4034725341, alpha: 1)]
        
        
        
        
        var dataSets = [ChartDataSet]()
        
        dataSets.append(lineChartDataSet)
        dataSets.append(lineChartDataSet2)
        
        let lineChartData = LineChartData(dataSets: dataSets)
        lineChartView.data = lineChartData
       // SVProgressHUD.dismiss()
        CustomHUD.shredObject.removeXib(view: self.view)
    }
    
    
    
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    
    
    
    
    //pragma mark - SlideNavigationController Methods -
    
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    
}
