//
//  MainScreenTableViewCell.h
//  PCS
//
//  Created by lab4code on 14/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainScreenTableViewCell : UITableViewCell
{
    
}
@property(weak,nonatomic)IBOutlet UIImageView *imgMain;
@property(weak,nonatomic)IBOutlet UILabel *lblPortifolioCode;
@property(weak,nonatomic)IBOutlet UILabel *lblNocBuy;
@property(weak,nonatomic)IBOutlet UILabel *lblNocSell;
@property(weak,nonatomic)IBOutlet UILabel *lblRegistration;

@property(weak,nonatomic)IBOutlet UILabel *lblPortifolioCodeHeader;
@property(weak,nonatomic)IBOutlet UILabel *lblNocBuyHeader;
@property(weak,nonatomic)IBOutlet UILabel *lblNocSellHeader;
@property(weak,nonatomic)IBOutlet UILabel *lblRegistrationHeader;

@end
