//
//  MainScreenViewController.m
//  PCS
//
//  Created by lab4code on 14/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "MainScreenViewController.h"
#import "Services.h"
#import "MainScreenTableViewCell.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "NewBidMainViewController.h"
#import "ReportViewController.h"
#import "AppDelegate.h"
#import "NewBidViewController.h"
#import "CustmObj.h"

@class  ReportViewController;
@interface MainScreenViewController ()<UUChartDataSource>
{
    UUChart *chartView;
    NSMutableArray *arrDate;
    NSMutableArray *arrIEXValues;
    NSMutableArray *arrPXILValues;
    int row;
    
    BOOL isGreenBtnSelected;
    BOOL isRedBtnSelected;
    
}

@property(strong,nonatomic)NSMutableDictionary *dictData;
@property(weak,nonatomic)IBOutlet UITableView *myTableView;
@property(weak,nonatomic)IBOutlet UIView *graphView;


-(IBAction)menuButtonClick:(id)sender;

@end

@implementation MainScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view.
    
    
    arrDate=[[NSMutableArray alloc]init];
    arrIEXValues=[[NSMutableArray alloc]init];
    arrPXILValues=[[NSMutableArray alloc]init];
    
      NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"access_key"],@"accesskey", nil];
    
    
    isGreenBtnSelected=YES;
    isRedBtnSelected=YES;
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    //  NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
     [[Services sharedInstance]serviceCallbyPost:@"html/fulldetail.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
     
       // NSLog(@"Response %@",response);
         
         
         if (type == kResponseTypeFail)
         {
             // NSLog(@"fail Response:----> %@",response);
             self.view.userInteractionEnabled=YES;
            // [SVProgressHUD dismiss];
             [CustmObj removeHud:self.view];
             
             
             NSError *error=(NSError*)response;
             
             if (error.code == -1009) {
                 
                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                message:@"The Internet connection appears to be offline."
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                       style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                           
                                                                           // NSLog(@"You pressed button three");
                                                                       }];
                 
                 [alert addAction:thirdAction];
                 [self presentViewController:alert animated:YES completion:nil];
             }
             
             
             
         }
         else if (type == kresponseTypeSuccess)
         {
         
         //[SVProgressHUD dismiss];
             [CustmObj removeHud:self.view];
         
          NSMutableDictionary *dictResponsedata=(NSMutableDictionary *)response;
         if (dictResponsedata)
         {
             self.dictData=dictResponsedata;
             
             
             if ([dictResponsedata objectForKey:@"seriesData"])
             {
                 for (NSMutableDictionary *dict in [dictResponsedata objectForKey:@"seriesData"]) {
                    
                     if ([dict objectForKey:@"DATE"] && [dict objectForKey:@"IEX"] && [dict objectForKey:@"PXIL"])
                     {
                         [arrDate addObject:[dict objectForKey:@"DATE"]];
                         [arrIEXValues addObject:[dict objectForKey:@"IEX"]];
                         [arrPXILValues addObject:[dict objectForKey:@"PXIL"]];
                         
                         
                         
                        
                         
                     }
                     
                     
                 }
             }
             
             
             
             [self.myTableView reloadData];
             
         }
         
             
             
         }
     
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - graph



- (NSArray *)UUChart_xLableArray:(UUChart *)chart
{
    
    return arrDate;
}

- (NSArray *)UUChart_yValueArray:(UUChart *)chart
{
    if (isGreenBtnSelected && isRedBtnSelected)
    {
        return @[arrIEXValues,arrPXILValues];
    }
    else
    {
        if (isGreenBtnSelected)
        {
            return @[arrIEXValues];
        }
        else if(isRedBtnSelected)
        {
            return @[arrPXILValues];
        }
        else
        {
            return nil;
        }
    }
    
    
}
#pragma mark - @optional

- (NSArray *)UUChart_ColorArray:(UUChart *)chart
{
    if (isGreenBtnSelected && isRedBtnSelected)
    {
         return @[UUGreen,UURed];
    }
    else
    {
        if (isGreenBtnSelected)
        {
             return @[UUGreen];
        }
        else if(isRedBtnSelected)
        {
             return @[UURed];
        }
        else
        {
            return nil;
        }
    }
    
}

- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart
{
   // return CGRangeMake(100, 0);
     return CGRangeZero;
}

#pragma mark


- (CGRange)UUChartMarkRangeInLineChart:(UUChart *)chart
{
    //return CGRangeMake(25, 75);
    return CGRangeZero;
}


- (BOOL)UUChart:(UUChart *)chart ShowHorizonLineAtIndex:(NSInteger)index
{
    return YES;
}


- (BOOL)UUChart:(UUChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return 1;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)menuButtonClick:(id)sender
{
   //[SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table View Delegate and datasource
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
 
    cell.alpha = 0;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        cell.alpha = 1;
    }
                     completion:^ (BOOL completed) {} ];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (section == 0)
//    {
//        return 1;
//    }
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0) {
//        return 252;
//    }
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0)
//    {
//        static NSString *simpleTableIdentifier = @"graphCell";
//      //  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
//
//         UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
//                                       reuseIdentifier: simpleTableIdentifier];
//
//
//
//        row=(int)indexPath.row;
//
//
//
//        cell.backgroundColor=[UIColor clearColor];
//
//        chartView=nil;
//
//
//        chartView= [[UUChart alloc]initwithUUChartDataFrame:CGRectMake(28, 3, [UIScreen mainScreen].bounds.size.width-40, 200)
//                                                     withSource:self
//                                                      withStyle:UUChartLineStyle];
//
//
//
//
//
//
//        chartView.backgroundColor=[UIColor clearColor];
//
//        [chartView showInView:cell.contentView];
//
//
//        UILabel *labelVertical = [[UILabel alloc] initWithFrame:CGRectMake(-35, 80, 120, 20)];
//        labelVertical.font = [UIFont systemFontOfSize:14];
//        labelVertical.textAlignment = NSTextAlignmentCenter;
//        labelVertical.textColor = [UIColor whiteColor];
//        [labelVertical setText:@"Values in Mw/h"];
//        labelVertical.transform=CGAffineTransformMakeRotation( -M_PI / 2);
//        [cell.contentView addSubview:labelVertical];
//
//
//        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-30,208,100,20)];
//        label.font = [UIFont systemFontOfSize:14];
//        label.textAlignment = NSTextAlignmentCenter;
//        label.textColor = [UIColor whiteColor];
//        label.text = @"Trading Date";
//        [cell.contentView addSubview:label];
//
//
//        UIButton *btnGreen=[UIButton buttonWithType:UIButtonTypeSystem];
//        btnGreen.frame=CGRectMake([UIScreen mainScreen].bounds.size.width/2-60, 230, 62, 17);
//        [btnGreen addTarget:self action:@selector(greenIEXClick) forControlEvents:UIControlEventTouchUpInside];
//        if (isGreenBtnSelected) {
//            [btnGreen setBackgroundImage:[UIImage imageNamed:@"IEX_graph"] forState:UIControlStateNormal];
//        }
//        else
//        {
//            [btnGreen setBackgroundImage:[UIImage imageNamed:@"inactive-IEX"] forState:UIControlStateNormal];
//        }
//        [cell.contentView addSubview:btnGreen];
//
//        UIButton *btnRed=[UIButton buttonWithType:UIButtonTypeSystem];
//        btnRed.frame=CGRectMake(btnGreen.frame.origin.x+75, 230, 75, 17);
//        [btnRed addTarget:self action:@selector(redPXILClick) forControlEvents:UIControlEventTouchUpInside];
//        if (isRedBtnSelected) {
//
//            [btnRed setBackgroundImage:[UIImage imageNamed:@"PXIL_graph"] forState:UIControlStateNormal];
//
//        }
//        else
//        {
//
//            [btnRed setBackgroundImage:[UIImage imageNamed:@"inactive-PXIL"] forState:UIControlStateNormal];
//
//        }
//        [cell.contentView addSubview:btnRed];
//
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//
//        return cell;
//    }
    
    static NSString *simpleTableIdentifier = @"MainScreenCell";
    MainScreenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.lblPortifolioCodeHeader.text=@"";
    cell.lblNocBuyHeader.text=@"";
    cell.lblNocSellHeader.text=@"";
    cell.lblRegistrationHeader.text=@"";
    
    
    if (indexPath.row == 1) {
        
        cell.imgMain.image=[UIImage imageNamed:@"IEX"];
        
        if ([self.dictData objectForKey:@"portfolioIex"])
        {
            cell.lblPortifolioCode.text=[self.dictData objectForKey:@"portfolioIex"];
        }
        
        if ([self.dictData objectForKey:@"iexNocDetailBuy"])
        {
            cell.lblNocBuy.text=[self.dictData objectForKey:@"iexNocDetailBuy"];
        }
        
        if ([self.dictData objectForKey:@"iexNocDetailSell"])
        {
            cell.lblNocSell.text=[self.dictData objectForKey:@"iexNocDetailSell"];
        }
        
        if ([self.dictData objectForKey:@"iexRegistrationDetail"])
        {
            cell.lblRegistration.text=[self.dictData objectForKey:@"iexRegistrationDetail"];
        }
        
        cell.lblPortifolioCodeHeader.text=@"Portfolio Code";
        cell.lblNocBuyHeader.text=@"NOC Valid Upto(Buy)";
        cell.lblNocSellHeader.text=@"NOC Valid Upto(Sell)";
        cell.lblRegistrationHeader.text=@"Registration Valid Upto";
        
        
    }
    else if(indexPath.row == 2)
    {
        cell.imgMain.image=[UIImage imageNamed:@"PXIL"];
        
        if ([self.dictData objectForKey:@"portfolioPxil"])
        {
            cell.lblPortifolioCode.text=[self.dictData objectForKey:@"portfolioPxil"];
        }
        
        if ([self.dictData objectForKey:@"pxilNocDetailBuy"])
        {
            cell.lblNocBuy.text=[self.dictData objectForKey:@"pxilNocDetailBuy"];
        }
        
        if ([self.dictData objectForKey:@"pxilNocDetailSell"])
        {
            cell.lblNocSell.text=[self.dictData objectForKey:@"pxilNocDetailSell"];
        }
        
        if ([self.dictData objectForKey:@"pxilRegistrationDetail"])
        {
            cell.lblRegistration.text=[self.dictData objectForKey:@"pxilRegistrationDetail"];
        }
        
        cell.lblPortifolioCodeHeader.text=@"Portfolio Code";
        cell.lblNocBuyHeader.text=@"NOC Valid Upto(Buy)";
        cell.lblNocSellHeader.text=@"NOC Valid Upto(Sell)";
        cell.lblRegistrationHeader.text=@"Registration Valid Upto";
        
    }
    else
    {
        cell.imgMain.image=[UIImage imageNamed:@"Transaction"];
        
        if ([self.dictData objectForKey:@"clientBalance"])
        {
            cell.lblPortifolioCode.text=[self.dictData objectForKey:@"clientBalance"];
        }
        
        if ([self.dictData objectForKey:@"lastpaid"])
        {
            cell.lblNocBuy.text=[self.dictData objectForKey:@"lastpaid"];
        }
        
        if ([self.dictData objectForKey:@"lastpaiddate"])
        {
            cell.lblNocSell.text=[self.dictData objectForKey:@"lastpaiddate"];
        }
        
        if ([self.dictData objectForKey:@"lastpaidmode"])
        {
          
            cell.lblRegistration.text=[self.dictData objectForKey:@"lastpaidmode"];
        }
        
        cell.lblPortifolioCodeHeader.text=@"Balance Amount";
        cell.lblNocBuyHeader.text=@"Last Amount Paid";
        cell.lblNocSellHeader.text=@"On Date";
        cell.lblRegistrationHeader.text=@"Payment Mode";
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0)
//    {
        if (indexPath.row == 0)
        {
            ReportViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportViewController"];
            [self.navigationController pushViewController:newView animated:YES];
        }
        else if (indexPath.row == 1)
        {
            //IEX
            NewBidViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"DAMNewBidId"];
            
           
//            newView.TypeSelection = @"IEX";
            
            [self.navigationController pushViewController:newView animated:YES];
            
            
            
            
//            
//            RealtimeApproval *raelview = [self.storyboard instantiateViewControllerWithIdentifier:@"RealTimeApproval"];
//            
//            
//            NSArray *arrTemp=[dictResponse[@"data"] objectAtIndex:indexPath.row];
//            
//            raelview.Dataid=[arrTemp valueForKeyPath:@"data_id"];
//            raelview.ApprovalNo=[arrTemp valueForKeyPath:@"approval_no"];
//            raelview.QuantumNo=strrevision;
//            
//            
//            
//            
//            [self.navigationController pushViewController:raelview animated:YES];
//            
//            
            
            
            
            
        }
        else
        {
            // PXIL
            NewBidViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"DAMNewBidId"];
//             newView.TypeSelection = @"PXIL";
            [self.navigationController pushViewController:newView animated:YES];
            
        }
//    }
}

-(void)greenIEXClick
{
    if (isGreenBtnSelected)
    {
        isGreenBtnSelected=NO;
    }
    else
    {
        isGreenBtnSelected=YES;
    }
    [self.myTableView reloadData];
}

-(void)redPXILClick
{
    if (isRedBtnSelected)
    {
        isRedBtnSelected=NO;
    }
    else
    {
        isRedBtnSelected=YES;
    }
    [self.myTableView reloadData];
}

-(IBAction)btnLogout_Click:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
