//
//  commonTableViewCell.swift
//  PCS
//
//  Created by pavan yadav on 05/04/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit
import Charts

class commonTableViewCell: UITableViewCell
{
    
// State load tableViewCell.
    @IBOutlet var labelSerial: UILabel!
    @IBOutlet var labelState: UILabel!
    @IBOutlet var labelGeneration: UILabel!
    @IBOutlet var labelDemand: UILabel!
    @IBOutlet var labelSCHDrawal: UILabel!
    @IBOutlet var labelACTDrawal: UILabel!
    @IBOutlet var labelDeviation: UILabel!
    
    
    //  MySchedule tableViewCell property.
    @IBOutlet var labelSeria: UILabel!
    @IBOutlet var labelImport: UILabel!
    @IBOutlet var labelBuyer: UILabel!
    @IBOutlet var labelSeller: UILabel!
    @IBOutlet var labelApproval: UILabel!
    @IBOutlet var labelQtm: UILabel!
    @IBOutlet weak var labelFiltermySchedule: UILabel!
    
     //  MySchedule GraphView property.
    @IBOutlet weak var SrNo: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var R31label: UILabel!
    @IBOutlet weak var R32label: UILabel!
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    
    
    
    
    
    
    
    
    
    
    
    

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
