//
//  FeedBackviewcontroller.m
//  FeedbackViewcontroller
//
//  Created by pavan yadav on 23/12/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import "FeedBackviewcontroller.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "LogonViewController.h"
#import "MenuViewController.h"
#import "SlideNavigationController.h"
#import "CustmObj.h"
@class SideMenuViewController;

@interface FeedBackviewcontroller ()

{
    NSString *Lognotification;
}
@end

@implementation FeedBackviewcontroller

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:TRUE];
}


- (IBAction)sendButton:(id)sender
{

    if ([_NameTextfield.text length]<3)
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Try again"
                                     message:@"Name should not be empty"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
        
    {
        [self MobileNovalidation];
        
    }
}


-(void)MobileNovalidation
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:_MobileNotxtfld.text] == YES)
    {
        if ([_MobileNotxtfld.text isEqualToString:@"0000000000"])
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:@"Please enter valid Mobile no."
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"ok"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                        }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        else
            
        {
            [self emailValidation];
        }
    }
    
    else
        
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@""
                                     message:@"Please enter valid Mobile no."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}



-(void) emailValidation
{
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    if ([emailTest evaluateWithObject:_EmailTextfield.text] == NO || [_EmailTextfield.text length]<8)
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@""
                                     message:@"Please Enter Valid Email Address."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else
        
    {
        if ([_FeedBackTextview.text length]>10)
        {
           [self callsendbutoon];
        }
        else
        {
            
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Feedback length should be greater than 10 characters"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        }
       
    }
    
}

#pragma mark - Api call -
- (void)callsendbutoon
{
    
   // NSLog(@"%@devicce id",[AppHelper userDefaultsForKey:@"deviceId"]);

    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"deviceId"],@"device_ip",_NameTextfield.text,@"name",_MobileNotxtfld.text,@"mobile",_EmailTextfield.text,@"email",_FeedBackTextview.text,@"feedback",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/submitfeedback.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {

            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Succes"
                                                                message:response[@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            
            _NameTextfield.text = nil;
            _MobileNotxtfld.text = nil;
            _EmailTextfield.text = nil;
            _FeedBackTextview.text = nil;
            
            _FeedBackTextview.textColor=[UIColor lightGrayColor];
            _FeedBackTextview.text=@"Type your message here..";

        }
        
    }];
}
#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [_FeedBackTextview resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
}


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
   
    if ([_FeedBackTextview.text isEqualToString:@"Type your message here.."])
    {
        _FeedBackTextview.text = nil;
        _FeedBackTextview.textColor = [UIColor blackColor];
    }
    
    return YES;
}


@end
