//
//  FeedBackviewcontroller.h
//  FeedbackViewcontroller
//
//  Created by pavan yadav on 23/12/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedBackviewcontroller : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *NameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *MobileNotxtfld;
@property (weak, nonatomic) IBOutlet UITextField *EmailTextfield;
@property (weak, nonatomic) IBOutlet UITextView *FeedBackTextview;
@property (weak, nonatomic) IBOutlet UIButton *Sendbtn;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@end
