//
//  Copy Bid Area.m
//  PCS
//
//  Created by Pavan Yadav on 24/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "Copy Bid Area.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "TablecellTableViewCell.h"

@implementation Copy_Bid_Area

{
    NSString *Copydate;
    NSString *Fromdate;
    NSString *ToDate;
    NSString *Datepass;
    
    NSMutableArray * arraydata;
}


- (void)viewDidLoad {
    arraydata = [[NSMutableArray alloc]init];
    [super viewDidLoad];
    self.datePickerView.hidden=YES;
   
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _DatePickerFrom.datePickerMode = UIDatePickerModeDate;
    _DatepickerTo.datePickerMode = UIDatePickerModeDate;

    
    _Linelabel.hidden=YES;
    _tableView.hidden=YES;
    
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

}



- (IBAction)cancelBtnClcik:(id)sender
{
    
        self.datePickerView.hidden=YES;
   }




- (IBAction)doneBtnClick:(id)sender
{
      NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _DatePickerFrom.datePickerMode = UIDatePickerModeDate;
    _DatepickerTo.datePickerMode = UIDatePickerModeDate;
    
    
    
   Copydate = [dateFormatter stringFromDate:_datePicker.date];
    Fromdate = [dateFormatter stringFromDate:_DatePickerFrom.date];
    ToDate = [dateFormatter stringFromDate:_DatepickerTo.date];
    
    
  
     _CopyBidlbl.text = Copydate;
    _FromDatelbl.text= Fromdate;
    _Todatelbl.text= ToDate;
    
    
    self.datePickerView.hidden=YES;

}



- (IBAction)CopyFromBtn:(id)sender {
    
      self.datePickerView.hidden=NO;
      self.datePicker.hidden=NO;
    
   self.DatePickerFrom.hidden=YES;
   self.DatepickerTo.hidden=YES;
    

   

}

- (IBAction)FromDatebtn:(id)sender {
       self.datePickerView.hidden=NO;
    self.DatePickerFrom.hidden=NO;
    
    self.datePicker.hidden=YES;
    self.DatepickerTo.hidden=YES;
    

   
}



- (IBAction)ToDateBtn:(id)sender {
    self.datePickerView.hidden=NO;
    self.DatepickerTo.hidden=NO;
    
    self.DatePickerFrom.hidden=YES;
    self.datePicker.hidden=YES;
    
}




-(IBAction)copyBid:(id)sender


{
    
      NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *strFromDate;
    NSString *strToDate;
    
    
    strFromDate = self.FromDatelbl.text;
    strToDate = self.Todatelbl.text;
    
//     NSLog(@"From  :%@",strFromDate);
    
[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    
        NSDate *dtFromDate = [dateFormatter dateFromString:strFromDate];
        NSDate *dtToDate = [dateFormatter dateFromString:strToDate];
    
    
    
//    
//     NSLog(@"From  :%@",dtFromDate);
    
    NSDateComponents *oneDay = [NSDateComponents new];
    oneDay.day = 1;
    
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    
    [currentCalendar rangeOfUnit:NSWeekCalendarUnit startDate:&dtFromDate interval:NULL forDate:dtFromDate];
    
   
    
//     NSLog(@"From Date :%@, To Date : %@",dtFromDate,dtToDate);
    
    if([dtFromDate compare:dtToDate] == NSOrderedAscending)
    {
//        NSLog(@"Go Ahead");
        
        
        while ([dtFromDate compare:dtToDate] == NSOrderedAscending) {
            dtFromDate = [currentCalendar dateByAddingComponents:oneDay toDate:dtFromDate options:0];
//            NSLog(@"From Date :%@",dtFromDate);
            
       Datepass = [dateFormatter stringFromDate:dtFromDate];
//             NSLog(@"From Date :%@",Datepass);
            
            [self HiteApi];
        }
    }else
    
    {
       [SVProgressHUD showSuccessWithStatus:@"to date should be greater than or equal to From Date"];
    }

}





-(void) HiteApi

{
          [SVProgressHUD showWithStatus:@"Please wait.."];
          NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:Copydate,@"copydate",Datepass,@"todate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    
        NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
        [[Services sharedInstance]serviceCallbyPost:@"service/newbid/copybid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
    
    
    
    
    
//    
//            NSLog(@"Error: %@",response);
//    
//    
    
            if (type == kResponseTypeFail)
            {
                [SVProgressHUD dismiss];
    
                NSError *error=(NSError*)response;
    
                if (error.code == -1009) {
    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                   message:@"The Internet connection appears to be offline."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
    
                    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          }];
    
                    [alert addAction:thirdAction];
    
                }
    
            }
            else if (type == kresponseTypeSuccess)
            {
    
    
    
                NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
                if (arrResponsedata)
                {
    
                    if ([[arrResponsedata objectForKey:@"status"] isEqualToString:@"SUCCESS"])
                    {
                        
                        _Linelabel.hidden=NO;
                        _tableView.hidden=NO;
//                        [SVProgressHUD showSuccessWithStatus:[arrResponsedata objectForKey:@"message"] ];
                        
                        
                        
    
                        
                   
                        
                        NSString*get;
                        get=[arrResponsedata objectForKey:@"message"];
                       [arraydata addObject:get];
                       [self.tableView reloadData];
                        [SVProgressHUD dismiss];
    
                    }
                    else
                    {
                        _Linelabel.hidden=NO;
                        _tableView.hidden=NO;
//                        [SVProgressHUD showErrorWithStatus:[arrResponsedata objectForKey:@"message"]];
 
                        

                        NSString*get;
                        get=[arrResponsedata objectForKey:@"message"];
                        [arraydata addObject:get];
                        [self.tableView reloadData];
                        [SVProgressHUD dismiss];
                    }
                    
                    
                    
                }
                else
                {
                    [SVProgressHUD dismiss];
                }
    
            }
            
        }];
   
  
}





-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraydata.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
   
    TablecellTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    
   
    
    if ([cell.CellLabel.text isEqualToString:@"You Cant Submit bid after11:00:00 AM."])
    {
        
        cell.CellLabel.textColor = [UIColor redColor];
        cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
    }
    else if ([cell.CellLabel.text isEqualToString:@"You Cant change/place bid for previous days."])
    {
         cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
        cell.CellLabel.textColor = [UIColor yellowColor];
    }
    
    else
    {
      cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
        
    }
      cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
    return cell;
    
}


















- (IBAction)BackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
 
}

@end
