//
//  DemoUserRequestViewController.h
//  PCS
//
//  Created by Ashish Karn on 01/06/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoUserRequestViewController : UIViewController

@property(weak,nonatomic)IBOutlet UITextField *txtUserName;
@property(weak,nonatomic)IBOutlet UITextField *txtCompanyName;
@property(weak,nonatomic)IBOutlet UITextField *txtEmailID;
@property(weak,nonatomic)IBOutlet UITextField *txtMobileNo;

@property(weak, nonatomic)IBOutlet UIScrollView *scrollViewDemo;
@property(weak, nonatomic)IBOutlet UIView *viewDemoContent;

@property(weak,nonatomic)IBOutlet UIButton *btnSubmit;
- (IBAction)btnSubmit_Click:(id)sender;

@property(weak,nonatomic)IBOutlet UIButton *btnCancel;
- (IBAction)btnCancel_Click:(id)sender;


@end
