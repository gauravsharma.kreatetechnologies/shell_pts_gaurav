//
//  DemoUserRequestViewController.m
//  PCS
//
//  Created by Ashish Karn on 01/06/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "DemoUserRequestViewController.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "LogonViewController.h"
#import "CustmObj.h"

@interface DemoUserRequestViewController (){
    UITextField *activeField;
    UIToolbar* numberToolbar;
}



@end

@implementation DemoUserRequestViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.scrollViewDemo.contentSize=CGSizeMake(self.viewDemoContent.frame.size.width, self.viewDemoContent.frame.size.height);
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(touchPadDown)],
                           nil];
    [numberToolbar sizeToFit];
    self.txtMobileNo.inputAccessoryView = numberToolbar;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSubmit_Click:(id)sender {
    NSString *strUserName=self.txtUserName.text;
    NSString *strCompanyName=self.txtCompanyName.text;
    NSString *strEmailID=self.txtEmailID.text;
    NSString *strMobileNo=self.txtMobileNo.text;
   // NSString *strDeviceID=@"iphone123456";//[AppHelper userDefaultsForKey:@"deviceId"];
    
//   data={"name":"Manoj Corporation Ltd","companyname":"Manoj Corporation Ltd","email":"manoj.rajput@invetechsolutions.com","mobile_no":"9654618370"}
    
    NSMutableDictionary *dictUserData=[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                       strUserName,@"name",
                                       strCompanyName,@"companyname",
                                       strEmailID,@"email",
                                       strMobileNo,@"mobile_no",
                                       nil];

    
    
    NSError *err;
    NSData *jsonData = [NSJSONSerialization  dataWithJSONObject:dictUserData options:0 error:&err];
    NSString *myString = [[NSString alloc] initWithData: jsonData  encoding:NSUTF8StringEncoding];
    
    // NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    self.view.userInteractionEnabled=NO;
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
   
    [[Services sharedInstance]serviceCallbyPost:@"service/registerclient.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
  
    
   // NSLog(@"%@",dictUserData);
        
        if (type == kResponseTypeFail)
        {
           //  NSLog(@"fail Response:----> %@",response);
            
            self.view.userInteractionEnabled=YES;
            
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
            self.view.userInteractionEnabled=YES;
            
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSMutableDictionary *arrResponsedata=(NSMutableDictionary *)response;
            if ([arrResponsedata count]>0)
            {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:arrResponsedata[@"status"]
                                                                               message:arrResponsedata[@"message"]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                         [self.navigationController popViewControllerAnimated:YES];
                                                                      }];
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
                if ([arrResponsedata[@"status"] isEqualToString:@"SUCCESS"]){
                    [_txtEmailID setText:nil];
                    [_txtMobileNo setText:nil];
                    [_txtUserName setText:nil];
                    [_txtCompanyName setText:nil];
                    
                    
                    
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:arrResponsedata[@"status"]
                                                                                   message:arrResponsedata[@"message"]
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                              
                                                                              // NSLog(@"You pressed button three");
                                                                          }];
                    [alert addAction:thirdAction];

                }
                 
                
                
                
                
            }
            self.view.userInteractionEnabled=YES;
            
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
                   }
        
    }];
    
}

#pragma mark -TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField=textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField=nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtUserName)
    {
        [self.txtUserName resignFirstResponder];
        [self.txtCompanyName becomeFirstResponder];
    }
    else if (textField == self.txtCompanyName)
    {
        [self.txtCompanyName resignFirstResponder];
        [self.txtEmailID becomeFirstResponder];
        
            }
    else if (textField == self.txtEmailID)
    {
        [self.txtEmailID resignFirstResponder];
        [self.txtMobileNo becomeFirstResponder];
            }
    else if (textField == self.txtMobileNo)
    {
        [textField resignFirstResponder];
        
        
            }
    
    return YES;
}

#pragma mark - Keyboard Activity
- (void)keyboardWillShow:(NSNotification *)notif
{
    
    CGSize keyboardSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    self.scrollViewDemo.contentInset = contentInsets;
    self.scrollViewDemo.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        
        [self.scrollViewDemo scrollRectToVisible:activeField.frame animated:YES];
    }
    
    
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    // move the toolbar frame down as keyboard animates into view
    //    NSDictionary *info = [notification userInfo];
    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // Write code to adjust views accordingly using kbSize.height
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollViewDemo.contentInset = contentInsets;
    self.scrollViewDemo.scrollIndicatorInsets = contentInsets;
    
}

-(IBAction)btnCancel_Click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)touchPadDown
{
    [activeField resignFirstResponder];
}

@end
