//
//  DownloadViewCell.h
//  PCS
//
//  Created by lab4code on 15/02/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadViewCell : UICollectionViewCell

@property(weak,nonatomic)IBOutlet UILabel *lblName;
@property(weak,nonatomic)IBOutlet UIImageView *imgCell;
@end
