//
//  DownloadsmainViewController.m
//  PCS
//
//  Created by lab4code on 29/12/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import "DownloadsmainViewController.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "WebbrowserControllerViewController.h"
#import "DownloadViewCell.h"
#import "AppDelegate.h"
#import "CustmObj.h"

@interface DownloadsmainViewController ()
{
    NSMutableArray *arrayMenues;
    NSMutableArray *arrayKeynames;
    NSMutableArray *arrayQueueDownloadFiles;
}
@property(weak,nonatomic)IBOutlet UICollectionView *menuCollectionView;

@property(weak,nonatomic)IBOutlet UILabel *lblTradingdate;
@property(strong,nonatomic)NSString *strSaveDate;
@property(strong,nonatomic)NSString *strURLDate;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *viewNoData;


@end

@implementation DownloadsmainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

     [self.viewNoData setHidden:YES];
    arrayMenues=[[NSMutableArray alloc]init];
    arrayKeynames=[[NSMutableArray alloc]init];
    arrayQueueDownloadFiles=[[NSMutableArray alloc]init];
    
    self.strSaveDate=@"LASTDATE";
    
    self.datePicker.datePickerMode=UIDatePickerModeDate;
    
    self.tableHeightConstraint.constant= - (self.datePickerView.frame.size.height+80);
    [self.datePickerView needsUpdateConstraints];
    
    
    UITapGestureRecognizer *dobTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dobTap:)];
    dobTap.numberOfTapsRequired=1;
    [self.lblTradingdate addGestureRecognizer:dobTap];
    
    [self fetchDataFromServer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //lxryygsyhspawmhrxbidfjzgfoesusvczybptbuedxbocpvyuktnwdronoxvmusj
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark IBActions

-(IBAction)menuButtonClick:(id)sender
{
   // [SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)goButtonClick:(id)sender
{
    [self fetchDataFromServer];
}

#pragma mark Service hit

-(void)fetchDataFromServer
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:self.strSaveDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    [[Services sharedInstance]serviceCallbyPost:@"html/downloads.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        NSLog(@"Response %@",response);
        
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            self.view.userInteractionEnabled=YES;
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
        
        
//            [self->arrayMenues removeAllObjects];
//            [self->arrayKeynames removeAllObjects];
        
        NSMutableDictionary *dictResponse=(NSMutableDictionary *)response;
        
if ([dictResponse count]>0)
{
     if (![[dictResponse objectForKey:@"ratesheet"] isEqualToString:@"NA"])
        {
            
            [self.viewNoData setHidden:YES];
            _menuCollectionView.hidden = NO;
            
            if (![[dictResponse objectForKey:@"ratesheet"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"ratesheet"]];
                [arrayKeynames addObject:@"ratesheet"];
            }
            
            
            if (![[dictResponse objectForKey:@"iexobligation"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"iexobligation"]];
                [arrayKeynames addObject:@"iexobligation"];
            }
            
            if (![[dictResponse objectForKey:@"pxilobligation"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"pxilobligation"]];
                [arrayKeynames addObject:@"pxilobligation"];
            }
            
            
            if (![[dictResponse objectForKey:@"iexprofitability"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"iexprofitability"]];
                [arrayKeynames addObject:@"iexprofitability"];
            }
            
            if (![[dictResponse objectForKey:@"pxilprofitability"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"pxilprofitability"]];
                [arrayKeynames addObject:@"pxilprofitability"];
            }
            
            
            if (![[dictResponse objectForKey:@"bill"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"bill"]];
                [arrayKeynames addObject:@"bill"];
            }
            
            if (![[dictResponse objectForKey:@"recbill"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"recbill"]];
                [arrayKeynames addObject:@"recbill"];
            }
            
           
            
            if (![[dictResponse objectForKey:@"iexschedule"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"iexschedule"]];
                [arrayKeynames addObject:@"iexschedule"];
            }
            
            if (![[dictResponse objectForKey:@"ipxilschedule"] isEqualToString:@"NA"])
            {
                [arrayMenues addObject:[dictResponse objectForKey:@"ipxilschedule"]];
                [arrayKeynames addObject:@"ipxilschedule"];
            }
            
            
            if ([dictResponse objectForKey:@"url"]) {
                self.strURLDate=[dictResponse objectForKey:@"url"];
            }
            
            if ([dictResponse objectForKey:@"date"]) {
                
                
                NSArray *items = [[dictResponse objectForKey:@"date"] componentsSeparatedByString:@"-"];
                
                if ([items count]==3)
                {
                    self.lblTradingdate.text=[NSString stringWithFormat:@"%@-%@-%@",[items objectAtIndex:2],[items objectAtIndex:1],[items objectAtIndex:0]];
                }
                
            }
            
            [self.menuCollectionView reloadData];
            
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
        }
        else
        {
               // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
             _menuCollectionView.hidden = YES;
                [self.viewNoData setHidden:NO];
        }
}
else
{
      // [SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
    _menuCollectionView.hidden = YES;
    [self.viewNoData setHidden:NO];
}
        
    }
        
    }];
}



#pragma mark - date tap gesture

- (void)dobTap:(UIGestureRecognizer*)gestureRecognizer
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.tableHeightConstraint.constant=0;
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
    }];
    
    
    
    
}

- (IBAction)cancelBtnClcik:(id)sender
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
}

- (IBAction)doneBtnClick:(id)sender
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *dateOfBirth=[dateFormatter stringFromDate:self.datePicker.date];
    self.lblTradingdate.text=dateOfBirth;
    
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *savedate=[dateFormatter stringFromDate:self.datePicker.date];
    self.strSaveDate=savedate;
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.tableHeightConstraint.constant=- (self.datePickerView.frame.size.height+80);
        [self.datePickerView needsUpdateConstraints];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
        
    }];
    
    
    
    
}

#pragma mark Collection view Delegate and datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayMenues count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size for orientation
    if ([[UIScreen mainScreen] bounds].size.width >320)
    {
        
        return CGSizeMake(154.0f, 154.0f);
    }
    else
    {
        
        return CGSizeMake(154.0f, 154.0f);
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    ////top,left,botttom,right
    
    if ([[UIScreen mainScreen] bounds].size.width == 320)
    {
       return UIEdgeInsetsMake(0, 5, 0, 5);
        
    }
    else if([[UIScreen mainScreen] bounds].size.width == 375)
    {
        return UIEdgeInsetsMake(0, 20, 0, 20);
    }
    else
    {
        return UIEdgeInsetsMake(0, 42, 0, 42);
    }
    
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"downloadCell";
    
    DownloadViewCell *cell = (DownloadViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    
    
    if ([arrayKeynames count]>indexPath.row) {
        
       // NSLog(@"img name %@",[arrayKeynames objectAtIndex:indexPath.row]);
       
        
        if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"bill"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"BillImg"];
            cell.lblName.text=@"BILL";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"iexobligation"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"ObligationIng"];
            cell.lblName.text=@"OBLIGATION IEX";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"iexschedule"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"ScheduleImg"];
            cell.lblName.text=@"SCHEDULING IEX";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"ratesheet"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"ratesheet"];
            cell.lblName.text=@"RATE SHEET";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"iexprofitability"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"ObligationIng"];
            cell.lblName.text=@"PROFITABILITY IEX";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"ipxilschedule"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"ScheduleImg"];
            cell.lblName.text=@"SCHEDULING PXIL";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"pxilobligation"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"ObligationIng"];
            cell.lblName.text=@"OBLIGATION PXIL";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"pxilprofitability"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"ObligationIng"];
            cell.lblName.text=@"PROFITABILITY PXIL";
        }
        else if ([[arrayKeynames objectAtIndex:indexPath.row] isEqualToString:@"recbill"])
        {
            cell.imgCell.image=[UIImage imageNamed:@"BillImg"];
            cell.lblName.text=@"REC BILL";
        }
        
        
    }
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *filename=[NSString stringWithFormat:@"%@%@",self.strURLDate,[arrayMenues objectAtIndex:indexPath.row]];
   // NSLog(@"URL %@",filename);

    WebbrowserControllerViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"webBrowse"];
    newView.strWebUrl=filename;
    [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction)btnLogout_Click:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



@end
