//
//  Copy Bid Area.m
//  PCS
//
//  Created by Pavan Yadav on 24/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "Copy Bid Area.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "TablecellTableViewCell.h"
#import "AppDelegate.h"
#import "CustmObj.h"

@implementation Copy_Bid_Area

{
    NSString *strFromDate;
    NSString *strToDate;
    NSString *Copydate;
    NSString *Datepass;
    
    NSMutableArray * arraydata;
    
    
    NSDictionary *dictAttrGreen;
    NSDictionary *dictAttrRed;
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    arraydata = [[NSMutableArray alloc]init];
    
    self.datePickerView.hidden=YES;
    
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _DatePickerFrom.datePickerMode = UIDatePickerModeDate;
    _DatepickerTo.datePickerMode = UIDatePickerModeDate;
    [_txtViewResponse setUserInteractionEnabled:NO];
    _Linelabel.hidden=YES;
    
    dictAttrGreen = @{
                      NSFontAttributeName : [UIFont systemFontOfSize:10],
                      NSForegroundColorAttributeName : [UIColor greenColor]
                      };
    
    dictAttrRed = @{
                    NSFontAttributeName : [UIFont boldSystemFontOfSize:10],
                    NSForegroundColorAttributeName : [UIColor redColor]
                    };
    
    // _tableView.hidden=YES;
    
    // self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
}

- (IBAction)cancelBtnClcik:(id)sender
{
    self.datePickerView.hidden=YES;
}

- (IBAction)doneBtnClick:(id)sender
{
    NSDateFormatter *dateFormatterView = [[NSDateFormatter alloc] init];
    [dateFormatterView setDateFormat:@"dd-MM-yyyy"];
    
    NSDateFormatter *dateFormatterServer = [[NSDateFormatter alloc] init];
    [dateFormatterServer setDateFormat:@"yyyy-MM-dd"];
    
    if(_datePicker.hidden == NO){
        Copydate = [dateFormatterServer stringFromDate:_datePicker.date];
        _CopyBidlbl.text = [dateFormatterView stringFromDate:_datePicker.date];
    }else if (_DatePickerFrom.hidden == NO){
        strFromDate = [dateFormatterServer stringFromDate:_DatePickerFrom.date];
        _FromDatelbl.text= [dateFormatterView stringFromDate:_DatePickerFrom.date];
    }else if(_DatepickerTo.hidden == NO){
        strToDate = [dateFormatterServer stringFromDate:_DatepickerTo.date];
        _Todatelbl.text= [dateFormatterView stringFromDate:_DatepickerTo.date];
    }
    self.datePickerView.hidden=YES;
}

- (IBAction)CopyFromBtn:(id)sender {
    
    self.datePickerView.hidden=NO;
    self.datePicker.hidden=NO;
    
    self.DatePickerFrom.hidden=YES;
    self.DatepickerTo.hidden=YES;
}

- (IBAction)FromDatebtn:(id)sender {
    self.datePickerView.hidden=NO;
    self.DatePickerFrom.hidden=NO;
    
    self.datePicker.hidden=YES;
    self.DatepickerTo.hidden=YES;
}

- (IBAction)ToDateBtn:(id)sender {
    self.datePickerView.hidden=NO;
    self.DatepickerTo.hidden=NO;
    
    self.DatePickerFrom.hidden=YES;
    self.datePicker.hidden=YES;
    
}

-(IBAction)copyBid:(id)sender
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSDate *dtFromDate = [dateFormatter dateFromString:strFromDate];
    NSDate *dtToDate = [dateFormatter dateFromString:strToDate];
    dtToDate = [dtToDate dateByAddingTimeInterval:60*60*24*1];

    
    if([dtFromDate compare:dtToDate] == NSOrderedAscending)
    {
       // [SVProgressHUD showWithStatus:@"Please wait.."];
        [CustmObj addHud:self.view];
        self.txtViewResponse.attributedText = nil;
        while ([dtFromDate compare:dtToDate] == NSOrderedAscending) {
            
            
            NSDateFormatter *requiredDateFormat = [[NSDateFormatter alloc] init];
            [requiredDateFormat setDateFormat:@"yyyy-MM-dd"];
            
            NSString *currentDate = [requiredDateFormat stringFromDate:dtFromDate];
            
            [self CopyBidApi:[NSString stringWithFormat:@"%@",currentDate]];
            dtFromDate = [dtFromDate dateByAddingTimeInterval:60*60*24*1];
        }
       // [SVProgressHUD dismissWithDelay:0.5];
        [CustmObj removeHud:self.view];
        
    }else
    {
        [SVProgressHUD showErrorWithStatus:@"To date should be greater than or equal to from Date"];
    }
}

-(void)CopyBidApi:(NSString *)BidToBeCopied
{

    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:Copydate,@"copydate",strFromDate,@"fromdate",BidToBeCopied,@"todate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/copybid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        if (type == kResponseTypeFail)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
            }
        }
        else if (type == kresponseTypeSuccess)
        {
            
            //            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            //            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            
            NSAttributedString *currentResponse;
            
            
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]){
                currentResponse = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"\n%@",[response valueForKey:@"message"]] attributes:dictAttrGreen];
            }else{
                currentResponse = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"\n%@",[response valueForKey:@"message"]] attributes:dictAttrRed];
            }
            
            NSMutableAttributedString *finalResponse = [[NSMutableAttributedString alloc]initWithAttributedString:self.txtViewResponse.attributedText];
            
            [finalResponse appendAttributedString:currentResponse];
          
            [self.txtViewResponse setAttributedText:finalResponse];
            [CustmObj removeHud:self.view];
        }
    }];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraydata.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TablecellTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if ([cell.CellLabel.text isEqualToString:@"You can't submit bid after 11:00:00 AM."])
    {
        cell.CellLabel.textColor = [UIColor redColor];
        cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
    }
    else if ([cell.CellLabel.text isEqualToString:@"You cant change/place bid for previous days."])
    {
        cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
        cell.CellLabel.textColor = [UIColor yellowColor];
    }
    else
    {
        cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
    }
    cell.CellLabel.text=[arraydata objectAtIndex:indexPath.row];
    return cell;
}

- (IBAction)BackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnLogout_Click:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
