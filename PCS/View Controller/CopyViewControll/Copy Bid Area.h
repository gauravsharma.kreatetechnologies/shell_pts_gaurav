//
//  Copy Bid Area.h
//  PCS
//
//  Created by Pavan Yadav on 24/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Copy_Bid_Area : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;



@property (weak, nonatomic) IBOutlet UIButton *CopyFromPro;
@property (weak, nonatomic) IBOutlet UIButton *Fromdatepro;
@property (weak, nonatomic) IBOutlet UIButton *ToDatepr;

@property (weak, nonatomic) IBOutlet UILabel *CopyBidlbl;
@property (weak, nonatomic) IBOutlet UILabel *FromDatelbl;
@property (weak, nonatomic) IBOutlet UILabel *Todatelbl;
@property (weak, nonatomic) IBOutlet UITextView *TextView;
@property (weak, nonatomic) IBOutlet UILabel *Linelabel;

@property (weak, nonatomic) IBOutlet UIDatePicker *DatePickerFrom;
@property (weak, nonatomic) IBOutlet UIDatePicker *DatepickerTo;
@property (weak, nonatomic) IBOutlet UITextView *txtViewResponse;

@end
