//
//  TablecellTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 26/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TablecellTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *CellLabel;
@end
