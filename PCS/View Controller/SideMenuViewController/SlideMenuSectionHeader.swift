//
//  SlideMenuSectionHeader.swift
//  SlideMenuViewController
//
//  Created by lokesh chand on 18/07/17.
//  Copyright © 2017 InvetechSolutions. All rights reserved.
//

import UIKit

class SlideMenuSectionHeader: UITableViewHeaderFooterView
{
    
    
    typealias addBtnCompletionHandler = ()->Void
    var addBtnCompletionBlock : addBtnCompletionHandler?
    
    
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var sectioImageView: UIImageView!
    @IBOutlet weak var arrowImage: UIImageView!
    


}
