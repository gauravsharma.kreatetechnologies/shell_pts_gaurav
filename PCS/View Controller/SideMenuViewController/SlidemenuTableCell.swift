//
//  SlidemenuTableCell.swift
//  SlideMenuViewController
//
//  Created by lokesh chand on 18/07/17.
//  Copyright © 2017 InvetechSolutions. All rights reserved.
//

import UIKit

class SlidemenuTableCell: UITableViewCell
{

    
    @IBOutlet weak var cellViewNameLabel: UILabel!
    @IBOutlet weak var cellbackgroundView: UIView!
    var cellSelected = false
    @IBOutlet weak var cellimageview: UIImageView!

    @IBOutlet weak var myNewImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
