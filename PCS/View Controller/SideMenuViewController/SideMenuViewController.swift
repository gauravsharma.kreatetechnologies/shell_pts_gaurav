//
//  SideMenuViewController.swift
//  SideMenuViewController
//
//  Created by lokesh chand on 18/07/17.
//  Copyright © 2017 InvetechSolutions. All rights reserved.
//SideNewsFeed


//SideNewsFeedal

import UIKit
import Foundation
import Alamofire
import SwiftyJSON


struct Sectionname
{
    var name: String!
    var items: [String]!
    var images: String!
    var imagename = [UIImage]()
    var collapsed: Bool!
    
    
    init(name: String, items: [String], collapsed: Bool = false,images: String, imagename:[UIImage])
    {
        self.name = name
        self.items = items
        self.collapsed = collapsed
        self.images = images
        self.imagename = imagename
        
        
    }
}


var storyBoardId = ["DashboardId","", "","TransmissionCorridor","","MonthlyATC","MCPID","StateLoadID","notificationID","ContactUs","DisclaimerViewId","feedback"]

var subNews = [String]()


@objc class SideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet weak var slidemenuTableView: UITableView!
    var sections = [Sectionname]()
    var selectedIndex = [IndexPath]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.ApiCalling()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 10)
        {
            self.viewDidLoad()
        }
        
    }
    
    
    
    func tableReload()
    {
        slidemenuTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        if (UserDefaults.standard.object(forKey: "client_id") != nil)
        {
            
            sections = [
                Sectionname(name: "Dashboard", items: [],images : "ic_home", imagename: [] ),
                Sectionname(name: "Current Available URS", items: ["Station wise URS","State wise URS"], images: "ic_newsfeed", imagename: [#imageLiteral(resourceName: "ic_station"),#imageLiteral(resourceName: "State wise URSCol")]),
//                Sectionname(name: "Station wise URS", items: [],images : "ic_station" ),
//                Sectionname(name: "State wise URS", items: [],images : "ic_state" ),
                Sectionname(name: "News", items: subNews,images : "ic_news" , imagename: []),
                Sectionname(name: "Transmission Corridor", items: [],images : "ic_coridor", imagename: []),
                Sectionname(name: "Realtime Scheduling", items: ["RLDC wise Realtime Scheduling","State wise Realtime Scheduling"],images : "ic_realtime_scheduling", imagename: []),
                //Sectionname(name: "Landed Cost Calculator", items: [],images : "ic_landed_cost"),
                Sectionname(name: "Monthly ATC", items: [],images : "ic_monthlyatc", imagename: []),
                Sectionname(name: "MCP Comparison", items: [],images : "MCP Comparison", imagename: []),
                Sectionname(name: "State Load", items: [],images : "stateLD", imagename: []),
                Sectionname(name: "Notification Setting", items: [],images : "ic_notification_settings", imagename: []),
                Sectionname(name: "Contact Us", items: [],images : "ic_contact_us", imagename: []),
                Sectionname(name: "Disclaimer", items: [],images : "Desclaimer", imagename: []),
                Sectionname(name: "Feedback", items: [],images : "ic_Feedback", imagename: [])
                
                
            ]
             storyBoardId = ["DashboardId","", "","TransmissionCorridor","","MonthlyATC","MCPID","StateLoadID","notificationID","ContactUs","DisclaimerViewId","feedback"]
            
        }
        
        else
        {
            
            sections = [
                Sectionname(name: "Dashboard", items: [],images : "ic_home", imagename: [] ),
                //Sectionname(name: "Current Available URS", items: [], images: "ic_newsfeed"),
                Sectionname(name: "News", items: subNews,images : "ic_news" , imagename: []),
                Sectionname(name: "Transmission Corridor", items: [],images : "ic_coridor", imagename: []),
                Sectionname(name: "Realtime Scheduling", items: ["RLDC wise Realtime Scheduling","State wise Realtime Scheduling"],images : "ic_realtime_scheduling", imagename: []),
                //Sectionname(name: "Landed Cost Calculator", items: [],images : "ic_landed_cost"),
                Sectionname(name: "Monthly ATC", items: [],images : "ic_monthlyatc", imagename: []),
                Sectionname(name: "MCP Comparison", items: [],images : "MCP Comparison", imagename: []),
                Sectionname(name: "State Load", items: [],images : "stateLD", imagename: []),
                Sectionname(name: "Notification Setting", items: [],images : "ic_notification_settings", imagename: []),
                Sectionname(name: "Contact Us", items: [],images : "ic_contact_us", imagename: []),
                Sectionname(name: "Disclaimer", items: [],images : "Desclaimer", imagename: []),
                Sectionname(name: "Feedback", items: [],images : "ic_Feedback", imagename: [])

            ]
            
             storyBoardId = ["DashboardId", "","TransmissionCorridor","","MonthlyATC","MCPID","StateLoadID","notificationID","ContactUs","DisclaimerViewId","feedback"]

        }
        
        
        // MARK:- My Schedule Module add.
        
//        if (UserDefaults.standard.object(forKey: "client_id") != nil)
//        {
//
////            let sec =  Sectionname(name: "My Schedule", items: ["Setup Account","List View","Club View"],images : "MySchedule" )
////
////            sections.insert(sec, at: 7)
//
//            storyBoardId = ["DashboardId","CurrentAvailableURS","StationwiseURSVCID","StatewiseVCID", "","TransmissionCorridor","","MonthlyATC","MCPID","StateLoadID","notificationID","ContactUs","DisclaimerViewId","feedback"]
//
//        }
        
        
        let nib = UINib(nibName: "SlideMenuSectionHeader", bundle: nil)
        slidemenuTableView.register(nib, forHeaderFooterViewReuseIdentifier: "SlideMenuSectionHeader")
        slidemenuTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        slidemenuTableView.reloadData()
    }
    
    
    
    
    
    //1. Calling Initial api and getting response..
    func ApiCalling()
    {
        
        let someDict =  [String : String]()
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL:newsFeed, parameters:parameters)
            {(APIData) -> Void in
                
               // print(APIData)
                
                subNews.removeAll()
                
                for (key, subJson) in APIData
                {
                    subNews.append(key)

                }
                subNews.insert("All", at: 0)
                
                self.tableReload()
            }
            
        }
            
        catch
        {
            
        }
        
        
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension SideMenuViewController{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sections[section].items.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SlideCell", for: indexPath) as? SlidemenuTableCell ?? SlidemenuTableCell(style: .default, reuseIdentifier: "cell")
        cell.selectionStyle = UITableViewCellSelectionStyle .none
        
        
        
        
        let name = sections[(indexPath as NSIndexPath).section].items[(indexPath as NSIndexPath).row]
        cell.cellViewNameLabel?.text = name
//        cell.cellimageview.image = #imageLiteral(resourceName: "ic_calculator")
//        cell.cellimageview.layer.cornerRadius = 6
//        cell.cellimageview.clipsToBounds = true
        
//        cell.myNewImage.image = #imageLiteral(resourceName: "ic_realtime")
//        cell.myNewImage.layer.cornerRadius = 6
//        cell.myNewImage.clipsToBounds = true
        
        if sections[indexPath.section].imagename.count > 0
        {
            // cell.cellimageview.image = sections[indexPath.section].imagename[indexPath.row]
            cell.myNewImage.isHidden = false
            cell.myNewImage.image = sections[indexPath.section].imagename[indexPath.row]
        }
        else
        {
            cell.myNewImage.isHidden = true
        }
    
        
        cell.selectionStyle = UITableViewCellSelectionStyle .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SlideMenuSectionHeader")
        let header = cell as! SlideMenuSectionHeader
        header.titlelabel.translatesAutoresizingMaskIntoConstraints = false
        header.titlelabel.text = sections[section].name
        header.sectioImageView.image = UIImage(named: "\(sections[section].images!)")
        
        
        if (UserDefaults.standard.object(forKey: "client_id") != nil)
        {
            if section == 1 || section == 2 || section == 4
            {
                header.arrowImage.image = UIImage(named: "menu_dwn_arrow")
            }
            else
            {
                header.arrowImage.image = UIImage(named: "")
            }
            
        }
        else
        {
            if section == 1 || section == 3
            {
                header.arrowImage.image = UIImage(named: "menu_dwn_arrow")
            }
            else
            {
                header.arrowImage.image = UIImage(named: "")
            }
            
        }

        
        ///****tap button in section****///
        
        // print("tag section \(section)")
        
        
        header.tag = section
        let gesture = UITapGestureRecognizer(target: self, action: #selector(SideMenuViewController.tapHeader(_:)))
        header.addGestureRecognizer(gesture)
        
        weak var weakSelf : SideMenuViewController? = self
        
        header.addBtnCompletionBlock = { section
            
            if let strongself = weakSelf
            {
                strongself.toggleSection(header, section: section)
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return sections[(indexPath as NSIndexPath).section].collapsed! ? 35.0 : 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let section = (indexPath as NSIndexPath).section
        let row = (indexPath as NSIndexPath).row
        
        
        if (UserDefaults.standard.object(forKey: "client_id") != nil)
        {
            if section == 2
            {
                
                if row == 0
                {
                    let home  = storyboard?.instantiateViewController(withIdentifier: "SideNewsFeedal")
                    
                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: home, withSlideOutAnimation: true, andCompletion: nil)
                }
                else
                {
                    let home  = storyboard?.instantiateViewController(withIdentifier: "SideNewsFeed")
                    
                    UserDefaults.standard.removeObject(forKey: "newsHeading")
                    UserDefaults.standard.set(subNews[row], forKey: "newsHeading")
                    
                    SlideNavigationController.sharedInstance().pushViewController(home!, animated: true)
                }
                
            }
            
            
            if section == 4 && row == 0
            {
                let home  = storyboard?.instantiateViewController(withIdentifier: "RealTimeScheduling")
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: home, withSlideOutAnimation: true, andCompletion: nil)
            }
            else if section == 4 && row == 1
            {
                
                let home  = storyboard?.instantiateViewController(withIdentifier: "powerDemo")
                
                SlideNavigationController.sharedInstance().popAllAndSwitch(to: home, withCompletion: nil)
                
            }
            else if section == 1 && row == 0
            {
                let home  = storyboard?.instantiateViewController(withIdentifier: "StationwiseURSVCID")
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: home, withSlideOutAnimation: true, andCompletion: nil)
            }
            else if section == 1 && row == 1
            {
                
                let home  = storyboard?.instantiateViewController(withIdentifier: "StatewiseVCID")
                
                SlideNavigationController.sharedInstance().popAllAndSwitch(to: home, withCompletion: nil)
                
            }
            
            
            
        }
        
        else
        {
            
            if section == 1
            {
                
                if row == 0
                {
                    let home  = storyboard?.instantiateViewController(withIdentifier: "SideNewsFeedal")
                    
                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: home, withSlideOutAnimation: true, andCompletion: nil)
                }
                else
                {
                    let home  = storyboard?.instantiateViewController(withIdentifier: "SideNewsFeed")
                    
                    UserDefaults.standard.removeObject(forKey: "newsHeading")
                    UserDefaults.standard.set(subNews[row], forKey: "newsHeading")
                    
                    SlideNavigationController.sharedInstance().pushViewController(home!, animated: true)
                }
                
            }
            
            
            if section == 3 && row == 0
            {
                let home  = storyboard?.instantiateViewController(withIdentifier: "RealTimeScheduling")
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: home, withSlideOutAnimation: true, andCompletion: nil)
            }
            else if section == 3 && row == 1
            {
                
                let home  = storyboard?.instantiateViewController(withIdentifier: "powerDemo")
                
                SlideNavigationController.sharedInstance().popAllAndSwitch(to: home, withCompletion: nil)
                
            }
            
            
        }
        
        

//        else if section == 5 && row == 0
//        {
//
//            let home  = storyboard?.instantiateViewController(withIdentifier: "setUp")
//
//            SlideNavigationController.sharedInstance().popAllAndSwitch(to: home, withCompletion: nil)
//
//        }
//        else if section == 5 && row == 1
//        {
//
//            let home  = storyboard?.instantiateViewController(withIdentifier: "ScheduleId")
//
//            SlideNavigationController.sharedInstance().popAllAndSwitch(to: home, withCompletion: nil)
//
//        }
//        else if section == 5 && row == 2
//        {
//
//            let home  = storyboard?.instantiateViewController(withIdentifier: "Club")
//
//            SlideNavigationController.sharedInstance().popAllAndSwitch(to: home, withCompletion: nil)
//
//        }
        if !selectedIndex.contains(indexPath)
        {
            selectedIndex.append(indexPath)
        }
        else
        {
            selectedIndex.remove(at: selectedIndex.index(of: indexPath)!)
        }
        
    }
    
    
    
    ////**** Gesture on Section ****////
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer)
    {
        
        guard let cell = gestureRecognizer.view as? SlideMenuSectionHeader
            
            else
        {
            return
        }
        
        toggleSection(cell, section: cell.tag)
        // print("success",cell.tag)
        
        if (UserDefaults.standard.object(forKey: "client_id") != nil)
        {
            if cell.tag != 1 && cell.tag != 2 && cell.tag != 4
            {
                let home  = storyboard?.instantiateViewController(withIdentifier: storyBoardId[cell.tag])
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: home, withSlideOutAnimation: true, andCompletion: nil)
            }
            
        }
        else
        {
            
            if cell.tag != 1 && cell.tag != 3
            {
                let home  = storyboard?.instantiateViewController(withIdentifier: storyBoardId[cell.tag])
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: home, withSlideOutAnimation: true, andCompletion: nil)
            }
            
        }

        
        if cell.arrowImage.image ==  UIImage(named: "menu_dwn_arrow")
        {
            cell.arrowImage.image = UIImage(named: "menu_up_arow")
        }
        else if cell.arrowImage.image ==  UIImage(named: "menu_up_arow")
        {
            cell.arrowImage.image = UIImage(named: "menu_dwn_arrow")
        }
        
        
    }
    
    
    
    
    
    func toggleSection(_ header: SlideMenuSectionHeader, section: Int)
    {
        let collapsed = !sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = collapsed
        // Adjust the height of the rows inside the section
        
        slidemenuTableView.beginUpdates()
        for i in 0 ..< sections[section].items.count
        {
            slidemenuTableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        slidemenuTableView.endUpdates()
    }
    
}


extension UIView
{
    
    func rotateed(_ toValue: CGFloat, duration: CFTimeInterval = 0.2)
    {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        self.layer.add(animation, forKey: nil)
    }
}













