//
//  summaryViewController.m
//  PCS
//
//  Created by pavan yadav on 02/02/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

#import "summaryViewController.h"
#import "TableViewCellData.h"
#import "NewBidMainViewController.h"
#import "SVProgressHUD.h"
#import "AppHelper.h"
#import "Services.h"
#import "CustmObj.h"


@interface summaryViewController ()<UITableViewDelegate,UITableViewDataSource>
    {
        NSArray *blockIEX;
        NSArray *blockPXIL;
        NSArray *SingleIEX;
        NSArray *SinglePXIL;
        NSDictionary *dictionaryEditdatapass;
        NSString *convertedString;
        
        NSString *strToDate;
        NSDateFormatter *dateFormatter;
        
        
        NSString *lastDate;
        
    }
    @end


@implementation summaryViewController
    
- (void)viewDidLoad
    {
        [super viewDidLoad];
        
        _viewCopypopUp.hidden = YES;
        _datePickerView.hidden = YES;
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString: _selectedDate];
        dateFormatter = [[NSDateFormatter alloc] init] ;
        [dateFormatter setDateFormat:@"dd MMMM yyyy"];
        convertedString = [dateFormatter stringFromDate:date];
        
        _labelDate.text =[NSString stringWithFormat:@"%@- %@",@"Trading Date",convertedString];
        
        _myTableview.separatorStyle = NO;
        blockIEX =[[_ResponseData valueForKeyPath:@"block.iex"] objectAtIndex:0];
        blockPXIL =[[_ResponseData valueForKeyPath:@"block.pxil"] objectAtIndex:0];
        SingleIEX =[[_ResponseData valueForKeyPath:@"single.iex"] objectAtIndex:0];
        SinglePXIL =[[_ResponseData valueForKeyPath:@"single.pxil"] objectAtIndex:0];
        
        _viewCopypopUp.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _viewCopypopUp.layer.borderWidth = 1;
        
    }
    
- (void)didReceiveMemoryWarning
    {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
    }
    
-(IBAction)menuButtonClick:(id)sender
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        //
        //    if (indexPath.section == 0)
        //    {
        //    return 35;
        //    }
        
        return 30;
        
    }
    
    
#pragma mark :Table View
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        
        return  4;
    }
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        int rowCount = 0;
        
        
        if (section == 0)
        {
            rowCount =(int) [blockIEX count];
        }
        else if (section == 1)
        {
            rowCount =  (int) [SingleIEX count];
        }
        else if (section == 2)
        {
            rowCount =  (int) [blockPXIL count];
        }
        else if (section == 3)
        {
            rowCount =  (int) [SinglePXIL count];
        }
        else
        {
            return 0;
        }
        
        if(rowCount > 0 )
        {
            return rowCount+1;
        }
        else
        {
            return rowCount;
        }
    }
    
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        if (indexPath.section == 0)
        {
            
            if (indexPath.row==0)
            
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableHead" forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
            {
                int arrayIndex = (int)indexPath.row - 1;
                
                TableViewCellData *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                
                cell.labelSerialNo.text =[NSString stringWithFormat:@"%d",arrayIndex+1];
                cell.Pricelabel.text =[[blockIEX valueForKeyPath:@"price"]objectAtIndex:arrayIndex];
                cell.Bidlabel.text =[[blockIEX valueForKeyPath:@"bid"] objectAtIndex:arrayIndex];
                cell.Buylabel.text =[[blockIEX valueForKeyPath:@"btype"] objectAtIndex:arrayIndex];
                cell.Slotlabel.text =[NSString stringWithFormat:@"%@-%@",[[blockIEX valueForKeyPath:@"blockfrom"] objectAtIndex:arrayIndex],[[blockIEX valueForKeyPath:@"blockto"] objectAtIndex:arrayIndex]];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
        
        else if (indexPath.section == 1)
        {
            
            if (indexPath.row==0)
            
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableHead" forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
            {
                int arrayIndex = (int)indexPath.row - 1;
                
                TableViewCellData *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                
                cell.labelSerialNo.text =[NSString stringWithFormat:@"%d",arrayIndex+1];
                cell.Pricelabel.text =[[SingleIEX valueForKeyPath:@"price"]objectAtIndex:arrayIndex];
                cell.Bidlabel.text =[[SingleIEX valueForKeyPath:@"bid"] objectAtIndex:arrayIndex];
                cell.Buylabel.text =[[SingleIEX valueForKeyPath:@"btype"] objectAtIndex:arrayIndex];
                cell.Slotlabel.text =[NSString stringWithFormat:@"%@-%@",[[SingleIEX valueForKeyPath:@"blockfrom"] objectAtIndex:arrayIndex],[[SingleIEX valueForKeyPath:@"blockto"] objectAtIndex:arrayIndex]];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
        else if (indexPath.section == 2)
        
        {
            if (indexPath.row==0)
            
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableHead" forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
            {
                int arrayIndex = (int)indexPath.row - 1;
                
                TableViewCellData *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                cell.labelSerialNo.text =[NSString stringWithFormat:@"%d",arrayIndex+1];
                cell.Pricelabel.text =[[blockPXIL valueForKeyPath:@"price"]objectAtIndex:arrayIndex];
                cell.Bidlabel.text =[[blockPXIL valueForKeyPath:@"bid"] objectAtIndex:arrayIndex];
                cell.Buylabel.text =[[blockPXIL valueForKeyPath:@"btype"] objectAtIndex:arrayIndex];
                
                
                cell.Slotlabel.text =[NSString stringWithFormat:@"%@-%@",[[blockPXIL valueForKeyPath:@"blockfrom"] objectAtIndex:arrayIndex],[[blockPXIL valueForKeyPath:@"blockto"] objectAtIndex:arrayIndex]];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
        
        
        else if (indexPath.section == 3)
        {
            if (indexPath.row==0)
            
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableHead" forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
            {
                
                int arrayIndex = (int)indexPath.row - 1;
                TableViewCellData *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                
                cell.labelSerialNo.text =[NSString stringWithFormat:@"%d",arrayIndex+1];
                cell.Pricelabel.text =[[SinglePXIL valueForKeyPath:@"price"]objectAtIndex:arrayIndex];
                cell.Bidlabel.text =[[SinglePXIL valueForKeyPath:@"bid"] objectAtIndex:arrayIndex];
                cell.Buylabel.text =[[SinglePXIL valueForKeyPath:@"btype"] objectAtIndex:arrayIndex];
                
                cell.Slotlabel.text =[NSString stringWithFormat:@"%@-%@",[[SinglePXIL valueForKeyPath:@"blockfrom"] objectAtIndex:arrayIndex],[[SinglePXIL valueForKeyPath:@"blockto"] objectAtIndex:arrayIndex]];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                return cell;
            }
        }
        
        
        else
        {
            return nil;
        }
        
    }
    
    
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
    {
        
        
        if (section==0 && [blockIEX count] >0)
        {
            return 20;
        }
        
        
        else if (section==1 && [SingleIEX count] >0)
        {
            return 20;
        }
        
        
        
        else if (section==2 && [blockPXIL count] >0)
        {
            return 20;
        }
        
        
        
        else if (section==3 && [SinglePXIL count] >0)
        {
            return 20;
        }
        
        
        else
        {
            return 0;
        }
    }
    
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
    {
        
#pragma mark :Header View.
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
        
        
#pragma mark :Header View label.
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width ,20)];
        label.backgroundColor = [UIColor lightGrayColor];
        label.textColor = [UIColor darkGrayColor];
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont systemFontOfSize:14];
        
        
#pragma mark :Edit button on header
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(104,6,80,20);
        
        UIButton *buttonframe = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buttonframe.frame = CGRectMake(103,8,60,20);
        
        [button setTitle:@"EDIT"
                forState:(UIControlState)UIControlStateNormal];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@"Arial" size:13.0];
        
        [button addTarget:self
                   action:@selector(EditButton:)
         forControlEvents:UIControlEventTouchDown];
        
        [buttonframe addTarget:self
                        action:@selector(EditButton:)
              forControlEvents:UIControlEventTouchDown];
        
        
        
#pragma mark :Copy button on header
        
        UIButton *buttonCopy = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buttonCopy.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-70,4,80,20);
        
        [buttonCopy setTitle:@"Copy"
                    forState:(UIControlState)UIControlStateNormal];
        [buttonCopy setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        buttonCopy.titleLabel.font = [UIFont fontWithName:@"Arial" size:13.0];
        
        [buttonCopy addTarget:self
                       action:@selector(CopyHeaderButton:)
             forControlEvents:UIControlEventTouchDown];
        
        
#pragma mark :Copy image on header
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"copyIcon"]];
        imageView.frame =CGRectMake([UIScreen mainScreen].bounds.size.width-65,8,15,13);
        
        
        if ([blockIEX count] >0)
        {
            if (section == 0)
            {
                label.text = @"     Block Bid || IEX";
                button.tag = 0;
                
                [headerView addSubview:label];
                [headerView addSubview:button];
                [headerView addSubview:buttonframe];
                [headerView addSubview:buttonCopy];
                [headerView addSubview:imageView];
                return headerView;
            }
            
        }
        
        if ([SingleIEX count] >0)
        {
            if (section == 1)
            {
                label.text = @"     Single Bid || IEX";
                
                button.tag = 1;
                [headerView addSubview:label];
                [headerView addSubview:button];
                [headerView addSubview:buttonframe];
                [headerView addSubview:buttonCopy];
                [headerView addSubview:imageView];
                return headerView;
            }
        }
        if ([blockPXIL count] >0)
        {
            if (section == 2)
            {
                label.text = @"     Block Bid || PXIL";
                button.tag = 2;
                [headerView addSubview:label];
                [headerView addSubview:button];
                [headerView addSubview:buttonframe];
                [headerView addSubview:buttonCopy];
                [headerView addSubview:imageView];
                return headerView;
                
            }
        }
        if ([SinglePXIL count] >0)
        {
            if (section == 3)
            {
                label.text = @"     Single Bid || PXIL";
                
                button.tag = 3;
                [headerView addSubview:label];
                [headerView addSubview:button];
                [headerView addSubview:buttonframe];
                [headerView addSubview:buttonCopy];
                [headerView addSubview:imageView];
                return headerView;
            }
        }
        
        return label;
    }
    
#pragma mark :Edit Bid
    
-(void)EditButton:(UIButton *)sender
    {
        
        if (sender.tag == 0)
        {
            
            dictionaryEditdatapass = @{ @"Exchange":@"IEX", @"bidType":@"Block Bid",@"date":_selectedDate};
        }
        else if (sender.tag == 1)
        {
            dictionaryEditdatapass = @{ @"Exchange":@"IEX", @"bidType":@"Single Bid",@"date":_selectedDate};
        }
        else if (sender.tag == 2)
        {
            dictionaryEditdatapass = @{ @"Exchange":@"PXIL", @"bidType":@"Block Bid",@"date":_selectedDate};
        }
        else if (sender.tag == 3)
        {
            dictionaryEditdatapass = @{ @"Exchange":@"PXIL", @"bidType":@"Single Bid",@"date":_selectedDate};
        }
        
        NewBidMainViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"menunewbididd"];
        newView.dictEdit = dictionaryEditdatapass;
        
        [self.navigationController pushViewController:newView animated:YES];
        
        
    }
    
    
-(void)CopyHeaderButton:(UIButton *)sender
    {
        
        [_buttonToDate setTitle:@"select To Bid Date" forState:UIControlStateNormal];
        [_buttonToDate setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _viewCopypopUp.hidden = NO;
        _labelFromdate.text =convertedString;
        
    }
    
#pragma mark :Picker View
    
- (IBAction)cancelBtnClcik:(id)sender
    {
        self.datePickerView.hidden=YES;
    }
    
    
- (IBAction)doneBtnClick:(id)sender
    {
        [_buttonToDate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        NSDateFormatter *dateFormatterServer = [[NSDateFormatter alloc] init];
        [dateFormatterServer setDateFormat:@"dd MMMM yyyy"];
        [_buttonToDate setTitle:[dateFormatterServer stringFromDate:_datePicker.date] forState:UIControlStateNormal];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        strToDate = [dateFormatter stringFromDate:_datePicker.date];
        self.datePickerView.hidden=YES;
    }
    
    
    
    
- (IBAction)buttonToDate:(id)sender
    {
        _datePickerView.hidden = NO;
    }
    
    
- (IBAction)buttonCancel:(id)sender
    {
        _viewCopypopUp.hidden = YES;
    }
    
    
    
#pragma mark :Copy Bid
    
- (IBAction)buttonCopy:(id)sender
    {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        
        
        NSDate *dtFromDate = [dateFormatter dateFromString:_selectedDate];
        NSDate *dtToDate = [dateFormatter dateFromString:strToDate];
        
        
        
        if([dtFromDate compare:dtToDate] == NSOrderedAscending)
        {
            NSDateFormatter *requiredDateFormat = [[NSDateFormatter alloc] init];
            [requiredDateFormat setDateFormat:@"yyyy-MM-dd"];
            
            NSString *currentDate = [requiredDateFormat stringFromDate:dtToDate];
            
            [self CopyApicall:[NSString stringWithFormat:@"%@",currentDate]];
            
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:@"To date should be greater than or equal to from Date"];
            
        }
    }
    
    
    
    
-(void)CopyApicall:(NSString *)BidToBeCopied
    {
        
        if ([strToDate isEqualToString:BidToBeCopied])
        {
            lastDate = @"LAST";
            
        }
        else
        {
            lastDate = nil;
        }
        
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:_selectedDate,@"copydate",BidToBeCopied,@"todate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
        NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
        
        [[Services sharedInstance]serviceCallbyPost:@"service/newbid/copybid.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
         {
             if (type == kResponseTypeFail)
             {
                 //[SVProgressHUD dismiss];
                 [CustmObj removeHud:self.view];
                 
                 NSError *error=(NSError*)response;
                 
                 if (error.code == -1009) {
                     
                     UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                    message:@"The Internet connection appears to be offline."
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                           }];
                     
                     [alert addAction:thirdAction];
                     
                 }
                 
             }
             else if (type == kresponseTypeSuccess)
             {
                 
                 _viewCopypopUp.hidden = YES;
                 

                 
                 if ([lastDate  isEqual: @"LAST"])
                 {
                     if ([[response valueForKey:@"status"] isEqualToString:@"ERR"])
                     {
                         [SVProgressHUD showErrorWithStatus:[response valueForKey:@"message"]];
                     }
                     else if  ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"])
                     {
                         [SVProgressHUD showSuccessWithStatus:[response valueForKey:@"message"]];
                     }
                     
                     //[SVProgressHUD dismissWithDelay:0.8];
                 }
                 
             }
         }];
        
    }
    
    
    
    
    
    
    @end
