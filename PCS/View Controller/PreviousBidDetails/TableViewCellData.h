//
//  TableViewCellData.h
//  PCS
//
//  Created by pavan yadav on 02/02/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellData : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelSerialNo;
@property (weak, nonatomic) IBOutlet UILabel *Slotlabel;
@property (weak, nonatomic) IBOutlet UILabel *Pricelabel;
@property (weak, nonatomic) IBOutlet UILabel *Bidlabel;
@property (weak, nonatomic) IBOutlet UILabel *Buylabel;
@end
