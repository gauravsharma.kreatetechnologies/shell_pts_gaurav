//
//  BidsummaryViewController.m
//  BidSummary
//
//  Created by pavan yadav on 28/11/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import "BidsummaryViewController.h"
#import "SumarryCell.h"
#import "summaryViewController.h"
#import "Services.h"
#import "AppHelper.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "CustmObj.h"
@class GGProgress;
@interface BidsummaryViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *SummaryTableview;
@end

NSString *tradingDate;



@implementation BidsummaryViewController
{
    
    NSArray *arrResponse;
    NSMutableArray *arrDate;
    NSMutableArray *DeliveryDate;
    NSMutableArray *submitCheck;
    NSString *dater;
    NSString *parameterDate;
    BOOL dateselect;
    
    
    NSString *Datesingle;
    NSString *Datecheck;
   
    
}

- (void)viewDidLoad
    {
    [super viewDidLoad];
   
    self.buttonPicktradinDate.hidden=YES;
    self.datePickerView.hidden=YES;
    arrDate=[[NSMutableArray alloc]init];
    DeliveryDate=[[NSMutableArray alloc]init];
    submitCheck =[[NSMutableArray alloc]init];
    _SummaryTableview.separatorStyle = NO;
    parameterDate = @"ALL";
    [self initialServiceHitData];

       
    
}
    
-(void)viewWillAppear:(BOOL)animated
    {
      [self viewDidLoad];
    }


//strFromDate = [dateFormatterServer stringFromDate:_DatePickerFrom.date];


-(void)initialServiceHitData
{

    //[SVProgressHUD showWithStatus:@"Please wait..."];
    [CustmObj addHud:self.view];
    

//    [GGProgress.shared.showProgress];
       
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:parameterDate,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];

    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/getfullbiddetail.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else if (type == kresponseTypeSuccess)
        {
            
            [self->submitCheck removeAllObjects];
            [self->DeliveryDate removeAllObjects];
            [self->arrDate removeAllObjects];
            
            self->arrResponse=response;
            
            if (self->arrResponse.count == 0)
{
    [self->_SummaryTableview reloadData];
    [SVProgressHUD showErrorWithStatus:@"No Data found"];
    

    if (dateselect == NO)
    {
        [_buttonPicktradinDate setTitle:@"PICK TRADING DATE" forState:UIControlStateNormal];
         parameterDate = @"ALL";
    }
    else
    {
        parameterDate = Datesingle;
        [_buttonPicktradinDate setTitle:Datesingle forState:UIControlStateNormal];
        Datecheck = @"oldDate";
    }
    
//    [self performSelector:@selector(initialServiceHitData) withObject:nil afterDelay:1.0];
    [CustmObj removeHud:self.view];

}
        
else
{
    
    if (arrResponse.count == 1)
    {
        
        NSDateFormatter *dateFormatterServer = [[NSDateFormatter alloc] init];
        [dateFormatterServer setDateFormat:@"yyyy-MM-dd"];
        
        if ([Datecheck isEqualToString:@"oldDate"])
        {
            //            NSLog(@"date curren %@",Datesingle);
        }
        else
        {
            Datesingle = [dateFormatterServer stringFromDate:_datePicker.date];
        }
        
        Datecheck = @"no";
        dateselect = YES;
    }
   
    
   for (int i=0; i<[response count]; i++)
            {
                NSMutableDictionary *tempDicts = [[NSMutableDictionary alloc]init];
                tempDicts = (NSMutableDictionary *)[[response valueForKeyPath:@"date"] objectAtIndex:i];
                [arrDate addObject:tempDicts];
    
                NSMutableArray *temp = [[response valueForKey:@"delivery_date"] objectAtIndex:i];
                [DeliveryDate addObject:temp];
                
               
                
                NSMutableArray *tempCheck = [[response valueForKey:@"status"]  objectAtIndex:i];
                [submitCheck addObject:tempCheck];
            }
    
            
            [_SummaryTableview reloadData];
           // [SVProgressHUD dismiss];
           [CustmObj removeHud:self.view];
        }
}
        
        
    }];
}
-(IBAction)menuButtonClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 43;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return DeliveryDate.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SumarryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"summryid" forIndexPath:indexPath];

    cell.SubmitButton.tag = indexPath.row;
    
    if ([[submitCheck objectAtIndex:indexPath.row]isEqualToString:@"TRUE"])
    {
        cell.SubmitButton.hidden = true;
    }
    else
    {
        cell.SubmitButton.hidden = false;
    }
    
    cell.lblSrNo.text =[NSString stringWithFormat:@"%ld%@",indexPath.row+1,@"."];

    cell.labelHead.text=[NSString stringWithFormat:@"%@-%@",@"Trading Date",[DeliveryDate objectAtIndex:indexPath.row]];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    ;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    dater=[arrDate objectAtIndex:indexPath.row];
    [self Datehit];
}


-(void)Datehit
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:dater,@"date",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/getfullbiddetail.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            
            summaryViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"summaryId"];
            
            newView.ResponseData=response;
            newView.selectedDate=dater;
            [self.navigationController pushViewController:newView animated:YES];
        }
        
    }];
}

- (IBAction)submitButton:(UIButton*)sender
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Are you sure you want to Submit this bid ?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"YES"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {

        tradingDate = [arrDate objectAtIndex:sender.tag];
                                [self buttonSubmit];
        
    }];
    
    UIAlertAction *thirdActionNO = [UIAlertAction actionWithTitle:@"NO"
                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                
                                                                // NSLog(@"You pressed button three");
                                                            }];
    
    [alert addAction:thirdAction];
    [alert addAction:thirdActionNO];
    [self presentViewController:alert animated:YES completion:nil];
    
   
    
}


-(void)buttonSubmit
{
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:tradingDate,@"biddate",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    // NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/newbid/submitbiddatewise.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        //  NSLog(@"Response %@",response);
        
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            self.view.userInteractionEnabled=YES;
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
                    
            if ([[response objectForKey:@"status"] isEqualToString:@"SUCCESS"])
            {
                [SVProgressHUD showSuccessWithStatus:[response objectForKey:@"message"] ];
                
            }
            else
            {
                [SVProgressHUD showErrorWithStatus:[response objectForKey:@"message"]];
            }
            //[self performSelector:@selector(initialServiceHitData) withObject:nil afterDelay:3.0];
           
          [self initialServiceHitData];
            
        }
        
    }];
}



-(IBAction)logoutButtonClick:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}




- (IBAction)cancelBtnClcik:(id)sender
{
    self.datePickerView.hidden=YES;
}

- (IBAction)doneBtnClick:(id)sender
{
   
    NSDateFormatter *dateFormatterServer = [[NSDateFormatter alloc] init];
    [dateFormatterServer setDateFormat:@"yyyy-MM-dd"];
    
    
    parameterDate = [dateFormatterServer stringFromDate:_datePicker.date];
    [_buttonPicktradinDate setTitle:[dateFormatterServer stringFromDate:_datePicker.date] forState:UIControlStateNormal];
    self.datePickerView.hidden=YES;
     [self initialServiceHitData];
}

- (IBAction)buttonpickTrading:(id)sender
{
     self.datePickerView.hidden= NO;
    
}

- (IBAction)Buttonsearchimage:(id)sender
{
    self.buttonPicktradinDate.hidden=NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
