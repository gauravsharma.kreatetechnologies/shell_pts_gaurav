//
//  summaryViewController.h
//  PCS
//
//  Created by pavan yadav on 02/02/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface summaryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *myTableview;
@property(strong, nonatomic) NSDictionary *ResponseData;
@property(strong, nonatomic) NSString *selectedDate;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;

@property (weak, nonatomic) IBOutlet UIView *viewCopypopUp;
@property (weak, nonatomic) IBOutlet UILabel *labelFromdate;
@property (weak, nonatomic) IBOutlet UIButton *buttonToDate;

@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
