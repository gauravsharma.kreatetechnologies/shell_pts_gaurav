//
//  SumarryCell.h
//  BidSummary
//
//  Created by pavan yadav on 28/11/16.
//  Copyright © 2016 Invetech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SumarryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSrNo;
@property (weak, nonatomic) IBOutlet UILabel *labelHead;
@property (weak, nonatomic) IBOutlet UIButton *SubmitButton;

@end
