//
//  MarketPriceViewController.m
//  PCS
//
//  Created by lab4code on 19/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "MarketPriceViewController.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "MarketPriceTableViewCell.h"
#import "UUChart.h"
#import "AppDelegate.h"
#import "CustmObj.h"

@interface MarketPriceViewController ()<UUChartDataSource>
{
    UUChart *chartView;
    int row;
    BOOL isMinBtnSelected;
    BOOL isAvgBtnSelected;
    BOOL isMaxBtnSelected;
    
    BOOL isMinBtnSelectedRowOne;
    BOOL isAvgBtnSelectedRowOne;
    BOOL isMaxBtnSelectedRowOne;
}


@property(weak,nonatomic)IBOutlet UITableView *myTableView;
@property(strong,nonatomic)NSMutableArray *iexArray;
@property(strong,nonatomic)NSMutableArray *pxilArray;
@property(strong,nonatomic) NSString *strDate;
@end

@implementation MarketPriceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.myTableView.hidden=YES;
    
    isMinBtnSelected=YES;
    isAvgBtnSelected=YES;
    isMaxBtnSelected=YES;
    isMinBtnSelectedRowOne=YES;
    isAvgBtnSelectedRowOne=YES;
    isMaxBtnSelectedRowOne=YES;
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    

    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
   // [SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"html/IEXmarketclearingprice.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        
        if (type == kResponseTypeFail)
        {

            self.view.userInteractionEnabled=YES;
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {

                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSMutableDictionary *dictResponsedata=(NSMutableDictionary *)response;
            //  NSLog(@"Response %@",dictResponsedata);
            
            if (dictResponsedata)
            {
                if ([dictResponsedata objectForKey:@"iex"])
                {
                    self.iexArray=[dictResponsedata objectForKey:@"iex"];
                }
                
                if ([dictResponsedata objectForKey:@"pxil"])
                {
                    self.pxilArray=[dictResponsedata objectForKey:@"pxil"];
                }
                
                if ([dictResponsedata objectForKey:@"year"])
                {
                    self.strDate=[dictResponsedata objectForKey:@"year"];
                }
                
                
                self.myTableView.hidden=NO;
                [self.myTableView reloadData];
            }
        }
        
        
    }];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)menuButtonClick:(id)sender
{
    //[SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - graph

- (NSArray *)UUChart_xLableArray:(UUChart *)chart
{
    
    NSMutableArray *arrXdata=[[NSMutableArray alloc]init];
    
    for (NSMutableArray *arr in self.iexArray)
    {
        [arrXdata addObject:[arr objectAtIndex:0]];
    }
    
    
    return arrXdata;
}

- (NSArray *)UUChart_yValueArray:(UUChart *)chart
{
    
    
    
    
    NSMutableArray *arrMin=[[NSMutableArray alloc]init];
    NSMutableArray *arrAvg=[[NSMutableArray alloc]init];
    NSMutableArray *arrMax=[[NSMutableArray alloc]init];
    
    
    if (row == 0)
    {
        for (NSMutableArray *arr in self.iexArray)
        {
            [arrMin addObject:[arr objectAtIndex:1]];
            [arrAvg addObject:[arr objectAtIndex:2]];
            [arrMax addObject:[arr objectAtIndex:3]];
        }
        
        
    }
    else
    {
        for (NSMutableArray *arr in self.pxilArray)
        {
            [arrMin addObject:[arr objectAtIndex:1]];
            [arrAvg addObject:[arr objectAtIndex:2]];
            [arrMax addObject:[arr objectAtIndex:3]];
        }
    }
    
    if (row == 0) {
        
        if (isMaxBtnSelected && isMinBtnSelected && isAvgBtnSelected) {
            return @[arrMin,arrAvg,arrMax];
        }
        else
        {
            if (isMaxBtnSelected && isMinBtnSelected) {
                return @[arrMax,arrMin];
                
                
            }
            else if (isMaxBtnSelected && isAvgBtnSelected)
            {
                return @[arrAvg,arrMax];
            }
            else if (isMinBtnSelected && isAvgBtnSelected)
            {
                return @[arrMin,arrAvg];
            }
            else
            {
                if (isMaxBtnSelected) {
                    return @[arrMax];
                }
                else if (isMinBtnSelected)
                {
                    return @[arrMin];
                }
                else if (isAvgBtnSelected)
                {
                    return @[arrAvg];
                }
                else
                {
                    return nil;
                }
            }
        }
        
    }
    else
    {
        if (isMaxBtnSelectedRowOne && isMinBtnSelectedRowOne && isAvgBtnSelectedRowOne) {
            return @[arrMin,arrAvg,arrMax];
        }
        else
        {
            if (isMaxBtnSelectedRowOne && isMinBtnSelectedRowOne) {
                return @[arrMax,arrMin];
                
                
            }
            else if (isMaxBtnSelectedRowOne && isAvgBtnSelectedRowOne)
            {
                return @[arrAvg,arrMax];
            }
            else if (isMinBtnSelectedRowOne && isAvgBtnSelectedRowOne)
            {
                return @[arrMin,arrAvg];
            }
            else
            {
                if (isMaxBtnSelectedRowOne) {
                    return @[arrMax];
                }
                else if (isMinBtnSelectedRowOne)
                {
                    return @[arrMin];
                }
                else if (isAvgBtnSelectedRowOne)
                {
                    return @[arrAvg];
                }
                else
                {
                    
                    return nil;
                }
            }
        }
    }
    
    //return @[arrMin,arrAvg,arrMax];
}
#pragma mark - @optional

- (NSArray *)UUChart_ColorArray:(UUChart *)chart
{
    //Green:- Red Min
    //Red:- Green  Avg
    //Brown:- Blue  Max
    
    
    if (row == 0) {
        
        if (isMaxBtnSelected && isMinBtnSelected && isAvgBtnSelected) {
            return @[UURed,UUGreen,UUBlue];
        }
        else
        {
            if (isMaxBtnSelected && isMinBtnSelected) {
                return @[UUBlue,UURed];
                
                
            }
            else if (isMaxBtnSelected && isAvgBtnSelected)
            {
                return @[UUGreen,UUBlue];
            }
            else if (isMinBtnSelected && isAvgBtnSelected)
            {
                return @[UURed,UUGreen];
            }
            else
            {
                if (isMaxBtnSelected) {
                    return @[UUBlue];
                }
                else if (isMinBtnSelected)
                {
                    return @[UURed];
                }
                else if (isAvgBtnSelected)
                {
                    return @[UUGreen];
                }
                else
                {
                    
                    return nil;
                }
            }
        }
        
    }
    else
    {
        if (isMaxBtnSelectedRowOne && isMinBtnSelectedRowOne && isAvgBtnSelectedRowOne) {
            return @[UURed,UUGreen,UUBlue];
        }
        else
        {
            if (isMaxBtnSelectedRowOne && isMinBtnSelectedRowOne) {
                return @[UUBlue,UURed];
                
                
            }
            else if (isMaxBtnSelectedRowOne && isAvgBtnSelectedRowOne)
            {
                return @[UUGreen,UUBlue];
            }
            else if (isMinBtnSelectedRowOne && isAvgBtnSelectedRowOne)
            {
                return @[UURed,UUGreen];
            }
            else
            {
                if (isMaxBtnSelectedRowOne) {
                    return @[UUBlue];
                }
                else if (isMinBtnSelectedRowOne)
                {
                    return @[UURed];
                }
                else if (isAvgBtnSelectedRowOne)
                {
                    return @[UUGreen];
                }
                else
                {
                    
                    return nil;
                }
            }
        }
    }
}

- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart
{
    // return CGRangeMake(100, 0);
    return CGRangeZero;
}



#pragma mark - Table View Delegate and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
        
    }
    else if (section == 1)
    {
        
        if ([self.iexArray count] > [self.pxilArray count])
        {
            if ([self.pxilArray count] !=0) {
                return [self.pxilArray count];
            }
            else
            {
                return [self.iexArray count];
            }
            
        }
        else
        {
            if ([self.iexArray count] !=0) {
                return [self.iexArray count];
            }
            else
            {
                return [self.pxilArray count];
            }
        }
    }
    if ([self.iexArray count]>0 && [self.pxilArray count]>0)
    {
        return 2;
    }
    else if ([self.iexArray count]>0 || [self.pxilArray count]>0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 80;
    }
    
    if (indexPath.section == 1)
    {
        return 36;
    }
    return 300;
    
    /*
     if (indexPath.section == 0) {
     return 300;
     }
     
     if (indexPath.section == 1)
     {
     return 80;
     }
     return 36;
     */
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        
        
        static NSString *simpleTableIdentifier = @"firstMarketPriceCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        
        UILabel *lblDate=(UILabel *)[cell.contentView viewWithTag:5];
        lblDate.text=self.strDate;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
        
        
    }
    else if (indexPath.section==1)
    {
        static NSString *simpleTableIdentifier = @"MarketPriceScreenCell";
        MarketPriceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([self.iexArray count] > indexPath.row)
        {
            cell.lblDate.text=[[self.iexArray objectAtIndex:indexPath.row]objectAtIndex:0];
            [cell.lblIEXMin setTitle:[NSString stringWithFormat:@"%.01f",[[[self.iexArray objectAtIndex:indexPath.row]objectAtIndex:1]floatValue]] forState:UIControlStateNormal];
            
            [cell.lblIEXAvg setTitle:[NSString stringWithFormat:@"%.01f",[[[self.iexArray objectAtIndex:indexPath.row]objectAtIndex:2]floatValue]] forState:UIControlStateNormal];
            
            [cell.lblIEXMax setTitle:[NSString stringWithFormat:@"%.01f",[[[self.iexArray objectAtIndex:indexPath.row]objectAtIndex:3]floatValue]] forState:UIControlStateNormal];
        }
        
        if ([self.pxilArray count]> indexPath.row)
        {
            cell.lblDate.text=[[self.pxilArray objectAtIndex:indexPath.row]objectAtIndex:0];
            
            [cell.lblPXILMin setTitle:[NSString stringWithFormat:@"%.01f",[[[self.pxilArray objectAtIndex:indexPath.row]objectAtIndex:1]floatValue]] forState:UIControlStateNormal];
            
            [cell.lblPXILAvg setTitle:[NSString stringWithFormat:@"%.01f",[[[self.pxilArray objectAtIndex:indexPath.row]objectAtIndex:2]floatValue]] forState:UIControlStateNormal];
            
            [cell.lblPXILMax setTitle:[NSString stringWithFormat:@"%.01f",[[[self.pxilArray objectAtIndex:indexPath.row]objectAtIndex:3]floatValue]] forState:UIControlStateNormal];
            
        }
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"graphCell";
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                                       reuseIdentifier: simpleTableIdentifier];
        
        row=(int)indexPath.row;
        cell.backgroundColor=[UIColor clearColor];
        
        
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 47)];
        
        if (row == 0)
        {
            img.image=[UIImage imageNamed:@"IEX_market_clearing_price"];
            
        }
        else
        {
            img.image=[UIImage imageNamed:@"PXIL_MARKET_CLEARING_PRICE"];
        }
        
        [cell.contentView addSubview:img];
        
        chartView= nil;
        
        chartView= [[UUChart alloc]initwithUUChartDataFrame:CGRectMake(28, 50, [UIScreen mainScreen].bounds.size.width-40, 200)
                                                 withSource:self
                                                  withStyle:UUChartLineStyle];
        
        chartView.backgroundColor=[UIColor clearColor];
        
        [chartView showInView:cell.contentView];
        
        UILabel *labelVertical = [[UILabel alloc] initWithFrame:CGRectMake(-35, 160, 120, 20)];
        labelVertical.font = [UIFont systemFontOfSize:14];
        labelVertical.textAlignment = NSTextAlignmentCenter;
        labelVertical.textColor = [UIColor grayColor];
        [labelVertical setText:@"Values in Rupees"];
        labelVertical.transform=CGAffineTransformMakeRotation( -M_PI / 2);
        [cell.contentView addSubview:labelVertical];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-30,250,100,20)];
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor grayColor];
        label.text = @"Trading Date";
        [cell.contentView addSubview:label];
        
        float btnLegend = ([UIScreen mainScreen].bounds.size.width-35)/3;
        
        UIButton *btnGreen=[UIButton buttonWithType:UIButtonTypeSystem];
        btnGreen.frame=CGRectMake(20, 280, btnLegend, 15);
        
        [btnGreen addTarget:self action:@selector(greenMinClick:) forControlEvents:UIControlEventTouchUpInside];
        btnGreen.tag=indexPath.row;
        if (indexPath.row == 0) {
            if (isMinBtnSelected) {
                [btnGreen setBackgroundImage:[UIImage imageNamed:@"minimum_graph"] forState:UIControlStateNormal];
                
            }
            else
            {
                [btnGreen setBackgroundImage:[UIImage imageNamed:@"inactive-minimum"] forState:UIControlStateNormal];
            }
        }
        else
        {
            if (isMinBtnSelectedRowOne) {
                
                [btnGreen setBackgroundImage:[UIImage imageNamed:@"minimum_graph"] forState:UIControlStateNormal];
            }
            else
            {
                [btnGreen setBackgroundImage:[UIImage imageNamed:@"inactive-minimum"] forState:UIControlStateNormal];
            }
        }
        
        [cell.contentView addSubview:btnGreen];
        
        UIButton *btnRed=[UIButton buttonWithType:UIButtonTypeSystem];
        btnRed.frame=CGRectMake(btnGreen.frame.origin.x+btnLegend+5, 280, btnLegend, 15);
        [btnRed addTarget:self action:@selector(redAvgClick:) forControlEvents:UIControlEventTouchUpInside];
        btnRed.tag=indexPath.row;
        if (indexPath.row == 0) {
            if (isAvgBtnSelected) {
                [btnRed setBackgroundImage:[UIImage imageNamed:@"average_graph"] forState:UIControlStateNormal];
            }
            else
            {
                [btnRed setBackgroundImage:[UIImage imageNamed:@"inactive-average"] forState:UIControlStateNormal];
            }
        }
        else
        {
            if (isAvgBtnSelectedRowOne) {
                [btnRed setBackgroundImage:[UIImage imageNamed:@"average_graph"] forState:UIControlStateNormal];
            }
            else
            {
                
                [btnRed setBackgroundImage:[UIImage imageNamed:@"inactive-average"] forState:UIControlStateNormal];
            }
        }
        
        [cell.contentView addSubview:btnRed];
        
        
        UIButton *btnBrown=[UIButton buttonWithType:UIButtonTypeSystem];
        btnBrown.frame=CGRectMake(btnRed.frame.origin.x+btnLegend+5, 280, btnLegend, 15);
        
        [btnBrown addTarget:self action:@selector(redMaxClick:) forControlEvents:UIControlEventTouchUpInside];
        btnBrown.tag=indexPath.row;
        if (indexPath.row == 0) {
            if (isMaxBtnSelected) {
                [btnBrown setBackgroundImage:[UIImage imageNamed:@"maximum_graph"] forState:UIControlStateNormal];
            }
            else
            {
                [btnBrown setBackgroundImage:[UIImage imageNamed:@"inactive-maximum"] forState:UIControlStateNormal];
            }
        }
        else
        {
            if (isMaxBtnSelectedRowOne) {
                [btnBrown setBackgroundImage:[UIImage imageNamed:@"maximum_graph"] forState:UIControlStateNormal];
            }
            else
            {
                [btnBrown setBackgroundImage:[UIImage imageNamed:@"inactive-maximum"] forState:UIControlStateNormal];
            }
        }
        
        [cell.contentView addSubview:btnBrown];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
        
        
    }
    
    
}

-(void)greenMinClick:(id)sender
{
    UIButton *but=(UIButton *)sender;
    int tag=(int)but.tag;
    row=tag;
    
    if (row == 0) {
        if (isMinBtnSelected)
        {
            isMinBtnSelected=NO;
        }
        else
        {
            isMinBtnSelected=YES;
        }
    }
    else
    {
        if (isMinBtnSelectedRowOne)
        {
            isMinBtnSelectedRowOne=NO;
        }
        else
        {
            isMinBtnSelectedRowOne=YES;
        }
    }
    
    
    [self.myTableView reloadData];
}

-(void)redAvgClick:(id)sender
{
    UIButton *but=(UIButton *)sender;
    int tag=(int)but.tag;
    row=tag;
    

    
    if (row == 0) {
        
        // NSLog(@"Row and isMaxBtnSelected,isMinBtnSelected,isAvgBtnSelected %d, %d, %d, %d ",row,isMaxBtnSelected,isMinBtnSelected,isAvgBtnSelected);
        
        if (isAvgBtnSelected)
        {
            isAvgBtnSelected=NO;
        }
        else
        {
            
            isAvgBtnSelected=YES;
        }
    }
    else
    {
        if (isAvgBtnSelectedRowOne)
        {
            isAvgBtnSelectedRowOne=NO;
        }
        else
        {
            isAvgBtnSelectedRowOne=YES;
        }
    }
    
    [self.myTableView reloadData];
}

-(void)redMaxClick:(id)sender
{
    UIButton *but=(UIButton *)sender;
    int tag=(int)but.tag;
    row=tag;
    
    if (row == 0) {
        if (isMaxBtnSelected)
        {
            isMaxBtnSelected=NO;
        }
        else
        {
            isMaxBtnSelected=YES;
        }
    }
    else
    {
        if (isMaxBtnSelectedRowOne)
        {
            isMaxBtnSelectedRowOne=NO;
        }
        else
        {
            isMaxBtnSelectedRowOne=YES;
        }
    }
    [self.myTableView reloadData];
}

-(IBAction)btnLogout_Click:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
