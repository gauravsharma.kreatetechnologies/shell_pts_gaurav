//
//  MarketPriceTableViewCell.h
//  PCS
//
//  Created by lab4code on 20/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketPriceTableViewCell : UITableViewCell

@property(weak,nonatomic)IBOutlet UILabel *lblDate;

@property(weak,nonatomic)IBOutlet UIButton *lblIEXMin;
@property(weak,nonatomic)IBOutlet UIButton *lblIEXAvg;
@property(weak,nonatomic)IBOutlet UIButton *lblIEXMax;
@property(weak,nonatomic)IBOutlet UIButton *lblPXILMin;
@property(weak,nonatomic)IBOutlet UIButton *lblPXILAvg;
@property(weak,nonatomic)IBOutlet UIButton *lblPXILMax;


@end
