//
//  AlarmNotification.h
//  PCS
//
//  Created by pavan yadav on 27/12/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AlarmNotification : UIViewController
@property (weak, nonatomic) IBOutlet UIView *uivieww;
@property (weak, nonatomic) IBOutlet UITextView *TextView;
@property (strong, nonatomic) IBOutlet UIView *supUiview;
@property (weak, nonatomic) IBOutlet UILabel *labelClickHere;

@property(strong, nonatomic) NSDictionary *arrResponse;
@property (weak, nonatomic) IBOutlet UIButton *ButtonKnowm;
@end
