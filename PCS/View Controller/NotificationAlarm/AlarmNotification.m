//
//  AlarmNotification.m
//  PCS
//
//  Created by pavan yadav on 27/12/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "AlarmNotification.h"
#import "AppHelper.h"
#import "YLImageView.h"
#import "YLGIFImage.h"
#import "LogonViewController.h"
#import "AppDelegate.h"
#import "Power_Trading_Solutions-Swift.h"


@interface AlarmNotification ()


@end

@implementation AlarmNotification

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([_arrResponse[@"button"] isEqualToString:@"hide"])
    {
        _ButtonKnowm.hidden=YES;
        _labelClickHere.hidden=YES;
        
    }
    else
    {
        _ButtonKnowm.hidden=NO;
        _labelClickHere.hidden=NO;
    }
    
    _TextView.text = _arrResponse[@"body"];
    
    YLImageView * imageView = [[YLImageView alloc] initWithFrame:CGRectMake(0,0,150,150)];
    [self.uivieww addSubview:imageView];
    
    if ([_arrResponse[@"screen"] isEqualToString:@"REALTIME"])
    {
        _supUiview.backgroundColor = [UIColor redColor];
        imageView.image = [YLGIFImage imageNamed:@"light_red_bell.gif"];
        _TextView.backgroundColor = [UIColor redColor];
        _labelClickHere.textColor=[UIColor whiteColor];
    }
    
    else if ([_arrResponse[@"screen"] isEqualToString:@"RESTORE"])
    {
        _supUiview.backgroundColor = [UIColor yellowColor];
        imageView.image = [YLGIFImage imageNamed:@"yellow_bell.gif"];
        _TextView.backgroundColor = [UIColor yellowColor];
        _TextView.textColor = [UIColor blackColor];
        _labelClickHere.textColor=[UIColor blackColor];
    }
    
    else if ([_arrResponse[@"screen"] isEqualToString:@"precurtailment"])
    {
        _supUiview.backgroundColor = [UIColor blueColor];
        imageView.image = [YLGIFImage imageNamed:@"new_bell_blue.gif"];
        _TextView.backgroundColor = [UIColor blueColor];
        _labelClickHere.textColor=[UIColor blackColor];
    }
    
    else if ([_arrResponse[@"screen"] isEqualToString:@"NOTAFFECTED"])
    {
        _supUiview.backgroundColor = [UIColor greenColor];
        imageView.image = [YLGIFImage imageNamed:@"light_green_bell.gif"];
        _TextView.backgroundColor = [UIColor greenColor];
        _TextView.textColor = [UIColor blackColor];
        _labelClickHere.textColor=[UIColor whiteColor];
    }
    
    else if ([_arrResponse[@"screen"] isEqualToString:@"NLDCTOIEX"])
    {
        _supUiview.backgroundColor = [UIColor colorWithRed:137.0f/255.0f green:5.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
        imageView.image = [YLGIFImage imageNamed:@"dark_red_bell.gif"];
        _TextView.backgroundColor =  [UIColor colorWithRed:137.0f/255.0f green:5.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
        _labelClickHere.textColor=[UIColor whiteColor];
        }
    
}
- (IBAction)ButtonKnowmore:(id)sender
{
    if(![AppHelper userDefaultsForKey:@"client_id"])
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
   
        CurtailmentViewController *pushView = [self.storyboard instantiateViewControllerWithIdentifier:@"CurtailmentView"];
        
        pushView.dateapicall = [AppHelper userDefaultsForKey:@"curtailmentdate"];
        pushView.revisionApicall = [AppHelper userDefaultsForKey:@"curtailmentrevision"];
        
        [self.navigationController pushViewController:pushView animated:YES];
        
#pragma mark - Remove key from AppHelper.
        [AppHelper removeFromUserDefaultsWithKey:@"curtailmentdate"];
        [AppHelper removeFromUserDefaultsWithKey:@"curtailmentrevision"];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)btnBack_Click:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [AppHelper removeFromUserDefaultsWithKey:@"curtailmentdate"];
    [AppHelper removeFromUserDefaultsWithKey:@"curtailmentrevision"];
}




@end





