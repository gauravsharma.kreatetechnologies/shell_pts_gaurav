//
//  CollectionViewCellAllnews.h
//  PCS
//
//  Created by pavan yadav on 30/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCellAllnews : UICollectionViewCell

@property(weak,nonatomic)IBOutlet UILabel *lblHeader;
@property(weak,nonatomic)IBOutlet UIImageView *imgHeader;
@end
