//
//  allnewsTableViewCell.h
//  PCS
//
//  Created by pavan yadav on 30/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface allnewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNewsContent;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsSource;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewNewsIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsDate;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsTime;
@end
