//
//  SidemenuAllnewsfeed.m
//  PCS
//
//  Created by pavan yadav on 30/09/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "SidemenuAllnewsfeed.h"
#import "SideMenuNewsfeedViewController.h"
#import "allnewsTableViewCell.h"
#import "CollectionViewCellAllnews.h"
#import "SlideNavigationController.h"
#import "LogonViewController.h"
#import "WebbrowserControllerViewController.h"
#import "MenuViewController.h"
#import "AppHelper.h"
#import "Services.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import "SlideNavigationController.h"
#import "CustmObj.h"
@class SideMenuViewController;

@interface SidemenuAllnewsfeed ()<UISearchBarDelegate>
{
    int imgShowRow;
    NSString *Strkeyword;
    NSString *strtype;
    NSString *strtypesec;
    BOOL apiselect;
    
     BOOL defaultVlueapi1;
     BOOL defaultVlueapi2;
    
     NSString *Lognotification;
    
}
@property (weak, nonatomic) IBOutlet UITableView *mytableview;
@property(weak,nonatomic)IBOutlet UICollectionView *collectionHeaderView;
@property (strong, nonatomic) NSString *strNewsHead;

@end

@implementation SidemenuAllnewsfeed

- (void)viewDidLoad {
    [super viewDidLoad];

    _SearchBarView.delegate=self;
    
    imgShowRow=0;
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }

    [self FirstServiceHitData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)FirstServiceHitData
{
    
    //[SVProgressHUD showWithStatus:@"Please wait..."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/getnewsfeedinfo.php" param:nil andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
   
            
            if ([response count]>0)
            {
                
                if (defaultVlueapi2==NO)
                {
                    
                    _strApidata=response[[[response allKeys] objectAtIndex:0]];
                }
                else
                {
                    _strApidata=response[strtype];
                }
                
                
                
                _strMenuitem=[response allKeys];
                
                //            NSLog(@"%@",response);
                [_collectionHeaderView reloadData];
                [_mytableview reloadData];
                
            }}
    }];
}



-(void)initialServiceHitData
{
    
    
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:Strkeyword,@"keyword",[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    // NSLog(@"string %@",myString);
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
   
    
    [[Services sharedInstance]serviceCallbyPost:@"service/searchnewsfeeddetails.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
        // NSLog(@"Response %@",response);
        
        if (type == kResponseTypeFail)
        {
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        
        
        else if (type == kresponseTypeSuccess)
        {
            
            if ([response count]>0)
                
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];

                if (defaultVlueapi1==NO)
                {
                    
                      _strApidata=response[[[response allKeys] objectAtIndex:0]];
                }
                else
                {
                     _strApidata=response[strtypesec];
                }
                
                _strMenuitem=[response allKeys];
                
                [_collectionHeaderView reloadData];
                [_mytableview reloadData];
            }
            else
            {
                [SVProgressHUD showErrorWithStatus:@"No Data found"];
                // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
        }
    }];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.strApidata count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    allnewsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *dictNewsData=[self.strApidata objectAtIndex:indexPath.row];
    
    
    
    NSDictionary *attrAvailableGreen = @{
                                         NSFontAttributeName : [UIFont italicSystemFontOfSize:10],
                                         NSForegroundColorAttributeName : [UIColor blackColor]
                                         };
    
    
    
    [cell.lblNewsContent setText:dictNewsData[@"title"]];
    
    //NSLog(@"%@",dictNewsData[@"source"]);
    
    if (dictNewsData[@"source"] == [NSNull null] || dictNewsData[@"pubdate"] == [NSNull null] ||dictNewsData[@"time"] == [NSNull null] )
    
    {
        if (dictNewsData[@"source"] == [NSNull null])
        {
        
        NSAttributedString *strDate = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" - %@",dictNewsData[@"pubdate"]] attributes:attrAvailableGreen];
        
        
         NSMutableAttributedString *strSourceDateTime=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"  %@",dictNewsData[@"time"]] attributes:attrAvailableGreen];
        
        [strSourceDateTime appendAttributedString:strDate];
        
        [cell.lblNewsSource setAttributedText:strSourceDateTime];
        
        }
        else if (dictNewsData[@"pubdate"] == [NSNull null])
        {
        
            
            NSAttributedString *strTime = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" - %@",dictNewsData[@"time"]] attributes:attrAvailableGreen];
            
            
            NSMutableAttributedString *strSourceDateTime=[[NSMutableAttributedString alloc]initWithString:dictNewsData[@"source"] attributes:nil];
            
            [strSourceDateTime appendAttributedString:strTime];
            
            
            [cell.lblNewsSource setAttributedText:strSourceDateTime];
            
        }
        else if (dictNewsData[@"time"] == [NSNull null])
        {
            NSAttributedString *strDate = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" - %@",dictNewsData[@"pubdate"]] attributes:attrAvailableGreen];
     
            
            NSMutableAttributedString *strSourceDateTime=[[NSMutableAttributedString alloc]initWithString:dictNewsData[@"source"] attributes:nil];
            
    
            
            [strSourceDateTime appendAttributedString:strDate];
            
            [cell.lblNewsSource setAttributedText:strSourceDateTime];
        }
        else
        {
             cell.lblNewsSource.text=@" ";
        }
        
    }
    
    
    else
    {
    NSAttributedString *strDate = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" - %@",dictNewsData[@"pubdate"]] attributes:attrAvailableGreen];
    
    NSAttributedString *strTime = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" - %@",dictNewsData[@"time"]] attributes:attrAvailableGreen];
    
 
    NSMutableAttributedString *strSourceDateTime=[[NSMutableAttributedString alloc]initWithString:dictNewsData[@"source"] attributes:nil];

    [strSourceDateTime appendAttributedString:strTime];
    
    [strSourceDateTime appendAttributedString:strDate];
    
    [cell.lblNewsSource setAttributedText:strSourceDateTime];
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    if(tableView == self.mytableview){
        
        
        NSArray *arrTemp = self.strApidata;
        
        NSDictionary *dictTemp = [arrTemp objectAtIndex:indexPath.row];
        
        NSString *strUrl=[dictTemp objectForKey:@"link"];

        WebbrowserControllerViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"webBrowse"];
        newView.strWebUrl=strUrl;
        [self.navigationController pushViewController:newView animated:YES];
}}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CATransform3D rotationTransform = CATransform3DTranslate(CATransform3DIdentity,0,100,0);
    cell.layer.transform = rotationTransform;
    cell.alpha = 0;
    
    [UIView animateWithDuration:.6 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        cell.layer.transform = CATransform3DIdentity;
        cell.alpha = 1;
    }
                     completion:^ (BOOL completed) {} ];
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.strMenuitem count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width=[self getExpectedSizeOfText:[self.strMenuitem objectAtIndex:indexPath.row]];
    
    
    return CGSizeMake(width+15, 23.0f);
    
    
}

-(CGFloat)getExpectedSizeOfText:(NSString *)text
{
    CGSize size = [text sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:16.0f]}];
    return size.width;
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    static NSString *cellIdentifier = @"newsHead";
    
    CollectionViewCellAllnews *cell = (CollectionViewCellAllnews *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblHeader.text=[self.strMenuitem objectAtIndex:indexPath.row];
    
    if ((int)indexPath.row == imgShowRow)
    {
        cell.lblHeader.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        cell.imgHeader.hidden=NO;
    }
    else
    {
        cell.lblHeader.font = [UIFont fontWithName:@"Helvetica" size:14];
        
        cell.imgHeader.hidden=YES;
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [self.view layoutIfNeeded];
    
    
    
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
    
    
    
    if (apiselect==NO)
    {
    strtype=[self.strMenuitem objectAtIndex: indexPath.row];
         [self FirstServiceHitData];
        defaultVlueapi2=YES;
    }
    else
    {
    strtypesec=[self.strMenuitem objectAtIndex: indexPath.row];
        defaultVlueapi1=YES;
        [self initialServiceHitData];
    }
    
 
    
    //
    imgShowRow=(int)indexPath.row;
    [UIView animateWithDuration:1
                     animations:^{
                         [collectionView reloadData]; // Called on parent view
                     }];
    
   
}



#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
    
}

//- (void)receiveTestNotification:(NSNotification *) notification
//{
//    // [notification name] should always be @"TestNotification"
//    // unless you use this method for observation of other notifications
//    // as well.
//
//   // NSLog(@"obj %@",notification.userInfo);
//
//  //  if ([[notification name] isEqualToString:@"TestNotification"])
//  //      NSLog (@"Successfully received the test notification!");
//
//    NSDictionary *dictReceivedData = notification.userInfo;
//    if([_strNewsHead isEqualToString:dictReceivedData[@"NewsHead"]]){
//
//    }else{
//        _strNewsHead = dictReceivedData[@"NewsHead"];
//        NSLog(@"%@",_strNewsHead);
//    }
//
//    /*
//    self.strApidata =dictReceivedData[@"selectedNewsData"];
//    self.strMenuitem =dictReceivedData[@"NewsHead"];
//
//    [self.mytableview reloadData];
//    [self.collectionHeaderView reloadData];
//     */
//}




- (IBAction)btnLogin_Click:(id)sender {
    
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // NSLog(@"%@",searchBar.text);
    Strkeyword=searchBar.text;
    
    [self initialServiceHitData];
    apiselect=YES;
    
}




@end
