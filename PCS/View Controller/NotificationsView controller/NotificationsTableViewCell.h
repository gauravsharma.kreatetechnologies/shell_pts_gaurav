//
//  NotificationsTableViewCell.h
//  PCS
//
//  Created by lab4code on 15/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblNotificationContent;
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationDate;
@property(weak,nonatomic)IBOutlet UILabel *lblNotificationTime;
@property(weak,nonatomic)IBOutlet UIImageView *imgNotification;

@end
