//
//  NotificationsViewController.h
//  PCS
//
//  Created by lab4code on 06/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsViewController : UIViewController

@property(weak,nonatomic) IBOutlet UIButton *btnBack;
-(IBAction)btnBack_Click:(id)sender;

@property(weak,nonatomic)IBOutlet UITableView *myTableView;
@property (strong, nonatomic) NSMutableArray *arrTableData;
@property (weak, nonatomic) IBOutlet UIView *viewNextbutton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHightconst;
    @property (weak, nonatomic) IBOutlet UIView *noDataView;

@end
