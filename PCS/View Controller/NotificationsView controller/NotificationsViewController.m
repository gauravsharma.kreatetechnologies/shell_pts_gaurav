//
//  NotificationsViewController.m
//  PCS
//
//  Created by lab4code on 06/01/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "NotificationsViewController.h"
#import "NotificationsTableViewCell.h"
#import "Services.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import "CustmObj.h"

@interface NotificationsViewController ()
    
    @property(strong,nonatomic)NSMutableArray *arrData;
    
    @end

@implementation NotificationsViewController
    
- (void)viewDidLoad
    {
        [super viewDidLoad];
        _viewHightconst.constant = 0;
        [self initialServiceHitDataNotofications];
        
    }
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    
-(void)initialServiceHitDataNotofications
    {
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];
        
        // NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"awyjxbjwuursttmdsvsgmhpiczxuywizsgjphslbnduhwgkpbcwoklxmkvgjrpjj",@"access_key", nil];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
        // NSLog(@"string %@",myString);
        
        NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
        
        
       // [SVProgressHUD showWithStatus:@"Please wait..."];
        [CustmObj addHud:self.view];
        
        [[Services sharedInstance]serviceCallbyPost:@"service/notification/getnotification.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
            
            if (type == kResponseTypeFail)
            {
                //[SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                
                NSError *error=(NSError*)response;
                
                if (error.code == -1009) {
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                   message:@"The Internet connection appears to be offline."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          }];
                    
                    [alert addAction:thirdAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            else if (type == kresponseTypeSuccess)
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                
                NSMutableDictionary *dictResponse=(NSMutableDictionary *)response;
                
                _arrTableData = dictResponse[@"details"];
                
                if(_arrTableData.count > 0)
                {
                    _noDataView.hidden = true;
                    
                    if ([dictResponse[@"more"]  isEqual: @"FALSE"])
                    {
                        [UIView animateWithDuration:1
                                         animations:^{
                                             _viewHightconst.constant = 0;
                                             [self.view layoutIfNeeded];
                                         }];
                    }
                    else
                    {
                        [UIView animateWithDuration:1
                                         animations:^{
                                             _viewHightconst.constant = 30;
                                             [self.view layoutIfNeeded];
                                         }];
                    }
                    
                    [self.myTableView reloadData];
                }
                else
                {
                    _noDataView.hidden = false;
                
                }

                
            }
            
        }];
    }
    
    
    
    
    
#pragma mark - date tap gesture
#pragma mark - Table View Delegate and datasource
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        return 1;
    }
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        return [_arrTableData count];
    }
    
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        static NSString *simpleTableIdentifier = @"NotificationsScreenCell";
        NotificationsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        NSDictionary *dictTemp = [_arrTableData objectAtIndex:indexPath.row];
        
        
        if ([[[_arrTableData objectAtIndex:indexPath.row]valueForKey:@"status"]isEqualToString:@"1"])
        {
            [cell setBackgroundColor:[UIColor clearColor]];
        }
        else
        {
            [cell setBackgroundColor:[UIColor lightGrayColor]];
        }
        
        
        [cell.lblNotificationContent setText:[dictTemp objectForKey:@"message"]];
        [cell.lblNotificationContent sizeToFit];
        
        NSArray* foo = [[dictTemp objectForKey:@"timestamp"] componentsSeparatedByString: @" "];
        
        [cell.lblNotificationDate setText:[foo objectAtIndex: 0]];
        [cell.lblNotificationTime setText:[foo objectAtIndex: 1]];
        
        NSString *strModuleType=[dictTemp objectForKey:@"module_type"];
        
        NSString *strIconName;
        
        if([strModuleType isEqualToString:@"MCP"])
        {
            strIconName = @"menumarketprice";
        }
        else if([strModuleType isEqualToString:@"NEWSLETTER"])
        {
            strIconName = @"newsletter";
        }
        else if([strModuleType isEqualToString:@"menuhome"])
        {
            strIconName = @"newhomedashboard";
        }
        else if([strModuleType isEqualToString:@"menumarketprice"])
        {
            strIconName = @"menumarketprice";
        }
        else  if([strModuleType isEqualToString:@"menudownload"])
        {
            strIconName = @"menudownload";
        }
        else  if([strModuleType isEqualToString:@"menunewbid"])
        {
            strIconName = @"menunewbid";
        }
        else if([strModuleType isEqualToString:@"menubid"])
        {
            strIconName = @"menunewbid";
        }
        else  if([strModuleType isEqualToString:@"menureport"])
        {
            strIconName = @"menureport";
        }
        else if([strModuleType isEqualToString:@"menunobid"])
        {
            strIconName = @"menunobid";
        }
        else if([strModuleType isEqualToString:@"curtailment"])
        {
            strIconName = @"curtailment";
        }
        else if([strModuleType isEqualToString:@"recplacenewbid"])
        {
            strIconName = @"menunewbid";
        }
        else if([strModuleType isEqualToString:@"recnobid"])
        {
            strIconName = @"nobid_rec";
        }
        
        cell.imgNotification.image=[UIImage imageNamed:strIconName];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
- (IBAction)buttonNext:(id)sender
    {
        [self nextApiCall];
    }
    
    
-(void)nextApiCall
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[_arrTableData objectAtIndex:_arrTableData.count-1]valueForKey:@"id"],@"id",[AppHelper userDefaultsForKey:@"access_key"],@"access_key", nil];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
        
        NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
        
        
       // [SVProgressHUD showWithStatus:@"Please wait..."];
        [CustmObj addHud:self.view];
        
        [[Services sharedInstance]serviceCallbyPost:@"service/notification/getnotification.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
            
            if (type == kResponseTypeFail)
            {
               // [SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                
                NSError *error=(NSError*)response;
                
                if (error.code == -1009) {
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                   message:@"The Internet connection appears to be offline."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          }];
                    
                    [alert addAction:thirdAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            else if (type == kresponseTypeSuccess)
            {
                
                //[SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
                NSMutableDictionary *dictResponse=(NSMutableDictionary *)response;
                _arrTableData = dictResponse[@"details"];
                
                if ([dictResponse[@"more"]  isEqual: @"FALSE"])
                {
                    [UIView animateWithDuration:1
                                     animations:^{
                                         _viewHightconst.constant = 0;
                                         [self.view layoutIfNeeded];
                                     }];
                }
                else
                {
                    [UIView animateWithDuration:1
                                     animations:^{
                                         _viewHightconst.constant = 30;
                                         [self.view layoutIfNeeded];
                                     }];
                }
                
                [self.myTableView reloadData];
            }
            
        }];
    }
    
    
    
    
-(IBAction)btnBack_Click:(id)sender
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
-(IBAction)btnLogout_Click:(id)sender
    {
        [AppDelegate logout];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
    @end
