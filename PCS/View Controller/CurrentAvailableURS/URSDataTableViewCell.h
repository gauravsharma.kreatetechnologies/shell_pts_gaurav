//
//  URSDataTableViewCell.h
//  PCS
//
//  Created by lab4code on 26/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URSDataTableViewCell : UITableViewCell

@property (weak,nonatomic)IBOutlet UIView *viewCell;
@property(weak,nonatomic)IBOutlet UILabel *lblSrNo;
@property(weak,nonatomic)IBOutlet UILabel *lblBlock;
@property(weak,nonatomic)IBOutlet UILabel *lblQtm;

@end
