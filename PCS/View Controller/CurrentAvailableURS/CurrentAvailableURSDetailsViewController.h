//
//  CurrentAvailableURSDetailsViewController.h
//  PCS
//
//  Created by lab4code on 25/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrentAvailableURSViewController.h"

@interface CurrentAvailableURSDetailsViewController : UIViewController

@property(weak,nonatomic)IBOutlet UITableView *tableViewURSDetailsData;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)btnBack_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblTableHeaderSrNo;

@property (weak, nonatomic) IBOutlet UILabel *lblTableHeaderBlock;

@property (weak, nonatomic) IBOutlet UILabel *lblTableHeaderVal;

@property (weak, nonatomic) IBOutlet UILabel *lblTableFooterTh;
@property (weak, nonatomic) IBOutlet UILabel *lblTableFooterVal;


@property(strong, nonatomic) NSString *strSelectedGenerator;
@property(strong, nonatomic) NSString *strGeneratorTotalQuantum;
@property (weak, nonatomic) IBOutlet UILabel *ursRegionLbl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
