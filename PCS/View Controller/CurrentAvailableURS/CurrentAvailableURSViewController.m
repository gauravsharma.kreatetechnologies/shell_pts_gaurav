//
//  CurrentAvailableURSViewController.m
//  PCS
//
//  Created by lab4code on 25/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "CurrentAvailableURSViewController.h"
#import "SlideNavigationController.h"
#import "LogonViewController.h"
#import "MenuViewController.h"
#import "CustmObj.h"

@interface CurrentAvailableURSViewController ()

{
    NSString *selectedRegion;
    NSString *regionnamesend;
    NSArray  *arrydetails;
    NSArray  *regionarray;
    NSMutableAttributedString *strFinalData;
    NSString *salect;
    NSString *Lognotification;
}

@property (strong,nonatomic) NSArray *arrRegions;
@property (strong,nonatomic) NSArray *arrgenerator;
@property (strong,nonatomic) NSArray *arrayRate;
@property (strong,nonatomic) NSArray *arrayamt;



@end

@implementation CurrentAvailableURSViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
    
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotification:) name:@"TestNotification" object:nil];
    
    self.lblNoData.hidden = YES;
    
    self.lblRegionalStats.tag = 201;
    self.lblRegionalStats.marqueeType = MLContinuous;
    self.lblRegionalStats.textAlignment = NSTextAlignmentCenter;
    self.lblRegionalStats.lineBreakMode = NSLineBreakByTruncatingHead;
    self.lblRegionalStats.scrollDuration = 60.0;
    self.lblRegionalStats.fadeLength = 5.0f;
    self.lblRegionalStats.leadingBuffer = 0.0f;
    
    salect =@"yes";
   self.activityIndicator.hidden=YES;
    
    self.TableViewListbtn.hidden =YES;

    if (_URSType.length>0)
    {
        selectedRegion = _URSType;
        [self.btnRegionDropDown setTitle:_URSType forState:UIControlStateNormal];
    }
    else
    {
        selectedRegion=@"NRLDC";
        
    }
    
    
    [self performSelector:@selector(GetRegionalGeneratoAPIHit) withObject:nil afterDelay:2.0];
    regionnamesend=[[NSString alloc]init];
    
    strFinalData=[[NSMutableAttributedString alloc]init];
    
    [self.tableViewGenerators setContentInset:UIEdgeInsetsMake(4, 0, 0, 0)];

    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(4000,0);
    

}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.TableViewListbtn setHidden:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    NSLog(@"will appear %@",self.strValue);
//}


- (IBAction)btnRegionDropDown_Click:(id)sender {
    if(self.TableViewListbtn.hidden){
        [self.TableViewListbtn setHidden:NO];
    }else{
        [self.TableViewListbtn setHidden:YES];
    }
    
  //  [self.TableViewListbtn setHidden:NO];
    
    [self.TableViewListbtn reloadData];
}

//-(void) viewDidAppear:(BOOL)animated{
//    self.isFromMainMenu = NO;
//}

-(void) GetRegionalGeneratoAPIHit

{
    self.lblRegionalStats.scrollDuration = 60.0f;
    
    if ( [salect isEqualToString:@"yes"])
    {
       // [SVProgressHUD showWithStatus:@"Please wait.."];
        [CustmObj addHud:self.view];
    }
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"REGION",@"type",selectedRegion,@"id",[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/getursdetails.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
    {
        
        if (type == kResponseTypeFail)
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
        }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            
            NSMutableDictionary *dictResponse= (NSMutableDictionary *) response;
            
            arrydetails = dictResponse[@"data"];
            regionarray = dictResponse[@"region"];
            
            
            
            NSDictionary *dictTempRegion=[response valueForKey:@"region"];
            self.arrRegions = [dictTempRegion allKeys];
            
            if([arrydetails count]>0)
            {
                [self.lblNoData setHidden:YES];
                [self.tableViewGenerators setHidden:NO];
            [self.tableViewGenerators reloadData];
            }else{
                [self.lblNoData setHidden:NO];
                [self.tableViewGenerators setHidden:YES];
            }
            
            NSDictionary *attrAvailableGreen = @{
                                                 NSFontAttributeName : [UIFont systemFontOfSize:15],
                                                 NSForegroundColorAttributeName : [UIColor greenColor]
                                                 };
            
            NSDictionary *attrRevisionBoldWhite = @{
                                                    NSFontAttributeName : [UIFont boldSystemFontOfSize:15],
                                                    NSForegroundColorAttributeName : [UIColor whiteColor]
                                                    };
            NSAttributedString *strSeparator = [[NSAttributedString alloc]initWithString:@" | "];
            
            
            for (int i=0; i < [self.arrRegions count]; i++)
            {
                
                
                
NSString *strRegionName=[self.arrRegions objectAtIndex:i];
NSString *strRegionAvailable = [NSString stringWithFormat:@" %@ ",[dictTempRegion valueForKeyPath:[NSString stringWithFormat:@"%@.available",strRegionName]]];
                
NSString *strRegionRevision = [NSString stringWithFormat:@"R %@",[dictTempRegion valueForKeyPath:[NSString stringWithFormat:@"%@.revision",strRegionName]]];
                
NSAttributedString *strRegion = [[NSAttributedString alloc]initWithString:strRegionName attributes:nil];
                
NSAttributedString *strAvailable = [[NSAttributedString alloc]initWithString: strRegionAvailable attributes:attrAvailableGreen];
                
NSAttributedString *strRevision = [[NSAttributedString alloc]initWithString: strRegionRevision attributes:attrRevisionBoldWhite];
      
                
                [strFinalData appendAttributedString:strRegion];
                
                [strFinalData appendAttributedString:strAvailable];
                
                [strFinalData appendAttributedString:strRevision];
                
                if(i == [self.arrRegions count])
                {
                }
                else
                {
                    [strFinalData appendAttributedString:strSeparator];
                }
                
            }

            
            self.lblRegionalStats.attributedText = strFinalData;
            self.lblCurrentDate.text = [NSString stringWithFormat:@"Dated : %@",dictResponse[@"date"]];
            
            
            [_activityIndicator stopAnimating];
            self.activityIndicator.hidden=YES;
            [self performSelector:@selector(tablereload) withObject:nil afterDelay:60.0];
            
           

            
        }
        else
        {
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
        }
        
        
       // [SVProgressHUD dismiss];
        [CustmObj removeHud:self.view];
        
        
        
    }];
    
    
}


#pragma mark - Table View Generators

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 31.5;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView==self.tableViewGenerators)
        
    {
        return 1;
    }
    else
    {
        return 1;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.tableViewGenerators)
        
    {
        return [arrydetails count];
        
    }
    else
    {
        return [self.arrRegions count];
    }
    
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == self.TableViewListbtn)
    {
        CellbtnURS *cell=[tableView dequeueReusableCellWithIdentifier:@"celldropdwn"];
        {
            
            cell.regionlbl.text = [_arrRegions objectAtIndex:indexPath.row];
            
        }
        return cell;
    }
    else
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        GeneratorsTableViewCell *cellb=[tableView dequeueReusableCellWithIdentifier:@"GeneratorCellView"];
        
        cellb.selectionStyle= UITableViewCellSelectionStyleNone;
        NSDictionary *dictCellData=[arrydetails objectAtIndex:indexPath.row];
        
        {
            
cellb.GenertorLbl.text =[dictCellData objectForKey:@"station_name"];
cellb.Ratelbl.text = [NSString stringWithFormat:@"%@",[dictCellData objectForKey:@"rate"]];
cellb.Amtlbl.text = [NSString stringWithFormat:@"%@",[dictCellData objectForKey:@"available"]];
cellb.VariableChargesLabel.text = [NSString stringWithFormat:@"%@",[dictCellData objectForKey:@"variable"]];
            
            
            
        }
        return cellb;
        
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(tableView == self.TableViewListbtn){
      
        
        salect =@"yes";
        selectedRegion = [self.arrRegions objectAtIndex:indexPath.row];
        [self GetRegionalGeneratoAPIHit];
        
        [self.btnRegionDropDown setTitle:selectedRegion forState:UIControlStateNormal];
        
        [self.TableViewListbtn setHidden:YES];
        
    }
    else
    {
        
        CurrentAvailableURSDetailsViewController *vcDetailedURS = [self.storyboard instantiateViewControllerWithIdentifier:@"URSDetailView"];
        
        
        NSDictionary *dictCellData=[arrydetails objectAtIndex:indexPath.row];
        
        vcDetailedURS.strSelectedGenerator=[dictCellData objectForKey:@"station_name"];
        vcDetailedURS.strGeneratorTotalQuantum=[dictCellData objectForKey:@"available"];
        
        
        [self.navigationController pushViewController:vcDetailedURS animated:YES];
        
        [self.TableViewListbtn setHidden:YES];
        
    }
    
}



-(void)tablereload
{
    salect=@"no";
    self.activityIndicator.hidden=NO;
    [_activityIndicator startAnimating];
    [self performSelector:@selector(GetRegionalGeneratoAPIHit) withObject:nil afterDelay:3.0];
}

- (IBAction)btnBack_Click:(id)sender {
    
    //[SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(IBAction)btnToggleSideMenu_Click:(id)sender{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
    
//    if (self.isFromMainMenu) {
//        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"MENU",@"REQUEST", nil];
//        [[NSNotificationCenter defaultCenter] postNotificationName: @"MyNotification" object:nil userInfo:userInfo];
//    }
    
}

- (IBAction)btnLogin_Click:(id)sender
{    
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
}




@end
