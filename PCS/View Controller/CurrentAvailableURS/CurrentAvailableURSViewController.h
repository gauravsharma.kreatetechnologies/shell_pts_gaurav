//
//  CurrentAvailableURSViewController.h
//  PCS
//
//  Created by lab4code on 25/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneratorsTableViewCell.h"
#import "CellbtnURS.h"
#import "CurrentAvailableURSDetailsViewController.h"
#import "Services.h"
#import "SVProgressHUD.h"
#import "AppHelper.h"
#import "MarqueeLabel.h"



@interface CurrentAvailableURSViewController : UIViewController

@property (weak,nonatomic) IBOutlet UITableView *tableViewGenerators;
@property (weak,nonatomic) IBOutlet UITableView *TableViewListbtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak,nonatomic) IBOutlet MarqueeLabel *lblRegionalStats;

@property (weak,nonatomic) IBOutlet UILabel *lblCurrentDate;
@property (weak,nonatomic) IBOutlet UILabel *lblNoData;
@property(strong, nonatomic) NSString *URSType;

//@property(strong,nonatomic)NSString *strValue;

@property (weak, nonatomic) IBOutlet UIButton *btnRegionDropDown;
- (IBAction)btnRegionDropDown_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
//@property (assign, nonatomic) BOOL isFromMainMenu;
- (IBAction)btnLogin_Click:(id)sender;


@end
