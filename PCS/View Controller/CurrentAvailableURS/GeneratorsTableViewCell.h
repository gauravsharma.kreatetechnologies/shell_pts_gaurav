//
//  GeneratorsTableViewCell.h
//  PCS
//
//  Created by lab4code on 25/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneratorsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *GenertorLbl;
@property (weak, nonatomic) IBOutlet UILabel *Ratelbl;
@property (weak, nonatomic) IBOutlet UILabel *Amtlbl;
@property (strong, nonatomic) IBOutlet UILabel *VariableChargesLabel;

@end
