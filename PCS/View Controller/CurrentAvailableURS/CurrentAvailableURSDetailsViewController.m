//
//  CurrentAvailableURSDetailsViewController.m
//  PCS
//
//  Created by lab4code on 25/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "CurrentAvailableURSDetailsViewController.h"
#import "URSDataTableViewCell.h"
#import "CustmObj.h"

@interface CurrentAvailableURSDetailsViewController (){
    BOOL selected;
}
@property (strong,nonatomic) NSArray *qtmtime;
@property (strong, nonatomic) NSArray *arrDetailsedURS;


@end

@implementation CurrentAvailableURSDetailsViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
     self.activityIndicator.hidden=YES;
    selected=YES;

    _ursRegionLbl.text=self.strSelectedGenerator;
    [self regionApiHit];
    
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, 30.0f);
    leftBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.lblTableHeaderBlock.layer addSublayer:leftBorder];
    
    CALayer *leftQtmBorder = [CALayer layer];
    leftQtmBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, 30.0f);
    leftQtmBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.lblTableHeaderVal.layer addSublayer:leftQtmBorder];
    
    CALayer *leftQtmTotalBorder = [CALayer layer];
    leftQtmTotalBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, 30.0f);
    leftQtmTotalBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.lblTableFooterVal.layer addSublayer:leftQtmTotalBorder];
   
    self.lblTableFooterTh.layoutMargins = UIEdgeInsetsMake(0,0,0,10);
    
    self.lblTableFooterVal.text = self.strGeneratorTotalQuantum;
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Generators

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrDetailsedURS count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    URSDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"URSDetailViewCell" forIndexPath:indexPath];
    
    NSDictionary *dictCellData=[_arrDetailsedURS objectAtIndex:indexPath.row];
    
    
    cell.lblSrNo.text =[NSString stringWithFormat:@"%ld",indexPath.row+1];
    cell.lblBlock.text = [NSString stringWithFormat:@"%@ - %@",dictCellData[@"fromtime"],dictCellData[@"totime"]];
    cell.lblQtm.text = [dictCellData objectForKey:@"available"];
    
    
    
    CALayer* layer = [cell.viewCell layer];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [UIColor lightGrayColor].CGColor;
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(0, layer.frame.size.height-1, layer.frame.size.width, 1);
   
    // [bottomBorder setBorderColor:[UIColor lightGrayColor].CGColor];
    [layer addSublayer:bottomBorder];
    
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, cell.viewCell.frame.size.height);
    leftBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [cell.lblBlock.layer addSublayer:leftBorder];
    
    CALayer *leftQtmBorder = [CALayer layer];
    leftQtmBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, cell.viewCell.frame.size.height);
    leftQtmBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [cell.lblQtm.layer addSublayer:leftQtmBorder];
    
    if([dictCellData[@"disable"] isEqualToString:@"YES" ]){
        cell.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
        //cell.backgroundColor = [UIColor redColor];
        cell.lblQtm.textColor = [UIColor redColor];
    }
    else
    {
       cell.lblQtm.textColor = [UIColor greenColor];
        cell.backgroundColor = [UIColor whiteColor];
    }


    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) regionApiHit

{
    if (selected)
    {
        //[SVProgressHUD showWithStatus:@"Please wait.."];
        [CustmObj addHud:self.view];
    }
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:_strSelectedGenerator,@"name",@"2016-07-11",@"date",[AppHelper userDefaultsForKey:@"deviceId"],@"device_id",[AppHelper userDefaultsForKey:@"access_key"],@"access_key",nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    
    
    NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/getursdata_generatorwise.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
     {
        
        if (type == kResponseTypeFail)
        {
            //[SVProgressHUD dismiss];
             [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            NSMutableDictionary *dictResponse= (NSMutableDictionary *) response;
            _arrDetailsedURS =dictResponse[@"data"];
            
            [self.tableViewURSDetailsData reloadData];
            
            [_activityIndicator stopAnimating];
            [self performSelector:@selector(tablereload) withObject:nil afterDelay:60.0];
            self.activityIndicator.hidden=YES;
        }
        //[SVProgressHUD dismiss];
         [CustmObj removeHud:self.view];
    }];
    
    
}

-(void)tablereload
{
    selected=NO;
    self.activityIndicator.hidden=NO;
    [_activityIndicator startAnimating];
    [self performSelector:@selector(regionApiHit) withObject:nil afterDelay:2.0];
    
}


- (IBAction)btnBack_Click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
