//
//  ContactUsViewController.m
//  PCS
//
//  Created by lab4code on 05/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "ContactUsViewController.h"
#import "SVProgressHUD.h"
#import "LogonViewController.h"
#import "AppHelper.h"
#import "MenuViewController.h"
#import "SlideNavigationController.h"
@class SideMenuViewController;

@interface ContactUsViewController ()
{
    NSString *Lognotification;
}
@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)btnMobile_Click:(id)sender{
     NSURL *phoneURL = [NSURL URLWithString:@"tel:9818954768"];
        [[UIApplication sharedApplication] openURL:phoneURL];
   
}

-(IBAction)btnDeskphone_Click:(id)sender{
    NSURL *url = [NSURL URLWithString:@"tel:01166666983"];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)btnTollfree_Click:(id)sender{
    NSURL *url = [NSURL URLWithString:@"tel:18008336775"];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)btnEmail_Click:(id)sender{
    NSURL *url = [NSURL URLWithString:@"mailto:apps-support@invetechsolutions.com"];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)btnWebsite_Click:(id)sender{
    NSURL *url = [NSURL URLWithString:@"https://www.invetechsolutions.com"];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}



- (IBAction)btnLogin_Click:(id)sender
{
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    
    
}


@end
