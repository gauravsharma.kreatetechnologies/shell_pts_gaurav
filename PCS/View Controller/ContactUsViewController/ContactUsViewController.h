//
//  ContactUsViewController.h
//  PCS
//
//  Created by lab4code on 05/07/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;
@property (weak, nonatomic)IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@end
