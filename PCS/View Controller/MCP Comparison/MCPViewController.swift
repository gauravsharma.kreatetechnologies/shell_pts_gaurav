//
//  MCPViewController.swift
//  MCP module
//
//  Created by pavan yadav on 13/04/17.
//  Copyright © 2017 Invetech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts


class MCPViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource ,UIPickerViewDelegate,UIPickerViewDataSource ,ChartViewDelegate
    
{
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var pickerType: UIPickerView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var lineGraphView: LineChartView!
    
    @IBOutlet var label3size: NSLayoutConstraint!
    @IBOutlet var ScrollViewHeight: NSLayoutConstraint!
    @IBOutlet var tableHight: NSLayoutConstraint!
    @IBOutlet var lineChartHeight: NSLayoutConstraint!
    @IBOutlet var buttonView: UIButton!
    @IBOutlet var buttonBidType: UIButton!
    @IBOutlet var buttonRegionType: UIButton!
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    @IBOutlet var labelDate1: UILabel!
    @IBOutlet var labelDate2: UILabel!
    @IBOutlet var labelDate3: UILabel!
    @IBOutlet var labelDate4: UILabel!
    @IBOutlet var buttonCross3: UIButton!
    @IBOutlet var buttonCross4: UIButton!
    @IBOutlet var buttonPlushSec: UIButton!
    @IBOutlet var buttonPlush3: UIButton!
    @IBOutlet var calenderImage3: UIImageView!
    @IBOutlet var calenderImage4: UIImageView!
    
    @IBOutlet var buttonMin: UIButton!
    @IBOutlet var buttonAvg: UIButton!
    @IBOutlet var buttonMax: UIButton!
    @IBOutlet var minLabel: UILabel!
    @IBOutlet var avgLabel: UILabel!
    @IBOutlet var maxLabel: UILabel!
    
    var min = ""
    var avg = ""
    var max = ""
    
    
    var dateFormatter1 = DateFormatter()
    var dateFormatter2 = DateFormatter()
    var buttonBorderColor =  UIColor.init(red: 30/255.0, green: 142/255.0, blue: 135/255.0, alpha: 1).cgColor
    
    var stateId = ""
    var Locationtype = [JSON]()
    var Lognotification = ""
    var dataCount = CGFloat()
    var strings = ["a", "b", "c"]
    var BidType = ["IEX","PXIL"]
    var pickerDateSelection = ""
    var minvalue = [String]()
    var avgvalue = [String]()
    var maxvalue = [String]()
    var date = [String]()
    var dateData = [String]()
    var responseTable = [JSON]()
    var date1 = ""
    var date2 = ""
    var date3 = ""
    var date4 = ""
    
    var bidTypePairameter = ""
    var pickerData = ""
    var stateName = [String]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        min = "minselect"
        avg = "avgselect"
        max = "maxselect"
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle .none
        button1.layer.borderColor = buttonBorderColor
        button2.layer.borderColor = buttonBorderColor
        button3.layer.borderColor = buttonBorderColor
        button4.layer.borderColor = buttonBorderColor
        
        datePicker.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        datePicker.isHidden = true
        pickerType.isHidden = true
        buttonPlushSec.isHidden = true
        buttonPlush3.isHidden = true
        buttonCross3.isHidden = true
        dateFormatter1.dateFormat = "dd-MM-yyyy"
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        bidTypePairameter = "IEX"
        label3size.constant = 45
        buttonRegionType.setTitle( AppHelper.userDefaults(forKey: "StateName"), for: .normal)
        stateId = AppHelper.userDefaults(forKey: "StateId")
        
        if (AppHelper.userDefaults(forKey: "client_id") != nil)
        {
            Lognotification = "home"
            btnLogin.setTitle("Home", for: .normal)
        }
        else
        {
            btnLogin.setTitle("Login", for: .normal)
            Lognotification = "no"
        }
        
        self.FirstApiCalling()
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
   
    //MARK:- Calling Initial api and getting response..
    
    func FirstApiCalling()
    {
       // SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        
        let someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : stateId ,"type" : bidTypePairameter]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: DashboardLocationSec, parameters:parameters){(APIData) -> Void in
                
                if  APIData["type"].stringValue == "ERROR"
                {
                   // SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    let alert = UIAlertController(title: "Network Not found", message: "Please check network connection", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                    
                else
                {
                    if APIData.count>0
                    {
                        self.responseTable = APIData .arrayValue
                        
                        
                        
                        self.labelDate1.text = self.dateFormatter1.string(from:self.dateFormatter2.date(from:APIData[0]["showdate"] .stringValue)!)
                        
                        self.labelDate2.text = self.dateFormatter1.string(from:self.dateFormatter2.date(from:APIData[1]["showdate"] .stringValue)!)
                        
                        self.labelDate3.text = self.dateFormatter1.string(from:self.dateFormatter2.date(from:APIData[2]["showdate"] .stringValue)!)
                        
                        self.labelDate4.text = self.dateFormatter1.string(from:self.dateFormatter2.date(from:APIData[3]["showdate"] .stringValue)!)
                        
                        
                        self.minvalue.removeAll()
                        self.avgvalue.removeAll()
                        self.maxvalue.removeAll()
                        self.date.removeAll()
                        
                        for i in 0..<self.responseTable.count
                        {
                            self.minvalue.append(self.responseTable [i]["min"] .stringValue)
                            self.avgvalue.append(self.responseTable[i]["avg"] .stringValue)
                            self.maxvalue.append(self.responseTable[i]["max"] .stringValue)
                            self.date.append(self.responseTable[i]["date"] .stringValue)
                            
                        }
                        
                        self.ScrollViewHeight.constant = CGFloat(18*(self.responseTable[0]["data"].count+23))
                        self.tableHight.constant = CGFloat(18*(self.responseTable[0]["data"].count+23))
                        self.lineChart()
                        self.tableView.reloadData()
                        //SVProgressHUD.dismiss()
                        CustomHUD.shredObject.removeXib(view: self.view)
                        
                    }
                        
                    else
                    {
                        //SVProgressHUD.dismiss()
                        CustomHUD.shredObject.removeXib(view: self.view)
                        let alert = UIAlertController(title: "Data Not found", message: "Please select another state", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }}}
        catch
        {
        }}
    
    
    
    
    
    //MARK:- 2nd  api Calling..
    func SecondApicalling()
    {
        //SVProgressHUD.show(withStatus: "Please wait..")
        CustomHUD.shredObject.loadXib(view: self.view)
        date1.removeAll()
        date2.removeAll()
        date3.removeAll()
        date4.removeAll()
        
        let D1 = dateFormatter1.date(from: labelDate1.text!)
        date1 = dateFormatter2.string(from: D1!)
        
        
        let D2 = dateFormatter1.date(from: labelDate2.text!)
        date2 = dateFormatter2.string(from: D2!)
        
        
        if button3.isHidden == true
        {
            date3 = ""
            date4 = ""
        }
        else
        {
            if labelDate3.text == "Add Date"
            {
            }
            else
            {
                let D3 = dateFormatter1.date(from: labelDate3.text!)
                date3 = dateFormatter2.string(from: D3!)
            }
        }
        
        if button4.isHidden == true
        {
            date4 = ""
        }
        else
        {
            if labelDate4.text == "Add Date"
            {
            }
            else
            {
                let D4 = dateFormatter1.date(from: labelDate4.text!)
                date4 = dateFormatter2.string(from: D4!)
            }
            
        }
        
        
        
        var someDict = [String: Any]()
        
        if button1.isHidden == false && button2.isHidden == false && button3.isHidden == false && button4.isHidden == false
        {
            
            if labelDate4.text == "Add Date"
            {
                if labelDate3.text == "Add Date"
                {
                    someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : stateId ,"type" : bidTypePairameter,"date" :[ date1, date2]]
                }
                else
                {
                    someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : stateId ,"type" : bidTypePairameter,"date" :[ date1, date2, date3]]
                }
            }
            else
            {
                someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : stateId ,"type" : bidTypePairameter,"date" :[ date1, date2,date3,date4]]
            }
            
            
        }
            
        else if button1.isHidden == false && button2.isHidden == false && button3.isHidden == false && button4.isHidden == true
        {
            if labelDate3.text == "Add Date"
            {
                someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : stateId ,"type" : bidTypePairameter,"date" :[ date1, date2]]
            }
            else
            {
                someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : stateId ,"type" : bidTypePairameter,"date" :[ date1, date2, date3]]
            }
        }
            
        else if button1.isHidden == false && button2.isHidden == false && button3.isHidden == true && button4.isHidden == true
        {
            
            
            someDict =  ["device_id" : AppHelper.userDefaults(forKey: "deviceId"),"state_id" : stateId ,"type" : bidTypePairameter,"date" :[ date1, date2]]
        }
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: DashboardLocationSec, parameters:parameters){(APIData) -> Void in
                
                if  APIData["type"].stringValue == "ERROR"
                {
                    //SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    let alert = UIAlertController(title: "Network Not found", message: "Please check network connection", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                    
                else
                {
                    
                    if APIData.count>0
                    {
                        
                        self.responseTable = APIData .arrayValue
                        
                        
                        self.minvalue.removeAll()
                        self.avgvalue.removeAll()
                        self.maxvalue.removeAll()
                        self.date.removeAll()
                        
                        for i in 0..<self.responseTable.count
                        {
                            self.minvalue.append(self.responseTable [i]["min"] .stringValue)
                            self.avgvalue.append(self.responseTable[i]["avg"] .stringValue)
                            self.maxvalue.append(self.responseTable[i]["max"] .stringValue)
                            self.date.append(self.responseTable[i]["date"] .stringValue)
                            
                        }
                        
                        self.ScrollViewHeight.constant = CGFloat(18*(self.responseTable[0]["data"].count+23))
                        self.tableHight.constant = CGFloat(18*(self.responseTable[0]["data"].count+23))
                        
                        self.lineChart()
                        self.tableView.reloadData()
                       // SVProgressHUD.dismiss()
                        CustomHUD.shredObject.removeXib(view: self.view)
                    }
                        
                    else
                    {
                       // SVProgressHUD.dismiss()
                        CustomHUD.shredObject.removeXib(view: self.view)
                        let alert = UIAlertController(title: "Data Not found", message: "Please select other state and date", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }}}
        catch
        {
        }
    }
    
    
    
    
    
    
    
    
    //MARK:- All Button Methode--------------------
    
    @IBAction func buttonFirst(_ sender: UIButton)
    {
        datePicker.isHidden = false
        pickerDateSelection = "1"
    }
    
    
    @IBAction func buttonSec(_ sender: UIButton)
    {
        datePicker.isHidden = false
        pickerDateSelection = "2"
    }
    
    @IBAction func buttonThird(_ sender: UIButton)
    {
        datePicker.isHidden = false
        pickerDateSelection = "3"
    }
    
    @IBAction func buttonFour(_ sender: UIButton)
    {
        
        datePicker.isHidden = false
        pickerDateSelection = "4"
    }
    
    
    
    
    @IBAction func UIButtonPlushSec(_ sender: UIButton)
    {
        button3.isHidden = false
        labelDate3.isHidden = false
        buttonCross3.isHidden = false
        calenderImage3.isHidden = false
        buttonPlush3.isHidden = false
        buttonPlushSec.isHidden = true
        datePicker.isHidden = true
        label3size.constant = 31
        labelDate3.text = "Add Date"
    }
    
    @IBAction func buttonPlushThird(_ sender: UIButton)
    {
        button4.isHidden = false
        labelDate4.isHidden = false
        buttonCross4.isHidden = false
        calenderImage4.isHidden = false
        buttonPlush3.isHidden = true
        buttonCross3.isHidden = true
        label3size.constant = 45
        datePicker.isHidden = true
        
        labelDate4.text = "Add Date"
    }
    
    
    
    
    @IBAction func buttonThirdCross(_ sender: UIButton)
    {
        button3.isHidden = true
        labelDate3.isHidden = true
        buttonCross3.isHidden = true
        calenderImage3.isHidden = true
        buttonPlush3.isHidden = true
        buttonPlushSec.isHidden = false
        label3size.constant = 45
        datePicker.isHidden = true
        labelDate3.text = ""
        
    }
    
    
    @IBAction func buttonFourCross(_ sender: UIButton)
    {
        button4.isHidden = true
        labelDate4.isHidden = true
        buttonCross4.isHidden = true
        calenderImage4.isHidden = true
        
        buttonPlush3.isHidden = false
        buttonCross3.isHidden = false
        buttonCross3.isHidden = false
        datePicker.isHidden = true
        label3size.constant = 31
        labelDate4.text = ""
        
    }
    
    
    
    
    @IBAction func buttonBidType(_ sender: UIButton)
    {
        pickerType.isHidden = false
        pickerData = "Bid"
        pickerType.reloadAllComponents()
    }
    
    @IBAction func buttonregionType(_ sender: UIButton)
    {
        self.apicallLocation()
        pickerType.isHidden = false
        pickerData = "Region"
        pickerType.reloadAllComponents()
    }
    
    
    
    func apicallLocation()
    {
        let someDict =  ["latitude" : "-1","logtitude" : "-1"]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: DashboardLocation, parameters:parameters){(APIData) -> Void in
                
                
                if APIData["type"] == "ERROR"
                {
                    SVProgressHUD.showError(withStatus: APIData["message"] .stringValue)
                }
                else
                {
                    self.Locationtype = APIData ["states"].arrayValue
                    
                    self.stateName.removeAll()
                    for i in 0..<APIData ["states"].count
                    {
                        self.stateName.append("\(self.Locationtype[i]["state_name"] .stringValue)\("(")\(self.self.Locationtype[i]["iexregion"] .stringValue)\(")")")
                    }
                    
                    
                    
                    
                    self.pickerType.reloadAllComponents()
                }
                
            }
        }
            
        catch
        {
            
        }
        //SVProgressHUD.dismiss()
        CustomHUD.shredObject.removeXib(view: self.view)
    }
    
    
    
    
    
    
    @IBAction func buttonView(_ sender: UIButton)
    {
        
        pickerType.isHidden = true
        datePicker.isHidden = true
        self.SecondApicalling()
    }
    
    
    @IBAction func switchButton(_ sender: UISwitch)
    {
        if  sender.isOn
        {
            lineChartHeight.constant = 44
            
            buttonMin.isHidden = true
            buttonAvg.isHidden = true
            buttonMax.isHidden = true
            minLabel.isHidden = true
            avgLabel.isHidden = true
            maxLabel.isHidden = true
            
        }
        else
        {
            lineChartHeight.constant = 215
            buttonMin.isHidden = false
            buttonAvg.isHidden = false
            buttonMax.isHidden = false
            minLabel.isHidden = false
            avgLabel.isHidden = false
            maxLabel.isHidden = false
        }
        pickerType.isHidden = true
        datePicker.isHidden = true
    }
    
    
    
    
    
    
    //MARK:-DatePICKER VIEW METHODE CALLING--------------------------------------------------------------
    
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker)
    {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "dd-MM-yyyy"
        
        if pickerDateSelection == "1"
        {
            labelDate1.text =  formatter.string(from: sender.date)
        }
            
        else if pickerDateSelection == "2"
        {
            labelDate2.text =  formatter.string(from: sender.date)
        }
            
        else if pickerDateSelection == "3"
        {
            labelDate3.text =  formatter.string(from: sender.date)
        }
            
        else if pickerDateSelection == "4"
        {
            labelDate4.text =  formatter.string(from: sender.date)
        }
        datePicker.isHidden = true
    }
    
    
    
    
    // PICKER VIEW METHODE CALLING-------------------------------------------------------------------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        
        if pickerData == "Region"
        {
            return stateName[row]
        }
        else
        {
            return BidType[row]
        }
        
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        if pickerData == "Region"
        {
            return stateName.count
        }
        else
        {
            return BidType.count
        }
        
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerData == "Region"
        {
            
            buttonRegionType.setTitle( stateName[row], for: .normal)
            stateId =  Locationtype[row]["id"] .stringValue
        }
        else
        {
            buttonBidType.setTitle(BidType[row], for: UIControlState.normal)
            bidTypePairameter = BidType[row]
            
        }
        
        pickerType.isHidden = true
    }
    
    
    
    
    
    // TableView Methode----------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        if responseTable.count>0
        {
            return self.responseTable[0]["data"].count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 20
        }
        return 18
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellSize = tableView.contentSize.width
        
        
        if indexPath.section==0
        {
            let Cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            
            Cell.selectionStyle = UITableViewCellSelectionStyle .none
            
            
            let labelBlock = UILabel(frame: CGRect(x:0, y:0, width:cellSize/7, height: 20))
            labelBlock.textAlignment = .center
            labelBlock.text = "Blocks"
            labelBlock.font = labelBlock.font.withSize(8)
            labelBlock.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
            Cell.addSubview(labelBlock)
            
            let line1 = UILabel(frame: CGRect(x:cellSize/7, y:0, width: 1, height: 20))
            line1.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.addSubview(line1)
            
            
            if responseTable.count == 1
            {
                self.dataCount = 1.2
                
                
                let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
                labelDate1.textAlignment = .center
                labelDate1.text = self.responseTable[0]["date"] .stringValue
                labelDate1.font = labelBlock.font.withSize(8)
                labelDate1.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate1)
                
            }
                
            else if responseTable.count == 2
            {
                self.dataCount = 2.2
                
                
                let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
                labelDate1.textAlignment = .center
                labelDate1.text = self.responseTable[0]["date"] .stringValue
                labelDate1.font = labelBlock.font.withSize(8)
                labelDate1.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate1)
                
                let line2 = UILabel(frame: CGRect(x:cellSize/7+1+cellSize/dataCount, y:0, width: 1, height: 20))
                line2.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                Cell.addSubview(line2)
                
                
                
                let labelDate2 = UILabel(frame: CGRect(x:cellSize/7+2+cellSize/dataCount, y:0, width: cellSize/dataCount, height: 20))
                labelDate2.textAlignment = .center
                labelDate2.text = self.responseTable[1]["date"] .stringValue
                labelDate2.font = labelBlock.font.withSize(8)
                labelDate2.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate2)
            }
                
                
            else if responseTable.count == 3
            {
                self.dataCount = 3.4
                
                let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
                labelDate1.textAlignment = .center
                labelDate1.text = self.responseTable[0]["date"] .stringValue
                labelDate1.font = labelBlock.font.withSize(8)
                labelDate1.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate1)
                
                let line2 = UILabel(frame: CGRect(x:cellSize/7+1+cellSize/dataCount, y:0, width: 1, height: 20))
                line2.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                Cell.addSubview(line2)
                
                
                
                let labelDate2 = UILabel(frame: CGRect(x:cellSize/7+2+cellSize/dataCount, y:0, width: cellSize/dataCount, height: 20))
                labelDate2.textAlignment = .center
                labelDate2.text = self.responseTable[1]["date"] .stringValue
                labelDate2.font = labelBlock.font.withSize(8)
                labelDate2.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate2)
                
                let line3 = UILabel(frame: CGRect(x:cellSize/7+2+(cellSize/dataCount*2), y:0, width: 1, height: 20))
                line3.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                Cell.addSubview(line3)
                
                
                
                let labelDate3 = UILabel(frame: CGRect(x:cellSize/7+3+(cellSize/dataCount*2), y:0, width: cellSize/dataCount, height: 20))
                labelDate3.textAlignment = .center
                labelDate3.text = self.responseTable[2]["date"] .stringValue
                labelDate3.font = labelBlock.font.withSize(8)
                labelDate3.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate3)
                
            }
                
                
            else if responseTable.count == 4
            {
                self.dataCount = 4.6
                
                let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
                labelDate1.textAlignment = .center
                labelDate1.text = self.responseTable[0]["date"] .stringValue
                labelDate1.font = labelBlock.font.withSize(8)
                labelDate1.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate1)
                
                let line2 = UILabel(frame: CGRect(x:cellSize/7+1+cellSize/dataCount, y:0, width: 1, height: 20))
                line2.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                Cell.addSubview(line2)
                
                
                
                let labelDate2 = UILabel(frame: CGRect(x:cellSize/7+2+cellSize/dataCount, y:0, width: cellSize/dataCount, height: 20))
                labelDate2.textAlignment = .center
                labelDate2.text = self.responseTable[1]["date"] .stringValue
                labelDate2.font = labelBlock.font.withSize(8)
                labelDate2.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate2)
                
                let line3 = UILabel(frame: CGRect(x:cellSize/7+2+(cellSize/dataCount*2), y:0, width: 1, height: 20))
                line3.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                Cell.addSubview(line3)
                
                
                
                let labelDate3 = UILabel(frame: CGRect(x:cellSize/7+3+(cellSize/dataCount*2), y:0, width: cellSize/dataCount, height: 20))
                labelDate3.textAlignment = .center
                labelDate3.text = self.responseTable[2]["date"] .stringValue
                labelDate3.font = labelBlock.font.withSize(8)
                labelDate3.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate3)
                
                let line4 = UILabel(frame: CGRect(x:cellSize/7+3+(cellSize/dataCount*3), y:0, width: 1, height: 20))
                line4.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                Cell.addSubview(line4)
                
                
                
                let labelDate4 = UILabel(frame: CGRect(x:cellSize/7+4+(cellSize/dataCount*3), y:0, width: cellSize/5, height: 20))
                labelDate4.textAlignment = .center
                labelDate4.text = self.responseTable[3]["date"] .stringValue
                labelDate4.font = labelBlock.font.withSize(8)
                labelDate4.backgroundColor =  #colorLiteral(red: 1, green: 0.8835386236, blue: 0.8899420037, alpha: 1)
                Cell.addSubview(labelDate4)
                
                
            }
            
            let line = UILabel(frame: CGRect(x:0, y:19.5, width: cellSize, height: 0.5))
            line.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
            Cell.addSubview(line)
            
            return Cell
        }
        
        
        let Cell = UITableViewCell()
        
        Cell.selectionStyle = .none
        
        let labelBlock = UILabel(frame: CGRect(x:0, y:0, width: cellSize/7, height: 20))
        
        labelBlock.textAlignment = .center
        labelBlock.text = "\(indexPath.row + 1)"
        labelBlock.font = labelBlock.font.withSize(8)
        Cell.addSubview(labelBlock)
        
        
        let line1 = UILabel(frame: CGRect(x:cellSize/7, y:0, width: 1, height: 20))
        line1.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        Cell.addSubview(line1)
        
        
        if responseTable.count == 1
        {
            
            let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
            labelDate1.textAlignment = .center
            labelDate1.text = self.responseTable[0]["data"][indexPath.row] .stringValue
            labelDate1.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate1)
            
        }
            
        else if responseTable.count == 2
        {
            let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
            labelDate1.textAlignment = .center
            labelDate1.text = self.responseTable[0]["data"][indexPath.row] .stringValue
            labelDate1.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate1)
            
            let line2 = UILabel(frame: CGRect(x:cellSize/7+1+cellSize/dataCount, y:0, width: 1, height: 20))
            line2.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.addSubview(line2)
            
            
            let labelDate2 = UILabel(frame: CGRect(x:cellSize/7+2+cellSize/dataCount, y:0, width: cellSize/dataCount, height: 20))
            labelDate2.textAlignment = .center
            labelDate2.text = self.responseTable[1]["data"][indexPath.row] .stringValue
            labelDate2.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate2)
        }
            
        else if responseTable.count == 3
        {
            
            let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
            labelDate1.textAlignment = .center
            labelDate1.text = self.responseTable[0]["data"][indexPath.row] .stringValue
            labelDate1.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate1)
            
            let line2 = UILabel(frame: CGRect(x:cellSize/7+1+cellSize/dataCount, y:0, width: 1, height: 20))
            line2.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.addSubview(line2)
            
            
            let labelDate2 = UILabel(frame: CGRect(x:cellSize/7+2+cellSize/dataCount, y:0, width: cellSize/dataCount, height: 20))
            labelDate2.textAlignment = .center
            labelDate2.text = self.responseTable[1]["data"][indexPath.row] .stringValue
            labelDate2.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate2)
            
            let line3 = UILabel(frame: CGRect(x:cellSize/7+2+(cellSize/dataCount*2), y:0, width: 1, height: 20))
            line3.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.addSubview(line3)
            
            
            
            let labelDate3 = UILabel(frame: CGRect(x:cellSize/7+3+(cellSize/dataCount*2), y:0, width: cellSize/dataCount, height: 20))
            labelDate3.textAlignment = .center
            labelDate3.text = self.responseTable[2]["data"][indexPath.row] .stringValue
            labelDate3.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate3)
            
        }
        else if responseTable.count == 4
        {
            
            let labelDate1 = UILabel(frame: CGRect(x:cellSize/7+1, y:0, width: cellSize/dataCount, height: 20))
            labelDate1.textAlignment = .center
            labelDate1.text = self.responseTable[0]["data"][indexPath.row] .stringValue
            labelDate1.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate1)
            
            let line2 = UILabel(frame: CGRect(x:cellSize/7+1+cellSize/dataCount, y:0, width: 1, height: 20))
            line2.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.addSubview(line2)
            
            
            
            let labelDate2 = UILabel(frame: CGRect(x:cellSize/7+2+cellSize/dataCount, y:0, width: cellSize/dataCount, height: 20))
            labelDate2.textAlignment = .center
            labelDate2.text = self.responseTable[1]["data"][indexPath.row] .stringValue
            labelDate2.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate2)
            
            let line3 = UILabel(frame: CGRect(x:cellSize/7+2+(cellSize/dataCount*2), y:0, width: 1, height: 20))
            line3.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.addSubview(line3)
            
            
            
            let labelDate3 = UILabel(frame: CGRect(x:cellSize/7+3+(cellSize/dataCount*2), y:0, width: cellSize/dataCount, height: 20))
            labelDate3.textAlignment = .center
            labelDate3.text = self.responseTable[2]["data"][indexPath.row] .stringValue
            labelDate3.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate3)
            
            let line4 = UILabel(frame: CGRect(x:cellSize/7+3+(cellSize/dataCount*3), y:0, width: 1, height: 20))
            line4.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            Cell.addSubview(line4)
            
            
            
            let labelDate4 = UILabel(frame: CGRect(x:cellSize/7+4+(cellSize/dataCount*3), y:0, width: cellSize/5, height: 20))
            labelDate4.textAlignment = .center
            labelDate4.text = self.responseTable[3]["data"][indexPath.row] .stringValue
            labelDate4.font = labelBlock.font.withSize(8)
            Cell.addSubview(labelDate4)
            
        }
        
        
        let line = UILabel(frame: CGRect(x:0, y:17.5, width: cellSize, height: 0.5))
        line.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        Cell.addSubview(line)
        
        Cell.selectionStyle = UITableViewCellSelectionStyle .none
        return Cell
    }
    
    
    
    // LineChart in cell methode MCP  ---------------------------------------
    
    
    func lineChart()
    {
        
        lineGraphView.chartDescription.text="Values in Rupees";
        lineGraphView.chartDescription.font = UIFont .systemFont(ofSize: 8.0)
        lineGraphView.chartDescription.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        lineGraphView.drawGridBackgroundEnabled = true
        lineGraphView.gridBackgroundColor = #colorLiteral(red: 0.9735955968, green: 0.9735955968, blue: 0.9735955968, alpha: 1)
        
        
        lineGraphView.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lineGraphView.borderLineWidth = 0.5
        lineGraphView.drawBordersEnabled = true
        lineGraphView.dragEnabled = true
        lineGraphView.setScaleEnabled(true)
        lineGraphView.pinchZoomEnabled = false
        lineGraphView.legend.enabled = false
        lineGraphView.legend.textHeightMax = 1.0
        
        // chart right Detail
        lineGraphView.rightAxis.enabled = true
        lineGraphView.rightAxis.drawAxisLineEnabled = true
        lineGraphView.rightAxis.drawGridLinesEnabled = true
        lineGraphView.rightAxis.labelTextColor = UIColor .black
        lineGraphView.rightAxis.axisMinimum = 0.0
        lineGraphView.rightAxis.axisMaximum = 10.0
        
        
        
        // chart leftAxis Detail
        lineGraphView.leftAxis.enabled = true
        lineGraphView.leftAxis.drawAxisLineEnabled = true
        lineGraphView.leftAxis.drawGridLinesEnabled = true
        lineGraphView.leftAxis.labelTextColor = UIColor .black
        lineGraphView.leftAxis.axisMinimum = 0.0
        lineGraphView.leftAxis.axisMaximum = 10.0
       
        
       
        
        
        setChart(lineGraphView,dataPoints:date, values: date)
    }
    
    
    
    
    @IBAction func buttonMin(_ sender: UIButton)
    {
        if min == "minselectno"
        {
            min = "minselect"
            minLabel.backgroundColor = #colorLiteral(red: 0.7766539089, green: 0.1082429728, blue: 1, alpha: 1)
            buttonMin.setTitleColor(#colorLiteral(red: 0.7766539089, green: 0.1082429728, blue: 1, alpha: 1), for: .normal)
        }
        else
        {
            min = "minselectno"
            minLabel.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            buttonMin.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: .normal)
        }
        setChart(lineGraphView,dataPoints:date, values: date)
        
    }
    
    
    @IBAction func buttonAvg(_ sender: UIButton)
    {
        if avg == "avgselectno"
        {
            avg = "avgselect"
            avgLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            buttonAvg.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        }
        else
        {
            avg = "avgselectno"
            avgLabel.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            buttonAvg.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: .normal)
        }
        
        setChart(lineGraphView,dataPoints:date, values: date)
    }
    
    
    @IBAction func buttonMax(_ sender: UIButton)
    {
        if max == "maxselectno"
        {
            max = "maxselect"
            maxLabel.backgroundColor = #colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1)
            buttonMax.setTitleColor(#colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1), for: .normal)
        }
        else
        {
            max = "maxselectno"
            maxLabel.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            buttonMax.setTitleColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: .normal)
            
        }
        setChart(lineGraphView,dataPoints:date, values: date)
    }
    
    
    
    
    
    
    fileprivate func setChart(_ lineChartView: LineChartView, dataPoints: [String], values: [String])
    {
        
        var dataEntries: [ChartDataEntry] = []
        var dataEntrires1: [ChartDataEntry] = []
        var dataEntrires2: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = ChartDataEntry(x: Double(i+1), y: Double(minvalue[i])!)
            let dataEntry1 = ChartDataEntry(x: Double(i+1), y: Double(avgvalue[i])!)
            let dataEntry2 = ChartDataEntry(x: Double(i+1), y: Double(maxvalue[i])!)
            
            
            dataEntries.append(dataEntry)
            dataEntrires1.append(dataEntry1)
            dataEntrires2.append(dataEntry2)
        }
        
        
        lineChartView.animate(yAxisDuration: 2.5)
        
        
        
        let xaxis = lineGraphView.xAxis
        xaxis.drawLabelsEnabled = true
        xaxis.enabled = true
        xaxis.labelFont = UIFont .systemFont(ofSize: 7.0)
        xaxis.granularity = 1
        xaxis.labelPosition = .bottom
        xaxis.drawLimitLinesBehindDataEnabled = true
        xaxis.avoidFirstLastClippingEnabled = true
        xaxis.centerAxisLabelsEnabled = true
        
        xaxis.valueFormatter = IndexAxisValueFormatter(values: date)
        
        
        
        //arrMin Line Data Setting------------------------
        
        let lineChartDataSet = LineChartDataSet(entries: dataEntries, label: "Min")
        
        
        lineChartDataSet.lineWidth = 1.0
        lineChartDataSet.circleHoleRadius = 1.0
        lineChartDataSet.circleRadius = 2.0
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.colors = [#colorLiteral(red: 0.7766539089, green: 0.1082429728, blue: 1, alpha: 1)]
        lineChartDataSet.valueColors = [#colorLiteral(red: 0.7766539089, green: 0.1082429728, blue: 1, alpha: 1)]
        lineChartDataSet.circleColors = [#colorLiteral(red: 0.7766539089, green: 0.1082429728, blue: 1, alpha: 1)]
        lineChartDataSet.drawValuesEnabled = true
        
        let lineChartDataSet1 = LineChartDataSet(entries: dataEntrires1, label: "Avg")
        lineChartDataSet1.lineWidth = 1.0
        lineChartDataSet1.circleHoleRadius = 1.0
        lineChartDataSet1.circleRadius = 2.0
        lineChartDataSet1.mode = .cubicBezier
        lineChartDataSet1.colors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        lineChartDataSet1.valueColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        lineChartDataSet1.circleColors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        lineChartDataSet1.drawValuesEnabled = true
        
        
        
        let lineChartDataSet2 = LineChartDataSet(entries: dataEntrires2, label: "Max")
        lineChartDataSet2.lineWidth = 1.0
        lineChartDataSet2.circleHoleRadius = 1.0
        lineChartDataSet2.circleRadius = 2.0
        lineChartDataSet2.mode = .cubicBezier
        lineChartDataSet2.colors = [#colorLiteral(red: 0.06536025693, green: 0.9864253844, blue: 0.9671963582, alpha: 1)]
        lineChartDataSet2.valueColors = [#colorLiteral(red: 0.1697122677, green: 1, blue: 0.9690900738, alpha: 1)]
        lineChartDataSet2.drawValuesEnabled = true
        
        var dataSets = [ChartDataSet]()
        
        
        
        if   min == "minselect"
        {
            dataSets.append(lineChartDataSet)
        }
        
        if   avg == "avgselect"
        {
            dataSets.append(lineChartDataSet1)
        }
        
        
        if   max == "maxselect"
        {
            dataSets.append(lineChartDataSet2)
        }
        
        
        
        if !(min == "minselect") && !(avg == "avgselect") && !(max == "maxselect")
        {
            dataSets.append(lineChartDataSet)
            dataSets.append(lineChartDataSet1)
            dataSets.append(lineChartDataSet2)
            
            min = "minselect"
            minLabel.backgroundColor = #colorLiteral(red: 0.7766539089, green: 0.1082429728, blue: 1, alpha: 1)
            buttonMin.setTitleColor(#colorLiteral(red: 0.7766539089, green: 0.1082429728, blue: 1, alpha: 1), for: .normal)
            
            avg = "avgselect"
            avgLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            buttonAvg.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            max = "maxselect"
            maxLabel.backgroundColor = #colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1)
            buttonMax.setTitleColor(#colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1), for: .normal)
        }
        
        
        let lineChartData = LineChartData(dataSets: dataSets)
        lineChartView.data = lineChartData
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        pickerType.isHidden = true
        datePicker.isHidden = true
    }
    
    
    //pragma mark - SlideNavigationController Methods -
    @IBAction func btnToggleSideMenu_Click(_ sender: UIButton)
    {
        SlideNavigationController .sharedInstance().toggleLeftMenu()
    }
    
    
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    
    @IBAction func btnLoginWindow_Click(_ sender: UIButton)
    {
        if (Lognotification == "home")
        {
            let pushView: MenuViewController? = storyboard?.instantiateViewController(withIdentifier: "menuItem") as! MenuViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        else
        {
            let pushView: LogonViewController? = storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
            navigationController?.pushViewController(pushView!, animated: true)
        }
        
    }
    
    
    
    
    
    
}
