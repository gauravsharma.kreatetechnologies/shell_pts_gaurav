//
//  WebbrowserControllerViewController.h
//  PCS
//
//  Created by lab4code on 01/02/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface WebbrowserControllerViewController : UIViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate>
@property(strong,nonatomic)NSString *strWebUrl;
@end
