//
//  WebbrowserControllerViewController.m
//  PCS
//
//  Created by lab4code on 01/02/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "WebbrowserControllerViewController.h"
#import "AppDelegate.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "CustmObj.h"
#import "AppHelper.h"


@interface WebbrowserControllerViewController ()
{
    
}
@property(weak,nonatomic)IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@end

@implementation WebbrowserControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

   // [SVProgressHUD showWithStatus:@"Page is loading..."];
    [CustmObj addHud:self.view];
    
    NSURL *myURL;
    NSString*getNewsLetterBaseUrl = [AppHelper userDefaultsForKey:@"newsletterBaseUrl"];
    
//    if ([self.strWebUrl.lowercaseString hasPrefix:@"http://"] || [self.strWebUrl.lowercaseString hasPrefix:@"https://"])
//    {
//
//        myURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.strWebUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
//
//
//    } else
//    {
//
//        myURL=[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[self.strWebUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
//    }
    
    NSString*combineUrl = [NSString stringWithFormat:@"%@/%@",getNewsLetterBaseUrl,self.strWebUrl];
    myURL = [NSURL URLWithString:combineUrl];

    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:myURL];
    [_webView loadRequest:urlRequest];
    _webView.scalesPageToFit=YES;
    
    if([AppHelper userDefaultsForKey:@"client_id"]){
        [self.btnLogin setHidden:NO];
    }
    else
    {
        [self.btnLogin setHidden:YES];
    }
}


- (BOOL)validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

-(IBAction)menuButtonClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
  // [SVProgressHUD dismiss];
    [CustmObj removeHud:self.view];
   // NSLog(@"finish");
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
   
   // NSLog(@"Error for WEBVIEW: %@", [error description]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)saveFile:(id)sender {
    // Get the URL of the loaded ressource
    NSURL *theRessourcesURL = [[_webView request] URL];
   
    
        // Get the filename of the loaded ressource form the UIWebView's request URL
        NSString *filename = [theRessourcesURL lastPathComponent];
      //  NSLog(@"Filename: %@", filename);
    
        // Get the path to the App's Documents directory
       // NSString *docPath = [self documentsDirectoryPath];
        // Combine the filename and the path to the documents dir into the full path
      //  NSString *pathToDownloadTo = [NSString stringWithFormat:@"%@/%@", docPath, filename];
        
        
        // Load the file from the remote server
        NSData *tmp = [NSData dataWithContentsOfURL:theRessourcesURL];
        // Save the loaded data if loaded successfully
        if (tmp != nil) {
          
            NSError *error = nil;
            // Write the contents of our tmp object into a file
            //[tmp writeToFile:pathToDownloadTo options:NSDataWritingAtomic error:&error];
            
            if (error != nil) {
                NSLog(@"Failed to save the file: %@", [error description]);
            }
            else
            {
               // NSLog(@"%@",[NSString stringWithFormat:@"The file %@ has been saved.", filename]);
                
                
                if ([MFMailComposeViewController canSendMail])
                {
                    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                    
                    mailer.mailComposeDelegate = self;
                    
                    //[mailer setSubject:@"A Message from MobileTuts+"];
                    
//                    NSArray *toRecipients = [NSArray arrayWithObjects:@"fisrtMail@example.com", nil];
//                    [mailer setToRecipients:toRecipients];
                    
                    [mailer addAttachmentData:tmp mimeType:@"application/pdf" fileName:filename];
                    
                    //NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
                    //[mailer setMessageBody:emailBody isHTML:NO];
                    
                    [self presentViewController:mailer animated:YES completion:nil];
                    
                    
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                    message:@"Your device doesn't support the composer sheet"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles: nil];
                    [alert show];
                    
                }
            }
            
        }
        else
        {
            // File could notbe loaded -> handle errors
            
           // NSLog(@"file is not  loaded");
        }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (NSString *)documentsDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    return documentsDirectoryPath;
}

-(IBAction)btnLogout_Click:(id)sender
{
    [AppDelegate logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
