//
//  CurtailmentViewController.swift
//  PCS
//
//  Created by pavan yadav on 03/10/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CurtailmentViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var labelBlockno: UILabel!
    @IBOutlet weak var labelOriginal: UILabel!
    @IBOutlet weak var labelrealtime: UILabel!
    @IBOutlet weak var labelRevised: UILabel!
    @IBOutlet weak var pickerview: UIPickerView!
    
    @IBOutlet weak var blackDatestripView: UIView!
    @IBOutlet weak var buttonView: UIButton!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var buttonGo: UIButton!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var btnSelectedDate: UIButton!
    @IBOutlet weak var btnSelectRevisn: UIButton!
    @IBOutlet weak var imgViewNoCurtailment: UIImageView!
    
    
    
    @objc var dateapicall = ""
     @objc var revisionApicall = ""
    
    var arrresponseobj = [String]()
    var stringFromDate = ""
    var arrRevision = [String]()
    var AllResponce = [JSON]()
    
    // MARK:– api post parameter in after Go.
    var arrResponceKey = [String]()
    var arrResponce = [JSON]()
    var level = Bool()
    var blocknumber = [String]()
    
    var actual = [String]()
    var revised = [String]()
    
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.noDataView.isHidden = true
        
        myTableView.delegate = self
        myTableView.dataSource = self
        let revisions = revisionApicall
        let dates = dateapicall
        
        // MARK: - Check Notification Condition.
        if dateapicall.characters.count > 0
        {
            buttonGo.isEnabled = false
            btnSelectRevisn.isEnabled = false
            btnSelectRevisn.setTitle(revisions, for: .normal)
            btnSelectedDate.setTitle(dates, for: .normal)
            GoapiCall()
            initialServiceHitData()
        }
        else
        {
            initialServiceHitData()
        }
        
        
        pickerview.isHidden = true
        myTableView.separatorStyle = .none
        btnGraph.isHidden = true
        labelBlockno.isHidden = true
        labelOriginal.isHidden = true
        labelrealtime.isHidden = true
        labelRevised.isHidden = true
        myTableView.isHidden = true
        level = false
         AppHelper.save(toUserDefaults: "", withKey: "curtelmentBackView")
        
        blocknumber = ["00:15", "00:30", "00:45", "01:00", "01:15", "01:30", "01:45", "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45", "24:00"]
        
        blackDatestripView.isHidden = true
    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        if (AppHelper.userDefaults(forKey: "curtelmentBackView") == "showView")
        {
            blackDatestripView.isHidden = false
        }
        else
        {
            blackDatestripView.isHidden = true
        }
        AppHelper.removeFromUserDefaults(withKey: "curtelmentBackView")
    }
    
    
    
    func initialServiceHitData()
    {
        
        let someDict =  ["access_key" : AppHelper.userDefaults(forKey: "access_key")]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL:"https://www.mittalpower.com/mobile/pxs_app/service/curtailment/getrevision.php", parameters:parameters)
            {(APIData) -> Void in
                
                if APIData.count>0
                {
                    self.AllResponce = [APIData]
                    
                    self.noDataView.isHidden = true
                    self.imgViewNoCurtailment.isHidden = true
                    
                    for (key, subJson) in APIData
                    {
                        self.arrresponseobj.append(key)
                    }
                    
                    
                }
                    
                else
                {
                    self.noDataView.isHidden = false;
                    self.imgViewNoCurtailment.isHidden = false;
                    
                }
                
            }
            
        }
        catch
        {
            
        }
    }
    
    
    
    
    
    func GoapiCall()
    {
        
        let someDict =  ["date" : dateapicall,"revision" : revisionApicall,"access_key" : AppHelper.userDefaults(forKey: "access_key")]
        
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL:"https://www.mittalpower.com/mobile/pxs_app/service/curtailment/getcurtailmentinfo.php", parameters:parameters){(APIData) -> Void in
                
                if APIData.count>0
                {
                    //SVProgressHUD.dismiss()
                    CustomHUD.shredObject.removeXib(view: self.view)
                    self.arrResponce = [APIData]
                    
                    self.arrResponceKey.removeAll()
                    for (key, subJson) in APIData
                    {
                        self.arrResponceKey.append(key)
                    }
                    
                    self.actual.removeAll()
                    self.revised.removeAll()
                    for i in 0..<self.arrResponceKey.count
                    {
                        let get = self.arrResponce[0][self.arrResponceKey[i]]
                        self.actual.append(get["actual"].stringValue)
                        self.revised.append(get["revised"].stringValue)
                        
                    }

                    self.btnGraph.isHidden = false
                    self.labelBlockno.isHidden = false
                    self.labelOriginal.isHidden = false
                    self.labelrealtime.isHidden = false
                    self.labelRevised.isHidden = false
                    self.myTableView.isHidden = false
                    self.myTableView.reloadData()
                }
                
            }
            
        }
        catch
        {
            
        }
        
    }
    
    
    
    
    
    // MARK:-picker View
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        
        if level == false
        {
            return arrresponseobj.count
        }
        return arrRevision.count
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        
        if level == false
        {
            return arrresponseobj[row]
        }
        return arrRevision[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        if level == false
        {
            
            dateapicall = arrresponseobj[row]
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let yourDate: Date? = dateFormatter.date(from: dateapicall)
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            stringFromDate = dateFormatter.string(from: yourDate!)
            btnSelectedDate.setTitle(stringFromDate, for: .normal)
            buttonView.setTitle(stringFromDate, for: .normal)
            
            arrRevision.removeAll()
            for i in 0..<AllResponce[0].count
            {
                arrRevision.append(AllResponce[0][dateapicall][i].stringValue)
            }
            
            
            pickerview.reloadAllComponents()
            pickerview.isHidden = true
        }
        else
        {
            btnSelectRevisn.setTitle(arrRevision[row], for: .normal)
            revisionApicall = arrRevision[row]
            pickerview.isHidden = true
        }
    }
    
    
    
    
    
    
    // MARK:- Table View
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
     
        return actual.count
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellvalue", for: indexPath)as! cellValueAll
        
        cell.selectionStyle = UITableViewCellSelectionStyle .none
        
        cell.lblrealtimecurtailment.layer.cornerRadius = 8.0
        cell.lblrealtimecurtailment.layer.masksToBounds = true
        //
        cell.lblRevised.layer.cornerRadius = 8.0
        cell.lblRevised.layer.masksToBounds = true
        cell.lblOriginal.layer.cornerRadius = 8.0
        cell.lblOriginal.layer.masksToBounds = true
        cell.selectionStyle = .none

   
        cell.lblBlockNo.text = blocknumber[indexPath.row]
        cell.lblOriginal.text = actual[indexPath.row]
        cell.lblRevised.text = revised[indexPath.row]
        cell.lblrealtimecurtailment.text = "-"

        
        return cell
    }
    
    
    
    @IBAction func btnSelectDate(_ sender: UIButton)
    {
        buttonGo.isEnabled = true
        btnSelectRevisn.isEnabled = true
        pickerview.isHidden = false
        btnGraph.isHidden = true
        labelBlockno.isHidden = true
        labelOriginal.isHidden = true
        labelrealtime.isHidden = true
        labelRevised.isHidden = true
        myTableView.isHidden = true
        level = false
        pickerview.reloadAllComponents()
    }
    
    
    @IBAction func btnSelectRevsn(_ sender: UIButton)
    {
        pickerview.isHidden = false
        btnGraph.isHidden = true
        labelBlockno.isHidden = true
        labelOriginal.isHidden = true
        labelrealtime.isHidden = true
        labelRevised.isHidden = true
        myTableView.isHidden = true
        level = true
        pickerview.reloadAllComponents()
    }
    
    @IBAction func btnBack_Click(_ sender: UIButton)
    {
        //SVProgressHUD.dismiss()
        CustomHUD.shredObject.removeXib(view: self.view)
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func goButtonClick(_ sender: UIButton)
    {
        if !(btnSelectedDate.currentTitle == "Select Date")
        {
            if !(btnSelectRevisn.currentTitle == "Select Revision")
            {
                GoapiCall()
            }
            else
            {
                let alert = UIAlertController(title: "", message: "Please select Revision", preferredStyle: .alert)
                let firstAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                })
                alert.addAction(firstAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Please select Date", preferredStyle: .alert)
            let firstAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            })
            alert.addAction(firstAction)
            present(alert, animated: true)
        }
    }
    
    
    
    
    
    @IBAction func buttonView(_ sender: UIButton)
    {
        AppHelper.removeFromUserDefaults(withKey: "curtelmentBackView")
        blackDatestripView.isHidden = true
    }
    
    
    
    @IBAction func btnViewGraph(_ sender: UIButton)
    {
        let newView = storyboard?.instantiateViewController(withIdentifier: "CurtailmentGraphViewId") as? CurtailmentGraphViewController
        
        newView?.revision = revised
        newView?.actual = actual
        navigationController?.pushViewController(newView!, animated: true)

        
    }
    
    
    
    @IBAction func btnLogout_Click(_ sender: UIButton)
    {
        AppHelper.removeFromUserDefaults(withKey: "curtelmentBackView")
        AppDelegate.logout()
        navigationController?.popToRootViewController(animated: true)
    }
    
}



class cellValueAll: UITableViewCell
{
    @IBOutlet weak var lblBlockNo: UILabel!
    @IBOutlet weak var lblOriginal: UILabel!
    @IBOutlet weak var lblRevised: UILabel!
    @IBOutlet weak var lblrealtimecurtailment: UILabel!
}
