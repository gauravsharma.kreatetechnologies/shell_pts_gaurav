//
//  CurtailmentGraphViewController.swift
//  PCS
//
//  Created by pavan yadav on 25/09/17.
//  Copyright © 2017 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts

@objc class CurtailmentGraphViewController: UIViewController,ChartViewDelegate
{
    
    //@IBOutlet weak var myArrData: NSMutableArray!
    var originalSchdleBtnSelected = true
    var revisedSchdleBtnSelected = true
    var ActualSelected = ""
    var RevisedSelected = ""
    
    var arrXvalue = [String]()
    
   @objc var isPresented = false
    @IBOutlet weak var originalSchdleBtn: UIButton!
    @IBOutlet weak var greenhorizontalBtn: UIButton!
    @IBOutlet weak var revisedSchdleBtn: UIButton!
    @IBOutlet weak var redhorizentalBtn: UIButton!
    @IBOutlet weak var redGraphIndicationLbl: UILabel!
    @IBOutlet weak var greenGraphIndicationLbl: UILabel!
    var revision = [String]()
    var actual = [String]()
    
    
    
    @IBOutlet var lChartView: LineChartView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        isPresented = true
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        ActualSelected = "actualselect"
        RevisedSelected = "revisedselect"
        
        
        AppHelper.save(toUserDefaults: "showView", withKey: "curtelmentBackView")
        
        
        arrXvalue = ["00:15","00:30","00:45","01:00", "01:15", "01:30", "01:45", "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45","24:00"]
        
        setLineChartDataNew(2, range: 100.0)
    }
    
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    func setLineChartDataNew(_ count: Int, range: Double)
    {
        
        lChartView.chartDescription.enabled = false
        lChartView.leftAxis.enabled = true
        lChartView.rightAxis.drawAxisLineEnabled = true
        lChartView.rightAxis.drawGridLinesEnabled = true
        lChartView.drawGridBackgroundEnabled = true
        lChartView.drawBordersEnabled = true
        lChartView.dragEnabled = true
        lChartView.pinchZoomEnabled = false
        lChartView.legend.enabled = false
        
        lChartView.gridBackgroundColor = UIColor(red: 62.0 / 255.0, green: 71.0 / 255.0, blue: 89.0 / 255.0, alpha: 1.0)
        lChartView.backgroundColor = UIColor(red: 62.0 / 255.0, green: 71.0 / 255.0, blue: 89.0 / 255.0, alpha: 1.0)
        lChartView.chartDescription.enabled = true
        lChartView.chartDescription.text = "Values in MW/h"
        lChartView.chartDescription.textColor = UIColor.white
        
        // chart leftAxis Detail
        let leftAxis =  lChartView.leftAxis
        leftAxis.labelPosition = .outsideChart
        leftAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 10.0)!
        leftAxis.labelTextColor = .white
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = false
        leftAxis.axisMinimum = 0.0
        leftAxis.axisMaximum = 2.2
        
        
        
        // chart RightAxis Detail
        let rightAxis: YAxis? = lChartView.rightAxis
        rightAxis?.labelPosition = .outsideChart
        rightAxis?.labelFont = UIFont(name: "HelveticaNeue-Light", size: 10.0)!
        rightAxis?.labelTextColor = .white
        rightAxis?.drawGridLinesEnabled = true
        rightAxis?.granularityEnabled = false
        rightAxis?.axisMinimum = 0.0
        rightAxis?.axisMaximum = 2.2
        
        
        
        // chart XAxis Detail
        lChartView.xAxis.drawAxisLineEnabled = true
        lChartView.xAxis.drawGridLinesEnabled = true
        
        let xAxis = lChartView.xAxis
        xAxis.axisMinimum = 0
        xAxis.axisMaximum = Double(arrXvalue.count)
        xAxis.labelTextColor = .white
        xAxis.labelPosition  = .top
        xAxis.drawLabelsEnabled = true
        xAxis.drawLimitLinesBehindDataEnabled = false
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.labelFont = UIFont.systemFont(ofSize: 8.0)
        xAxis.granularityEnabled = true
        xAxis.granularity = 1.0
        
        xAxis.valueFormatter = IndexAxisValueFormatter(values: arrXvalue)
        
        
        var dataSets = [ChartDataSet]()
        var arrActualValues: [ChartDataEntry] = []
        var arrRevisedValues: [ChartDataEntry] = []
     
        
        for i in 0..<revision.count
        {
            let dataEntry = ChartDataEntry(x: Double(i+1), y: Double(actual[i])!)
            let dataEntry1 = ChartDataEntry(x: Double(i+1), y: Double(revision[i])!)
            
            arrActualValues.append(dataEntry)
            arrRevisedValues.append(dataEntry1)
        }
        
        
        
        if (ActualSelected == "actualselect")
        {
            //Actual Line Data Setting
            let lcDataSetActual = LineChartDataSet(entries: arrActualValues , label: "Actual")
            lcDataSetActual.lineWidth = 1
            lcDataSetActual.circleRadius = 2.0
            lcDataSetActual.circleHoleRadius = 1.0
            let color = UIColor.green
            lcDataSetActual.colors = [color]
            lcDataSetActual.circleColors = [color]
            dataSets.append(lcDataSetActual)
            
        }
        
        if (RevisedSelected == "revisedselect")
        {
            //Revised Line Data Setting
            let lcDataSetRevised = LineChartDataSet(entries: arrRevisedValues , label: "Revise")
            lcDataSetRevised.lineWidth = 1
            lcDataSetRevised.circleRadius = 2.0
            lcDataSetRevised.circleHoleRadius = 1.0
            var color: UIColor?
            color = UIColor.red
            lcDataSetRevised.colors = [color!]
            lcDataSetRevised.circleColors = [color!]
            dataSets.append(lcDataSetRevised)
        }
        
        let data = LineChartData(dataSets: dataSets )
        lChartView.data = data
        
        
    }
    
    
    
    
    
    
    
    @IBAction func originalbtnClick(_ sender: Any)
    {
        
        if originalSchdleBtnSelected
        {
            ActualSelected = ""
            originalSchdleBtnSelected = false
            greenGraphIndicationLbl.isHidden = true
            originalSchdleBtn.setTitleColor(UIColor.gray, for: .normal)
            greenhorizontalBtn.backgroundColor = UIColor.gray
            
        }
        else
        {
            
            ActualSelected = "actualselect"
            greenGraphIndicationLbl.isHidden = false
            originalSchdleBtnSelected = true
            originalSchdleBtn.setTitleColor(UIColor.green, for: .normal)
            greenhorizontalBtn.backgroundColor = UIColor.green
            
        }
        
        
        setLineChartDataNew(1, range: 100.0)
    }
    
    
    
    
    
    @IBAction func revisedBtnClick(_ sender: UIButton)
    {
        
        
        if revisedSchdleBtnSelected
        {
            RevisedSelected = ""
            revisedSchdleBtnSelected = false
            redGraphIndicationLbl.isHidden = true
            revisedSchdleBtn.setTitleColor(UIColor.gray, for: .normal)
            redhorizentalBtn.backgroundColor = UIColor.gray
        }
        else
        {
            
            RevisedSelected = "revisedselect"
            redGraphIndicationLbl.isHidden = false
            revisedSchdleBtnSelected = true
            revisedSchdleBtn.setTitleColor(UIColor.red, for: .normal)
            redhorizentalBtn.backgroundColor = UIColor.red
        }
        
        
        setLineChartDataNew(1, range: 100.0)
    }
    
    
    
    
    @IBAction func backBtnAction(_ sender: UIButton)
    {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        navigationController?.popViewController(animated: true)
    }
    
    
}
