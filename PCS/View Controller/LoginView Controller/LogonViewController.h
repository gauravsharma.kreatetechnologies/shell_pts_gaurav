//
//  LogonViewController.h
//  Reusable Componenet
//
//

#import <UIKit/UIKit.h>

@interface LogonViewController : UIViewController

@property(weak,nonatomic) IBOutlet UIButton *btnBack;
-(IBAction)btnBack_Click:(id)sender;

@property (weak, nonatomic) IBOutlet NSDictionary *dictMyschedule;
@property (weak, nonatomic) IBOutlet NSString *strMyschedule;
@property (weak, nonatomic) IBOutlet NSString *getBaseUrl;

@end
