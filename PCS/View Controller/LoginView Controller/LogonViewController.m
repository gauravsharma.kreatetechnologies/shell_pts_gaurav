//
//  LogonViewController.m
//  Reusable Componenet
//
#import "LogonViewController.h"
#import "Defines.h"
#import "Services.h"
#import "AppDelegate.h"
#import "KeychainWrapper.h"
#import "AppHelper.h"
#import "MenuViewController.h"
#import "SVProgressHUD.h"
#import "DemoUserRequestViewController.h"
#import "Power_Trading_Solutions-Swift.h"
#import "CustmObj.h"

@class CurtailmentViewController;
@class CurtailmentGraphViewController;
@class SideMenuViewController;
@class EnterServerURLViewController;
@class PDFVC;

#define txtFieldOneKey   @"username"
#define txtFieldtwoKey   @"password"
#define deviceToken      @"deviceToken"
#define deviceType       @"deviceType"
#define AppKey           @"AppKey"

/* Google Plus Crendentials*/
#define kClientId                         @"623352088149-vfdqit52jd0qgmi0vkv0qbvrqudgbmtn.apps.googleusercontent.com"
#define kClientSecret                     @"Q9SctYaFYv14iVr8IHyAG5FX"


@interface LogonViewController  ()
{
    BOOL isUernameTextField;
    BOOL isEmailTextField;
    BOOL isMobileTextField;
    BOOL isRememberedUserCredential;
    UITextField *activeField;
}
@property (weak, nonatomic) IBOutlet UIView *viewBG;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *constrainViewWidth;
@property(nonatomic,weak)IBOutlet UITextField *txtFieldOne;
@property(nonatomic,weak)IBOutlet UITextField *txtFieldTwo;
@property(nonatomic,weak)IBOutlet UIScrollView *myScrollView;
@property(nonatomic,weak)IBOutlet UIView *contentView;
@property(nonatomic,weak)IBOutlet UIView *btnLogin;
@property(nonatomic,weak)IBOutlet UIView *imgUserName;
@property(nonatomic,weak)IBOutlet UIView *imgPassword;

@property (weak, nonatomic) IBOutlet UIButton *btnBack_outlet;


@property (weak,nonatomic)IBOutlet UIButton *btnRequestForDemo;
-(IBAction)btnRequestForDemo_Click:(id)sender;

@property(weak,nonatomic)IBOutlet NSLayoutConstraint *leftConstrainLogo;
@property(weak,nonatomic)IBOutlet UIView *logoView;

-(IBAction)doneButtonClick:(id)sender;

@end

@implementation LogonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    
    //self.myScrollView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    // self.contentView.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    _imgPassword.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _imgPassword.layer.borderWidth = 0.4f;
    
    _imgPassword.layer.cornerRadius = 20;
    _imgPassword.layer.masksToBounds = true;

    
    _imgUserName.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _imgUserName.layer.borderWidth = 0.4f;
    
    _imgUserName.layer.cornerRadius = 20;
    _imgUserName.layer.masksToBounds = true;
    
    _btnLogin.layer.cornerRadius = 20;
    _btnLogin.layer.masksToBounds = true;
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.view.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];

    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.viewBG.layer.mask = maskLayer;
    
    float width=[[UIScreen mainScreen]bounds].size.width;
    if (width <=320)
    {
        self.leftConstrainLogo.constant=54;
    }
    else
    {
        self.leftConstrainLogo.constant=85;
    }
    
    [self.logoView needsUpdateConstraints];
    
    
    
    _constrainViewWidth.constant=self.view.frame.size.width;
    
    self.myScrollView.contentSize=CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
    
    
    [self setUpKeyBoardForRespectiveTextFields];
}



-(void)viewWillAppear:(BOOL)animated
{
    
    //    if ([AppHelper userDefaultsForKey:@"isRememberedCredential"])
    //    {
    //        isRememberedUserCredential=(BOOL)[AppHelper userDefaultsForKey:@"isRememberedCredential"];
    //    }
    //
    //    if (isRememberedUserCredential)
    //    {
    //        self.txtFieldOne.text = [KeychainWrapper keychainStringFromMatchingIdentifier:txtFieldOneKey];
    //        self.txtFieldTwo.text = [KeychainWrapper keychainStringFromMatchingIdentifier:txtFieldtwoKey];
    //    }
    
    self.txtFieldOne.text=@"";
    self.txtFieldTwo.text=@"";
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SetUpKeyBoardForRespectiveTextFields
/* Set the bool value for textfields (Username/Email/Mobile) */
-(void)setUpKeyBoardForRespectiveTextFields
{
    isUernameTextField = YES;
    
    isEmailTextField  = NO;
    isMobileTextField = NO;
    isRememberedUserCredential = NO;
    
    
    if (isMobileTextField)
    {
        self.txtFieldOne.keyboardType=UIKeyboardTypePhonePad;
    }
    else if (isEmailTextField)
    {
        self.txtFieldOne.keyboardType=UIKeyboardTypeEmailAddress;
    }
    else
    {
        self.txtFieldOne.keyboardType=UIKeyboardTypeDefault;
    }
    
}

#pragma mark - IBActions

-(IBAction)doneButtonClick:(id)sender
{
    [self sendDataToServerWithFieldsValidation];
    
}

-(IBAction)btnChangeURL:(id)sender
{
    EnterServerURLViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"EnterServerURLViewController"];
    newView.isChangeUrl=YES;
    [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction)isRememberMe:(id)sender
{
    
    if (isRememberedUserCredential)
    {
        isRememberedUserCredential=NO;
    }
    else
    {
        isRememberedUserCredential=YES;
    }
    [AppHelper saveBoolToUserDefaults:isRememberedUserCredential withKey:@"isRememberedCredential"];

}



#pragma mark - Keyboard Activity
- (void)keyboardWillShow:(NSNotification *)notif
{
    
    CGSize keyboardSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        
        [self.myScrollView scrollRectToVisible:activeField.frame animated:YES];
    }
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    // move the toolbar frame down as keyboard animates into view
    //    NSDictionary *info = [notification userInfo];
    //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // Write code to adjust views accordingly using kbSize.height
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    
}


#pragma mark -TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField=textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    activeField=nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (isUernameTextField || isEmailTextField)
    {
        if ([string isEqualToString:@" "])
        {
            textField.text=[textField.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            //textField.text=[textField.text stringByReplacingCharactersInRange:range withString:@"_"];
        }
    }
    
    return YES;
}

-(void)textFieldDidChange:(UITextField *)textField
{
    textField.text=[textField.text stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    //textField.text=[textField.text stringByReplacingCharactersInRange:range withString:@"_"];
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtFieldOne)
    {
        [self.txtFieldTwo becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.myScrollView.contentInset = contentInsets;
        self.myScrollView.scrollIndicatorInsets = contentInsets;
        
        // [self sendDataToServerWithFieldsValidation];
        
    }
    
    return YES;
}
#pragma mark - checkField
-(void)sendDataToServerWithFieldsValidation
{
    
    if ([[self.txtFieldOne.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    {
        [self.txtFieldOne becomeFirstResponder];
        
        if (isEmailTextField) {
            
        }
        else if (isUernameTextField)
        {
            
        }
        else
        {
            
        }
    }
    else if (isEmailTextField)
    {
        [self.txtFieldOne becomeFirstResponder];
        
        if ([self emailValidate:self.txtFieldOne.text])
        {
            
        }
    }
    else if ([[self.txtFieldTwo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    {
        [self.txtFieldTwo becomeFirstResponder];
    }
    else
    {
        
        //        NSString *token;
        //#if TARGET_IPHONE_SIMULATOR
        //        token = @"211466";
        //#else
        //        token = [AppHelper userDefaultsForKey:@"deviceToken"];
        //#endif
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[self.txtFieldOne.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:txtFieldOneKey];
        [dict setObject:[self.txtFieldTwo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:txtFieldtwoKey];
        //  [dict setObject:token forKey:deviceToken];
        [dict setObject:[AppHelper userDefaultsForKey:@"deviceId"] forKey:@"device_id"];
        
//        [dict setObject: @"1.2" forKey:@"version"];
         [dict setObject: @"1.5" forKey:@"version"];
        
        
//  NEXT SEND .
        
//      NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        
        
        
        // [dict setObject:@"app" forKey:AppKey];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
 
        
        NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
        
        self.view.userInteractionEnabled=NO;
        
       // [SVProgressHUD showWithStatus:@"Please wait.."];
        [CustmObj addHud:self.view];

        
        [[Services sharedInstance] serviceCallbyPost:@"service/login.php" param:dictFinaldata andCompletion:^(ResponseType type, id response)
         {
            
            
             NSLog(@"Main Response:----> %@",response);
            
             if (type == kResponseTypeFail)
             {
                NSLog(@"fail Response:----> %@",response);
                 self.view.userInteractionEnabled=YES;
                 //[SVProgressHUD dismiss];
                 [CustmObj removeHud:self.view];
                 
                 
                 NSError *error=(NSError*)response;
                 
                 if (error.code == -1009) {
                     
                     UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                    message:@"The Internet connection appears to be offline."
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                               
                                                                               // NSLog(@"You pressed button three");
                                                                           }];
                     
                     [alert addAction:thirdAction];
                     [self presentViewController:alert animated:YES completion:nil];
                 }
                 
                 
                 
             }
             else if (type == kresponseTypeSuccess)
             {
                 self.view.userInteractionEnabled=YES;
                NSMutableDictionary *dictResponsedata=(NSMutableDictionary *)response;
                 
               
                 if ([[dictResponsedata objectForKey:@"status"] isEqualToString:@"ERR"])
                 {
                     
                     if ([dictResponsedata objectForKey:@"message"]) {
                         [SVProgressHUD showErrorWithStatus:[dictResponsedata objectForKey:@"message"]];
                     }
                     else
                     {
                         //[SVProgressHUD dismiss];
                         [CustmObj removeHud:self.view];
                     }
                 }
                 
                 else if ([[dictResponsedata objectForKey:@"status"] isEqualToString:@"VERSION"])
                 {
                     //[SVProgressHUD dismiss];
                     [CustmObj removeHud:self.view];
                     
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[dictResponsedata objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                     
                     
                     [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                     {
                         [self HitAppStore];
                     }]];
                     
                     [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                         [self closeAlertview];
                     }]];
                     
                     
                     dispatch_async(dispatch_get_main_queue(), ^ {
                         [self presentViewController:alertController animated:YES completion:nil];
                     });
                     
                 }
                 else
                 {
                     {
   

    
                         if ([dictResponsedata objectForKey:@"access_key"])
                         {
                             [AppHelper saveToUserDefaults:[dictResponsedata objectForKey:@"access_key"] withKey:@"access_key"];
                         }
                         
                         if ([dictResponsedata objectForKey:@"companyName"])
                         {
                             [AppHelper saveToUserDefaults:[dictResponsedata objectForKey:@"companyName"] withKey:@"companyName"];
                         }
                         
                         
                         if ([dictResponsedata objectForKey:@"memberid"])
                         {
                             [AppHelper saveToUserDefaults:[dictResponsedata objectForKey:@"memberid"] withKey:@"memberid"];
                         }
                         
                         if ([dictResponsedata objectForKey:@"client_id"])
                         {
                             [AppHelper saveToUserDefaults:[dictResponsedata objectForKey:@"client_id"] withKey:@"client_id"];
                         }
                         
                         if ([dictResponsedata objectForKey:@"lastBidTime"])
                         {
                             [AppHelper saveToUserDefaults:[dictResponsedata objectForKey:@"lastBidTime"] withKey:@"lastBidTime"];
                         }
                         
                         if ([dictResponsedata objectForKey:@"recprice"])
                         {
                             [AppHelper saveToUserDefaults:[dictResponsedata objectForKey:@"recprice"] withKey:@"RECprice"];
                         }
                         
                         // New Changes BaseUrl dynamic:-
                       if ([dictResponsedata objectForKey:@"newsletterBaseUrl"])
                       {
                           
                       if ([[dictResponsedata objectForKey:@"newsletterBaseUrl"] isKindOfClass:[NSNull class]] == false)
                       {
                          [AppHelper saveToUserDefaults:[dictResponsedata objectForKey:@"newsletterBaseUrl"] withKey:@"newsletterBaseUrl"];
                       }
              }
 
                         
//                    _getBaseUrl = [AppHelper userDefaultsForKey:@"newsletterBaseUrl"];
//                     NSLog(@"For Base url:----> %@",_getBaseUrl);
                         
                       if ([dictResponsedata objectForKey:@"domain"])
                       {
                           NSString*newBaseUrl = [NSString stringWithFormat:@"%@%@",[dictResponsedata objectForKey:@"domain"],@"/mobile/pxs_app/"];
//                        [AppHelper saveToUserDefaults:newBaseUrl withKey:@"domain"];
                       }
 
                         
                         //  refresh SlidemenuView by local Notification.
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"aUniqueNameForTheNotification"  object:self];
                         
                         
                    
                         
                        // [SVProgressHUD dismiss];
                         [CustmObj removeHud:self.view];
                         
                         
                         NSMutableArray *arrMenusData=[NSMutableArray arrayWithArray:[dictResponsedata objectForKey:@"menujson"]];
                         
                         NSMutableDictionary *dictCurtailment = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                                                 @"Curtailment",@"Caption",
                                                                 @"curtailment",@"activity_name",
                                                                 @"curtailment",@"activty_icon", nil];
                         
//                         [arrMenusData insertObject:dictCurtailment atIndex: [arrMenusData count]];
//
                         
                         NSMutableDictionary *dictNewsLetter = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                                                @"News Letter",@"Caption",
                                                                @"newsletter",@"activity_name",
                                                                @"newsletter",@"activty_icon", nil];
                         
//                         [arrMenusData insertObject:dictNewsLetter atIndex: [arrMenusData count]];
                         
                         
                         
                         NSMutableDictionary *dictNewsUpdate = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                                                @"Report",@"Caption",
                                                                @"newsupdate",@"activity_name",
                                                                @"newsupdate",@"activty_icon", nil];
                         
//                         [arrMenusData insertObject:dictNewsUpdate atIndex: [arrMenusData count]];
                         
//                         [arrMenusData removeLastObject];
                         [AppHelper saveToUserDefaults:arrMenusData withKey:@"menuitems"];
                   
                         
                         
#pragma mark - check alarm login
                         
#pragma mark - Datapass revision & date in CurtailmentViewController.
                         
                     if([AppHelper userDefaultsForKey:@"curtailmentdate"])
                     {
                         CurtailmentViewController *pushView = [self.storyboard instantiateViewControllerWithIdentifier:@"CurtailmentView"];
                         

                pushView.dateapicall = [AppHelper userDefaultsForKey:@"curtailmentdate"];
                pushView.revisionApicall = [AppHelper userDefaultsForKey:@"curtailmentrevision"];
                         
                [self.navigationController pushViewController:pushView animated:YES];
                         
#pragma mark - Remove key from AppHelper.
                         [AppHelper removeFromUserDefaultsWithKey:@"curtailmentdate"];
                         [AppHelper removeFromUserDefaultsWithKey:@"curtailmentrevision"];
                         
                     }
                         
                         else
                             
                         {
    // My schedule login check----------------------------------------------
                          
                             if ([_strMyschedule isEqual:@"mySchedule"])
                             {
                                 
                                 _strMyschedule = @"notset";
                                 MyScheduleViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleId"];
                                          pushView.arrResponse = _dictMyschedule;
                                 [self.navigationController pushViewController:pushView animated:YES];
                                 
                             }
                             else
                             {
                             MenuViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
                             
                             if ([dictResponsedata objectForKey:@"lastBidTime"])
                             {
                                 newView.strLastTimeBid=[dictResponsedata objectForKey:@"lastBidTime"];
                             }
                             
                             [[NSNotificationCenter defaultCenter] postNotificationName: @"MyNotification" object:nil userInfo:nil];
                                 
                                 [AppHelper saveToUserDefaults:@"login" withKey:@"IsUserLogin"];
                                 

                             [self.navigationController pushViewController:newView animated:YES];
                             }
                         }
                     }
                 }
             }
             [CustmObj removeHud:self.view];
         }];
    }
    
    
    
    
}


-(void)HitAppStore
{
    NSString *iTunesLink = @"https://itunes.apple.com/in/app/power-trading-solutions/id1088152219?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

-(void)closeAlertview
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - KeyChainWrapper Methods

-(void)killAuthInfo {
    //NSLog(@"We should be deleting all of the user info here");
    [KeychainWrapper deleteItemFromKeychainWithIdentifier:@"userid"];
    [KeychainWrapper deleteItemFromKeychainWithIdentifier:txtFieldOneKey];
    [KeychainWrapper deleteItemFromKeychainWithIdentifier:txtFieldtwoKey];
    
    for (NSHTTPCookie *value in [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:value];
    }
    
}

#pragma mark - email validation
-(BOOL)emailValidate:(NSString *)email1
{
    if([email1 rangeOfString:@"@"].location==NSNotFound || [email1 rangeOfString:@"."].location==NSNotFound)
    {
        return YES;
    }
    NSString *accountName=[email1 substringToIndex: [email1 rangeOfString:@"@"].location];
    email1=[email1 substringFromIndex:[email1 rangeOfString:@"@"].location+1];
    if([email1 rangeOfString:@"."].location==NSNotFound)
    {
        return YES;
    }
    NSString *domainName=[email1 substringToIndex:[email1 rangeOfString:@"."].location];
    NSString *subDomain=[email1 substringFromIndex:[email1 rangeOfString:@"."].location+1];
    
    NSString *unWantedInUName = @" ~!@#$^&*()={}[]|;':\"<>,?/`";
    NSString *unWantedInDomain = @" ~!@#$%^&*()={}[]|;':\"<>,+?/`";
    NSString *unWantedInSub = @" `~!@#$%^&*()={}[]:\";'<>,?/1234567890";
    if(subDomain.length < 2)
    {
        return YES;
    }
    if([accountName isEqualToString:@""] || [accountName rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInUName]].location!=NSNotFound || [domainName isEqualToString:@""] || [domainName rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInDomain]].location!=NSNotFound || [subDomain isEqualToString:@""] || [subDomain rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInSub]].location!=NSNotFound)
    {
        return YES;
    }
    return NO;
}

-(IBAction)btnRequestForDemo_Click:(id)sender{
    DemoUserRequestViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"DemoUserRequest"];
    [self.navigationController pushViewController:newView animated:YES];
}

-(IBAction)btnBack_Click:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - touch
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
