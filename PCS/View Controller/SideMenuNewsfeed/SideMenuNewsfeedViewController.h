//
//  SideMenuNewsfeedViewController.h
//  PCS
//
//  Created by pavan yadav on 10/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuNewsfeedViewController : UIViewController


@property(strong, nonatomic) NSArray *strApidata;
@property (weak, nonatomic) IBOutlet UILabel *labelHead;

@property(strong, nonatomic) NSString *strresponse;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLogin_Click:(id)sender;

@end
