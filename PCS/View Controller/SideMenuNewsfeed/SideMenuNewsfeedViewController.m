//
//  SideMenuNewsfeedViewController.m
//  PCS
//
//  Created by pavan yadav on 10/08/16.
//  Copyright © 2016 lab4code. All rights reserved.
//

#import "SideMenuNewsfeedViewController.h"
#import "TableViewCell.h"
#import "SlideNavigationController.h"
#import "LogonViewController.h"
#import "WebbrowserControllerViewController.h"
#import "MenuViewController.h"
#import "AppHelper.h"
#import "Services.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
#import "SlideNavigationController.h"
#import "CustmObj.h"
@class SideMenuViewController;

@interface SideMenuNewsfeedViewController ()
{
    int imgShowRow;
    NSString *Lognotification;
    
}
@property (weak, nonatomic) IBOutlet UITableView *mytableview;
@property (strong, nonatomic) NSString *strNewsHead;

@end
@implementation SideMenuNewsfeedViewController


- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    
    if ((self = [super initWithCoder:aDecoder]))
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotification:) name:@"TestNotification" object:nil];
    }
    
    return self;
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
   
    _strNewsHead = [AppHelper userDefaultsForKey:@"newsHeading"];
    _labelHead.text = _strNewsHead;
    
    [self FirstServiceHitData];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
    imgShowRow=0;
    
    if([AppHelper userDefaultsForKey:@"client_id"])
    {
        Lognotification=@"home";
        [self.btnLogin setTitle:@"Home" forState:UIControlStateNormal];
        
    }else{
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        Lognotification=@"no";
    }
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.strApidata count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    TableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *dictNewsData=[self.strApidata objectAtIndex:indexPath.row];
    
    
    
    NSDictionary *attrAvailableGreen = @{
                                         NSFontAttributeName : [UIFont italicSystemFontOfSize:10],
                                         NSForegroundColorAttributeName : [UIColor blackColor]
                                         };
    
    
    
    [cell.lblNewsContent setText:dictNewsData[@"title"]];
    
    NSMutableAttributedString *strSourceDateTime=[[NSMutableAttributedString alloc]initWithString:dictNewsData[@"source"] attributes:nil];
    
    NSAttributedString *strDate = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" - %@",dictNewsData[@"pubdate"]] attributes:attrAvailableGreen];
    
    NSAttributedString *strTime = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" - %@",dictNewsData[@"time"]] attributes:attrAvailableGreen];
    
    [strSourceDateTime appendAttributedString:strTime];
    
    [strSourceDateTime appendAttributedString:strDate];
    [cell.lblNewsSource setAttributedText:strSourceDateTime];
    
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == self.mytableview)
    {
        NSArray *arrTemp = self.strApidata;
        NSDictionary *dictTemp = [arrTemp objectAtIndex:indexPath.row];
        NSString *strUrl=[dictTemp objectForKey:@"link"];
        WebbrowserControllerViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"webBrowse"];
        newView.strWebUrl=strUrl;
        [self.navigationController pushViewController:newView animated:YES];
        
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CATransform3D rotationTransform = CATransform3DTranslate(CATransform3DIdentity,0,100,0);
    cell.layer.transform = rotationTransform;
    cell.alpha = 0;
    
    [UIView animateWithDuration:.6 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        cell.layer.transform = CATransform3DIdentity;
        cell.alpha = 1;
    }
                     completion:^ (BOOL completed) {} ];
    
}



-(CGFloat)getExpectedSizeOfText:(NSString *)text
{
    CGSize size = [text sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:16.0f]}];
    return size.width;
}

#pragma mark - SlideNavigationController Methods -

-(IBAction)btnToggleSideMenu_Click:(id)sender
{
   //[self.navigationController popViewControllerAnimated:YES];
    
    [[SlideNavigationController sharedInstance]toggleLeftMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;

}

- (void)receiveTestNotification:(NSNotification *) notification

{
    NSDictionary *dictReceivedData = notification.userInfo;
    //    if([_strNewsHead isEqualToString:dictReceivedData[@"NewsHead"]]){
    //
    //    }else{
    _strNewsHead = dictReceivedData[@"NewsHead"];
    
    [self viewDidLoad];
    // NSLog(@"%@",_strNewsHead);
}

-(void)FirstServiceHitData

{
   // [SVProgressHUD showWithStatus:@"Please wait..."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/getnewsfeedinfo.php" param:nil andCompletion:^(ResponseType type, id response) {
        
        if (type == kResponseTypeFail)
        {
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else if (type == kresponseTypeSuccess)
        {
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            if ([response count]>0)
            {
                
                _strApidata=response[_strNewsHead];
                [_mytableview reloadData];
            }}
    }];
}



- (IBAction)btnLogin_Click:(id)sender
{
    
    if ([Lognotification isEqualToString:@"home"])
    {
        MenuViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
    else
    {
        LogonViewController *pushView=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:pushView animated:YES];
    }
}
@end
