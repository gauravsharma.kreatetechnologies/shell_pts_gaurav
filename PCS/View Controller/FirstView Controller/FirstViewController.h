//
//  FirstViewController.h
//  PCS
//
//  Created by lab4code on 24/11/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btncheckedpro;
@property (weak, nonatomic) IBOutlet UIWebView *WebViewDisclaimer;
@property (weak, nonatomic) IBOutlet UIView *DisclaimerViewe;
@end
