//
//  FirstViewController.m
//  PCS
//
//  Created by lab4code on 24/11/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import "FirstViewController.h"
#import "Services.h"
#import "LogonViewController.h"
#import "ServiceSessionManger.h"
#import "AppHelper.h"
#import "SVProgressHUD.h"
#import "CustmObj.h"
@class DashboardViewController;
@class EnterServerURLViewController;
@class MenuViewController;

#import <SafariServices/SafariServices.h>


@interface FirstViewController ()<UIWebViewDelegate>
{
    BOOL btnchecked;
    UIWebView *description;;
}
@property(weak,nonatomic)IBOutlet NSLayoutConstraint *leftConstrainLogo;
@property(weak,nonatomic)IBOutlet UIView *logoView;
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _DisclaimerViewe.hidden=YES;
    _WebViewDisclaimer.delegate = self;
    
    self.WebViewDisclaimer.layer.borderWidth = 1;
    [[_WebViewDisclaimer layer] setBorderColor:
     [[UIColor colorWithRed:0 green:0 blue:0 alpha:1] CGColor]];
   

}

-(void)viewWillAppear:(BOOL)animated
{

    float width=[[UIScreen mainScreen]bounds].size.width;
    float height=[[UIScreen mainScreen]bounds].size.height;
    
    if (width <=320)
    {
        self.leftConstrainLogo.constant=54;
    }
    else
    {
        self.leftConstrainLogo.constant=85;
    }
    
    [self.logoView needsUpdateConstraints];

    float iOSVersion=[[UIDevice currentDevice].systemVersion floatValue];
    
    if (![AppHelper userDefaultsForKey:@"deviceId"])
    {
        [self generateGUID];
    }
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[UIDevice currentDevice] name],@"name",@"iOS",@"platform",[AppHelper userDefaultsForKey:@"deviceId"],@"uuid",[NSString stringWithFormat:@"%f",iOSVersion],@"version",[NSString stringWithFormat:@"%f",width],@"width",[NSString stringWithFormat:@"%f",height],@"height",@"",@"colordepth", nil];
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];

    
     NSMutableDictionary *dictFinaldata=[[NSMutableDictionary alloc]initWithObjectsAndKeys:myString,@"data", nil];
    
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [CustmObj addHud:self.view];
    
    [[Services sharedInstance]serviceCallbyPost:@"service/registerdevice.php" param:dictFinaldata andCompletion:^(ResponseType type, id response) {
        
       
        if (type == kResponseTypeFail)
        {
            // NSLog(@"fail Response:----> %@",response);
            
           // [SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
            
            
            NSError *error=(NSError*)response;
            
            if (error.code == -1009) {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                               message:@"The Internet connection appears to be offline."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          
                                                                          // NSLog(@"You pressed button three");
                                                                      }];
                
                [alert addAction:thirdAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
            
        }
        else if (type == kresponseTypeSuccess)
        {
        NSMutableDictionary *dictResponsedata=(NSMutableDictionary *)response;
        
        if ([[dictResponsedata objectForKey:@"status"] isEqualToString:@"ERR"])
        {
            if ([dictResponsedata objectForKey:@"message"]) {
                [SVProgressHUD showErrorWithStatus:[dictResponsedata objectForKey:@"message"]];
            }
            else
            {
                //[SVProgressHUD dismiss];
                [CustmObj removeHud:self.view];
            }
        }
        else
        {
            //[SVProgressHUD dismiss];
            [CustmObj removeHud:self.view];
        }
        
            if ([[AppHelper userDefaultsForKey:@"Disclaimerget"] isEqualToString:@"get"])
            {
                //IsUserLogin
                if ([[AppHelper userDefaultsForKey:@"IsUserLogin"] isEqualToString:@"login"]){
                    MenuViewController *ContactUs = [self.storyboard instantiateViewControllerWithIdentifier:@"menuItem"];
                    [self.navigationController pushViewController:ContactUs animated:YES];
                }else{
                    LogonViewController *ContactUs = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
                    [self.navigationController pushViewController:ContactUs animated:YES];
                }
                                
                
            }
            
            else
            {
                EnterServerURLViewController *ContactUs = [self.storyboard instantiateViewControllerWithIdentifier:@"EnterServerURLViewController"];


                [self.navigationController pushViewController:ContactUs animated:YES];
                
//                _DisclaimerViewe.hidden=NO;
//                NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"Disclaimer" ofType:@"html"];
//                NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
//                [_WebViewDisclaimer loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];

            }
 
        }
        
    }];
}


//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//
//{
//    if (navigationType == UIWebViewNavigationTypeLinkClicked ) {
//        [[UIApplication sharedApplication] openURL:[request URL]];
//        return NO;
//    }
//    
//    return YES;
//}

- (IBAction)BtnChecked:(id)sender {

    
    if (btnchecked==0) {
        
        [self.btncheckedpro setImage:[UIImage imageNamed:@"checkeddis"] forState:UIControlStateNormal];
        btnchecked=1;
        
    }
    else
    {
        [self.btncheckedpro setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        btnchecked=0;
    }

}

- (IBAction)BtnOk:(id)sender {
    
    if (btnchecked==1) {
        [AppHelper saveToUserDefaults:@"get" withKey:@"Disclaimerget"];
        _DisclaimerViewe.hidden=YES;
        
        DashboardViewController *ContactUs = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardId"];
        [self.navigationController pushViewController:ContactUs animated:YES];
        
    }
    else
    {
UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Check the box."
                                        message:@" "
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionalert = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        }];
        
        [alert addAction:actionalert];
        [self presentViewController:alert animated:YES completion:nil];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)generateGUID
{
   
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    NSString *guid =  (__bridge NSString *)string;
    
    [AppHelper saveToUserDefaults:[guid lowercaseString] withKey:@"deviceId"];
    return [guid lowercaseString];
}


-(IBAction)pushBtnClick:(id)sender
{
    //[self performSegueWithIdentifier:@"logCon" sender:self];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    if ([segue.identifier isEqualToString:@"logCon"])
    {
//        LogonViewController *loginObj=segue.destinationViewController;
//        [self.navigationController pushViewController:loginObj animated:YES];
    }
    
}




@end
