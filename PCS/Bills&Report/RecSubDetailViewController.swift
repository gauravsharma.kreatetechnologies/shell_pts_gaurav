//
//  RecSubDetailViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/24/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class RecSubDetailViewController: UIViewController {
    
    @IBOutlet weak var viewObligation:UIView!
    @IBOutlet weak var viewSolarcert:UIView!
    @IBOutlet weak var viewNonSolarcert:UIView!
    @IBOutlet weak var lblTitle:UILabel!
    var recData:RecReportData?
    var strTitle:String = ""
    var iexReportData:iexReportDat?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewObligation.layer.cornerRadius = 10.0
        self.viewSolarcert.layer.cornerRadius = 10.0
        self.viewNonSolarcert.layer.cornerRadius = 10.0
        self.lblTitle.text =  strTitle
        
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnObligation(_ sender: UIButton) {
        let data = iexReportData?.OBLIGATION
        if data?.isEmpty == false{
            let url =  data?[0].url
            let basePath = data?[0].base64filepath ?? ""
           
            let strUrl =  "http://\(url ?? "")\(basePath )"
            let convertedURL:URL = URL(string: strUrl)!
            print(basePath)

            if let url = URL(string: strUrl) {
                UIApplication.shared.open(url)
            }
            FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                print("PDF File downloaded to : \(path!)")

            }
        }
        else{
            self.showToast(controller: self, message: "No data found", seconds: 2)
        }
    }
    
    @IBAction func btnSolarcert(_ sender: UIButton) {
        let data = iexReportData?.SOLARCERT
        if data?.isEmpty == false{
            let url =  data?[0].url
            let basePath = data?[0].base64filepath ?? ""
           
            let strUrl =  "http://\(url ?? "")\(basePath )"
            let convertedURL:URL = URL(string: strUrl)!
            print(basePath)

            if let url = URL(string: strUrl) {
                UIApplication.shared.open(url)
            }
            FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                print("PDF File downloaded to : \(path!)")
            }
        }
        else{
            self.showToast(controller: self, message: "No data found", seconds: 2)
        }
    }
    
    @IBAction func btnNonSolarcert(_ sender: UIButton) {
        let data = iexReportData?.NONSOLARCERT
        if data?.isEmpty == false{
            let url =  data?[0].url
            let basePath = data?[0].base64filepath ?? ""
           
            let strUrl =  "http://\(url ?? "")\(basePath )"
            let convertedURL:URL = URL(string: strUrl)!
            print(basePath)

            if let url = URL(string: strUrl) {
                UIApplication.shared.open(url)
            }
            FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                print("PDF File downloaded to : \(path!)")
            }
        }
        else{
            self.showToast(controller: self, message: "No data found", seconds: 2)
        }
    }
}
