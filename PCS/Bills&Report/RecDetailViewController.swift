//
//  RecDetailViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/24/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class RecDetailViewController: UIViewController {

    @IBOutlet weak var viewBill:UIView!
    @IBOutlet weak var viewIEX:UIView!
    @IBOutlet weak var viewPXIL :UIView!
    var recData:RecReportData?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewBill.layer.cornerRadius = 10.0
        self.viewIEX.layer.cornerRadius = 10.0
        self.viewPXIL.layer.cornerRadius = 10.0

        
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBill(_ sender: UIButton) {
        let data = recData?.BILL
        if data?.isEmpty == false{
            let url =  data?[0].url
            let basePath = data?[0].base64filepath ?? ""
            
            let strUrl =  "http://\(url ?? "")\(basePath )"
            let convertedURL:URL = URL(string: strUrl)!
            print(basePath)
            
            if let url = URL(string: strUrl) {
                UIApplication.shared.open(url)
            }
            FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                print("PDF File downloaded to : \(path!)")
            }
        }else{
            self.showToast(controller: self, message: "No data found", seconds: 2)
        }
    }
    
    @IBAction func btnIEX(_ sender: UIButton) {
        let vc: RecSubDetailViewController? = storyboard?.instantiateViewController(withIdentifier: "RecSubDetailViewController") as! RecSubDetailViewController?
        vc?.strTitle = "IEX"
        vc?.iexReportData =  recData?.IEX
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func btnPXIL(_ sender: UIButton) {
        let vc: RecSubDetailViewController? = storyboard?.instantiateViewController(withIdentifier: "RecSubDetailViewController") as! RecSubDetailViewController?
        vc?.strTitle = "PXIL"
        vc?.iexReportData =  recData?.PXIL
        navigationController?.pushViewController(vc!, animated: true)
    }
}
