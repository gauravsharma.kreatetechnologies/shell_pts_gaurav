//
//  BillAndReportViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/22/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class BillAndReportViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var viewDam:UIView!
    @IBOutlet weak var viewTam:UIView!
    @IBOutlet weak var viewGTam:UIView!
    @IBOutlet weak var viewGDam:UIView!
    @IBOutlet weak var viewRec:UIView!
    @IBOutlet weak var viewRtm:UIView!
    @IBOutlet weak var viewBill:UIView!
    @IBOutlet weak var viewRateSheet:UIView!
    @IBOutlet weak var viewIex:UIView!
    @IBOutlet weak var viewPxil:UIView!
    @IBOutlet weak var txtDate: UITextField!
    
    var strDate:String = "LASTDATE"
    
    var damReportData:DamReportData?
    var gDamReportData:DamReportData?
    var bILLData = [IexobligationData]()
    var gTamData:DamReportData?
    var recData:DamReportData?
    var rtmData:DamReportData?
    var tamData:DamReportData?
    var iEXPROFITABILITYData = [IexobligationData]()
    var PXILPROFITABILITY = [IexobligationData]()
    var rATESHEET:DamReportData?
    var reportsAndDownloadsModel:ReportsAndDownloadsModel?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        roundCorner(viewDam)
        roundCorner(viewTam)
        roundCorner(viewGTam)
        roundCorner(viewGDam)
        roundCorner(viewRec)
        roundCorner(viewRtm)
        roundCorner(viewBill)
        roundCorner(viewRateSheet)
        roundCorner(viewIex)
        roundCorner(viewPxil)
        self.txtDate.delegate =  self
        self.txtDate.setInputViewDatePicker(target: self, selector: #selector(dateToPressed))
            self.callIHtmldownloads()
    }
    
    @objc func dateToPressed() {
        if let  datePicker = self.txtDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.txtDate.text = dateFormatter.string(from: datePicker.date)
            self.strDate =  formatter.string(from: datePicker.date)
        }
        self.txtDate.resignFirstResponder()
    }
    
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 10.0
    }
    func callIHtmldownloads()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")

        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["date"] = strDate

        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: htmldownloads, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(ReportsAndDownloadsModel.self, from: productData)
                    self.reportsAndDownloadsModel = decodedData
                   print(decodedData)

                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
       
    }
    @IBAction func btnGo(_ sender: UIButton) {
        self.callIHtmldownloads()
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func btnDam(_ sender: UIButton) {
        let vc: DownloadBillandReportVC? = storyboard?.instantiateViewController(withIdentifier: "DownloadBillandReportVC") as! DownloadBillandReportVC?
        vc?.damReportData =  self.reportsAndDownloadsModel?.DAM
        vc?.strTitle = "DAM"
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func btnTam(_ sender: UIButton) {
        let vc: DownloadBillandReportVC? = storyboard?.instantiateViewController(withIdentifier: "DownloadBillandReportVC") as! DownloadBillandReportVC?
        vc?.damReportData =  self.reportsAndDownloadsModel?.TAM
        vc?.strTitle = "TAM"

        navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func btnGtam(_ sender: UIButton) {
        let vc: DownloadBillandReportVC? = storyboard?.instantiateViewController(withIdentifier: "DownloadBillandReportVC") as! DownloadBillandReportVC?
        vc?.damReportData =  self.reportsAndDownloadsModel?.GTAM
        vc?.strTitle = "GTAM"

        navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func btnGdam(_ sender: UIButton) {
        let vc: DownloadBillandReportVC? = storyboard?.instantiateViewController(withIdentifier: "DownloadBillandReportVC") as! DownloadBillandReportVC?
        vc?.damReportData =  self.reportsAndDownloadsModel?.GDAM
        vc?.strTitle = "GDAM"

        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func btnRec(_ sender: UIButton) {
        let vc: RecDetailViewController? = storyboard?.instantiateViewController(withIdentifier: "RecDetailViewController") as! RecDetailViewController?
        vc?.recData =  self.reportsAndDownloadsModel?.REC

        navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func btnRtm(_ sender: UIButton) {
        let vc: DownloadBillandReportVC? = storyboard?.instantiateViewController(withIdentifier: "DownloadBillandReportVC") as! DownloadBillandReportVC?
        vc?.damReportData =  self.reportsAndDownloadsModel?.RTM
        vc?.strTitle = "RTM"

        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func btnBill(_ sender: UIButton) {
        
    }
    
    @IBAction func btnRatesheet(_ sender: UIButton) {
        
    }
    
    @IBAction func btnIex(_ sender: Any) {
        
    }
    @IBAction func btnPxil(_ sender: UIButton) {
        
    }
}
