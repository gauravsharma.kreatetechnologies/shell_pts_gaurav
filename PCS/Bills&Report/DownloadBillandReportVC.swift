//
//  DownloadBillandReportVC.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/23/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class DownloadBillandReportVC: UIViewController {

    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var lblTitle:UILabel!
    var damReportData:DamReportData?
    var recData:RecReportData?
    
    
    var strTitle:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblTitle.text =  strTitle
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
   

}

extension DownloadBillandReportVC:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if strTitle == "TAM" || strTitle == "GTAM"{
            return 2
        }
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "BillandReportCell", for: indexPath) as! BillandReportCell
        if strTitle == "TAM" || strTitle == "GTAM"{
            if indexPath.row == 0{
                cell.img.image = UIImage(named: "ObligationIng")
                cell.lblSubTitle.text = "IEX"
                cell.lblTitle.text = "OBLIGATION"
            }
            if indexPath.row == 1{
                cell.img.image = UIImage(named: "ObligationIng")
                cell.lblSubTitle.text = "PXIL"

                cell.lblTitle.text = "OBLIGATION"
            }
        }else{
            if indexPath.row == 0{
                cell.img.image = UIImage(named: "ObligationIng")
                cell.lblSubTitle.text = "IEX"
                cell.lblTitle.text = "OBLIGATION"
            }
            if indexPath.row == 1{
                cell.img.image = UIImage(named: "ScheduleImg")
                cell.lblSubTitle.text = "IEX"

                cell.lblTitle.text = "SCHEDULING"
            }
            if indexPath.row == 2{
                cell.img.image = UIImage(named: "ScheduleImg")
                cell.lblSubTitle.text = "PXIL"

                cell.lblTitle.text = "SCHEDULING"
            }
            if indexPath.row == 3{
                cell.img.image = UIImage(named: "ObligationIng")
                cell.lblSubTitle.text = "PXIL"
                cell.lblTitle.text = "OBLIGATION"
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if strTitle == "TAM" || strTitle == "GTAM"{
            if indexPath.row == 0{
                let data = damReportData?.iexobligation
                if data?.isEmpty == false{
                    let url =  data?[0].url
                    let basePath = data?[0].base64filepath ?? ""
                   
                    let strUrl =  "http://\(url ?? "")\(basePath )"
                    let convertedURL:URL = URL(string: strUrl)!
                    print(basePath)

                    if let url = URL(string: strUrl) {
                        UIApplication.shared.open(url)
                    }
                    FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                        print("PDF File downloaded to : \(path!)")

                    }
                }
                else{
                    self.showToast(controller: self, message: "No data found", seconds: 2)
                }
            }
            if indexPath.row == 1{
                let data = damReportData?.pxilobligation
                if data?.isEmpty == false{
                    let url =  data?[0].url
                    let basePath = data?[0].base64filepath
                   
                    let strUrl =  "http://\(url ?? "")\(basePath ?? "")"
                    let convertedURL:URL = URL(string: strUrl)!
                    print(basePath)

                    if let url = URL(string: strUrl) {
                        UIApplication.shared.open(url)
                    }
                    FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                        print("PDF File downloaded to : \(path!)")

                    }
                }
                else{
                    self.showToast(controller: self, message: "No data found", seconds: 2)
                }
            }
        }else{
            if indexPath.row == 0{
                let data = damReportData?.iexobligation
                if data?.isEmpty == false{
                    let url =  data?[0].url
                    let basePath = data?[0].base64filepath
                   
                    let strUrl =  "http://\(url ?? "")\(basePath ?? "")"
                    let convertedURL:URL = URL(string: strUrl)!
                    print(basePath)

                    if let url = URL(string: strUrl) {
                        UIApplication.shared.open(url)
                    }
                    FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                        print("PDF File downloaded to : \(path!)")

                    }
                }else{
                    self.showToast(controller: self, message: "No data found", seconds: 2)
                }
               
            }
            if indexPath.row == 1{
                let data = damReportData?.iexschedule
                if data?.isEmpty == false{
                    let url =  data?[0].url
                    let basePath = data?[0].base64filepath ?? ""
                   
                    let strUrl =  "http://\(url ?? "")\(basePath )"
                    let convertedURL:URL = URL(string: strUrl)!
                    print(basePath)

                    if let url = URL(string: strUrl) {
                        UIApplication.shared.open(url)
                    }
                    FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                        print("PDF File downloaded to : \(path!)")

                    }
                }
                else{
                    self.showToast(controller: self, message: "No data found", seconds: 2)
                }
            }
            if indexPath.row == 2{
                let data = damReportData?.pxilschedule
                if data?.isEmpty == false{
                    let url =  data?[0].url
                    let basePath = data?[0].base64filepath ?? ""
                   
                    let strUrl =  "http://\(url ?? "")\(basePath )"
                    let convertedURL:URL = URL(string: strUrl)!
                    print(basePath)

                    if let url = URL(string: strUrl) {
                        UIApplication.shared.open(url)
                    }
                    FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                        print("PDF File downloaded to : \(path!)")

                    }
                }
                else{
                    self.showToast(controller: self, message: "No data found", seconds: 2)
                }
            }
            if indexPath.row == 3{
                let data = damReportData?.pxilobligation
                if data?.isEmpty == false{
                    let url =  data?[0].url
                    let basePath = data?[0].base64filepath ?? ""
                   
                    let strUrl =  "http://\(url ?? "")\(basePath )"
                    let convertedURL:URL = URL(string: strUrl)!
                    print(basePath)

                    if let url = URL(string: strUrl) {
                        UIApplication.shared.open(url)
                    }
                    FileDownloader.loadFileSync(url:  convertedURL) { (path, error) in
                        print("PDF File downloaded to : \(path!)")
                        self.showToast(controller: self, message: "File Successfully download", seconds: 2)
                    }
                    
                }
                else{
                    self.showToast(controller: self, message: "No data found", seconds: 2)
                }
            }
        }
    }
}

extension DownloadBillandReportVC:UICollectionViewDelegateFlowLayout{

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let collectionWidth = self.collectionView.bounds.width
    return CGSize(width: collectionWidth/2-2, height: collectionWidth/2-2)
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 2
}
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 2
}
}
