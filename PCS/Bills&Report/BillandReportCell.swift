//
//  BillandReportCell.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/23/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class BillandReportCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle:UILabel!
}
