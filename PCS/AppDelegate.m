//
//  AppDelegate.m
//  PCS
//
//  Created by lab4code on 21/11/15.
//  Copyright © 2015 lab4code. All rights reserved.
//

#import "AppDelegate.h"
#import "AppHelper.h"
#import "Services.h"
#import "MainScreenViewController.h"
#import "NoBidViewController.h"
#import "NewBidMainViewController.h"
#import "SidemenuAllnewsfeed.h"
#import "WebbrowserControllerViewController.h"
#import "AlarmNotification.h"
#import "monthlyATCviewcontroller.h"
#import "TransmissionCorridorViewController.h"
#import "Power_Trading_Solutions-Swift.h"
#import "LogonViewController.h"
@class CurtailmentViewController;
@class CurtailmentGraphViewController;
@class SideMenuViewController;



#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize strLatitude,strLongitude;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
        
    //#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    //
    //
    //
    //#else
    //    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
    //     UIRemoteNotificationTypeBadge |
    //     UIRemoteNotificationTypeAlert |
    //     UIRemoteNotificationTypeSound];
    //
    //#endif
    
    [self registerForRemoteNotification];
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    SideMenuViewController *leftMenu = (SideMenuViewController*)[mainStoryboard
                                                                 instantiateViewControllerWithIdentifier: @"slide"];
    
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .35;
    
    
    
    
    
    
    // Override point for customization after application launch.
    
    //PROFITABILITY IEX image download
    // App icons and all given images in png format
    // new bid:- cancel btn service api
    
    return YES;
    
}


#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
   // NSLog(@"Device Token = %@",strDevicetoken);
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:[AppHelper userDefaultsForKey:@"deviceId"] forKey:@"deviceid"];
    [dict setValue:strDevicetoken forKey:@"regId"];
    
    
    [[Services sharedInstance] serviceCallbyPost:@"service/register.php" param:dict andCompletion:^(ResponseType type, id response)
     {
         if (type == kResponseTypeFail)
         {
             NSError *error=(NSError*)response;
             
             if (error.code == -1009)
             {
                 
                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                                message:@"The Internet connection appears to be offline."
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"OK"
                                                                       style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                           
                                                                           // NSLog(@"You pressed button three");
                                                                       }];
                 
                 [alert addAction:thirdAction];
                 
             }
             
             
             
         }
         else if (type == kresponseTypeSuccess)
         {
             // NSMutableDictionary *dictResponse=(NSMutableDictionary *)response;
             
             // NSLog(@"SuccessResponse : %@",dictResponse);
         }
     }];
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    // NSLog(@"Push Notification Information : %@",userInfo);
    
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    // NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    // NSLog(@"Error Notification = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    // NSLog(@"User Info = %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

#pragma mark - notification response.

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler
{
    //NSLog(@"User Info res = %@",response.notification.request.content.userInfo);
    completionHandler();
    
    NSDictionary *Response = [response.notification.request.content.userInfo valueForKeyPath:@"aps.alert"];
    
    
    if ([Response[@"type"]isEqualToString:@"MCP" ])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        BlockViewController *detailViewController = (BlockViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"blockviewId"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"menunewbid" ])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        NewBidMainViewController *detailViewController = (NewBidMainViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"menunewbididd"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"menunobid" ])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        NoBidViewController *detailViewController = (NoBidViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"menunobid"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
        
    }
    
    else if ([Response[@"type"]isEqualToString:@"curtailment"])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        CurtailmentViewController *detailViewController = (CurtailmentViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"CurtailmentView"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"menubid"])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        NewBidMainViewController *detailViewController = (NewBidMainViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"menunewbididd"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"curtail" ])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        CurtailmentViewController *detailViewController = (CurtailmentViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"CurtailmentView"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"recplacenewbid" ])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        NewBidMainViewController *detailViewController = (NewBidMainViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"menunewbididd"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"recnobid" ])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        NoBidViewController *detailViewController = (NoBidViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"menunobid"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    
    else if ([Response[@"type"]isEqualToString:@"newsfeedactivity" ])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        WebbrowserControllerViewController *detailViewController = (WebbrowserControllerViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"webBrowse"];
        detailViewController.strWebUrl = [Response valueForKey:@"url"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"intra-regional"])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        monthlyATCviewcontroller *detailViewController = (monthlyATCviewcontroller*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"MonthlyATC"];
        
        detailViewController.buttonType = [Response valueForKey:@"type"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    
    else if ([Response[@"type"]isEqualToString:@"inter-country"])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        monthlyATCviewcontroller *detailViewController = (monthlyATCviewcontroller*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"MonthlyATC"];
        
        detailViewController.buttonType = [Response valueForKey:@"type"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    else if ([Response[@"type"]isEqualToString:@"inter-regional"])
    {
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        monthlyATCviewcontroller *detailViewController = (monthlyATCviewcontroller*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"MonthlyATC"];
        
        detailViewController.buttonType = [Response valueForKey:@"type"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    
    else if ([Response[@"type"]isEqualToString:@"curtailmentAlarm"])
    {
#pragma mark - Add revision & date in AppHelper
        
        [AppHelper saveToUserDefaults:Response[@"date"] withKey:@"curtailmentdate"];
        [AppHelper saveToUserDefaults:Response[@"revision"] withKey:@"curtailmentrevision"];
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        AlarmNotification *detailViewController = (AlarmNotification*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"curtailAlarm"];
        detailViewController.arrResponse=Response;
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
    }
    
    
    if ([Response[@"type"]isEqualToString:@"scheduleTrack"])
    {
        
        if([AppHelper userDefaultsForKey:@"client_id"])
        {
            UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        MyScheduleViewController *detailViewController = (MyScheduleViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"ScheduleId"];
              detailViewController.arrResponse = Response[@"msg"];
        
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
        }
        else
        {
            
        UIStoryboard *mainStoryboarda = self.window.rootViewController.storyboard;
        LogonViewController *detailViewController = (LogonViewController*)[mainStoryboarda instantiateViewControllerWithIdentifier: @"LoginView"];
        
        detailViewController.strMyschedule = @"mySchedule";
              detailViewController.dictMyschedule = Response[@"msg"];
        [(UINavigationController*)self.window.rootViewController pushViewController:detailViewController animated:NO];
            

        }
    }
    
    
    
}

#pragma mark - Class Methods
/**
 Notification Registration
 */
- (void)registerForRemoteNotification
{
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error )
            {
                //Main Thread LOKESH
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
                
                
            }
            
            
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

#pragma mark - Orientation

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([[SlideNavigationController sharedInstance].topViewController isKindOfClass:[CurtailmentGraphViewController class]])
    {
        CurtailmentGraphViewController *secondController = (CurtailmentGraphViewController *) [SlideNavigationController sharedInstance].topViewController;
        
        if (secondController.isPresented)
        {
            return UIInterfaceOrientationMaskAll;
        }
        else return UIInterfaceOrientationMaskPortrait;
    }
    else if ([[SlideNavigationController sharedInstance].topViewController isKindOfClass:[BlockViewController class]])
    {
        BlockViewController *secondController = (BlockViewController *) [SlideNavigationController sharedInstance].topViewController;
        
        if (secondController.isPresented)
        {
            return UIInterfaceOrientationMaskAll;
        }
        else return UIInterfaceOrientationMaskPortrait;
    }
    
    else if ([[SlideNavigationController sharedInstance].topViewController isKindOfClass:[myScheduleGraph class]])
    {
        myScheduleGraph *thirdViewcontroller = (myScheduleGraph *) [SlideNavigationController sharedInstance].topViewController;
        
        if (thirdViewcontroller.isPresented)
        {
            return UIInterfaceOrientationMaskAll;
        }
        else return UIInterfaceOrientationMaskPortrait;
    }
    
    else return UIInterfaceOrientationMaskPortrait;
    
}

+(AppDelegate *)getAppdelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

+(void)logout
{
    [AppHelper removeFromUserDefaultsWithKey:@"access_key"];
    [AppHelper removeFromUserDefaultsWithKey:@"companyName"];
    [AppHelper removeFromUserDefaultsWithKey:@"memberid"];
    [AppHelper removeFromUserDefaultsWithKey:@"client_id"];
    // [AppHelper removeFromUserDefaultsWithKey:@"lastBidTime"];
//    [AppHelper removeFromUserDefaultsWithKey:@"menuitems"];
    [AppHelper saveToUserDefaults:@"logout" withKey:@"IsUserLogin"];
    [AppHelper removeFromUserDefaultsWithKey:@"RECprice"];
    // New Changes BaseUrl dynamic:-
    [AppHelper removeFromUserDefaultsWithKey:@"newsletterBaseUrl"];
    [AppHelper removeFromUserDefaultsWithKey:@"domain"];
    

    
[[NSNotificationCenter defaultCenter] postNotificationName:@"aUniqueNameForTheNotification"  object:self];

    
    // [[NSNotificationCenter defaultCenter] postNotificationName: @"MyNotification" object:nil userInfo:nil];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
    [AppDelegate logout];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.lab4code.PCS" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PCS" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"PCS.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        //    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //  NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}




@end



