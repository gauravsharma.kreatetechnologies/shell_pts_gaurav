//
//  StatewiseDetailVC.swift
//  PCS
//
//  Created by lokesh chand on 27/09/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StatewiseDetailVC: UIViewController {
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myPickerView: UIPickerView!
    @IBOutlet weak var NTPCButton: UIButton!
    @IBOutlet weak var dataDescriptionLbl: UILabel!
    @IBOutlet weak var noDataLbl: UILabel!
    var ntpcArray = ["coal","gas","other"]
    var buttonType = "ntPCButton"
    var responseTable = [JSON]()
    var getpickerrevision = ""
    var getState_name = ""
    var getRegion_name = ""
    var getbeneficiary = ""
    var initialValue = "coal"
    var lowertouppStr = [""]
    var setTitleLbl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lowertouppStr = ntpcArray.map {$0.uppercased()}
        dataDescriptionLbl.text = getState_name
        myPickerView.isHidden = true
        tableDetail_api()
        self.NTPCButton .setTitle(lowertouppStr[0], for: UIControlState .normal)
        
    }
    
    @IBAction func btnBack_Click(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }

    //MARK:- Click Any where on view Hide the UIElemnts.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        myPickerView.isHidden = true
    }
    
    func tableDetail_api()
    {
        CustomHUD.shredObject.loadXib(view: self.view)
        let someDict =  ["state":getState_name,"region":getRegion_name,"device_id":AppHelper.userDefaults(forKey: "deviceId")] as [String : Any]
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: someDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            let parameters : Parameters = ["data":theJSONText!.description]
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: stateTable_detail, parameters:parameters)
            {
                (APIData) -> Void in
                
               // print(APIData)
                if APIData["status"].stringValue == "Success"
                {
                    self.responseTable = APIData["station"][self.initialValue].arrayValue
                    if self.responseTable.count > 0
                    {
                        
                    }else
                    {
                        self.noDataLbl.isHidden = false
                        self.myTableView.isHidden = true
                    }
                }
                else
                {
                    self.noDataLbl.isHidden = false
                    self.myTableView.isHidden = true
                }
                self.myTableView.reloadData()
                CustomHUD.shredObject.removeXib(view: self.view)
            }
        }
        catch
        {
            
        }
        
    }
    
}
extension StatewiseDetailVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseTable.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatewiseDetailTableCell", for: indexPath) as! StatewiseDetailTableCell
        cell.stateLbl.text = responseTable[indexPath.row]["station"].stringValue
//        let chang = Float(responseTable[indexPath.row]["qtmvalue"].stringValue)
//        cell.URSLbl.text = String(format: "%.1f",chang!)
        
        cell.URSLbl.text = responseTable[indexPath.row]["qtmvalue"].stringValue
        

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let second = storyboard?.instantiateViewController(withIdentifier: "StationwiseAvlDetailVCID") as! StationwiseAvlDetailVC
        second.getregion_namee = getRegion_name
        second.getStn_name = responseTable[indexPath.row]["station"].stringValue
        second.getbeneficiary_name = getState_name
        second.getpickerrevision = getpickerrevision
        second.setTitleLbl = setTitleLbl
        second.setType = "STATEWISE"
        
        
        self.navigationController?.pushViewController(second, animated: true)
        
    }

}

extension StatewiseDetailVC:UIPickerViewDelegate,UIPickerViewDataSource
{
    @IBAction func pickerViewClick(_ sender: UIButton)
    {
        myPickerView.isHidden = false
        if sender.tag == 1
        {
            buttonType = "ntPCButton"
        }
        self.myPickerView.reloadAllComponents()
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if  buttonType == "ntPCButton"
        {
            return lowertouppStr.count
        }
        else
        {
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if buttonType == "ntPCButton"
        {
            return lowertouppStr[row]
        }
        else
        {
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if buttonType == "ntPCButton"
        {
            NTPCButton.setTitle(lowertouppStr[row], for: UIControlState.normal)
            initialValue = ntpcArray[row]
        }
        tableDetail_api()
        myPickerView.isHidden = true
    }
    
    
}
