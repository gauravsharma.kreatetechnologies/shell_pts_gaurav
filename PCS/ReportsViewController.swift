//
//  ReportsViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/24/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift

class ReportsViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtFromDate:UITextField!
    @IBOutlet weak var txtToDate:UITextField!
    @IBOutlet weak var viewFrom:UIView!
    @IBOutlet weak var lblMWH: UILabel!
    @IBOutlet weak var lblQA: UILabel!
    @IBOutlet weak var lblStoa: UILabel!
    @IBOutlet weak var lblFees: UILabel!
    @IBOutlet weak var lblTaxes: UILabel!
    @IBOutlet weak var lblNetAmount: UILabel!
    @IBOutlet weak var viewTo:UIView!
    @IBOutlet weak var txtProduct:UITextField!
    @IBOutlet weak var viewSelectType:UIView!
    @IBOutlet weak var viewStack:UIStackView!
    var sellerTypeArray:[BuyerType] = []
    var pickerView = ToolbarPickerView()
    var totalData:ReportTotalData?
    var reportTableData = [ReportTotalTableData]()
    var fromDate:String? = ""
    var toDate:String? = ""
    var type:String? = ""
    var productType:String? = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtFromDate.delegate =  self
        self.txtFromDate.delegate =  self
        self.viewStack.isHidden =  true
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtProduct.inputView =  pickerView
        self.pickerView.toolbarDelegate = self
        self.txtProduct.inputAccessoryView = self.pickerView.toolbar
        self.sellerTypeArray.insert(BuyerType(key: "Dam", value: "DAM"), at: 0)
        self.sellerTypeArray.insert(BuyerType(key: "Gdam", value: "GDAM"), at: 1)
        self.sellerTypeArray.insert(BuyerType(key: "Rtm", value: "RTM"), at: 2)
        self.sellerTypeArray.insert(BuyerType(key: "Tam", value: "TAM"), at: 3)
        self.tableView.isHidden =  true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        roundCorner(self.viewSelectType)
        self.txtFromDate.setInputViewDatePicker(target: self, selector: #selector(dateFromPressed))
        self.txtToDate.setInputViewDatePicker(target: self, selector: #selector(dateToPressed))
    }
    
    @objc func dateFromPressed() {
        if let  datePicker = self.txtFromDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.txtFromDate.text = dateFormatter.string(from: datePicker.date)
            self.fromDate =  formatter.string(from: datePicker.date)
        }
        self.txtFromDate.resignFirstResponder()
    }
    
    @objc func dateToPressed() {
        if let  datePicker = self.txtToDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.txtToDate.text = dateFormatter.string(from: datePicker.date)
            self.toDate =  formatter.string(from: datePicker.date)
        }
        self.txtToDate.resignFirstResponder()
    }
    
    func callReportsGetMisReport()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")

        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["chart"] = "no"
        param["fromDate"] = self.fromDate
        param["toDate"] = self.toDate
        param["app_type"] = self.productType

        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: reportsGetMisReport, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(ReportDataModel.self, from: productData)
                   print(decodedData)
                    self.reportTableData =  decodedData.value?.tabledata ?? []
                    self.totalData =  decodedData.value?.total
                    self.viewStack.isHidden =  self.reportTableData.count == 0 ? true:false

                    self.tableView.isHidden = self.reportTableData.count == 0 ? true:false
                    self.setUpUI()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
       
    }
    
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }
    func setUpUI(){
        let data =  self.totalData
        self.lblMWH.text =  "\(data?.new_mwh ?? 0.0)"
        self.lblQA.text =  "\(data?.fpp ?? 0.0)"
        self.lblStoa.text = "\(data?.stoacharges ?? 0.0)"
        self.lblFees.text = "\(data?.fees ?? 0.0)"
        self.lblTaxes.text =  "\(data?.igst ?? 0.0)"
        self.lblNetAmount.text = "\(data?.total ?? 0.0)"
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGo(_ sender: UIButton) {
        if  txtProduct.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Product Type")
            return
        }else if self.txtFromDate.text?.isEmpty ?? true{
            self.alertView(message: "Please Select From Date")
            return
        }
        else if self.txtToDate.text?.isEmpty ?? true{
            self.alertView(message: "Please Select To Date")
            return
        }
        self.callReportsGetMisReport()
    }
   
}


extension ReportsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reportTableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ReportsTableViewCell")as! ReportsTableViewCell
        let data =  reportTableData[indexPath.row]
        cell.lblCompanyName.text =  data.companyname
        cell.lblBuySell.text = "SELL"
        cell.lblMWH.text =  data.new_mwh
        cell.lblObligationAmount.text =  data.fpp
        cell.lblStoaCharges.text =  data.stoacharges
        cell.lblFess.text =  data.fees
        cell.lblTaxes.text =  data.igst
        cell.lblNetOA.text =  data.total
        cell.lblDate.text =  data.date
        return cell
        
    }
    
    
}

extension ReportsViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return sellerTypeArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return sellerTypeArray[row].key
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        txtProduct.text =  sellerTypeArray[row].key
        self.productType =  sellerTypeArray[row].value ?? ""
       
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}

extension ReportsViewController:ToolbarPickerViewDelegate{
    func didTapDone() {
        if txtProduct.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtProduct.text = sellerTypeArray[row].key
            self.productType =  sellerTypeArray[row].value ?? ""
            self.txtProduct.resignFirstResponder()
            
        }
    }
    
    func didTapCancel() {
        if txtProduct.isFirstResponder{
            self.txtProduct.text = nil
            self.txtProduct.resignFirstResponder()
        }
    }
}
