//
//  CustmObj.m
//  PCS
//
//  Created by lokesh chand on 14/08/18.
//  Copyright © 2018 lab4code. All rights reserved.
//

#import "CustmObj.h"
@interface CustmObj()

@property (strong, nonatomic) IBOutlet UIView *ContentView;
@property (weak, nonatomic) IBOutlet UIImageView *cusImage;
@end
@implementation CustmObj
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self customInit];
    }
    
    return self;
    
}


-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self customInit];
    }
    
    return self;
}

-(void)customInit
{
    
    [[NSBundle mainBundle] loadNibNamed:@"CustmObj" owner:self options:nil];
    [self addSubview:self.ContentView];
    self.ContentView.frame = self.bounds;
    
    NSArray *animationArray = [NSArray arrayWithObjects:
                               [UIImage imageNamed:@"ptslogo_loader.png"],
                               nil];
    self.cusImage.animationImages = animationArray;
    [self.cusImage startAnimating];
    [UIView animateWithDuration:6.f delay:0.f options:UIViewAnimationOptionRepeat |
     UIViewAnimationOptionCurveEaseIn animations:^{
        self.cusImage.frame = CGRectMake(self.frame.origin.x/2, self.frame.origin.y/2, self->_cusImage.frame.size.width+20, self->_cusImage.frame.size.height+20);
      [self.cusImage setAlpha:0.f];
         
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:4.f delay:0.f options:UIViewAnimationOptionRepeat |
         UIViewAnimationOptionCurveEaseOut animations:^{
            [self.cusImage setAlpha:0.f];
             
        } completion:nil];
    }];

}

-(void)loadCusXib:(UIView *)view
{
    
    [[NSBundle mainBundle] loadNibNamed:@"CustmObj" owner:self options:nil];
   // [self addSubview:self.ContentView];
    self.ContentView.frame = self.bounds;
    
    NSArray *animationArray = [NSArray arrayWithObjects:
                               [UIImage imageNamed:@"Pecten - colour"],
                               nil];
    self.cusImage.animationImages = animationArray;
    [self.cusImage startAnimating];
    [UIView animateWithDuration:20.f delay:0.f options:UIViewAnimationOptionRepeat |
     UIViewAnimationOptionCurveEaseIn animations:^{
        self.cusImage.frame = CGRectMake(view.frame.size.width/2 - 30, view.frame.size.height/2, self->_cusImage.frame.size.width+20, self->_cusImage.frame.size.height+20);
         self.cusImage.center = view.center;

        [self.cusImage setAlpha:0.f];
         
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:20.f delay:0.f options:UIViewAnimationOptionRepeat |
         UIViewAnimationOptionCurveEaseOut animations:^{
            [self.cusImage setAlpha:0.f];
             
        } completion:nil];
    }];
    
    self.ContentView.frame = view.frame;
    self.ContentView.center = view.center;
    //self.ContentView.frame.size = view.frame.size;
    
    self.ContentView.tag = 25000;
    [view addSubview:self.ContentView];

    
}


-(void)removeCusXib:(UIView *)myView
{
    
    UIView *view  = [myView viewWithTag:25000];
    [view removeFromSuperview];

}




+(void)addHud:(UIView *)viewa
{
    CustmObj *object = [[CustmObj alloc] init];
    [object loadCusXib:viewa];
}

+(void)removeHud:(UIView *)viewb
{
    CustmObj *object = [[CustmObj alloc] init];
    [object removeCusXib:viewb];
    
    
}


@end
