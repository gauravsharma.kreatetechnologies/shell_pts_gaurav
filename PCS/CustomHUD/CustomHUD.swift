//
//  CustomHUD.swift
//  progrss
//
//  Created by lokesh chand on 10/08/18.
//  Copyright © 2018 invetech. All rights reserved.
//

import UIKit

 @objc class CustomHUD: UIView{
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var ptsImg: UIImageView!
    static let shredObject = CustomHUD()
    
//    private init() {
//        super.init(frame:CGPoint.zero)
//    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initalSetup()
//        loadXib(view: self)
//        removeXib(view: self)
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalSetup()
//        loadXib(view: self)
//        removeXib(view: self)
    }

    
    
    
    
    func initalSetup()
    {
        Bundle.main.loadNibNamed("CustomHUD", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        ptsImg.animationImages  = [UIImage(named: "ptslogo_loader.png")!]
       // ptsImg.animationDuration = 1.0
       // ptsImg.animationRepeatCount = 2
        ptsImg.startAnimating()

        UIView.animate(withDuration: 4, animations:
            {
            self.ptsImg.frame.size.width += 10
            self.ptsImg.frame.size.height += 10
        })
        { _ in
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut, .repeat], animations: {
            })
        }
        
        

    }

    
//    func stopAnimating(){
//        ptsImg.stopAnimating()
//    }
    
    
    
    
    func loadXib(view:UIView)
    {
        
        Bundle.main.loadNibNamed("CustomHUD", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        ptsImg.animationImages  = [UIImage(named: "ptslogo_loader.png")!]
        // ptsImg.animationDuration = 1.0
        // ptsImg.animationRepeatCount = 2
        ptsImg.startAnimating()
        
        UIView.animate(withDuration: 4, animations:
            {
                self.ptsImg.frame.size.width += 10
                self.ptsImg.frame.size.height += 10
        })
        { _ in
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut, .repeat], animations: {
            })
        }
        
        
        contentView.frame = view.frame
        contentView .center = view .center
        contentView.frame.size = view  .frame.size
        view.addSubview(contentView)
    }
    
    func removeXib(view:UIView)
    {
      contentView.removeFromSuperview()
    }


    
}










