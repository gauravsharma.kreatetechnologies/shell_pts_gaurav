//
//  ReportViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 9/6/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ReportViewController: UIViewController {

    @IBOutlet weak var lblCompanyName:UILabel!
    @IBOutlet weak var lblFrom:UILabel!
    @IBOutlet weak var lblTo:UILabel!
    @IBOutlet weak var myTableView:UITableView!
    @IBOutlet weak var viewNoData:UIView!
    
    @IBOutlet weak var txtFrom:UITextField!
    @IBOutlet weak var txtTo:UITextField!
    var strFromDate:String?
    var strToDate:String?
    var isDateSelected:Bool? = false
    var reportData:ReportsDataModel?
    
    var arrdate = [String]()
    var arrDescription = [String]()
    var arrAmountRecievalbe = [String]()
    var arrAmountRecieved = [String]()
    var arrBalance = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewNoData.isHidden = true
        self.txtFrom.setInputViewDatePicker(target: self, selector: #selector(dateFromPressed))
        self.txtTo.setInputViewDatePicker(target: self, selector: #selector(dateToPressed))

            self.callReportInitialAPI()

    }
    

    @objc func dateToPressed() {
        if let  datePicker = self.txtTo.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            self.txtTo.text = dateFormatter.string(from: datePicker.date)
            self.strToDate =  formatter.string(from: datePicker.date)
            print(strToDate)
        }
        self.txtTo.resignFirstResponder()
    }
    @objc func dateFromPressed() {
        if let  datePicker = self.txtFrom.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            self.txtFrom.text = dateFormatter.string(from: datePicker.date)
            self.strFromDate =  formatter.string(from: datePicker.date)
            print(strFromDate)
        }
        self.txtFrom.resignFirstResponder()
    }
    func callReportInitialAPI()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
       
        var param:BRParameters = [:]
        if isDateSelected == true{
            param["access_key"] = access_key
            param["fromdate"] = strFromDate
            param["todate"] = strToDate
        }else{
            param["access_key"] = access_key
            param["fromdate"] = strFromDate
            param["todate"] = strToDate
            param["lasttransaction"] = "LASTTRANSACTION"
        }
       
        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            print(parameters)
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: htmlaccountsummary, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(ReportsDataModel.self, from: productData)
                   print(decodedData)
                    self.reportData = decodedData
                    self.myTableView.isHidden = self.reportData?.detail?.count == 0 ? true:false
                    if let detailArray =   decodedData.detail{
                        for i in 0..<detailArray.count{
                            self.arrdate.append(self.reportData?.detail?[i]["0"] ?? "")
                            self.arrDescription.append(self.reportData?.detail?[i]["1"] ?? "")
                            self.arrAmountRecievalbe.append(self.reportData?.detail?[i]["6"] ?? "")
                            self.arrAmountRecieved.append(self.reportData?.detail?[i]["8"] ?? "")
                            self.arrBalance.append(self.reportData?.detail?[i]["8"] ?? "")

                        }
                    }
                    print(self.arrdate)
                    DispatchQueue.main.async {
                        self.myTableView.reloadData()
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
        
    }

    @IBAction func btnGo(_ sender: UIButton) {
        self.isDateSelected = true
        if  txtFrom.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter From Date")
            return
        }else if self.txtTo.text?.isEmpty ?? true{
            self.alertView(message: "Please Enter To Date")
            
            return
        }
        self.callReportInitialAPI()
        
    }
    @IBAction func btnPdf(_ sender: UIButton) {
        let vc: PDFVC? = self.storyboard?.instantiateViewController(withIdentifier: "PDFVC") as! PDFVC?
        vc?.balanceArray =  self.arrBalance
        vc?.descrArray =  self.arrDescription
        vc?.dateArray =  self.arrdate
        vc?.amountRecievedArray = self.arrAmountRecieved
        vc?.amountRecievableArray = self.arrAmountRecievalbe
        vc?.modalPresentationStyle = .overFullScreen
        
        self.present(vc!, animated: true, completion: nil)

    }
    @IBAction func btnLogin(_ sender: UIButton) {
        AppDelegate.logout()
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension ReportViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.myTableView{
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            return 1;
        }
        
        else
        {
            return self.reportData?.detail?.count ?? 1
        }

       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 0)
        {
            return 40;
        }else{
            return 50;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section==0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "firstNoBid")!
            return cell
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "ReportTableViewCell")as! ReportTableViewCell
            let data  =  self.reportData?.detail?[indexPath.row]
            if ((data?["0"]) != nil){
                cell.lblDate.text =  data?["0"]

            }
            if ((data?["1"]) != nil){
                cell.lblUnitName.text = data?["1"]

            }
            if ((data?["6"]) != nil){
                cell.lblAmntRecievable.text = data?["6"]

            }
            if ((data?["7"]) != nil){
                cell.lblAmntyRecieved.text =  data?["7"]

            }
            if ((data?["8"]) != nil){
                cell.lblbalance.text =  data?["8"]
            }
            return cell
        }
    }
    
    
}
