//
//  EnterServerURLViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 7/28/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift

class EnterServerURLViewController: UIViewController {

    @IBOutlet weak var viewURL:UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtURL:UITextField!
    
    @objc var isChangeUrl:Bool = false
    @objc var isSuccess:String = ""
    @objc var completion:((String)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewURL.layer.cornerRadius =  6
        viewURL.layer.borderWidth = 0.6
        viewURL.layer.borderColor =  UIColor.lightGray.cgColor
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        if self.isChangeUrl == true{
            self.btnBack.isHidden = false
        }else{
            self.btnBack.isHidden = true
        }
    }
    

    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConnect(_ sender: UIButton) {
        if self.txtURL.text == ""{
            // create the alert
            let alert = UIAlertController(title: "Error", message: "Please Enter Server URL", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else{
            print("success")
            self.httpRequest()
            
        }
    }
    
    
    func httpRequest() {
        let url = URL(string: "https://powertradingsolutions.com/mobile/test.php")!
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                error == nil,
                let data = data,
                let string = String(data: data, encoding: .utf8)
            else {
                print(error ?? "Unknown error")
                return
            }
            self.isSuccess =  string
            print(string)
            
            if string == "TRUE"{
                
                
            DispatchQueue.main.async {
                if let url = self.txtURL.text{
                    
//                    AppHelper.save(toUserDefaults: "domain", withKey: "https://\(url)/mobile/pxs_app/")
                    
                }
                let pushView: PrivacyPolicyViewController? = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController?

                pushView?.completion = { str in

                    if str == "Checked"{
                        if let url1 = self.txtURL.text{
                            AppHelper.save(toUserDefaults: "https://\(url1)/mobile/pxs_app/", withKey:  "domain")
                        }
                       
                        AppHelper.save(toUserDefaults: "get", withKey: "Disclaimerget")
                        let pushView: LogonViewController? = self.storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LogonViewController?
                        self.navigationController?.pushViewController(pushView!, animated: true)
                    }else{

                    }

                }
                self.present(pushView!, animated: true, completion: nil)

            }
                
            }
        }
        task.resume()
    }
}
