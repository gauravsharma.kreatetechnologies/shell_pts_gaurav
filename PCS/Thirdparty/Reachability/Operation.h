//
//  Operation.h
//  Pepsi
//
//  Created by Admin on 31.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "OperationQueue.h"

@interface Operation : NSOperation {
	BOOL _isExecuting;
    BOOL _isFinished;
	OperationQueue *ownerQueue;
}

@property (readonly) BOOL isExecuting;
@property (readonly) BOOL isFinished;

@property (assign) OperationQueue *ownerQueue;

- (void)cancelOperations:(NSString *)errorMessage;

- (void)finish;

- (void)createTask;

@end
