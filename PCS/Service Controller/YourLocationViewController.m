//
//  YourLocationViewController.m
//  Groomefy
//
//  Created by Arvind Kumar on 21/10/15.
//  Copyright © 2015 Arvind Kumar. All rights reserved.
//

#import "YourLocationViewController.h"
#import "AppDelegate.h"

@interface YourLocationViewController ()
{
    
}
@end

@implementation YourLocationViewController

+(instancetype)sharedInstance{
   
    static YourLocationViewController *_sharedInstance = nil;
   
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[YourLocationViewController alloc] init];
        
    });
    return _sharedInstance;
}


-(void)gettingMyLocation{
   
    if (!locationManager)
    {
        locationManager = [[CLLocationManager alloc] init];
    }
    
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            //[locationManager requestWhenInUseAuthorization];
        }
        
        //[locationManager requestWhenInUseAuthorization];
      //  [locationManager startUpdatingLocation];
    }
    
    [locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
   
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    [AppDelegate getAppdelegate].strLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    [AppDelegate getAppdelegate].strLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    
    
    
   // [self reverseGeocode:newLocation];
    
    
    [locationManager stopUpdatingLocation];
    locationManager = nil;
    
    
}

- (void)reverseGeocode:(CLLocation *)location {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        //NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
            if (placemark) {
                if (placemark.subLocality) {
                    
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"NSNotification_location_updateHeader" object:self userInfo:nil];
                    
                    
                }
                
                
            }
            
        }
    }];
}



- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Failed to get location");
    if (!count) {
        count = 0;
    }
    if (count < 2) {
        [self gettingMyLocation];
        count ++;
    }
    else
    {
       
       // [[NSNotificationCenter defaultCenter] postNotificationName:@"NSNotification_location_fail" object:self userInfo:nil];
        
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if ([[UIApplication sharedApplication] applicationState]!=UIApplicationStateBackground){
        if (status == kCLAuthorizationStatusDenied) {
//            
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Service Disabled" message:@"Unable to get your location data. Please enable location service in your device (Settings > Privacy > Location Services) and allow The Sauce Shop to get your location data." preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alertController addAction:ok];
            
//            UIAlertView *alt=[[UIAlertView alloc]initWithTitle:@"Location Service Disabled" message:@"Unable to get your location data. Please enable location service in your device (Settings > Privacy > Location Services) and allow The Sauce Shop to get your location data." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alt show];
            
            NSLog(@"denied");
            
            
        }
        else if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways) {
            //[self gettingMyLocation];
            
             NSLog(@"used");
            
        }
    }
    
    [self gettingMyLocation];
    
     NSLog(@"status location %d",status);
}

- (void)updateCurrentLocation {
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

-(void)getCurrentLocation
{
    [self gettingMyLocation];
   
}




@end
