//
//  WebServiceURL.swift
//  Edumentis
//
//  Created by Ashish Karn on 20/12/16.
//  Copyright © 2016 Invetech Solution LLP. All rights reserved.
//
import Foundation

//let BASE_URL = "http://www.mittalpower.com/mobile/pxs_app/service/"
//let BASE_URL =  AppHelper.userDefaults(forKey: "domain") == nil ? "http://www.mittalpower.com/mobile/pxs_app/service/" : AppHelper.userDefaults(forKey: "domain") + "service/"
let BASE_URL =  AppHelper.userDefaults(forKey: "domain") == nil ? "http://shell.kreatetechnologies.com/mobile/pxs_app/" : AppHelper.userDefaults(forKey: "domain") + ""
let newBASE_URL = "http://ntpc.powertradingsolutions.com/services/mobile_service/new_urs/"

let DashboardLocation = "\(BASE_URL)service/getstatenamefromgeo.php"
let DashboardLocationSec = "\(BASE_URL)service/lastyearmarketprice.php"
let currentURS = "\(BASE_URL)service/getursdetails.php"
let newURSDetails = "\(newBASE_URL)service/getursdashboarddata.php"
let newsFeed = "\(BASE_URL)service/getnewsfeedinfo.php"
let MyscheduleSetup = "\(BASE_URL)service/schedule_track/getsaveschedule.php"
let MyscheduleSetupSave = "\(BASE_URL)service/schedule_track/savecode.php"
let MyscheduleSetupDelete = "\(BASE_URL)service/schedule_track/deleteclienttrack.php"


let MyscheduleGet = "\(BASE_URL)service/schedule_track/getcurrentflow.php"
let ClubbedViewGet = "\(BASE_URL)service//schedule_track/myscheduleclubbedview/getschedulingdata.php"

let MyscheduleRegion = "\(BASE_URL)service/schedule_track/getclientcodeofregion.php"
let MyscheduleSave = "\(BASE_URL)service/schedule_track/savecode.php"
let MyscheduleGetFilter = "\(BASE_URL)service/schedule_track/getsaveschedule.php"
let MyscheduleGraph = "\(BASE_URL)service/scheduling/compareqtmwithcurrentrev.php"
let statewiseData = "\(BASE_URL)service/scheduling/statewise.php"
let lastYearMarketprice = "\(BASE_URL)service//lastyearmarketprice.php"


let stateRealtime = "\(BASE_URL)service//scheduling/stateandimportwiseschedule.php"

//// Station wise
//let station_Table = "\(newBASE_URL)/getursdetailsstationwise.php"
//let stationTable_detail = "\(newBASE_URL)/stationwisedata.php"
//let stationblockwisedata_detail = "\(newBASE_URL)/blockwisedata.php"
//
//// State wise
//let state_Table = "\(newBASE_URL)/getursdetailsstatewise.php"
//let stateTable_detail = "\(newBASE_URL)/statewisedata.php"

// Newbase url Changes
// Station wise
let station_Table = "\(BASE_URL)service//getursdetailsstationwise.php"
let stationTable_detail = "\(BASE_URL)service//stationwisedata.php"
let stationblockwisedata_detail = "\(BASE_URL)service//blockwisedata.php"

// State wise

let state_Table = "\(BASE_URL)service//getursdetailsstatewise.php"
let stateTable_detail = "\(BASE_URL)service//statewisedata.php"
let sellerProductType = "\(BASE_URL)service/tam/getproducttype.php"
let serverHostURL = "https://powertradingsolutions.com/mobile/test.php"

// Tam/GTam

let fulltambiddetail = "\(BASE_URL)service/tam/getfulltambiddetail.php"
let tamfromtotime = "\(BASE_URL)service/tam/getfromtotime.php"
let tamsavetamgtambid = "\(BASE_URL)service/tam/savetamgtambid.php"
let tamdeletetamgtambid = "\(BASE_URL)service/tam/deletetamgtambid.php"
let tamsubmittamgtambid = "\(BASE_URL)service/tam/submittamgtambid.php"
let tamgetdailydates = "\(BASE_URL)service/tam/getdailydates.php"
let gdamgetGdamBiddata = "\(BASE_URL)service/gdam/getGdamBiddata.php"
let gdamgetGdamSouceType = "\(BASE_URL)service/gdam/getGdamSouceType.php"
let gdamsubmitGdamBiddata = "\(BASE_URL)service/gdam/submitGdamBiddata.php"
let gdamdeleteGdamBiddata = "\(BASE_URL)service/gdam/deleteGdamBiddata.php"
// No Bid
let nobidcommongetNoBidData = "\(BASE_URL)service/nobidcommon/getNoBidData.php"
let nobidcommonaddNoBidData = "\(BASE_URL)service/nobidcommon/addNoBidData.php"
let nobidcommondeleteNoBidData = "\(BASE_URL)service/nobidcommon/deleteNoBidData.php"
// Market Price
let htmlIEXmarketclearingprice = "\(BASE_URL)html/IEXmarketclearingprice.php"
let htmldownloads = "\(BASE_URL)html/downloads.php"

let reportsGetMisReport = "\(BASE_URL)service/reports/getMisReport.php"
let htmlaccountsummary = "\(BASE_URL)html/accountsummary.php"
