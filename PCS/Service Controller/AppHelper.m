//
//  AppHelper.m
//  IntroduceThem
//


#import "AppHelper.h"
#import "Defines.h"


@implementation AppHelper

//+(UIImage*)appLogoImage
//{
//    return [UIImage imageNamed:@"appLogo.png"];
//}


+(void)saveToUserDefaults:(id)value withKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    //NSLog(@"AppHElper Value:----> %@",standardUserDefaults);
    
	if (standardUserDefaults)
    {
		[standardUserDefaults setObject:value forKey:key];
		[standardUserDefaults synchronize];
	}
}
+(void)saveBoolToUserDefaults:(BOOL)value withKey:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
	if (standardUserDefaults)
    {
		[standardUserDefaults setBool:value forKey:key];
		[standardUserDefaults synchronize];
	} 
}
  
+(NSString*)userDefaultsForKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSString *val = nil;
	
	if (standardUserDefaults) 
		val = [standardUserDefaults objectForKey:key];
	
	return val;
}
+(NSDictionary*)userDictionaryForKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSDictionary *val = nil;
	
	if (standardUserDefaults)
		val = [standardUserDefaults objectForKey:key];
	
	return val;
}

+(NSArray*)userValueForKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSArray* val = nil;
	
	if (standardUserDefaults)
		val = [standardUserDefaults objectForKey:key];
	
	return val;
}

+(void)removeFromUserDefaultsWithKey:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardUserDefaults removeObjectForKey:key];
    [standardUserDefaults synchronize];
}
+ (NSString *)getCurrentLanguage {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    return [languages objectAtIndex:0];
}


//+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate 
//            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles
//{
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:CbtnTitle otherButtonTitles:otherBtnTitles, nil];
//    alert.tag = tag;
//	[alert show];
//	[alert release];
//}
//

#pragma mark - Get Path

+(NSString*)GetFilePathFor:(NSString*)type forURL:(NSString*)fileName
{
    NSString *documentFolderPath;
    documentFolderPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    documentFolderPath = [documentFolderPath stringByAppendingPathComponent:type];
    NSString *filePath = [[documentFolderPath stringByAppendingPathComponent:[fileName lastPathComponent]]stringByAppendingString:@""];
    NSFileManager *fileManager = [NSFileManager defaultManager ];
    BOOL isDir;
    
    if (([fileManager fileExistsAtPath:documentFolderPath isDirectory:&isDir] && isDir) == FALSE)
    {
        [[NSFileManager defaultManager]createDirectoryAtPath:documentFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
        
    }
    return filePath;
}

#pragma mark - For Incomplete URL path

+(NSURL*) findUrlPath:(NSString*)aUrl
{
    BOOL results = [[aUrl lowercaseString] hasPrefix:@"http://"] || [[aUrl lowercaseString] hasPrefix:@"https://"];
    NSURL *urlAddress = nil;
    
    if (results)
    {
        urlAddress = [NSURL URLWithString:aUrl];
    }
    else
    {
        
        // New Changes Dynamic Url
         NSString*baseUrl = [AppHelper userDefaultsForKey:@"domain"] == nil ? OLD_BASE_URL : [AppHelper userDefaultsForKey:@"domain"];
        NSString *resultUrl = [NSString stringWithFormat:@"%@%@",baseUrl,aUrl];
//        NSString *resultUrl = [NSString stringWithFormat:@"%@%@",OLD_BASE_URL,aUrl];
        urlAddress = [NSURL URLWithString: resultUrl];
        
        
    }
    return urlAddress;
}

@end
