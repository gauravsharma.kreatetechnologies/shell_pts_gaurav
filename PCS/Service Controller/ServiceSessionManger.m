//
//  ServiceSessionManger.m
//  InstaHomeController
//
//  Created by Jyoti Kumar on 01/12/15.
//  Copyright © 2015 Affle. All rights reserved.
//

#import "ServiceSessionManger.h"

@implementation ServiceSessionManger


- (id)init
{
    if (self = [super init])
    {
//        self.BaseURL = @"http://www.mittalpower.com/mobile/pxs_app/";
        self.BaseURL = @"https://shell.kreatetechnologies.com/mobile/pxs_app/";
    }
    return self;
}

#pragma POST
- (void)POST:(NSString *)URLString
  parameters:(id)parameters
     success:(serviceSuccessBlock)success
     failure:(serviceFailureBlock)failure
{
    [self setCompletionBlock:success andFailiureBlock:failure];
    [self HTTPRequestOperationWithHTTPMethod:@"POST" URLString:URLString parameters:parameters];
    
}
#pragma GET
- (void)GET:(NSString *)URLString
 parameters:(id)parameters
    success:(serviceSuccessBlock)success
    failure:(serviceFailureBlock)failure
{
    [self setCompletionBlock:success andFailiureBlock:failure];
    [self HTTPRequestOperationWithHTTPMethod:@"GET" URLString:URLString parameters:parameters];
    
    
}
#pragma mark MultipartPost
- (void)POSTMULTIPART:(NSString *)URLString
           parameters:(id)parameters
              success:(serviceSuccessBlock)success
              failure:(serviceFailureBlock)failure
{
    [self setCompletionBlock:success andFailiureBlock:failure];
   // [self HTTPMultiPartRequestOperationWithHTTPMethod:@"POST" URLString:URLString parameters:parameters];
}
// set the completion block
-(void)setCompletionBlock:(serviceSuccessBlock)success andFailiureBlock:(serviceFailureBlock)failure
{
    self.QumramApiCompletionBlock = success;
    self.QumramApiFailedBlock = failure;
}



//NSURLSession
#pragma mark NSURLSession
- (void)HTTPRequestOperationWithHTTPMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(id)parameters
{
    NSError *error;
    NSString* trackerUrl = [NSString stringWithFormat:@"%@%@",self.BaseURL,URLString];
    //Send Data to the Server and final url
    //Encoding final url
#ifdef DEBUG
    NSLog(@"Setting Final TrackerUrl  - %@", trackerUrl);
#endif

    
    NSURL *turl = [NSURL URLWithString:[trackerUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:turl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120];
    
    NSLog(@"REquest  - %@", request);
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if ([method isEqualToString:@"POST"]) {
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
        [request setHTTPBody:postData];
//        NSString *string =  [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
//        NSString *string2 =  [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        
    }
    [request setHTTPMethod:method];
    NSLog(@"URL :: %@", request);
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *responseData, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
#ifdef DEBUG
        NSLog(@"response code %ld",(long)httpResponse.statusCode);
#endif
        
        if([responseData length] > 0 && error == nil){
            NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            self.QumramApiCompletionBlock (jsonData);
        }
        else if ([responseData length] == 0 && error == nil){
#ifdef DEBUG
            NSLog(@"error: %@", error.userInfo);
            NSLog(@"error description: %@", error.description);
            NSLog(@"response code error==nil a: %ld",(long)httpResponse.statusCode);
#endif
            self.QumramApiFailedBlock(error.userInfo);
        }
        else if (error != nil){
#ifdef DEBUG
            NSLog(@"error: %@", error.userInfo);
            NSLog(@"error description: %@", error.description);
            NSLog(@"response code error!= nil b: %ld",(long)httpResponse.statusCode);
#endif
            self.QumramApiFailedBlock(error.userInfo);
        }
        
    }];
    
    [postDataTask resume];
    
}

#pragma mark HTTP POST MULTIPART
/*- (void)HTTPMultiPartRequestOperationWithHTTPMethod:(NSString *)method
                                          URLString:(NSString *)URLString
                                         parameters:(id)parameters
{
    NSError *error;
    NSString* trackerUrl = [NSString stringWithFormat:@"%@%@",self.BaseURL,URLString];
    //Encoding final url
#ifdef DEBUG
    NSLog(@"Setting Final TrackerUrl  - %@", trackerUrl);
#endif
    NSURL *turl = [NSURL URLWithString:[trackerUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
   // NSString *boundary = [QumramTrackerUtility generateGUID];
    
    // configure the request
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:turl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120];
    
    [request setHTTPMethod:method];
    
    // set content type
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:parameters fieldKey:@"screenshot"];
    
    
    NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *responseData, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
#ifdef DEBUG
        NSLog(@"response code %ld",(long)httpResponse.statusCode);
#endif
        
        if([responseData length] > 0 && error == nil){
            NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            self.QumramApiCompletionBlock (jsonData);
        }
        else if ([responseData length] == 0 && error == nil){
#ifdef DEBUG
            NSLog(@"error: %@", error.userInfo);
            NSLog(@"error description: %@", error.description);
            NSLog(@"response code error==nil a: %ld",(long)httpResponse.statusCode);
#endif
            self.QumramApiFailedBlock(error.userInfo);
        }
        else if (error != nil){
#ifdef DEBUG
            NSLog(@"error: %@", error.userInfo);
            NSLog(@"error description: %@", error.description);
            NSLog(@"response code error!= nil b: %ld",(long)httpResponse.statusCode);
#endif
            self.QumramApiFailedBlock(error.userInfo);
        }
    }];
    [task resume];
}
- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSMutableDictionary *)parameters fieldKey:(NSString *)fieldKey

{
    NSArray *imagePaths = [NSArray arrayWithObject:[parameters objectForKey:@"imagePath"]];
    [parameters removeObjectForKey:@"imagePath"];
    
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *key in [parameters allKeys])
    {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    // add image data
    
    for (NSString *path in imagePaths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = @"image/jpeg";
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldKey, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}*/


#pragma mark Delegate methods for download tasks

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
}
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    NSLog(@"Session %@ download task %@ resumed at offset %lld bytes out of an expected %lld bytes.\n",
          session, downloadTask, fileOffset, expectedTotalBytes);
    
    
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if (error != nil) {
        NSLog(@"Download completed with error: %@", [error localizedDescription]);
    }
    else{
        NSLog(@"Download finished successfully.");
    }
}
#pragma mark Session delegate methods for iOS background downloads

-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    NSLog(@"Background URL session %@ finished events.\n", session);
    
}


@end
