//
//  Services.m

//

#import "Services.h"
#import "AFNetworking.h"
#import "JSONKit.h"
#import "Defines.h"
#import "AppHelper.h"
//#import "CoreDataFunction.h"
#import <CoreLocation/CoreLocation.h>
@implementation Services

static Services *singletonInstance = nil;



+ (Services *)sharedInstance
{
    static Services *_sharedServicesHTTPClient = nil;
    
    
//    NSLog(@"BASE_URL:----> %@",[AppHelper userDefaultsForKey:@"domain"]);
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        //New Dynamic url Changes
        NSString*baseUrl = [AppHelper userDefaultsForKey:@"domain"] == nil ? OLD_BASE_URL : [AppHelper userDefaultsForKey:@"domain"];
        _sharedServicesHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
        
//         NSLog(@"BASE_URLLLLLLLLLVALLLLLLUUEEEEEE:----> %@",baseUrl);
        
        
//        _sharedServicesHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        
//         NSLog(@"Get Url:----> %@",_sharedServicesHTTPClient);
       // _sharedServicesHTTPClient.securityPolicy=[AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    return _sharedServicesHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
       // self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}


#pragma mark - serviceCall by Json

-(void) serviceCallThroughGet:(NSString *)serviceName param:(NSMutableDictionary *)dict andCompletion:(CompletionWithResponseTypeAndResponse) completion
{
  
    
    [self GET:serviceName parameters:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        
        //NSString *decodedeString=[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        // NSLog(@"decodedeString  Block:--->%@",decodedeString);
      //  NSDictionary *responceDict=[decodedeString JSONValue];
        //  NSLog(@"responceDict  Block:--->%@",responceDict);
        completion(kresponseTypeSuccess, nil);
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(kResponseTypeFail, nil);
    }];
    
}

#pragma mark - Service call For Image

-(void) serviceCallforImage:(NSString *)serviceName param:(NSMutableDictionary *)dict  andCompletion:(CompletionWithResponseTypeAndResponse) completion

{
    [self POST:serviceName parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
    {
        
        if([[dict objectForKey:@"file"] length]>0)
        {
            [formData appendPartWithFileData:[dict objectForKey:@"file"] name:@"file" fileName:@"image.png" mimeType:@"image/jpeg"];
        }
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        completion(kresponseTypeSuccess, nil);
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        
        completion(kResponseTypeFail, nil);
    }];
    
    
}


#pragma mark - Service call For Video

-(void) serviceCallforVideo:(NSString *)serviceName param:(NSMutableDictionary *)dict  andCompletion:(CompletionWithResponseTypeAndResponse) completion
{
    [self POST:serviceName parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if([dict objectForKey:@"videofile"])
        {
            [formData appendPartWithFileData:[dict objectForKey:@"videofile"] name:@"videofile" fileName:@"ios_video.mp4" mimeType:@"video/mp4"];
        }
        
        if([dict objectForKey:@"video_thumb"])
        {
            [formData appendPartWithFileData:[dict objectForKey:@"video_thumb"] name:@"video_thumb" fileName:@"image.png" mimeType:@"image/jpeg"];
        }
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        completion(kresponseTypeSuccess, nil);
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        
        completion(kResponseTypeFail, nil);
    }];
    
    
}

#pragma mark - Service call by Post

-(void) serviceCallbyPost:(NSString *)serviceName param:(NSMutableDictionary *)dict andCompletion:(CompletionWithResponseTypeAndResponse) completion
{
    
    NSLog(@"decodedeString  URL:--->%@",serviceName);
        [self POST:serviceName parameters:dict success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            
         NSLog(@"decodedeString  Block:--->%@",responseObject);
            
            completion(kresponseTypeSuccess, responseObject);
            
            
            
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            
            NSLog(@"Error %@",error);
            
            completion(kResponseTypeFail, error);
            
            
        }];
    
}
     

@end
