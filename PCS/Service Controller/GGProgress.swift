//
//  MBProgress.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/16/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
@objc open class GGProgress: NSObject {

    @objc  static var shared: GGProgress = GGProgress()

    var viewHub:UIView?

    static let _singletonInstance = GGProgress()
     private override init() {
       //This prevents others from using the default '()' initializer for this class.
     }

     // the sharedInstance class method can be reached from ObjC. (From OP's answer.)
     class func sharedInstance() -> GGProgress {
       return GGProgress._singletonInstance
     }

     // Some testing
     func testTheSingleton() -> String {
       return "Hello World"
     }
    
   @objc open func showProgress(){
        DispatchQueue.main.async {
            if self.viewHub == nil {

                self.viewHub = UIView(frame: UIApplication.shared.keyWindow!.frame)
                let img = UIImageView()
                img.image = UIImage(named: "circle")
                img.translatesAutoresizingMaskIntoConstraints = false

                let imgHr = UIImageView()
                imgHr.image = UIImage(named: "Pecten - colour")
                imgHr.translatesAutoresizingMaskIntoConstraints = false

                self.rotate2(imageView: img, aCircleTime: 1)

                self.viewHub?.addSubview(img)
                self.viewHub?.addSubview(imgHr)
                self.viewHub?.backgroundColor = .clear

                UIApplication.shared.keyWindow!.addSubview(self.viewHub!)
                let constraint =  NSLayoutConstraint(item: img,
                                                     attribute: .height,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 120)
                img.addConstraint(constraint)
                let constraint1 = NSLayoutConstraint(item: img,
                                                     attribute: .width,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 120)
                img.addConstraint(constraint1)
                let constraint2 =  NSLayoutConstraint(item: self.viewHub!,
                                                      attribute: .centerX,
                                                      relatedBy: .equal,
                                                      toItem: img,
                                                      attribute: .centerX,
                                                      multiplier: 1,
                                                      constant: 0)
                self.viewHub!.addConstraint(constraint2)
                let constraint3 = NSLayoutConstraint(item:self.viewHub!,
                                                     attribute: .centerY,
                                                     relatedBy: .equal,
                                                     toItem: img,
                                                     attribute: .centerY,
                                                     multiplier: 1,
                                                     constant: 0)
                self.viewHub!.addConstraint(constraint3)

                let constraint4 =  NSLayoutConstraint(item: imgHr,
                                                     attribute: .height,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 60)
                imgHr.addConstraint(constraint4)
                let constraint5 = NSLayoutConstraint(item: imgHr,
                                                     attribute: .width,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 60)
                imgHr.addConstraint(constraint5)
                let constraint6 =  NSLayoutConstraint(item: self.viewHub!,
                                                      attribute: .centerX,
                                                      relatedBy: .equal,
                                                      toItem: imgHr,
                                                      attribute: .centerX,
                                                      multiplier: 1,
                                                      constant: 0)
                self.viewHub!.addConstraint(constraint6)
                let constraint7 = NSLayoutConstraint(item:self.viewHub!,
                                                     attribute: .centerY,
                                                     relatedBy: .equal,
                                                     toItem: imgHr,
                                                     attribute: .centerY,
                                                     multiplier: 1,
                                                     constant: 0)
                self.viewHub!.addConstraint(constraint7)
            }else{
                self.viewHub!.isHidden = false
            }

        }
    }

    func hideProgress(_ file: String = #function){
        if viewHub != nil {
            self.viewHub!.isHidden = true
            self.viewHub = nil
            debugPrint("GGProgress : \(file) end")
        }
    }
    func rotate2(imageView: UIView, aCircleTime: Double) { //UIView

            UIView.animate(withDuration: aCircleTime/2, delay: 0.0, options: .curveLinear, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            }, completion: { finished in
                UIView.animate(withDuration: aCircleTime/2, delay: 0.0, options: .curveLinear, animations: {
                    imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi*2))
                }, completion: { finished in
                    self.rotate2(imageView: imageView, aCircleTime: aCircleTime)
                })
            })
    }
}
