//
//  APIManager.swift
//  Edumentis
//
//  Created by Ashish Karn on 20/12/16.
//  Copyright © 2016 Invetech Solution LLP. All rights reserved.
//

import Foundation

import Alamofire
import SwiftyJSON
import SystemConfiguration

typealias BRParameters = [String:Any]

class APIManager
{
    
    static let sharedInstance = APIManager()
    private init(){
        
    }
    let manager = NetworkReachabilityManager(host: "www.apple.com")

    
    func getCallAPI(controllername: UIViewController,apiURL: String, parameters:Parameters,isShow:Bool = true, onCompletion:@escaping (JSON) -> Void ) {
        
        self.showAlert(viewController: controllername)
        
        print("Api Responce Swift\((apiURL,parameters))")
        if (manager?.isReachable)! {
            if isShow{
                GGProgress.shared.showProgress()
                Alamofire.request(apiURL, method: .post,parameters:parameters,encoding:URLEncoding.default).validate()
                    .responseJSON { response in
                        GGProgress.shared.hideProgress()
                        switch response.result{
                        case .success(let data):
                            let  outputJSON = JSON(data)
                            onCompletion(outputJSON)
                           
                        case .failure(let error):
                            let outputJSON: JSON = JSON(["type":"ERROR","message": error.localizedDescription])
                            onCompletion(outputJSON)
                        }
                    }
            }
            
        }else {
            GGProgress.shared.hideProgress()
            //            Alerts.shared.warningMessage(title: Language.get("Alert"), subtitle: Language.get("No Internet Connection"))
        }
       
        
    }
    
    
    // MARK: - Internet Connection Check
    class func connectionAvailable() -> Bool
    {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags : SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    
    
    
    //MARK: - Show Alert
    func showAlert( viewController : UIViewController) -> Void
        
    {
        if APIManager.connectionAvailable() == true
        {
            
        }
        else
        {
            var refreshAlert = UIAlertController(title: "", message: "Internet Connection Required ", preferredStyle: UIAlertControllerStyle.alert)
            let Cancel = UIAlertAction(title:"RETRY", style: .default, handler: {(UIAlertAction) -> Void in
                
                self.showAlert(viewController: viewController)
                
            })
            refreshAlert.addAction(Cancel)
            viewController.present(refreshAlert, animated: true, completion: nil)
            
        }
    }
}

