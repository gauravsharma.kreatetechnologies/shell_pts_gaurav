//
//  ServiceSessionManger.h
//  InstaHomeController
//
//  Created by Jyoti Kumar on 01/12/15.
//  Copyright © 2015 Affle. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^serviceSuccessBlock)(id);
typedef void (^serviceFailureBlock)(id);

@interface ServiceSessionManger : NSObject<NSURLSessionDelegate,NSURLSessionTaskDelegate,NSURLSessionDataDelegate,NSURLSessionDownloadDelegate>
@property (nonatomic ,strong)NSString *BaseURL;

@property (nonatomic ,strong)serviceSuccessBlock QumramApiCompletionBlock;
@property (nonatomic ,strong)serviceFailureBlock   QumramApiFailedBlock;


- (void)POST:(NSString *)URLString
  parameters:(id)parameters
     success:(serviceSuccessBlock)success
     failure:(serviceFailureBlock)failure;

- (void)GET:(NSString *)URLString
 parameters:(id)parameters
    success:(serviceSuccessBlock)success
    failure:(serviceFailureBlock)failure;

@end
