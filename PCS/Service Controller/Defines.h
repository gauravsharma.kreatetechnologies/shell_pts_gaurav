

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)



typedef enum {
    kResponseTypeFail = 0,
    kresponseTypeSuccess
} ResponseType;

#pragma mark * Block Definitions

typedef void (^CompletionBlock) ();
typedef void (^CompletionWithIndexBlock) (NSUInteger index);
typedef void (^CompletionWithMessagesBlock) (id messages);
typedef void (^CompletionWithStringBlock) (NSString *string);
typedef void (^CompletionWithBoolBlock) (BOOL successful);
typedef void (^CompletionWithBoolAndStringBlock) (BOOL successful, NSString *info);
typedef void (^CompletionWithResponseTypeAndResponse) (ResponseType type, id response);
typedef void (^BusyUpdateBlock) (BOOL busy);


// Used to specify the application used in accessing the Keychain.
#define APP_NAME [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]

// Used for saving to NSUserDefaults that a PIN has been set, and is the unique identifier for the Keychain.
#define PIN_SAVED @"hasSavedPIN"

// Used for saving the user's name to NSUserDefaults.
#define USERNAME @"username"

// Used to help secure the PIN.
// Ideally, this is randomly generated, but to avoid the unnecessary complexity and overhead of storing the Salt separately, we will standardize on this key.
// !!KEEP IT A SECRET!!
#define SALT_HASH @"YOURMADEUPSALTHASHB8TQRygHaS2B1pgfy02wqGndj7PLHD2G3fxaZz4oGA3RsKdN2pxdAopXYgzzzz"

/**********************/
/*******************************************************************************************************************/
#pragma mark HTTP Request header values

#define TIMEOUT_INTERVAL							30
#define IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
//Facebook Constants
#define KToken                                  @"token"
#define KExpiryDate                             @"expiry_date"
#define KuserId									@"User_Id"
#define KFBEmailId							    @"userFBid"

#define kEmailKey                               @"kEmailKey"
#define kPasswordKey                            @"kPasswordKey"
/*******************************************************************************************************************/
#pragma mark - Strings

#define NSSTRING_HAS_DATA(_x) (((_x) != nil) && ( [(_x) length] > 0 ))

//NetManager
#define ERROR_INTERNET							@"Connection Error. Please check your internet connection and try again."
#define ERROR_TIMEOUT							@"Timeout error. Please check your internet connection and try again."
#define ERROR_INLOGIN                            @"Username OR Password is wrong."
//Near Me Tab


#pragma mark Alert title and messages
#define Alert_Cancel_Button                     @"Cancel"
#define Alert_Ok_Button                         @"Ok"
#define ERROR_GPS                               @"Error getting Current Location"
#define Login_Credential_Error                  @"Please enter your credentail for login."
#define NSSTRING_HAS_DATA(_x) (((_x) != nil) && ( [(_x) length] > 0 ))
#define NOTIFICATION_Login                            @"NOTIFICATION_Login"

/*******************************************************************************************************************/
#pragma mark - Web Services Base Url and Methods


#define BASE_URL                     [AppHelper userDefaultsForKey:@"domain"];
//#define OLD_BASE_URL                  @"https://www.mittalpower.com/mobile/pxs_app/"
#define OLD_BASE_URL @"http://shell.kreatetechnologies.com/mobile/pxs_app/"

#define Servicename    @"ServiceName"

/*******************************************************************************************************************/
#pragma mark- Notifications

#define Notification_Facebook_User_Cancled            @"Notification_Facebook_User_Cancled"
#define Notification_Facebook_User_Detail_Did_Load                                                     @"Notification_Facebook_User_Detail_Did_Load"
#define Notification_Facebook_Did_Login               @"Notification_Facebook_Did_Login"
#define NOTIFICATION_DidLogin                         @"NOTIFICATION_DidLogin"
#define NOTIFICATION_UserIdGot                        @"NOTIFICATION_UserIdGot"
#define NOTIFICATION_teamsDetailGot                   @"NOTIFICATION_teamsDetailGot"
#define Notification_Facebook_User_Loaded             @"Notification_Facebook_User_Loaded"
#define NOTIFICATION_Sign_up                          @"NOTIFICATION_Sign_up"
#define Notification_Facebook_Did_Logout              @"Notification_Facebook_Did_Logout"
#define NOTIFICATION_posttoWall                       @"NOTIFICATION_posttoWall"
#define Notification_Facebook_Friends_Loaded          @"Notification_Facebook_Friends_Loaded"
#define NOTIFICATION_getid                            @"NOTIFICATION_getid"
#define Notification_Facebook_Friends_FromFB          @"Notification_Facebook_Friends_FromFB"
#define NOTIFICATION_loginStatusChecked               @"NOTIFICATION_loginStatusChecked"
#define NOTIFICATION_Registration_Done                @"NOTIFICATION_Registration_Done"
#define NOTIFICATION_Check_UserName                   @"NOTIFICATION_Check_UserName"
#define NOTIFICATION_User_Comments_Got                @"NOTIFICATION_User_Comments_Got"
#define NOTIFICATION_Team_Comments_Got                @"NOTIFICATION_Team_Comments_Got"
#define NOTIFICATION_Comments_Posted                  @"NOTIFICATION_Comments_Posted"
#define NOTIFICATION_Like_Done                        @"NOTIFICATION_Like_Done"
#define NOTIFICATION_All_Likers_Got1                  @"NOTIFICATION_All_Likers_Got1"
#define NOTIFICATION_Reply_Done                       @"NOTIFICATION_Reply_Done"
#define NOTIFICATION_Teams_Following_Got              @"NOTIFICATION_Teams_Following_Got"
#define NOTIFICATION_Teams_Followed                   @"NOTIFICATION_Teams_Followed"
#define NOTIFICATION_Teams_UnFollowed                 @"NOTIFICATION_Teams_UnFollowed"
#define NOTIFICATION_Fans_Following_Got               @"NOTIFICATION_Fans_Following_Got"
#define NOTIFICATION_All_Fans_Got                     @"NOTIFICATION_All_Fans_Got"
#define NOTIFICATION_Fans_Followed                    @"NOTIFICATION_Fans_Followed"
#define NOTIFICATION_Fans_UnFollowed                  @"NOTIFICATION_Fans_UnFollowed"
#define NOTIFICATION_Fans_Comments_Got                @"NOTIFICATION_Fans_Comments_Got"
#define NOTIFICATION_Feeds                            @"NOTIFICATION_Feeds"
#define NOTIFICATION_Recent_Team_Comments_Got         @"NOTIFICATION_Recent_Team_Comments_Got"
#define NOTIFICATION_Recent_Fan_Comments_Got          @"NOTIFICATION_Recent_Fan_Comments_Got"
#define NOTIFICATION_Top_50_Topics                    @"NOTIFICATION_Top_50_Topics"
#define NOTIFICATION_App_Entered_Into_Background      @"NOTIFICATION_App_Entered_Into_Background"
#define NOTIFICATION_SubComments_Posted               @"NOTIFICATION_SubComments_Posted"
#define NOTIFICATION_Flag_This_Post                   @"NOTIFICATION_Flag_This_Post"
#define NOTIFICATION_Flag_This_User                   @"NOTIFICATION_Flag_This_User"
#define Notification_Facebook_User_Loaded             @"Notification_Facebook_User_Loaded"
#define Notification_Rivals_Got                       @"Notification_Rivals_Got"
#define Notification_User_Added_To_Rivals             @"Notification_User_Added_To_Rivals"
#define Notification_Rivals_Users_Got                 @"Notification_Rivals_Users_Got"
#define Notification_Trends_Comments_Got              @"Notification_Trends_Comments_Got"
#define Notification_Image_Changed                    @"Notification_Image_Changed"
#define Notification_User_LogOut                      @"Notification_User_LogOut"
#define Notification_All_Activities_Got               @"Notification_All_Activities_Got"
#define Notification_Activity_Read                    @"Notification_Activity_Read"
#define Notification_FbUser_Got                       @"Notification_FbUser_Got"
#define Notification_TwitterUser_Got                  @"Notification_TwitterUser_Got"
#define Notification_UpdateList                       @"Notification_UpdateList"
#define NOTIFICATION_Update_Team_List                 @"NOTIFICATION_Update_Team_List"
#define NOTIFICATION_Update_Fans_Comment_List                 @"NOTIFICATION_Update_Fans_Comment_List"

#define Notification_Team_Rival_Fan_User              @"Notification_Team_Rival_Fan_User"
#define Notification_Rival_Fan_User                   @"Notification_Rival_Fan_User"
#define Notification_Delete_Account                     @"Notification_Delete_Account"

#define NOTIFICATION_Get_Hash_Tag                       @"NOTIFICATION_Get_Hash_Tag"

#define NOTIFICATION_All_Fans                           @"NOTIFICATION_All_Fans"
#define NOTIFICATION_All_Rivals                         @"NOTIFICATION_All_Rivals"

#define NOTIFICATION_Forgot_Password                    @"NOTIFICATION_Forgot_Password"
#define NOTIFICATION_Delete_Comment                     @"NOTIFICATION_Delete_Comment"

#define NOTIFICATION_Data_Of_View_Is_Getting_Up         @"NOTIFICATION_Get_DataOfViewisgettingup"
#define NOTIFICATION_Change_Password                    @"NOTIFICATION_Change_Password"
#define NOTIFICATION_When_On_Tab0                       @"NOTIFICATION_When_On_Tab0"
#define Notification_Did_Purchase_Success               @"Notification_Did_Purchase_Success"
#define Notification_Did_Purchase_UnSuccess             @"Notification_Did_Purchase_UnSuccess"
#define NOTIFICATION_UserCredit                         @"NOTIFICATION_UserCredit"
#define NOTIFICATION_Push_Notification                  @"NOTIFICATION_Push_Notification"
#define NOTIFICATION_Push_Settings                      @"NOTIFICATION_Push_Settings"
#define NOTIFICATION_Change_Push_Settings               @"NOTIFICATION_Change_Push_Settings"



// New notification

#define Notification_GetData                        @"Notification_GetData"

//// Alert messages
#define Ok                              @"OK"
#define Username_Empty                  @"Please enter your Username"
#define MobileNumber_Empty              @"Please enter your MobileNo."
#define Password_Empty                  @"Please enter password"
#define Password_Length                 @"Password must be atleast 6 characters long"
#define Network_Error                   @"Please check your Internet connection and try again"
#define Invalid_Details                 @"Invalid Details"
#define Email_Empty                     @"Please enter email address"
#define Email_Invalid                   @"Please enter a valid email address"
#define Email_Invalid2                  @"Invalid email address"
#define Cancel                          @"CANCEL"
#define Take_Picture                    @"TAKE PHOTO"
#define Take_Video              @"TAKE VIDEO"
#define Take_Picture_Video              @"TAKE PHOTO/VIDEO"
#define Upload_photo                    @"Upload Photo"
#define Choose_From_Lib                 @"CHOOSE FROM LIBRARY"
#define Camera_Not_Available            @"Camera is not available"
#define UserName_Empty                  @"Username is empty"
#define Terms_Conditions                @"Please accept terms and conditions"
#define UserName_Length                 @"Username should be at least 4 characters long"
#define Select_Fav_Team                 @"Select one fan team"
#define Select_Hat_Team                 @"Select one rival team"
#define Account_Created                 @"Your account has been created"
