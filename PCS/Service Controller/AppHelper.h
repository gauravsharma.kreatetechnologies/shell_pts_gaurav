//
//  AppHelper.h
//  IntroduceThem
//


#import <Foundation/Foundation.h>



@interface AppHelper : NSObject {
    
}
//+(UIImage*)appLogoImage;
+(void)saveToUserDefaults:(id)value withKey:(NSString*)key;
+(void)saveBoolToUserDefaults:(BOOL)value withKey:(NSString*)key;
+(NSString*)userDefaultsForKey:(NSString*)key;
+(NSDictionary*)userDictionaryForKey:(NSString*)key;
+(NSArray*)userValueForKey:(NSString*)key;
+(void)removeFromUserDefaultsWithKey:(NSString*)key;
//+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate 
//            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles;
+ (NSString *)getCurrentLanguage;

+(NSString*)GetFilePathFor:(NSString*)type forURL:(NSString*)fileName;
+(NSURL*) findUrlPath:(NSString*)aUrl;

@end
