//
//  YourLocationViewController.h
//  Groomefy
//
//  Created by Arvind Kumar on 21/10/15.
//  Copyright © 2015 Arvind Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface YourLocationViewController : NSObject<CLLocationManagerDelegate>
{
     CLLocationManager *locationManager;
    int count;
}
-(void)getCurrentLocation;
+(instancetype)sharedInstance;

@end
