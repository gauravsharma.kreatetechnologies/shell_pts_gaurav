//
//  Services.h

//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "Defines.h"




@interface Services : AFHTTPSessionManager
{
  
    
}
+(Services *) sharedInstance;


-(void) serviceCallThroughGet:(NSString *)serviceName param:(NSMutableDictionary *)dict andCompletion:(CompletionWithResponseTypeAndResponse) completion;

-(void) serviceCallforImage:(NSString *)serviceName param:(NSMutableDictionary *)dict  andCompletion:(CompletionWithResponseTypeAndResponse) completion;

-(void) serviceCallbyPost:(NSString *)serviceName param:(NSMutableDictionary *)dict andCompletion:(CompletionWithResponseTypeAndResponse) completion;

-(void) serviceCallforVideo:(NSString *)serviceName param:(NSMutableDictionary *)dict  andCompletion:(CompletionWithResponseTypeAndResponse) completion;

@end
