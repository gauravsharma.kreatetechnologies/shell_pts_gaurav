//
//  MarKetPriceTableViewCell.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/18/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class MarKetPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewMin: UIView!
    @IBOutlet weak var lblMin: UILabel!
    @IBOutlet weak var viewMax: UIView!
    @IBOutlet weak var lblMax: UILabel!
    @IBOutlet weak var viewAvg: UIView!
    @IBOutlet weak var lblAvg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        roundCorner(viewMin)
        roundCorner(viewMax)
        roundCorner(viewAvg)
    }

    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
//        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
    }
}
