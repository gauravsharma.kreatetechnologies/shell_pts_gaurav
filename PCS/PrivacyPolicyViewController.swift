//
//  PrivacyPolicyViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 7/29/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import  WebKit

class PrivacyPolicyViewController: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var WebViewDisclaimer:WKWebView!
    @IBOutlet weak var DisclaimerViewe:UIView!
    @IBOutlet weak var btncheckedPro:UIButton!
    var btnchecked:Int?
    var urlStr:String?
    var completion:((String)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let htmlFile = Bundle.main.path(forResource: "Disclaimer", ofType: "html")
        let html = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
        WebViewDisclaimer.loadHTMLString(html!, baseURL: nil)
//        if let url  = html{
//            webview(url: url)
//        }
        
        WebViewDisclaimer.navigationDelegate = self
    }
    
    func webview(url:String){
        WebViewDisclaimer.navigationDelegate = self
        let url = URL(string: url)!
        WebViewDisclaimer.load(URLRequest(url: url))
        WebViewDisclaimer.allowsBackForwardNavigationGestures = true
    }
    

    @IBAction func btnChecked(_sender:UIButton){
        if (btnchecked==0) {
            
            btncheckedPro.setImage(UIImage(named: "checkeddis"), for: UIControlState.normal)
            btnchecked=1;
            
        }
        else
        {
            btncheckedPro.setImage(UIImage(named: "unchecked"), for: UIControlState.normal)
            btnchecked=0;
        }

    }
    
    @IBAction func btnDissmiss(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    @IBAction func btnOk(_sender:UIButton){
        if (btnchecked==1) {
            self.dismiss(animated: true) {
                self.completion?("Checked")
            }
        }
        else
        {
            
            let alert = UIAlertController(title: "Please Check the box.", message: " ", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
            self.completion?("UnChecked")

        }
        
    }

}
