//
//  DropDownViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/8/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit

class DropDownViewController: UIViewController {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var lblTitle:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnDismiss(_ sender: UIButton){
        self.dismiss(animated: true) {
            
        }
    }

}
extension DropDownViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableCell")as! DropDownTableCell
        return cell
    }
    
    
}



class DropDownTableCell:UITableViewCell{
    
    @IBOutlet weak var lblTitle:UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
