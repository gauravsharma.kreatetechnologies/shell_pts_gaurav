//
//  Model.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/4/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import Foundation
import SwiftyJSON
struct ProductDataModel:Codable {
    let data: [ProductData]?
}
struct GdamBiddataModel:Codable{
    let value:GdamData?
    let message:String?
    let status:String?
}
struct GdamData:Codable {
    var deliveryDt:String?
    var bidIdArr:BidIdArrData?
    var bidDetailArr:[BidDetailData]?
}

struct DeleteGdamData:Codable {
    var status:String?
    var message:String?
    var val:Int?
}

struct SouceTypeDataModel:Codable {
    var msg:String?
    var source_type:[String]?
}

struct BidIdArrData:Codable {
    var id:String?
    var buyer_seller:String?
    var biddingtype:String?
    var ocf:String?
    var p_d:String?
    var pd_price:String?
    var source_type:String?
    var exchange:String?
}
struct BidDetailData:Codable {
    var id:String?
    var bid_id:String?
    var blockfrom:String?
    var blockto:String?
    var price:String?
    var bid:String?
    var entry_by:String?
    var timestamp:String?
}
// MARK: - Datum
struct ProductData:Codable {
    let key, value: String?
}
struct TamDateData:Codable {
    let dailydates: String?
}

struct TamDataModel:Codable {
    let data:[TamData]?
    let message:String?
    let status:Bool?
}
struct TamBidModel:Codable {
    let value:String?
    let message:String?
    let status:String?
}

struct TamData:Codable {
    var status:String
    var todate: String?
    var print_end_daily_date:String?
    var bidcode:String?
    var appno:String?
    var bidquantum:String?
    var tstatus:String?
    var clientid:String?
    var fromtime:String?
    var energytype:String?
    var region:String?
    var print_start_daily_date:String?
    var end_daily_date:String?
    var ordernature:String?
    var type:String?
    var fixedprice:String?
    var id:String?
    var date:String?
    var totime:String?
    var platform:String?
    var timestamp:String?
    var entryby:String?
    var producttype:String?
    var product:String?
    var client_type:String?
    var price:String?
    var start_daily_date:String?
}
struct ReportDataModel:Codable {
    let value:ReportData?
    let status:String?
    let message:String?
}
struct ReportData:Codable{
    var total:ReportTotalData?
    var tabledata:[ReportTotalTableData]?
}
struct ReportTotalData:Codable{
    var fees:Double?
    var stoacharges:Double?
    var igst:Double?
    var fpp:Double?
    var total:Double?
    var new_mwh:Double?
}
struct ReportTotalTableData:Codable{
    var igst:String?
    var product:String?
    var fees:String?
    var new_mwh:String?
    var fpp:String?
    var date:String?
    var total:String?
    var stoacharges:String?
    var companyname:String?
}

struct TamFromToTimeModel:Codable {
    var data:TamFromToTimeData?
}

struct MarketPriceDataModel:Decodable {
    var year: String?
    var IEX: [JSON]?
    var PXIL:[JSON]?
}

struct ReportsAndDownloadsModel:Codable{
    var DAM:DamReportData?
    var GDAM:DamReportData?
    var BILL:[IexobligationData]?
    var GTAM:DamReportData?
    var REC:RecReportData?
    var RTM:DamReportData?
    var TAM:DamReportData?
    var IEXPROFITABILITY:[IexobligationData]?
    var PXILPROFITABILITY:[IexobligationData]?
    var RATESHEET:[IexobligationData]?

}
struct DamReportData:Codable {
    var url:String?
    var iexobligation:[IexobligationData]?
    var iexschedule:[IexobligationData]?
    var pxilobligation:[IexobligationData]?
    var pxilschedule:[IexobligationData]?

}
struct RecReportData:Codable {
    var url:String?
    var IEX:iexReportDat?
    var PXIL:iexReportDat?
    var BILL:[IexobligationData]?
}
struct iexReportDat:Codable {
    var OBLIGATION:[IexobligationData]?
    var NONSOLARCERT:[IexobligationData]?
    var SOLARCERT:[IexobligationData]?
}

struct IexobligationData:Codable {
    var url:String?
    var filename:String?
    var mimetype:String?
    var filepath:String?
    var base64filepath:String?
}

struct TamFromToTimeData:Codable {
    var fromslot:[String]?
    var toslot:[String]?
}

struct NoBidDataModel:Codable {
    let value:[NoBidData]?
    let msg:String?
    let status:String?
}
struct AddNoBidDataModel:Codable {
    let value:String?
    let msg:String?
    let status:String?
}

struct NoBidData:Codable {
    var id:String?
    var clientid:String?
    var date:String?
    var tstatus:String?
    var entryby:String?
    var timestamp:String?
}

struct OCFType {
    var key:String?
    var value:String?
}
struct DiscountType {
    var key:String?
    var value:String?
}


struct ReportsDataModel:Codable{
    var detail: [[String: String]]?
    var totalammount: String?
    var totalqty, totalamtrecieve: Int?
}

