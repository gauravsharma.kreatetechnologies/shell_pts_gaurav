//
//  MarKetPriceViewController.swift
//  PCS
//
//  Created by Gaurav Sharma on 8/18/22.
//  Copyright © 2022 lab4code. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift

class MarKetPriceViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var viewProduct: UIView!
    @IBOutlet weak var btnIEX: UIButton!
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var btnPXIL: UIButton!
    @IBOutlet weak var viewFromDate: UIView!
    @IBOutlet weak var viewToDate: UIView!
    @IBOutlet weak var txtProduct: UITextField!
    @IBOutlet weak var txtToDate: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtFromDate: UITextField!
    
    var pickerView = ToolbarPickerView()
    let platform: RadioButtonController = RadioButtonController()
    var fromDate:String? = ""
    var toDate:String? = ""
    var type:String? = ""
    var iexData = [JSON]()
    var yearArray = [String]()
    var maxArray = [Double]()
    var minArray = [String]()
    var avgArray = [String]()
    var sellerTypeArray:[BuyerType] = []
    var productType:String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtProduct.delegate =  self
        self.txtFromDate.delegate = self
        self.txtToDate.delegate = self

        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.txtProduct.inputView =  pickerView
        self.pickerView.toolbarDelegate = self
        self.tableView.isHidden = true
        platform.buttonsArray = [btnIEX,btnPXIL]

        self.txtProduct.inputAccessoryView = self.pickerView.toolbar
        roundCorner(viewProduct)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.txtFromDate.setInputViewDatePicker(target: self, selector: #selector(dateFromPressed))
        self.txtToDate.setInputViewDatePicker(target: self, selector: #selector(dateToPressed))
        self.sellerTypeArray.insert(BuyerType(key: "Dam", value: "DAM"), at: 0)
        self.sellerTypeArray.insert(BuyerType(key: "Gdam", value: "GDAM"), at: 1)
        self.sellerTypeArray.insert(BuyerType(key: "Rtm", value: "RTM"), at: 2)
     }
    
    @objc func dateFromPressed() {
        if let  datePicker = self.txtFromDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.txtFromDate.text = dateFormatter.string(from: datePicker.date)
            self.fromDate =  formatter.string(from: datePicker.date)
//            print(dateFrom)
            let calendar = Calendar.current
            let addOneWeekToCurrentDate = calendar.date(byAdding: .day, value: 7, to: datePicker.date)
            print(addOneWeekToCurrentDate)
            self.toDate =  formatter.string(from: addOneWeekToCurrentDate ?? Date())
            
            let formter = DateFormatter()
            formter.dateStyle = .medium
            self.txtToDate.text = formter.string(from: addOneWeekToCurrentDate ?? Date())
        }
        self.txtFromDate.resignFirstResponder()
    }
    
    @objc func dateToPressed() {
        if let  datePicker = self.txtToDate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            self.txtToDate.text = dateFormatter.string(from: datePicker.date)
            self.toDate =  formatter.string(from: datePicker.date)
//            print(dateFrom)
        }
        self.txtToDate.resignFirstResponder()
    }
    
    func callIEXmarketclearingprice()
    {
        let access_key:String  = AppHelper.userDefaults(forKey: "access_key")
        let client_id:String =  AppHelper.userDefaults(forKey: "client_id")

        var param:BRParameters = [:]
        param["access_key"] = access_key
        param["product"] = self.productType
        param["client_id"] = client_id
        param["fromdate"] = self.fromDate
        param["todate"] = self.toDate
        param["type"] = self.type

        do
        {
            let theJSONData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)
            
            let parameters : Parameters = ["data":theJSONText!.description]
            
            
            APIManager.sharedInstance.getCallAPI(controllername: self, apiURL: htmlIEXmarketclearingprice, parameters:parameters)
            {(APIData) -> Void in
                
                var productData = Data()
                do {
                   let data = try JSONEncoder().encode(APIData)
                  
                   if let jsonString = String(data: data, encoding: .utf8) {
                       print(jsonString)
                   }
                    productData = data
                } catch let err {
                    print("Failed to encode JSON")
                }
                do {
                   let decodedData = try JSONDecoder().decode(MarketPriceDataModel.self, from: productData)

                   print(decodedData)
                    if let data = decodedData.IEX{
                        self.iexData  = data
                        print(self.iexData.count)
                        for i in 0 ..< self.iexData.count{
                            self.yearArray.append(self.iexData[i].arrayObject?[0] as! String)
                            self.avgArray.append(self.iexData[i].arrayObject?[1] as! String)
                            self.minArray.append(self.iexData[i].arrayObject?[2] as! String)
                            self.maxArray.append(self.iexData[i].arrayObject?[3] as! Double)
                        }
                        print(self.yearArray)
                        print(self.maxArray)
                        print(self.minArray)
                        self.setChart(dataPoints:self.yearArray)
                    }
                    self.tableView.isHidden = self.iexData.count == 0 ? true:false

                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                catch let err {
                   print("Failed to decode JSON")
                }
            }
        }
        catch
        {
            
        }
       
    }
    
    func roundCorner(_ view:UIView){
        view.layer.cornerRadius = 06.0
        view.layer.masksToBounds = true
        view.layer.borderWidth =  0.6
        view.layer.masksToBounds = true
        let blueColor = UIColor(hexString: "#0097bb")
        view.layer.borderColor = blueColor.cgColor
    }
    
    @IBAction func btnIEXAction(_ sender: UIButton) {
        platform.buttonArrayUpdated(buttonSelected: sender)
        self.type = "IEX"
    }
    
    @IBAction func btnGo(_ sender: UIButton) {
        if  txtProduct.text?.isEmpty ?? true{
            self.alertView(message: "Please Select Product Type")
            return
        }else if self.txtFromDate.text?.isEmpty ?? true{
            self.alertView(message: "Please Select From Date")
            return
        }
        else if self.txtToDate.text?.isEmpty ?? true{
            self.alertView(message: "Please Select To Date")
            return
        }
        self.callIEXmarketclearingprice()
    }
    
    @IBAction func btnPXILAction(_ sender: UIButton) {
        platform.buttonArrayUpdated(buttonSelected: sender)
        self.type = "PXIL"
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
}
extension MarKetPriceViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableView{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            return 1;
        }
        
        else
        {
            return self.iexData.count
        }

       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 0)
        {
            return 60;
        }else{
            return 50;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section==0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarketPriceHeader")!
            return cell
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "MarKetPriceTableViewCell", for: indexPath)as! MarKetPriceTableViewCell
            let data = self.iexData[indexPath.row]
            cell.lblDate.text =  data[0].stringValue
            cell.lblMin.text  = data[1].stringValue
            cell.lblAvg.text =  data[2].stringValue
            cell.lblMax.text = "\(data[3].doubleValue)"
            return cell
        }
    }
    
    
}
extension MarKetPriceViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return sellerTypeArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return sellerTypeArray[row].key
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        txtProduct.text =  sellerTypeArray[row].key
        self.productType =  sellerTypeArray[row].value ?? ""
       
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}

extension MarKetPriceViewController:ToolbarPickerViewDelegate{
    func didTapDone() {
        if txtProduct.isFirstResponder{
            
            let row = self.pickerView.selectedRow(inComponent: 0)
            self.pickerView.selectRow(row, inComponent: 0, animated: false)
            self.txtProduct.text = sellerTypeArray[row].key
            self.productType =  sellerTypeArray[row].value ?? ""
            self.txtProduct.resignFirstResponder()
            
        }
    }
    
    func didTapCancel() {
        if txtProduct.isFirstResponder{
            self.txtProduct.text = nil
            self.txtProduct.resignFirstResponder()
        }
    }
}


extension MarKetPriceViewController{
    func setChart(dataPoints: [String]) {
        var dataEntries: [ChartDataEntry] = []
        var dataEntries2: [ChartDataEntry] = []
        var dataEntries3: [ChartDataEntry] = []

        let avgDouble =  self.avgArray.convertToDouble
        let minDouble =  self.minArray.convertToDouble
        let maxDouble =  self.maxArray
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: avgDouble[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
            
            let dataEntry2 = ChartDataEntry(x: Double(i), y: minDouble[i], data: dataPoints[i] as AnyObject)
            dataEntries2.append(dataEntry2)
            
            let dataEntry3 = ChartDataEntry(x: Double(i), y: maxDouble[i], data: dataPoints[i] as AnyObject)
            dataEntries3.append(dataEntry3)
        }

        let chartDataSet = LineChartDataSet(entries: dataEntries, label: "")
        chartDataSet.circleRadius = 5
        chartDataSet.circleHoleRadius = 2
        chartDataSet.drawValuesEnabled = false
        chartDataSet.setColor(UIColor.green)
        
        let chartDataSet2 = LineChartDataSet(entries: dataEntries2, label: "")
        chartDataSet2.circleRadius = 5
        chartDataSet2.circleHoleRadius = 2
        chartDataSet2.drawValuesEnabled = false
        chartDataSet2.setColor(UIColor.blue)

        
        let chartDataSet3 = LineChartDataSet(entries: dataEntries3, label: "")
        chartDataSet3.circleRadius = 5
        chartDataSet3.circleHoleRadius = 2
        chartDataSet3.drawValuesEnabled = false
        chartDataSet3.setColor(UIColor.red)

        
        let chartData = LineChartData(dataSets: [chartDataSet,chartDataSet2,chartDataSet3])


        chartView.data = chartData

        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: yearArray)
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.avoidFirstLastClippingEnabled = true

        chartView.rightAxis.drawAxisLineEnabled = false
        chartView.rightAxis.drawLabelsEnabled = false

        chartView.leftAxis.drawAxisLineEnabled = false
        chartView.pinchZoomEnabled = false
        chartView.doubleTapToZoomEnabled = false
        chartView.legend.enabled = false
    }
}


